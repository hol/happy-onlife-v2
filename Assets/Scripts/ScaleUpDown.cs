﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class ScaleUpDown : MonoBehaviour
{
    public bool scaleUp;
    public bool scaleDown;
    public bool locked;
    
    private Vector3 startScale;
    private Vector3 finalScale;
    
    private float t;
    private float _time;
    public AnimationCurve curve;

    void Start()
    {
        scaleUp = false;
        scaleDown = false;
        
        _time = 0.3f;

        startScale = Vector3.zero;
        finalScale = new Vector3(1f, 1f, 1f);

        transform.localScale = finalScale;
    }

    void Update()
    {
        if (scaleDown)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            transform.localScale=Vector3.Lerp(finalScale, startScale, curve.Evaluate(s));
            
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                scaleDown = false;
                t = 0;
            }
        }
        
        if (!locked)
        {
            if (scaleUp)
            {
                t += Time.deltaTime;
                float s = t / _time;

                transform.localScale = Vector3.Lerp(startScale, finalScale, curve.Evaluate(s));

                float checker = Mathf.Lerp(0, 1, curve.Evaluate(s));

                if (checker == 1)
                {
                    scaleUp = false;
                    t = 0;
                }
            }
        }
    }
}
