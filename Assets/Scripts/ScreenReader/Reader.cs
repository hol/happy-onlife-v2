/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Runtime.InteropServices;

public class Reader : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    /*
    Assign this Script to a UI Text and after a long press on it will read the content.
    For now only in English.
    It also automatically will check if Text is inside a button and will manage the click.
    */
    #if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern void SpeakWeb(string strPointer, string languageCodeStrPtr);
    #endif
    [Header("Will block automatically relative Button function\r\nduring long press, only if Button is directly parent of text.\r\n\r\nIf not use settings below")]
    public Button btnContainer;
    [Header("Will read only languages with permission in \r\nLanguages script")]
    public bool allLang;
    private Text txt;
    private Button btn;
    [Header("Checking")]
    public bool pressed;
    public bool talk;
    public static bool permission=false;

    void Start() 
    {
        if(txt==null)
        {
            txt=transform.GetComponent<Text>();
            txt.transform.gameObject.AddComponent<BoxCollider>();
        }

        if(btnContainer!=null)
        {
            btn=btnContainer;
        }
        else if(btn==null && txt.transform.parent.transform.GetComponent<Button>())
        {
            btn=txt.transform.parent.transform.GetComponent<Button>();
            //print("btn "+txt.transform.parent.name);
        }

        talk=true;
    }

    public void OnPointerDown(PointerEventData pointerEventData)
    {
        //if(talk && permission || allLang)
        //{
            pressed=true;
            StartCoroutine("LongPress");
            talk=false;
            //Debug.Log("LongPress in Progress");
        //}

        #if UNITY_WEBGL && !UNITY_EDITOR
            pressed=true;
            StartCoroutine("LongPress");
            talk=false;
            //Debug.Log("LongPress in Progress");
        #endif
    }

    //All In One! (..but WebGL)
    private void Speak()
    {
    #if UNITY_STANDALONE_WIN || UNITY_EDITOR
        WindowsTTS.Speak(txt.text);
    #endif
    #if UNITY_ANDROID && !UNITY_EDITOR
        AndroidTTS.Speak(txt.text);
        //Google_TTS.SpeakText(txt.text);
    #endif
    #if UNITY_IOS && !UNITY_EDITOR
        iOSTTS.StartSpeaking(txt.text);
    #endif
    }

    //4Web!
    private void Speak4Web()
    {
    #if UNITY_WEBGL && !UNITY_EDITOR
        SpeakWeb(txt.text, PlayerPrefs.GetString("linguaSuffix"));
    #endif
    }

    private IEnumerator LongPress()
    {
        print("presssssss");
        yield return new WaitForSeconds(0.2f);
        if(pressed)
        {
        #if !UNITY_WEBGL
            Speak();
        #endif
        #if UNITY_WEBGL && !UNITY_EDITOR
            Speak4Web();
        #endif

            if(btn!=null)
            {
                btn.interactable=false;
            }
            pressed=false;
        }
    }
    
    public void OnPointerUp(PointerEventData pointerEventData)
    {
        //if(permission || allLang)
        //{
            StopCoroutine("LongPress");
            pressed=false;
            talk=true;
            StartCoroutine("Release");
            //Debug.Log("No longer being clicked");
        //}

    #if UNITY_WEBGL && !UNITY_EDITOR
        StopCoroutine("LongPress");
        pressed=false;
        talk=true;
        StartCoroutine("Release");
        //Debug.Log("No longer being clicked");
    #endif
    }

    private IEnumerator Release()
    {
        print("releaseeeee");
        yield return new WaitForSeconds(0.2f);
        if(btn!=null)
        {
            btn.interactable=true;
        }
        StopCoroutine("Release");
    }
}
