﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour
{
    public static bool initMove;
    
    public Transform Obj1;
    public Transform Obj2;
    public Transform Obj3;
    public Transform Obj4;
    public Transform Obj5;
    public Transform Obj6;
    public Transform Obj7;
    public Transform Obj8;

    private int totObj;
    
    private Transform[] container;
    
    void Start()
    {
        container = new []{Obj1, Obj2, Obj3, Obj4, Obj5, Obj6, Obj7, Obj8};
        totObj = container.Length;
    }

    IEnumerator GoBack(Transform who)
    {
        int multi = 1;
        float time = 0.4f;
        multi = -(multi);
        Vector3 actPos = who.position;
        Vector3 to = actPos + new Vector3((200)*multi, 0, 0);
        ProviderMovement.MoveThis(who, actPos, to, time);
        yield return new WaitForSeconds(time);
        actPos = who.position;
        multi = -(multi);
        to = actPos + new Vector3((200)*multi, 0, 0);
        ProviderMovement.BackThat(who,  actPos, to, time);
    }

    IEnumerator Dispose()
    {
        for (int i = 0; i < totObj; i++)
        {
            if (container[i])
            {
                StartCoroutine(GoBack(container[i]));
                yield return new WaitForSeconds(0.2f);
            }
        }
    }


    void Update()
    {
        if (initMove)
        {
            //StartCoroutine(Dispose());
            initMove = false;
        }
    }
}
