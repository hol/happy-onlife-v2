﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

using UnityEngine;

public class SineWave : MonoBehaviour
{
    public static bool init;
    
    public bool editMode;
    
    public bool vertical;
    public bool horizontal;
    
    public Gradient gradActive;
    public Gradient gradStandBy;
    
    public int lengthOfLineRenderer;
    public int roundness;
    public float thickness;
    public int speed;
    public int frequency;
    public float length;
    public float height;
    
    LineRenderer lineRenderer;

    public Transform Target;
    private Vector3 posT;
    private Vector3 posTstore;

    public bool interacting;
    private bool locked;
    
    void Start()
    {
        thickness = (Screen.height / 1000f) * 5f;
        length = (Screen.height / 1000f) * 16f;
        height = (Screen.height / 1000f) * 8f;
        locked = true;
        //posT = Target.position;
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Mobile/Particles/Additive"));
        lineRenderer.colorGradient = gradStandBy;
        lineRenderer.startWidth = thickness;
        lineRenderer.endWidth = thickness;
        lineRenderer.widthMultiplier = thickness;
        lineRenderer.positionCount = lengthOfLineRenderer;
        lineRenderer.numCapVertices = roundness;
        lineRenderer.numCornerVertices = roundness;

        if (!vertical && !horizontal)
        {
            horizontal = true;
        }
    }
    void Update() 
    {
        if (init)
        {
            posT = Target.localPosition;

            thickness = (Screen.height / 1000f) * 5f;
            length = (Screen.height / 1000f) * 16f;
            height = (Screen.height / 1000f) * 8f;
            
            AudioSettings.maxHeight = height;
            AudioSettings.maxLength = length;
            AudioSettings.maxSize = thickness;
            
            lineRenderer.startWidth = thickness;
            lineRenderer.endWidth = thickness;
            lineRenderer.widthMultiplier = thickness;
            lineRenderer.positionCount = lengthOfLineRenderer;
            lineRenderer.numCapVertices = roundness;
            lineRenderer.numCornerVertices = roundness;
            locked = false;
            init = false;
        }

        if (editMode)
        {
            posT = Target.localPosition;

            if (vertical)
            {
                horizontal = false;
            }

            if (horizontal)
            {
                vertical = false;
            }

            lineRenderer.startWidth = thickness;
            lineRenderer.endWidth = thickness;
            lineRenderer.widthMultiplier = thickness;
            lineRenderer.positionCount = lengthOfLineRenderer;
            lineRenderer.numCapVertices = roundness;
            lineRenderer.numCornerVertices = roundness;
        }

        if (interacting)
        {
            lineRenderer.colorGradient = gradActive;
            
            int i = 0;

            while (i < lengthOfLineRenderer)
            {
                Vector3 pos = Vector3.zero;

                if (vertical)
                {
                    pos = new Vector3(posT.x + Mathf.Sin(i * frequency + Time.time * speed) * height,
                        posT.y + i * length * 0.5f, posT.z - 10);
                }
                else if(horizontal)
                {
                    pos = new Vector3(posT.x + i * length * 0.5f,
                        posT.y + i*Mathf.Sin(i * frequency + Time.time * speed) * height, posT.z - 10);
                }
            
                lineRenderer.SetPosition(i, pos);
            
                i++;
            }
            
            locked = false;
        }
        else
        {
            if (!locked)
            {
                posT = Target.position;
                
                lineRenderer.colorGradient = gradStandBy;
                
                int i = 0;

                while (i < lengthOfLineRenderer)
                {
                    Vector3 pos = Vector3.zero;

                    if (vertical)
                    {
                        pos = new Vector3(posT.x + Mathf.Sin(i * frequency + Time.time * speed) * height,
                            posT.y + i * length * 0.5f, posT.z - 10);
                    }
                    else if(horizontal)
                    {
                        pos = new Vector3(posT.x + i * length * 0.5f,
                            posT.y + i*Mathf.Sin(i * frequency + Time.time * speed) * height, posT.z - 10);
                    }
            
                    lineRenderer.SetPosition(i, pos);
            
                    i++;
                }
                
                locked = true;
            }
        }
    }
}