﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using UnityEngine;

public class Click4Question : MonoBehaviour
{
    public int ID;
    public ParticleSystem Ray;
    public string akContact;
    public AssistKeyboard ak;

    public static int activateByID;
    public static int disactivateByID;
    public static int signedByID;

    public static int passIDcell;

    public static int passCat;
    public int cat;
    public static int passNQ;
    public int nq;

    public bool active;
    private bool activeStore;
    public static bool free;

    bool sign;
    public static bool answer;

    void Start()
    {
        active=false;
        free=false;
        sign=false;
        answer=false;
        Ray=transform.GetChild(3).transform.GetComponent<ParticleSystem>();
        IDizer();
        activateByID=-1;
        disactivateByID=-1;
        signedByID=-1;
        passCat=-1;
        cat=-1;
        passNQ=-1;
        nq=-1;

        free=true;
    }

    void OnMouseDown()
    {
        if(active && free && !CoreEvents.computerTurn)
        {
            passCat=cat;
            passNQ=nq;
            passIDcell=ID;
            CoreEvents.interceptor=true;
            print("Clicked Cell N="+(ID));
        }
        else
        {
            print("Clicked Cell N="+(ID)+" but this is not the right moment");
            //print("active="+active+"free="+free+"computerTurn+"+CoreEvents.computerTurn);
        }
    }

    public void Click()
    {
        OnMouseDown();
    }

    void Update()
    {
        if(sign)
        {
            Ray.Play();
            sign=false;
        }

        if(answer)
        {
            if(active)
            {
                if(cat==passCat)
                {
                    sign=true;
                    StartCoroutine(Count());
                }
            }
        }

        if(ID==activateByID)
        {
            cat=passCat;
            nq=passNQ;
            active=true;
            activateByID=-1;
        }
        
        if(ID==disactivateByID)
        {
            cat=-1;
            nq=-1;
            active=false;
            disactivateByID=-1;
        }

        if(active!=activeStore)
        {
            if(active)
            {
                ak=GameObject.Find(akContact).GetComponent<AssistKeyboard>();
                ak.AddNASelectable(GetComponent<AssistKeyboardElement>());
            }
            else
            {
                print("Input 4 REMOVING "+transform.name);
                ak.RemoveNASelectable(transform.name);
            }

            activeStore=active;
        }
    }

    private IEnumerator Count()
    {
        //print("Count");
        yield return new WaitForSeconds(0.5f);
        answer=false;
        passCat=-1;
    }

    void IDizer()
    {
        string[] splitName=transform.gameObject.name.Split ('M');
        if(splitName.Length>1)
        {
            char c = splitName[1][0];
            if(c.ToString()=="0")
            {
                ID=int.Parse(splitName[1][1].ToString());
            }
            else
            {
                ID=int.Parse(splitName[1]);
            }
        }
    }
}
