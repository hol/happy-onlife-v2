﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class ChangeTurn : MonoBehaviour
{
    [Header("Modules")]
    public Views ui3D;
    public GenericUtils gu;
    public ViewBtn vb;

    public GameObject inFrontObjRef;
    public RectTransform winObj;
    public Camera main;
    public float offset=0.5f;

    public Transform[] IconObject1;
    public Transform[] IconObject2;

    public Transform iconPlayer;
    public Transform iconNotPlayer;
    public Transform PCavatar;
    public Transform PCavatarBig;
    public bool pcTurn;
    public bool phase1;
    public bool phase2;
    public bool phase3;
    public bool phase4;
    public bool phase5;
    public bool phase6;

    private float t;
    private float _time1;   
    private float _time2;   
    private float _time3;   
    public AnimationCurve curve1;
    public AnimationCurve curve2;
    public AnimationCurve curve3;

    public float posXcenter;

    private Vector3 sPos1;
    private Vector3 ePos1;
    public Vector3 sPos2;
    public Vector3 ePos2;
    public Vector3 sPos3;
    public Vector3 ePos3;

    public Vector3 sRot2;
    public Vector3 eRot2;
    public Vector3 sRot3;
    public Vector3 eRot3;

    private Vector3 s2Pos1;
    private Vector3 e2Pos1;
    public Vector3 s2Pos2;
    public Vector3 e2Pos2;
    private bool firstTime;
    private int type1;
	private int type2;

    void Start()
    {
        t=0;
        _time1=1f;
        _time2=1f;
        _time3=1f;

        sPos1=new Vector3(55,-32,5);
        ePos1=new Vector3(55,150,5);
        sPos2=new Vector3(2000,150,5);
        ePos2=new Vector3(1110,-530,-1260);//
        float rap=(float)Screen.width/(float)Screen.height;
        print("ChangeTurn:rap="+rap);
        
        posXcenter=Screen.width/rap*0.5f;

        if(rap<1.5f)
            ePos2=new Vector3(720,-530,-1260);
        else if(rap>=1.5f && rap<1.8f)
            ePos2=new Vector3(960,-530,-1260);
        else if(rap>=1.8f && rap<1.85f)
            ePos2=new Vector3(1000,-530,-1260);
        else if(rap>=1.85f && rap<2.01f)
            ePos2=new Vector3(1080,-530,-1260);
        else if(rap>=2.01f && rap<2.06f)
            ePos2=new Vector3(1110,-530,-1260);
        else if(rap>=2.06f)
            ePos2=new Vector3(1135,-530,-1260);
        
        sRot2=new Vector3(50f,0f,0f);
        eRot2=new Vector3(25f,130f,-25f);
        sPos3=ePos2;
        ePos3=new Vector3(84,-50,10);
        sRot3=new Vector3(25f,170f,-25f);
		eRot3=new Vector3(27.9f,115.7f,15.9f);

        s2Pos1=new Vector3(-13,0,63);
        e2Pos1=new Vector3(-13,150,63);
        s2Pos2=new Vector3(-13,150,63);
        e2Pos2=new Vector3(-13,0,63);

        firstTime=true;

		int pTp1 = PlayerPrefs.GetInt("p1type");
		int pTp2 = PlayerPrefs.GetInt("p2type");
        int players  = PlayerPrefs.GetInt ("numplayers");

        print("ptp1="+pTp1+" ptp2="+pTp2);

        for(int i=0; i<IconObject1.Length; i++)
        {
            if(i==pTp1)
            {
                iconPlayer=IconObject1[pTp1];
                PCavatarBig=IconObject1[pTp1];
                print("ptp1="+IconObject1[pTp1].name);
            }
            else
            {
                IconObject1[i].gameObject.SetActive(false);
            }
        }

        if(players==1)
        {
            for(int i=0; i<IconObject2.Length; i++)
            {
                IconObject2[i].gameObject.SetActive(false);
            }
            //iconNotPlayer.gameObject.SetActive(false);
            print("ptp2="+IconObject2[pTp2].name);
        }
        else
        {
            for(int i=0; i<IconObject2.Length; i++)
            {
                if(i==pTp2)
                {
                    iconNotPlayer=IconObject2[pTp2];
                    print("ptp22="+IconObject2[pTp2].name);
                }
                else
                {
                    IconObject2[i].gameObject.SetActive(false);
                }
            }
            PCavatar.gameObject.SetActive(false);
        }
    }
    
    void Update()
    {
        if(phase1)
        {
            if(!firstTime)
            {
                t += Time.deltaTime;
                float s = t / _time1;
                
                iconPlayer.localPosition=Vector3.Lerp(sPos1, ePos1, curve1.Evaluate(s));
                iconNotPlayer.localPosition=Vector3.Lerp(s2Pos1, e2Pos1, curve1.Evaluate(s));
                PCavatar.localPosition=Vector3.Lerp(s2Pos1, e2Pos1, curve1.Evaluate(s));
                float checker=Mathf.Lerp(0,  1, curve1.Evaluate(s));
                
                if (checker == 1)
                {
                    phase1 = false;
                    print("ct.PHASE1 not firstTime anymore ui3D.switchTo3DView wm.ct e gu.changeTurnStep2");
                    ui3D.switchTo3DView();
                    vb.SetMeOn();
                    WinModule.who="changeTurn";
                    WinModule.prepare=true;
                    t = 0;
                    gu.changeTurnStep2 ();
                    phase2=true;
                    print("ChangeTurn:PHASE1");
                    SettingScore2.showAS=true;
                }
            }
            else
            {
                phase1 = false;
                firstTime=false;
                print("ChangeTurn:PHASE1 firstTime wm.ct e gu.changeTurnStep2");
                WinModule.who="changeTurn";
                WinModule.prepare=true;
                t = 0;
                gu.changeTurnStep2 ();
                phase2=true;
            }
        }

        if(phase2)
        {
            t += Time.deltaTime;
            float s = t / _time2;
            
            if(pcTurn)
            {
                PCavatar.localPosition=Vector3.Lerp(sPos2, ePos2, curve2.Evaluate(s));
                PCavatar.eulerAngles=Vector3.Lerp(sRot2, eRot2, curve2.Evaluate(s));
            }
            else
            {
                iconPlayer.localPosition=Vector3.Lerp(sPos2, ePos2, curve2.Evaluate(s));
                iconPlayer.eulerAngles=Vector3.Lerp(sRot2, eRot2, curve2.Evaluate(s));

                iconNotPlayer.localPosition=Vector3.Lerp(s2Pos2, e2Pos2, curve2.Evaluate(s));
                PCavatar.localPosition=Vector3.Lerp(s2Pos2, e2Pos2, curve2.Evaluate(s));
            }
            float checker=Mathf.Lerp(0,  1, curve2.Evaluate(s));
            
            if (checker == 1)
            {
                //print(iconPlayer.localPosition);
                phase2 = false;
                t = 0;
                print("ChangeTurn:PHASE2");
            }
        }

        if(phase3)
        {
            t += Time.deltaTime;
            float s = t / _time3;
            
            if(pcTurn)
            {
                PCavatar.localPosition=Vector3.Lerp(ePos2, sPos2, curve2.Evaluate(s));
                PCavatar.eulerAngles=Vector3.Lerp(eRot2, sRot2, curve2.Evaluate(s));
            }
            else
            {
                iconPlayer.localPosition=Vector3.Lerp(sPos3, ePos3, curve3.Evaluate(s));
                iconPlayer.eulerAngles=Vector3.Lerp(sRot3, eRot3, curve3.Evaluate(s));
            }
            float checker=Mathf.Lerp(0,  1, curve3.Evaluate(s));
            
            if (checker == 1)
            {
                phase3 = false;
                if(!CoreEvents.computerTurn)
                {
                    SettingScore.showAS=true;
                    Click4Question.free=true;
                }
                t = 0;
                print("ChangeTurn:PHASE3");
            }
        }

        if(phase4)
        {
            t += Time.deltaTime;
            float s = t / _time1;
            
            PCavatar.localPosition=Vector3.Lerp(s2Pos1, e2Pos1, curve1.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve1.Evaluate(s));
            
            if (checker == 1)
            {
                phase4 = false;
                
                print("ChangeTurn:PHASE4   wm.ctPC");
                WinModule.who="changeTurnPC";
                WinModule.prepare=true;
                t = 0;
                phase5=true;
            }
        }

        if(phase5)
        {
            t += Time.deltaTime;
            float s = t / _time2;
            
            PCavatarBig.localPosition=Vector3.Lerp(sPos2, ePos2, curve2.Evaluate(s));
            PCavatarBig.eulerAngles=Vector3.Lerp(sRot2, eRot2, curve2.Evaluate(s));

            float checker=Mathf.Lerp(0,  1, curve2.Evaluate(s));
            
            if (checker == 1)
            {
                phase5 = false;
                t = 0;
                print("ChangeTurn:PHASE5");
            }
        }

        if(phase6)
        {
            t += Time.deltaTime;
            float s = t / _time3;
            
            PCavatar.localPosition=Vector3.Lerp(e2Pos1, s2Pos1, curve1.Evaluate(s));
            PCavatarBig.localPosition=Vector3.Lerp(ePos2, sPos2, curve2.Evaluate(s));
            PCavatarBig.eulerAngles=Vector3.Lerp(eRot2, sRot2, curve2.Evaluate(s));

            float checker=Mathf.Lerp(0,  1, curve3.Evaluate(s));
            
            if (checker == 1)
            {
                CoreEvents.challangeAccepted=true;
                phase6 = false;
                t = 0;
                print("ChangeTurn:PHASE6 ce.challangeAccepted=true");
            }
        }
    }
}
