﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class UsePWC : MonoBehaviour
{
    public int IDcat;
    public RectTransform Target;

    public TransformPWC ItSelf;
    private Button meBTN;

    public GenericUtils gu;
    ExaPowerBehaviour mine;

    public AssistKeyboard ak;

    public bool pwcFull;
    bool connect;

    Vector3 _from;
    Vector3 _to;

    private float t;
    private float _time;    
    public AnimationCurve curve;
    public Sound sound;

    public AssistKeyboardGroups akg;
    public int relativeFocus;
    
    void Start()
    {
        pwcFull=false;
        connect=false;
        mine=transform.GetComponent<ExaPowerBehaviour>();
        meBTN=transform.GetComponent<Button>();
        t=0;
        _time=0.5f;
    }

    void Update()
    {
        if(connect)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            transform.position=Vector3.Lerp(_from, _to, curve.Evaluate(s));
            float checker=Mathf.Lerp(0, 1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                t = 0;
                ItSelf.goForIt=true;
                pwcFull=true;
                connect = false;
            }
        }
    }

    public void UseIt()
    {
        if(mine.status==3 && !CoreEvents.deviantClick)//Segnale le Celle disponibili
        {
            sound.Play("redCell");
            gu.WhoIsDisposable(IDcat);
            print("1Step");
        }
        else if(!pwcFull && CoreEvents.deviantClick && CoreEvents.enoughPWC)
        {

            _from=transform.position;
            _to=Target.transform.position;
            connect=true;
            meBTN.interactable=false;
            akg.AssignFocus(relativeFocus);
            print("2Step");
        }
        else if(pwcFull)
        {
            ItSelf.hideIt=true;
            pwcFull=false;
            mine.status=0;
            ak.when[IDcat]=false;
            akg.AssignFocus(1);
            print("3Step");
        }
        print(IDcat);
    }
}
