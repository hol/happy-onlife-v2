/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class Consequence : MonoBehaviour
{
    [Header("Modules")]
    public Colors c;
    public GenericUtils gu;
    [Header("Master Objects")]
    public GameObject iconP1;
    public GameObject Avatar1;
    public GameObject Avatar2;
    public SetFX fx1;
    public SetFX fx2;

    [Header("Material Objects \r\n-Icon1")]
    public Renderer i1t1_Belly;
    public Renderer i1t1_Body;
    public Renderer i1t2_Body;
    public Renderer i1t3_Body;

    [Header("-Avatar1")]
    public Renderer a1t1_Belly;
    public Renderer a1t1_Body;
    public Renderer a1t2_Body;
    public Renderer a1t3_Body;

    [Header("-Avatar2")]
    public Renderer a2t1_Belly;
    public Renderer a2t1_Body;
    public Renderer a2t2_Body;
    public Renderer a2t3_Body;

    private WPlayerCTRL wpCtrl1;
    private WPlayerCTRL wpCtrl2;

    [Header("Switchers")]
    public static bool virus;
    public static bool virusAttack;
    public static bool antivirus;
    public static bool antivirusNoMore;
    public static bool lucky;
    public static bool luckyNoMore;
    public static bool pwc;
    public static int whichPwc;
    public static bool changeTurn;
    public static bool changeTurnFirst;
    public static bool changeTurnPC;
    public static bool opp;
    
    [Header("Colors")]
    public Color32 luckyColorEmit;
    public Color32 luckyColor;

    private int playerColor;

    [Header("Transitions")]
    private float t;
    private float _time;    
    public AnimationCurve curve;
    private int type1;
	private int type2;

    void Start()
    {
        type1=gu.type1;
		type2=gu.type2;

        luckyColorEmit=new Color32(0,255,0,255);
        luckyColor=new Color32(0,255,0,255);
        
        switch (type1)
        {
            case 0:
                Avatar1=Avatar1.transform.GetChild(0).gameObject;
                break;
            case 1:
                Avatar1=Avatar1.transform.GetChild(1).gameObject;
                break;
            case 2:
                Avatar1=Avatar1.transform.GetChild(2).gameObject;
                break;
        }
        fx1=Avatar1.GetComponent<SetFX>();
        wpCtrl1=Avatar1.GetComponent<WPlayerCTRL>();

        switch (type2)
        {
            case 0:
                Avatar2=Avatar2.transform.GetChild(0).gameObject;
                break;
            case 1:
                Avatar2=Avatar2.transform.GetChild(1).gameObject;
                break;
            case 2:
                Avatar2=Avatar2.transform.GetChild(2).gameObject;
                break;
        }
        fx2=Avatar2.GetComponent<SetFX>();
        wpCtrl2=Avatar1.GetComponent<WPlayerCTRL>();

        t=0;
        _time=1f;
    }

    // Update is called once per frame
    void Update()
    {
        if(opp)
        {
            gu.changeTurn();
            opp=false;
        }

        if(changeTurnFirst)
        {
            changeTurnFirst=false;
        }

        if(changeTurnPC)
        {
            changeTurnPC=false;
        }

        if(pwc)
        {
            if (gu.currentTurn == 1 ) 
            {
                ExaPowerBehaviour.waveOnlyId=whichPwc;
            }
            else 
            {
                ExaPowerBehaviour.waveOnlyId=whichPwc;
            }
            pwc=false;
        }

        if(antivirusNoMore)
        {
            if (gu.currentTurn == 1) 
            {
                if(type1==0)
                {
                    i1t1_Belly.material.SetFloat("_RimMax", 1);
			        i1t1_Body.material.SetFloat("_RimMax", 1);
                    a1t1_Belly.material.SetFloat("_RimMax", 1);
			        a1t1_Body.material.SetFloat("_RimMax", 1);
                }
                else if(type1==1)
                {
                    i1t2_Body.materials[0].SetFloat("_RimMax", 1);
                    a1t2_Body.materials[0].SetFloat("_RimMax", 1);
                }
                else if(type1==2)
                {
                    i1t3_Body.materials[0].SetFloat("_RimMax", 1);
                    a1t3_Body.materials[0].SetFloat("_RimMax", 1);
                }
            }
            else 
            {
                if(type2==0)
                {
                    i1t1_Belly.material.SetFloat("_RimMax", 1);
			        i1t1_Body.material.SetFloat("_RimMax", 1);
                    a2t1_Belly.material.SetFloat("_RimMax", 1);
			        a2t1_Body.material.SetFloat("_RimMax", 1);
                }
                else if(type2==1)
                {
                    i1t2_Body.materials[0].SetFloat("_RimMax", 1);
                    a2t2_Body.materials[0].SetFloat("_RimMax", 1);
                }
                else if(type2==2)
                {
                    i1t3_Body.materials[0].SetFloat("_RimMax", 1);
                    a2t3_Body.materials[0].SetFloat("_RimMax", 1);
                }
            }
            antivirusNoMore=false;
            if (gu.currentTurn == 1) 
            {
                if(CoreEvents.computerTurn)
                {
                    ExaPowerManager.waveOffAll=true;
                    gu.changeTurn();
                    opp=false;
                }
                else
                {
                    ExaPowerManager.waveOffAll=true;
                }
            }
            else
            {
                if(CoreEvents.computerTurn)
                {
                    //ple.showTurnCard();
                    gu.changeTurn();
                    opp=false;
                }
                else
                {
                    ExaPowerManager.waveOffAll=true;
                }
            }
        }

        if(antivirus)
        {
            if (gu.currentTurn == 1) 
            {
                fx1.afx=true;
                if(type1==0)
                {
                    i1t1_Belly.material.SetFloat("_RimMax", 0);
			        i1t1_Body.material.SetFloat("_RimMax", 0);
                    a1t1_Belly.material.SetFloat("_RimMax", 0);
			        a1t1_Body.material.SetFloat("_RimMax", 0);
                }
                else if(type1==1)
                {
                    i1t2_Body.materials[0].SetFloat("_RimMax", 0);
                    a1t2_Body.materials[0].SetFloat("_RimMax", 0);
                }
                else if(type1==2)
                {
                    i1t3_Body.materials[0].SetFloat("_RimMax", 0);
                    a1t3_Body.materials[0].SetFloat("_RimMax", 0);
                }
            }
            else 
            {
                fx2.afx=true;
                if(type2==0)
                {
                    i1t1_Belly.material.SetFloat("_RimMax", 0);
			        i1t1_Body.material.SetFloat("_RimMax", 0);
                    a2t1_Belly.material.SetFloat("_RimMax", 0);
			        a2t1_Body.material.SetFloat("_RimMax", 0);
                }
                else if(type2==1)
                {
                    i1t2_Body.materials[0].SetFloat("_RimMax", 0);
                    a2t2_Body.materials[0].SetFloat("_RimMax", 0);
                }
                else if(type2==2)
                {
                    i1t3_Body.materials[0].SetFloat("_RimMax", 0);
                    a2t3_Body.materials[0].SetFloat("_RimMax", 0);
                }
            }
            antivirus=false;
            if (gu.currentTurn == 1) 
            {
                if(CoreEvents.computerTurn)
                {
                    ExaPowerManager.waveOffAll=true;
                    gu.changeTurn();
                    opp=false;
                }
                else
                {
                    ExaPowerManager.waveOffAll=true;
                }
            }
            else
            {
                if(CoreEvents.computerTurn)
                {
                    //ple.showTurnCard();
                    gu.changeTurn();
                    opp=false;
                }
                else
                {
                    ExaPowerManager.waveOffAll=true;
                }
            }
        }

        if(virus)
        {
            print("virus");
            if (gu.currentTurn == 1) 
            {
                antivirusNoMore=true;
                wpCtrl1.numAntivirus=0;
                fx1.vr=true;
            }
            else 
            {
                antivirusNoMore=true;
                wpCtrl2.numAntivirus=0;
                fx2.vr=true;
            }
            virus=false;
        }

        if(virusAttack)
        {
            print("virus attack");
            if (gu.currentTurn == 1) 
            {
                wpCtrl1.virus=true;
                SettingScore.toSub=1000;
                SettingScore.sub=true;
                
                if(type1==0)
                {
                    i1t1_Belly.material.mainTexture = Resources.Load("Textures/Hufol/Hufol_Dirty/Hufo_low__obj_Hufo_Paint_BaseMap", typeof(Texture2D)) as Texture2D;
			        i1t1_Body.material.mainTexture = Resources.Load("Textures/Hufol/Hufol_Dirty/Hufo_low__obj_Hufo_Paint_BaseMap", typeof(Texture2D)) as Texture2D;
                    a1t1_Belly.material.mainTexture = Resources.Load("Textures/Hufol/Hufol_Dirty/Hufo_low__obj_Hufo_Paint_BaseMap", typeof(Texture2D)) as Texture2D;
                    a1t1_Body.material.mainTexture = Resources.Load("Textures/Hufol/Hufol_Dirty/Hufo_low__obj_Hufo_Paint_BaseMap", typeof(Texture2D)) as Texture2D;
                    print("t1");
                }
                else if(type1==1)
                {
                    i1t2_Body.materials[0].mainTexture = Resources.Load("Textures/Hearthol/Hearthol_Dirty/Spaceship01_low_Red_BaseMap", typeof(Texture2D)) as Texture2D;
                    a1t2_Body.materials[0].mainTexture = Resources.Load("Textures/Hearthol/Hearthol_Dirty/Spaceship01_low_Red_BaseMap", typeof(Texture2D)) as Texture2D;
                    print("t2");
                }
                else if(type1==2)
                {
                    i1t3_Body.materials[0].mainTexture = Resources.Load("Textures/Speedhol/HSpeedhol_Dirty/Spaceship02_low_Red_BaseMap", typeof(Texture2D)) as Texture2D;
                    a1t3_Body.materials[0].mainTexture = Resources.Load("Textures/Speedhol/Speedhol_Dirty/Spaceship02_low_Red_BaseMap", typeof(Texture2D)) as Texture2D;
                    print("t3");
                }
                fx1.va=true;
            }
            else 
            {
                wpCtrl2.virus=true;
                if(CoreEvents.computerTurn)
                {
                    SettingScore2.toSub=1000;
                    SettingScore2.sub=true;
                }
                else
                {
                    SettingScore.toSub=1000;
                    SettingScore.sub=true;
                    
                    if(type2==0)
                    {
                        i1t1_Belly.material.mainTexture = Resources.Load("Textures/Hufol/Hufol_Dirty/Hufo_low__obj_Hufo_Paint_BaseMap", typeof(Texture2D)) as Texture2D;
                        i1t1_Body.material.mainTexture = Resources.Load("Textures/Hufol/Hufol_Dirty/Hufo_low__obj_Hufo_Paint_BaseMap", typeof(Texture2D)) as Texture2D;
                        a2t1_Belly.material.mainTexture = Resources.Load("Textures/Hufol/Hufol_Dirty/Hufo_low__obj_Hufo_Paint_BaseMap", typeof(Texture2D)) as Texture2D;
                        a2t1_Body.material.mainTexture = Resources.Load("Textures/Hufol/Hufol_Dirty/Hufo_low__obj_Hufo_Paint_BaseMap", typeof(Texture2D)) as Texture2D;
                        print("t1");
                    }
                    else if(type2==1)
                    {
                        i1t2_Body.materials[0].mainTexture = Resources.Load("Textures/Hearthol/Hearthol_Dirty/Spaceship01_low_Red_BaseMap", typeof(Texture2D)) as Texture2D;
                        a2t2_Body.materials[0].mainTexture = Resources.Load("Textures/Hearthol/Hearthol_Dirty/Spaceship01_low_Red_BaseMap", typeof(Texture2D)) as Texture2D;
                        print("t2");
                    }
                    else if(type2==2)
                    {
                        i1t3_Body.materials[0].mainTexture = Resources.Load("Textures/Speedhol/HSpeedhol_Dirty/Spaceship02_low_Red_BaseMap", typeof(Texture2D)) as Texture2D;
                        a2t3_Body.materials[0].mainTexture = Resources.Load("Textures/Speedhol/Speedhol_Dirty/Spaceship02_low_Red_BaseMap", typeof(Texture2D)) as Texture2D;
                        print("t3");
                    }
                }
                fx2.va=true;
            }
            virusAttack=false;
            if(CoreEvents.computerTurn)
            {
                //ple.showTurnCard();
                gu.changeTurn();
                opp=false;
            }
            else
            {
                ExaPowerManager.waveOffAll=true;
            }
        }

        if(lucky)
        {
            if (gu.currentTurn == 1) 
            {
                playerColor = PlayerPrefs.GetInt ("p1color");
                t += Time.deltaTime;
                float s = t / _time;
                
                Color32 clrTransition2=Color32.Lerp(c.hcs[playerColor - 1], luckyColor, curve.Evaluate(s));
                float checker=Mathf.Lerp(0.5f,  2, curve.Evaluate(s));
                
                if(type1==0)
                {
                    i1t1_Belly.material.SetColor("_Color", clrTransition2);
			        i1t1_Body.material.SetColor("_Color", clrTransition2);
                    a1t1_Belly.material.SetColor("_Color", clrTransition2);
                    a1t1_Body.material.SetColor("_Color", clrTransition2);
                    print("t1");
                }
                else if(type1==1)
                {
                    i1t2_Body.materials[0].SetColor("_Color", clrTransition2);
                    a1t2_Body.materials[0].SetColor("_Color", clrTransition2);
                    print("t2");
                }
                else if(type1==2)
                {
                    i1t3_Body.materials[0].SetColor("_Color", clrTransition2);
                    a1t3_Body.materials[0].SetColor("_Color", clrTransition2);
                    print("t3");
                }

                if (checker == 2)
                {
                    lucky = false;
                    t = 0;
                    luckyNoMore=true;
                    fx1.lfx=true;
                }
            }
            else 
            {
                playerColor = PlayerPrefs.GetInt ("p2color");
                t += Time.deltaTime;
                float s = t / _time;
                
                Color32 clrTransition;
                Color32 clrTransition2;
                if(CoreEvents.computerTurn)
                {
                    clrTransition=Color32.Lerp(Color.black, luckyColorEmit, curve.Evaluate(s));
                    clrTransition2=Color32.Lerp(Color.white, luckyColor, curve.Evaluate(s));
                }
                else
                {
                    clrTransition=Color32.Lerp(c.em[playerColor - 1], luckyColorEmit, curve.Evaluate(s));
                    clrTransition2=Color32.Lerp(c.hcs[playerColor - 1], luckyColor, curve.Evaluate(s));
                }
                float checker=Mathf.Lerp(0.5f,  2, curve.Evaluate(s));

                if(CoreEvents.computerTurn)
                {
                    print("Mat?");
                    Avatar2.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_Color", clrTransition2);
                    Avatar2.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Renderer>().material.SetColor("_Color", clrTransition2);
                }
                else
                {
                    if(type2==0)
                    {
                        i1t1_Belly.material.SetColor("_Color", clrTransition2);
                        i1t1_Body.material.SetColor("_Color", clrTransition2);
                        a2t1_Belly.material.SetColor("_Color", clrTransition2);
                        a2t1_Body.material.SetColor("_Color", clrTransition2);
                        print("t1");
                    }
                    else if(type2==1)
                    {
                        i1t2_Body.materials[0].SetColor("_Color", clrTransition2);
                        a2t2_Body.materials[0].SetColor("_Color", clrTransition2);
                        print("t2");
                    }
                    else if(type2==2)
                    {
                        i1t3_Body.materials[0].SetColor("_Color", clrTransition2);
                        a2t3_Body.materials[0].SetColor("_Color", clrTransition2);
                        print("t3");
                    }
                }
                if (checker == 2)
                {
                    lucky = false;
                    t = 0;
                    luckyNoMore=true;
                    fx2.lfx=true;
                }
            }
        }

        if(luckyNoMore)
        {
            if (gu.currentTurn == 1) 
            {
                playerColor = PlayerPrefs.GetInt ("p1color");
                t += Time.deltaTime;
                float s = t / _time;

                Color32 clrTransition2=Color32.Lerp(luckyColor, c.hcs[playerColor - 1], curve.Evaluate(s));
                float checker=Mathf.Lerp(2, 0.5f, curve.Evaluate(s));

                if(type1==0)
                {
                    i1t1_Belly.material.SetColor("_Color", clrTransition2);
			        i1t1_Body.material.SetColor("_Color", clrTransition2);
                    a1t1_Belly.material.SetColor("_Color", clrTransition2);
                    a1t1_Body.material.SetColor("_Color", clrTransition2);
                    print("t1");
                }
                else if(type1==1)
                {
                    i1t2_Body.materials[0].SetColor("_Color", clrTransition2);
                    a1t2_Body.materials[0].SetColor("_Color", clrTransition2);
                    print("t2");
                }
                else if(type1==2)
                {
                    i1t3_Body.materials[0].SetColor("_Color", clrTransition2);
                    a1t3_Body.materials[0].SetColor("_Color", clrTransition2);
                    print("t3");
                }

                if (checker == 0.5f)
                {
                    luckyNoMore = false;
                    t = 0;
                    if(CoreEvents.computerTurn)
                    {
                        ExaPowerManager.waveOffAll=true;
                        gu.changeTurn();
                        opp=false;
                    }
                    else
                    {
                        ExaPowerManager.waveOffAll=true;
                    }
                }
            }
            else 
            {
                playerColor = PlayerPrefs.GetInt ("p2color");
                t += Time.deltaTime;
                float s = t / _time;
                
                Color32 clrTransition;
                Color32 clrTransition2;
                if(CoreEvents.computerTurn)
                {
                    clrTransition=Color32.Lerp(luckyColorEmit, Color.black, curve.Evaluate(s));
                    clrTransition2=Color32.Lerp(luckyColor, Color.white, curve.Evaluate(s));
                }
                else
                {
                    if(wpCtrl2.numAntivirus>0)
                    {
                        clrTransition=Color32.Lerp(luckyColorEmit, Color.white, curve.Evaluate(s));
                    }
                    else
                    {
                        clrTransition=Color32.Lerp(luckyColorEmit, c.em[playerColor - 1], curve.Evaluate(s));
                    }
                    clrTransition2=Color32.Lerp(luckyColor, c.hcs[playerColor - 1], curve.Evaluate(s));
                }
                float checker=Mathf.Lerp(2, 0.5f, curve.Evaluate(s));

                if(CoreEvents.computerTurn)
                {
                    if(type2==0)
                    {
                        i1t1_Belly.material.SetColor("_Color", clrTransition2);
                        i1t1_Body.material.SetColor("_Color", clrTransition2);
                        a2t1_Belly.material.SetColor("_Color", clrTransition2);
                        a2t1_Body.material.SetColor("_Color", clrTransition2);
                        print("t1");
                    }
                    else if(type2==1)
                    {
                        i1t2_Body.materials[0].SetColor("_Color", clrTransition2);
                        a2t2_Body.materials[0].SetColor("_Color", clrTransition2);
                        print("t2");
                    }
                    else if(type2==2)
                    {
                        i1t3_Body.materials[0].SetColor("_Color", clrTransition2);
                        a2t3_Body.materials[0].SetColor("_Color", clrTransition2);
                        print("t3");
                    }
                }
                else
                {
                    if(type2==0)
                    {
                        i1t1_Belly.material.SetColor("_Color", clrTransition2);
                        i1t1_Body.material.SetColor("_Color", clrTransition2);
                        a2t1_Belly.material.SetColor("_Color", clrTransition2);
                        a2t1_Body.material.SetColor("_Color", clrTransition2);
                        print("t1");
                    }
                    else if(type2==1)
                    {
                        i1t2_Body.materials[0].SetColor("_Color", clrTransition2);
                        a2t2_Body.materials[0].SetColor("_Color", clrTransition2);
                        print("t2");
                    }
                    else if(type2==2)
                    {
                        i1t3_Body.materials[0].SetColor("_Color", clrTransition2);
                        a2t3_Body.materials[0].SetColor("_Color", clrTransition2);
                        print("t3");
                    }
                }

                if (checker == 0.5f)
                {
                    luckyNoMore = false;
                    t = 0;
                    if(CoreEvents.computerTurn)
                    {
                        //ple.showTurnCard();
                        gu.changeTurn();
                        opp=false;
                    }
                    else
                    {
                        ExaPowerManager.waveOffAll=true;
                    }
                }
            }
        }
    }
}
