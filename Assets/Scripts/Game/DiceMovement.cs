﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class DiceMovement : MonoBehaviour
{
    private static RectTransform me;
    private Button meBtn;
    private Vector3 _from;
    private Vector3 _to;

    private float t;
    private float duration;

    public bool move;
    public bool back;
    public bool ok=true;//only for numPlayers=1 first PC turn

    public AnimationCurve curve;
    void Start()
    {
        me = transform.GetComponent<RectTransform>();
        meBtn = transform.GetComponent<Button>();
        _from = me.localPosition;
        _to = new Vector3(me.localPosition.x, -190f, me.localPosition.z);
        t = 0;
        duration = 0.46f;
    }

    // Update is called once per frame
    void Update()
    {
        if (move && ok)
        {
            t += Time.deltaTime;
            float s = t / duration;
            
            me.localPosition=Vector3.Lerp(_from, _to, curve.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                move = false;
                t = 0;
                meBtn.interactable=true;
            }
        }

        if (back)
        {
            t += Time.deltaTime;
            float s = t / duration;
            
            me.localPosition=Vector3.Lerp(_to, _from, curve.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                back = false;
                t = 0;
            }
        }
    }
}
