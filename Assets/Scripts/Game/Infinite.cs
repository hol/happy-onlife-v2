﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class Infinite : MonoBehaviour
{
    private Transform tx;
    private ParticleSystem.Particle[] points;

    public int starsMax=100;
    public Color32 starColor = new Color32(1,1,1,1);
    public float starSize=0.8f;
    public float starDistance=20f;
    public float starClipDistance=1f;
    private float starDistanceSqr;
    private float starClipDistanceSqr;

    
    void Start()
    {
        tx=transform;
        starDistanceSqr=starDistance*starDistance;
        starClipDistanceSqr=starClipDistance*starClipDistance;
    }

    void Update()
    {
        if(points==null)
        {
            CreateStars();
        }

        for (int i=0; i<starsMax; i++)
        {
            if ((points[i].position - tx.position).sqrMagnitude > starDistanceSqr) {
				points[i].position = Random.insideUnitSphere.normalized * starDistance + tx.position;
			}

			if ((points[i].position - tx.position).sqrMagnitude <= starClipDistanceSqr) {
				float percent = (points[i].position - tx.position).sqrMagnitude / starClipDistanceSqr;
				points[i].startColor = new Color(1,1,1, percent);
				points[i].startSize = percent * starSize;
			}
        }

        tx.GetComponent<ParticleSystem>().SetParticles(points, points.Length);
    }

    private void CreateStars()
    {
        points=new ParticleSystem.Particle[starsMax];

        for (int i=0; i<starsMax; i++)
        {
            points[i].position=Random.insideUnitSphere * starDistance + tx.position;
            points[i].startColor=starColor;
            points[i].startSize =starSize;
        }
    }
}
