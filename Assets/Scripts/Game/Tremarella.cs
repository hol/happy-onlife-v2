﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using UnityEngine;

public class Tremarella : MonoBehaviour
{
    RectTransform me;
    bool restart;

    float t;
    float t2;

    Quaternion incr;
    Quaternion dicr;

    Quaternion reset;
    void Start()
    {
        me=transform.GetComponent<RectTransform>();
        t=0.01f;
        t2=2f;
        incr=new Quaternion(0,1,0,0f);
        dicr=new Quaternion(0,0,0,0f);
        reset=me.localRotation;
        restart=true;
    }

    void Update()
    {
        if(restart)
        {
            me.localRotation=reset;
            StartCoroutine("Trumble");
            restart=false;
        }
    }

    IEnumerator Trumble()
    {
        yield return new WaitForSeconds(t2);
        me.localRotation=incr;
        yield return new WaitForSeconds(t);
        me.localRotation=dicr;
        yield return new WaitForSeconds(t);
        me.localRotation=incr;
        yield return new WaitForSeconds(t); 
        me.localRotation=dicr;
        yield return new WaitForSeconds(t);
        me.localRotation=incr;
        yield return new WaitForSeconds(t);
        restart=true;
    }
}
