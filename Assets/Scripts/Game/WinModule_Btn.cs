/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class WinModule_Btn : MonoBehaviour
{
    public CoreEvents ce;
	public ChangeTurnNew changeTurnN;
    public static bool able;
    public static int cat4PC;

    public void Continue()
    {
        if(able)
        {
            transform.GetComponent<Button>().interactable=false;
            able=false;
            if(WinModule.who=="changeTurn" || WinModule.who=="changeTurnPC")
            {
                ce.hideTurnCardOnClick();
                if(WinModule.who=="changeTurn")
                {
                    changeTurnN.phase3=true;
                }
                else
                {
                    changeTurnN.phase6=true;
                }

                if(!CoreEvents.computerTurn)
                {
                    ExaPowerManager.waveOn=true;
                }
            }
            WinModule.continu=true;
        }
        
    }
}
