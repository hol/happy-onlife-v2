﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class ExaPowerBehaviour : MonoBehaviour
{
    private WPlayerCTRL wp1;
    private WPlayerCTRL wp2;

    public GenericUtils gu;
    public AssistKeyboard ak;
    public int ID;
    private bool waveOn;
    private bool waveOnly;
    private bool waveOff;
    
    public static int waveOnId;
    public static int waveOnlyId;
    public static int waveOff0Id;
    public static int waveOff3Id;
    public static int waveOffAll;
    public static int connectByID;
    public static int disconnectByID;
    private Vector3 rightpos;
    private Vector3 backpos;

    private float t;
    private float _time;    
    public AnimationCurve curve;

    public static bool readStatus;
    public static int cont;
    private int tot;

    public static int actualPlayer;

    public static int changeStatus;
    public static int newStatus;

    public int status;

    public Exa2Dnew exa;
    public Image Icon;
    public PowerTrailsSetting trail;

    private Color32[] cs;

    void Start()
    {
        wp1=gu.avatar1.GetComponent<WPlayerCTRL>();
		wp2=gu.avatar2.GetComponent<WPlayerCTRL>();
        waveOn=false;
        waveOnly=false;
        waveOff=false;
        
        cs = new[]
		{
			new Color32(40,163,206,255), //1
			new Color32(4,62,132,255), //2
			new Color32(0,167,155,255), //3
			new Color32(106,172,35,255), //4
			new Color32(235,163,29,255),//5
			new Color32(231,102,44,255), //6
			new Color32(191,39,45,255), //7
			new Color32(194,35,128,255) //8
		};

        cont=0;
        tot=transform.parent.transform.childCount;
        string[] splitArray =  transform.gameObject.name.Split(char.Parse("_"));
        ID = int.Parse(splitArray[1]);
        waveOnId=-1;
        waveOnlyId=-1;
        waveOff0Id=-1;
        waveOff3Id=-1;
        waveOffAll=-1;
        connectByID=-1;
        disconnectByID=-1;
        changeStatus=-1;
        newStatus=-1;
        rightpos=transform.localPosition;
        backpos=new Vector3(rightpos.x-(transform.GetComponent<RectTransform>().sizeDelta.x*2f), rightpos.y, rightpos.z);
        transform.localPosition=backpos;
        t=0;
        _time=0.2f;
        Icon.color = new Color32(122,122,122,255);
    }

    void ReadStatus()
    {
        actualPlayer=gu.currentTurn;
        if(actualPlayer==1)
        {
            status=wp1.pcOwned[ID];
        }
        else
        {
            status=wp2.pcOwned[ID];
        }

        if(gu.wrongGenCat[ID]!=0 && status==2)
        {
            status=3;
        }

        exa.edit=true;
        exa.noInner=false;

        switch(status)
        {
            case 0:
                exa.noInner=true;
                exa.colorOutline = new Color32(88,88,88,255);
                Icon.color = new Color32(122,122,122,255);
                ak.when[ID]=false;
                break;
            case 1:
                exa.halfSx=true;
                exa.colorOutline = new Color32(88,88,88,255);
                Icon.color = new Color32(122,122,122,255);
                ak.when[ID]=false;
                break;
            case 2:
                exa.full=true;
                exa.colorOutline = new Color32(88,88,88,255);
                Icon.color = new Color32(122,122,122,255);
                ak.when[ID]=false;
                break;
            case 3:
                exa.full=true;
                exa.colorOutline = new Color32(255,255,255,255);
                Icon.color = new Color32(255,255,255,255);
                ak.when[ID]=true;
                break;
        }

        exa.colorInner=cs[ID];
        exa.colorNow=true;
    } 

    // Update is called once per frame
    void Update()
    {
        if(ID==connectByID)
        {
            trail.connected=true;
            connectByID=-1;
        }

        if(ID==disconnectByID)
        {
            trail.connected=false;
            disconnectByID=-1;
        }

        if(ID==waveOnId)
        {
            ReadStatus();
            waveOn=true;
        }

        if(ID==waveOnlyId)
        {
            ReadStatus();
            waveOnly=true;
            waveOn=true;
            waveOnlyId=-1;
        }

        if(ID==waveOff0Id)
        {
            if(status<3)
            {
                waveOff=true;
            }
        }

        if(ID==waveOff3Id)
        {
            if(status>=3)
            {
                waveOff=true;
            }
        }

        if(ID==waveOffAll)
        {
            waveOff=true;
        }

        if(waveOn)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            transform.localPosition=Vector3.Lerp(backpos, rightpos, curve.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                waveOn = false;
                t = 0;
                if(status>=3)
                {
                    gu.WhoIsDisposable(ID);
                }
                if(waveOnly)
                {
                    waveOnly=false;
                    ExaPowerManager.waveOffAll=true;
                }
            }
        }

        if(waveOff)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            transform.localPosition=Vector3.Lerp(rightpos, backpos, curve.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                waveOff = false;
                t = 0;
            }
        }
    }
}
