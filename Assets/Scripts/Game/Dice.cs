/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class Dice : MonoBehaviour
{
    public DiceMovement Move;
    public CoreEvents core;
    public Sound sound;
    public GenericUtils gu;
    
	public Image dice;
    public Button btnDice;

    public bool activeBtn;
	public bool diceEnabled = true;

    public int _diceFace = 0;
	public int newDiceNum=0;
	public int newDiceNumStore=0;

	private int diceStore=0;
	private int po1=0;
    private int po2=0;
	private int turn=1;

    void Start() 
    {
		diceEnabled = true;
    }
    
    public void ActiveBtn(bool de=false, bool move=false)
    {
        print("Dice btn interactable");
        btnDice.interactable = true;
        if(de)
        {
            print("Dice btn enabled");
            diceEnabled = true;
        }
        if(move)
        {
            print("Dice btn moving");
            Move.move=true;
        }
    }

    /*For avoid a normal random function manage the launch of dice, 
      that received a lot of critics of gamers that sometimes saw the opponent be luckier,
      this function set a smaller range from the first launch in order to make the challenge more balanced.
    */
    public int Formula()
    {
        if(diceStore==0)
        {
            //print("Reset Dice");
            _diceFace=Random.Range(1,7);
        }
        else
        {
            switch (diceStore)
            {
                case 1:
                    //print("1:from 1 to 3");
                    _diceFace=Random.Range(1,4);
                    break;
                case 2:
                    //print("2:from 1 to 4");
                    _diceFace=Random.Range(1,5);
                    break;
                case 3:
                    //print("3:from 1 to 5");
                    _diceFace=Random.Range(1,6);
                    break;
                case 4:
                    //print("4:from 2 to 6");
                    _diceFace=Random.Range(2,7);
                    break;
                case 5:
                    //print("5:from 3 to 6");
                    _diceFace=Random.Range(3,7);
                    break;
                case 6:
                    //print("6:from 4 to 6");
                    _diceFace=Random.Range(4,7);
                    break;
            }
        }

        diceStore=_diceFace;

        if(turn==1)
        {
            //print("Turn1");
            po1+=_diceFace;
            turn++;
        }
        else
        {
            //print("Turn2");
            po2+=_diceFace;
            turn=1;
        }

        if(po1==po2)
        {
            diceStore=0;
        }

        return _diceFace;
    }

// Dice hit and show
	public void hitDice() 
	{
		Click4Question.free=false;
        print("Did you clicked on Dice?");

        if(!diceEnabled)
        {
            diceEnabled=true;
        }

		if (diceEnabled) 
		{
			diceEnabled = false;
			//print("hitDice");
			
            _diceFace=Formula();
            
			Sprite sprite = null;
			switch (_diceFace) {
			case 1: 
				sprite = Resources.Load<Sprite> ("Textures/D1");
				break;
			case 2: 
				sprite = Resources.Load<Sprite> ("Textures/D2");
				break;
			case 3: 
				sprite = Resources.Load<Sprite> ("Textures/D3");
				break;
			case 4: 
				sprite = Resources.Load<Sprite> ("Textures/D4");
				break;
			case 5: 
				sprite = Resources.Load<Sprite> ("Textures/D5");
				break;
			case 6: 
				sprite = Resources.Load<Sprite> ("Textures/D6");
				break;
			}
			dice.sprite = sprite;
			dice.enabled = true;
            //Delay for this function
			Invoke ("hideDice", 1);
			if (core.playSounds)
				sound.Play("dice");
			btnDice.interactable = false;
		}
	}
	
	// Hide the dice
	public void hideDice() 
	{
		dice.enabled = false;
		gu.playTurn(_diceFace);
		Move.back=true;
	}
}
