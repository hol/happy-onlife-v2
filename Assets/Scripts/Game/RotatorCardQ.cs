﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class RotatorCardQ : MonoBehaviour
{
    public static int cardN;
    public int cardMax;
    
    public RectTransform card;
    public Exa2Dnew exaCard;
    private Color32 innerColorStore;

    public RectTransform ans_2;
    public Text display;
    public SizeMe txtSizeAns;
    public SizeMe txtSizeQuest;
    public bool prepare;
    
    public RectTransform ans2;
    public RectTransform ans3;
    public RectTransform ans4;

    public Button btn;
    public CoreEvents ce;
    public int firstWrongAttempt = -1;

    public string[] answers;
    public int newAns;

    public Point[] points;
    
    private float t;
    private float _time;

    public bool turn;
    public bool turnBack;
    
    public AnimationCurve curve;

    public static float startPY;
    public static float startPYB;
    private float endPY;
    private float endPYB;
    private float endPZ;
    private float endPZB;
    private bool changeNext;
    private bool changePrev;
    private bool locked;

    public string direction;
    void Start()
    {
        startPY = 0;
        endPY = 180;
        endPZ = -8;
        
        startPYB = 180;
        endPYB = 0;
        
        t = 0;
        _time = 0.5f;
        direction = "sx";
        cardN = 0;
    }

    void PointReset()
    {
        for (int i = 0; i < points.Length; i++)
        {
            points[i].wrong=false;
            points[i].exa2D.colorInner = Color.white;
            points[i].exa2D.colorOutline = Color.white;
        }
        //points[0].SetFull();
        Point.sectionCalling = "question";
        Point.on = 1;
        Point.check = true;
    }

    void Prepare()
    {
        PointReset();

        firstWrongAttempt=-1;

        answers=new string[]{
            ans_2.GetComponent<Text>().text,
            ans2.GetComponent<Text>().text,
            ans3.GetComponent<Text>().text,
            ans4.GetComponent<Text>().text
        };

        newAns=0;

        display.text=answers[newAns];
        display.color=Color.white;
        txtSizeAns.resizeH=true;
        txtSizeQuest.resizeH=true;
        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(delegate {
                ce.SelectAnswer(newAns+1);
            });
        
        innerColorStore=exaCard.colorInner;
        locked=false;
    }

    void Update()
    {
        if(prepare)
        {
            Prepare();
            prepare=false;
        }

        if (turn)
        {
            t += Time.deltaTime;
            float s = t / _time;
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            float y = Mathf.Lerp(startPY,  endPY, curve.Evaluate(s));
            
            float y2 = 0;
            float z = 0;
            float z2 = 0;
            
            if (y < endPY*0.5f)
            {
                z = (y * endPZ) / endPY;
                y2 = y;
                z2 = z;
            }
            else
            {
                changeNext = true;
                z = (endPZ)-((y * endPZ) / endPY);
                y2 = endPY - y;
                z2 = -z;
            }
            
            card.localEulerAngles = new Vector3(0, y2, z2);
            ans_2.localEulerAngles = new Vector3(0, y2, z2);
            
            if (checker == 1)
            {
                turn = false;
                t = 0;
                changeNext = false;
                locked = false;
            }
        }
        
        if (turnBack)
        {
            t += Time.deltaTime;
            float s = t / _time;
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            float y = Mathf.Lerp(startPYB,  endPYB, curve.Evaluate(s));
            
            float y2 = 0;
            float z = 0;
            float z2 = 0;
            
            if (y < endPY*0.5f)
            {
                changePrev = true;
                z = (y * endPZ) / endPY;
                y2 = y;
                z2 = z;
            }
            else
            {
                z = (endPZ)-((y * endPZ) / endPY);
                y2 = endPY - y;
                z2 = -z;
            }
            
            card.localEulerAngles = new Vector3(0, y2, z2);
            ans_2.localEulerAngles = new Vector3(0, y2, z2);
            
            if (checker == 1)
            {
                turnBack = false;
                t = 0;
                changePrev = false;
                locked = false;
            }
        }
        
        if (!locked)
        {
            if (changeNext)
            {
                print("changedNext");
                //Change Text of answer
                display.text = answers[newAns];
                //Change Text disposition
                txtSizeAns.resizeH=true;
                //Change color depending on previous wrong answer
                if(firstWrongAttempt==newAns+1)
                {
                    display.color = new Color32(60, 60, 60, 255);
                    exaCard.colorInner = new Color32(115, 115, 115, 255);
                    exaCard.colorOutline = new Color32(200, 0, 0, 255);
                }
                else
                {
                    display.color=Color.white;
                    exaCard.colorOutline=Color.white;
                    exaCard.colorInner=innerColorStore;
                }
                //Remove previous click listener
                btn.onClick.RemoveAllListeners();
                //Add new click listener
                btn.onClick.AddListener(delegate {
                        ce.SelectAnswer(newAns+1);
                    });
                //Manage points
                Point.sectionCalling = "question";
                Point.on = newAns+1;
                Point.check = true;
                locked = true;
            }
            
            if (changePrev)
            {
                print("changedPrev");
                //Change Text of answer
                display.text = answers[newAns];
                //Change Text disposition
                txtSizeAns.resizeH=true;
                //Change color depending on previous wrong answer
                if(firstWrongAttempt==newAns+1)
                {
                    display.color = new Color32(60, 60, 60, 255);
                    exaCard.colorInner = new Color32(115, 115, 115, 255);
                    exaCard.colorOutline = new Color32(200, 0, 0, 255);
                }
                else
                {
                    display.color = Color.white;
                    exaCard.colorOutline=Color.white;
                    exaCard.colorInner=innerColorStore;
                }
                //Remove previous click listener
                btn.onClick.RemoveAllListeners();
                //Add new click listener
                btn.onClick.AddListener(delegate {
                        ce.SelectAnswer(newAns+1);
                    });
                //Manage points
                Point.sectionCalling = "question";
                Point.on = newAns+1;
                Point.check = true;
                locked = true;
            }
        }
    }

    public void SetNewCard(int newCard)
    {
        //newCard++;
        if (newCard < cardN)
        {
            direction = "dx";
            newAns = newCard;
            cardN=newCard;
            turnBack = true;
        }
        else if (newCard > cardN)
        {
            direction = "sx";
            newAns = newCard;
            cardN=newCard;
            turn = true;
        }
        else
        {
            return;
        }
    }
    
    public void NextCard()
    {
        direction = "sx";
        newAns++;
        if(newAns==cardMax)
        {
            newAns=0;
        }
        turn = true;
    }
    
    public void PrevCard()
    {
        direction = "dx";
        newAns--;
        if(newAns<0)
        {
            newAns=cardMax-1;
        }
        turnBack = true;
    }
}
