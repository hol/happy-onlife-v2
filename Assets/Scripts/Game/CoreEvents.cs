﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

//The core of the game
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CoreEvents : MonoBehaviour 
{
	[Header("Modules")]
	public Sound sound;
	public Views view;
	public Dice dice;
	public IconsColorAndPos icp;
	public ChangeTurnNew changeTurnN;
	public Colors c;
	public SecondaryCards sc;
	public RotatorCardQ rotatorQuest2;
	public GetEndScore endScore;
	public GenericUtils gu;
    public AssistKeyboardGroups akg;
    public AssistKeyboard akQuest;
	public static int rightConnection=1; //comanded by ScaledTextView

	public static bool oldWrongQ;
	public static bool interceptor;
	public static bool enoughPWC;
	public static bool deviantClick;
	public static bool hideQ;
	public static bool activeQ;

	[Header("WPlyers Controllers\r\n(Automatically extracted at Start)")]
	public WPlayerCTRL avatar1;
	public WPlayerCTRL avatar2;
	private SetFX fx1;
	private SetFX fx2;

	[Header("Scores")]
	public int firstAttemptPoints = 1000;
	public int secondAttemptPoints = 500;
	
	[Header("QuestionCard Elements")]
	public GameObject card;
	public Image CatIconQ;
	public Text CatTxtQ;
	public Image CatIconQ_2;
	public Text CatTxtQ_2;
	public float alphaClrLabel=1;
	public Exa2Dnew QExa;
	public Exa2Dnew QExa_2;
	public Exa2Dnew AExa_2;
	public Exa2Dnew AExa1;
	public Exa2Dnew AExa2;
	public Exa2Dnew AExa3;
	public Exa2Dnew AExa4;
	public Exa2Dnew PWCExa;
	public Exa2Dnew PWCExa_2;
	public GameObject PWCExaObj;
	public GameObject PWCExaObj_2;
	public Text txtQ;
	public Text txtA1;
	public Text txtA2;
	public Text txtA3;
	public Text txtA4;
	public Button btnA1;
	public Button btnA2;
	public Button btnA3;
	public Button btnA4;
	public Text txtQ_2;
	public Text txtA1_2;
	public Text txtA2_2;
	public Text txtA3_2;
	public Text txtA4_2;
	public Button btnA_2;
	public Point point1;
	public Point point2;
	public Point point3;
	public Point point4;
	
	[Header("Sentences and Points")]
	public string[] sentences;
	public string[] extrapos;
	public string[] phrases;
	public int[] qseq0;
	public int[] qseq1;
	public int[] qseq2;
	public int[] qseq3;
	public int[] qseq4;
	public int[] qseq5;
	public int[] qseq6;
	public int[] qseq7;
	public int numLucky = 20;
	public int[] extrapospoints = new int[] {400,100,300,200,100,200,300,300,300,300,100,300,100,200,200,100,200,200,200,200};
	public int[] lseq = new int[] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	
	private int   q0 = 0;
	private int   q1 = 0;
	private int   q2 = 0;
	private int   q3 = 0;
	private int   q4 = 0;
	private int   q5 = 0;
	private int   q6 = 0;
	private int   q7 = 0;
	public int   l1 = 0;
	
	[Header("Categories Questions & Max Questions for each")]
	public int nCat0=20;
	public string[] qCat0;
	public int nCat1=18;
	public string[] qCat1;
	public int nCat2=21;
	public string[] qCat2;
	public int nCat3=19;
	public string[] qCat3;
	public int nCat4=19;
	public string[] qCat4;
	public int nCat5=18;
	public string[] qCat5;
	public int nCat6=19;
	public string[] qCat6;
	public int nCat7=21;
	public string[] qCat7;

	[Header("Bools")]
	public bool playSounds = true;
	public bool devClick;
	
	//
	private int selectedAnswer = 0;
	private int currentAttempt = 0;
	private int answerWaited = 1;
	private int questionType = 0;
	//private int _forcedMove = 0;

	//Service
	int wrongAnswers=0;
	int actualCell;
	int numQuestion;

	int catStore=-1;
	int nqStore=-1;
	int IDcellStore=-1;

	bool firstTime=true;
	bool firstTimePC=true;

	public static bool computerTurn;
	public static bool challangeAccepted;
	
	private bool refill=false;
	private int[] allRes=new int[]{4,4,4,2,2,1};
	private int[] allResPass=new int[]{4,4,4,2,2,1};

	public byte opacity;
	private bool btnVis = false;

	//General
    private int nCatTot=8;
    private int nAnsTot=4;
    private int[] q;
    private ArrayList qSeq;
    private ArrayList qCat;
    private int[] nCat;
	private Text[] nTxtA;
	private Text[] nTxtA_2;
	
	void Start() 
	{
		avatar1=gu.avatar1.GetComponent<WPlayerCTRL>();
		fx1=gu.avatar1.GetComponent<SetFX>();
		avatar2=gu.avatar2.GetComponent<WPlayerCTRL>();
		fx2=gu.avatar2.GetComponent<SetFX>();

		challangeAccepted=false;
		firstTime=true;
		firstTimePC=true;
		oldWrongQ=false;
		interceptor=false;
		enoughPWC=false;
		hideQ=false;
		activeQ=false;
		deviantClick=false;

		qseq0 = new int[nCat0];
		qseq1 = new int[nCat1];
		qseq2 = new int[nCat2];
		qseq3 = new int[nCat3];
		qseq4 = new int[nCat4];
		qseq5 = new int[nCat5];
		qseq6 = new int[nCat6];
		qseq7 = new int[nCat7];

		qCat0=new string [nCat0*5];
		qCat1=new string [nCat1*5];
		qCat2=new string [nCat2*5];
		qCat3=new string [nCat3*5];
		qCat4=new string [nCat4*5];
		qCat5=new string [nCat5*5];
		qCat6=new string [nCat6*5];
		qCat7=new string [nCat7*5];

        q=new int[] {q0, q1, q2, q3, q4, q5, q6, q7};

        qSeq=new ArrayList {qseq0, qseq1, qseq2, qseq3, qseq4, qseq5, qseq6, qseq7};

        qCat=new ArrayList {qCat0, qCat1, qCat2, qCat3, qCat4, qCat5, qCat6, qCat7};

        nCat=new int[] {nCat0, nCat1, nCat2, nCat3, nCat4, nCat5, nCat6, nCat7};

		nTxtA=new Text[] {txtA1, txtA2, txtA3, txtA4};
		nTxtA_2=new Text[] {txtA1_2, txtA2_2, txtA3_2, txtA4_2};

		QExa.edit=true;
		QExa_2.edit=true;
		AExa_2.edit=true;
		AExa1.edit=true;
		AExa2.edit=true;
		AExa3.edit=true;
		AExa4.edit=true;
		PWCExa.edit=true;
		PWCExa_2.edit=true;
		
		CatTxtQ.font =(Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		txtQ.font =(Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		txtA1.font =(Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		txtA2.font =(Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		txtA3.font =(Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		txtA4.font =(Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		CatTxtQ_2.font =(Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		txtQ_2.font =(Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		txtA1_2.font =(Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		txtA2_2.font =(Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		txtA3_2.font =(Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		txtA4_2.font =(Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		
		Random.InitState(System.Environment.TickCount);
		print("Hide dice");
		dice.hideDice();
		icp.updateControlPanel();
		print("icp.updateControlPanel");
		view.switchTo3DView();
		print("view.switchTo3DView");
		loadSentences();
		print("CoreStart.loadSentences");
		prepareSequence();
		print("CoreStart.prepareSequence");
		showTurnCard();
		print("CoreStart.showTurnCard");
	}
	
	void Update()
	{
		//ONLY FOR DEVELOPMENT!!!!!!!!!
		if(dice.newDiceNum!=dice.newDiceNumStore)
		{
			print("forced dice num "+dice.newDiceNum);
			dice._diceFace=dice.newDiceNum;
			dice.hideDice();
			dice.newDiceNum=0;
		}
		devClick=deviantClick;
		//ONLY FOR DEVELOPMENT!!!!!!!!!
		
		if(challangeAccepted)
		{
			print("challengeAccepted");
			view.switchTo2DView();
			int pcGame=dice.Formula();
			gu.playTurn(pcGame);
			//print("autoDice="+pcGame);
			challangeAccepted=false;
		}

		//Touch on Red Hexa3D
		if(interceptor)
		{
			print("interceptor");
			card.GetComponent<AssistKeyboard>().when[nCat.Length]=true;
			Question(Click4Question.passCat, false, Click4Question.passNQ);
			IDcellStore=Click4Question.passIDcell;
			akg.AssignFocus(rightConnection);
			interceptor=false;
		}

		//Hide Question
		if(hideQ)
		{
			print("hideQ");
			QuestionMovement.resetTxt=true;
			QuestionMovement.moveAway=true;
			dice.Move.move=true;//
			gu.gameBoardRotation=true;//
			ExaPowerBehaviour.disconnectByID=catStore;
			deviantClick=false;
			hideQ=false;
		}
		
		if(activeQ)
		{
			print("activeQ");
			ExaPowerBehaviour.disconnectByID=catStore;
			deviantClick=false;
			showQtoReTry();
			activeQ=false;
		}
	}
	
	// The function prepare random order numbers that will guides the order of presentation of questions for every categories
	void prepareSequence() 
	{
		// Sequence for card 1
		int index = 0;
		//Prepare sequence Cat0
		while (index<nCat0-1) 
		{
			bool trovato = false;
			int qNum = Random.Range(1,nCat0);
			for(int i=0;i<index;i++)
			{
				if(qseq0[i]==qNum)
				{
					trovato=true;
				}
			}
			if(!trovato) 
			{
				qseq0[index]=qNum;
				++index;
			}
		}
		index = 0;
		//Prepare sequence Cat1
		while (index<nCat1-1) 
		{
			bool trovato = false;
			int qNum = Random.Range(1,nCat1);
			for(int i=0;i<index;i++)
			{
				if(qseq1[i]==qNum)
				{
					trovato=true;
				}
			}
			if(!trovato) 
			{
				qseq1[index]=qNum;
				++index;
			}
		}
		index = 0;
		//Prepare sequence Cat2
		while (index<nCat2-1) 
		{
			bool trovato = false;
			int qNum = Random.Range(1,nCat2);
			for(int i=0;i<index;i++)
			{
				if(qseq2[i]==qNum)
				{
					trovato=true;
				}
			}
			if(!trovato) 
			{
				qseq2[index]=qNum;
				++index;
			}
		}
		index = 0;
		//Prepare sequence Cat3
		while (index<nCat3-1) 
		{
			bool trovato = false;
			int qNum = Random.Range(1,nCat3);
			for(int i=0;i<index;i++)
			{
				if(qseq3[i]==qNum)
				{
					trovato=true;
				}
			}
			if(!trovato) 
			{
				qseq3[index]=qNum;
				++index;
			}
		}
		index = 0;
		//Prepare sequence Cat4
		while (index<nCat4-1) 
		{
			bool trovato = false;
			int qNum = Random.Range(1,nCat4);
			for(int i=0;i<index;i++)
			{
				if(qseq4[i]==qNum)
				{
					trovato=true;
				}
			}
			if(!trovato) 
			{
				qseq4[index]=qNum;
				++index;
			}
		}
		index = 0;
		//Preparasequenza Cat5
		while (index<nCat5-1) 
		{
			bool trovato = false;
			int qNum = Random.Range(1,nCat5);
			for(int i=0;i<index;i++)
			{
				if(qseq5[i]==qNum)
				{
					trovato=true;
				}
			}
			if(!trovato) 
			{
				qseq5[index]=qNum;
				++index;
			}
		}
		index = 0;
		//Prepare sequence Cat6
		while (index<nCat6-1) 
		{
			bool trovato = false;
			int qNum = Random.Range(1,nCat6);
			for(int i=0;i<index;i++)
			{
				if(qseq6[i]==qNum)
				{
					trovato=true;
				}
			}
			if(!trovato) 
			{
				qseq6[index]=qNum;
				++index;
			}
		}
		index = 0;
		//Prepare sequence Cat7
		while (index<nCat7-1) 
		{
			bool trovato = false;
			int qNum = Random.Range(1,nCat7);
			for(int i=0;i<index;i++)
			{
				if(qseq7[i]==qNum)
				{
					trovato=true;
				}
			}
			if(!trovato) 
			{
				qseq7[index]=qNum;
				++index;
			}
		}
		q0 = 0;
		q1 = 0;
		q2 = 0;
		q3 = 0;
		q4 = 0;
		q5 = 0;
		q6 = 0;
		q7 = 0;
		// Prepare sequence for lucky cards
		index = 0;
		while (index<numLucky-1)
		{
			bool trovato = false;
			int qNum=0;
			while(qNum==0)
			{
				qNum = Random.Range(-19,21);
			}
			for(int i=0;i<index;i++)
			{
				if(lseq[i]==qNum)
				{
					trovato=true;
				}
			}
			if(!trovato) 
			{
				lseq[index]=qNum;
				++index;
			}
		}
		l1 = 0;
		//print("ple.prepareSequence");
	}
	
	//Get phrase for Categories, Win Card and Score
	public string getPhraseFromId (string id) 
	{
		for (int idx=0; idx<phrases.Length; idx++) 
		{
			string[] couple = phrases [idx].Split ('=');
			couple [0] = couple [0].Replace ("\n", "").Replace ("\r", "");
			if(string.Compare(couple[0],id)==0) 
			{
				// string found
				return couple[1];
			}
		}
		return "";
	}

	//Get phrase for Lucky Cards
	public string getPhraseFromId2 (string id) 
	{
		id = id.Replace("\n", "").Replace ("\r", "");
		return id;
	}
	
	//Load questions and answers based on selected language
	void loadSentences() 
	{
		TextAsset qAsset = null;
		string suffix = PlayerPrefs.GetString ("linguaSuffix");
		string fname = "Text/questions_" + suffix;
		qAsset = (TextAsset)Resources.Load(fname);
		string testoDomande = qAsset.text;
		sentences = testoDomande.Split ("\r\n" [0]);

		for(int i=0;i<sentences.Length;i++)
		{
			if(i<nCat0*5)
			{
				qCat0[i]=sentences[i];
			}
			else if(i==nCat0*5 || i<nCat0*5+nCat1*5)
			{
				qCat1[i-nCat0*5]=sentences[i];
			}
			else if(i==nCat0*5+nCat1*5 || i<nCat0*5+nCat1*5+nCat2*5)
			{
				qCat2[i-(nCat0*5+nCat1*5)]=sentences[i];
			}
			else if(i==nCat0*5+nCat1*5+nCat2*5 || i<nCat0*5+nCat1*5+nCat2*5+nCat3*5)
			{
				qCat3[i-(nCat0*5+nCat1*5+nCat2*5)]=sentences[i];
			}
			else if(i==nCat0*5+nCat1*5+nCat2*5+nCat3*5 || i<nCat0*5+nCat1*5+nCat2*5+nCat3*5+nCat4*5)
			{
				qCat4[i-(nCat0*5+nCat1*5+nCat2*5+nCat3*5)]=sentences[i];
			}
			else if(i==nCat0*5+nCat1*5+nCat2*5+nCat3*5+nCat4*5 || i<nCat0*5+nCat1*5+nCat2*5+nCat3*5+nCat4*5+nCat5*5)
			{
				qCat5[i-(nCat0*5+nCat1*5+nCat2*5+nCat3*5+nCat4*5)]=sentences[i];
			}
			else if(i==nCat0*5+nCat1*5+nCat2*5+nCat3*5+nCat4*5+nCat5*5 || i<nCat0*5+nCat1*5+nCat2*5+nCat3*5+nCat4*5+nCat5*5+nCat6*5)
			{
				qCat6[i-(nCat0*5+nCat1*5+nCat2*5+nCat3*5+nCat4*5+nCat5*5)]=sentences[i];
			}
			else if(i==nCat0*5+nCat1*5+nCat2*5+nCat3*5+nCat4*5+nCat5*5+nCat6*5 || i<nCat0*5+nCat1*5+nCat2*5+nCat3*5+nCat4*5+nCat5*5+nCat6*5+nCat7*5)
			{
				qCat7[i-(nCat0*5+nCat1*5+nCat2*5+nCat3*5+nCat4*5+nCat5*5+nCat6*5)]=sentences[i];
			}
		}

		fname = "Text/extrapos_" + suffix;
		qAsset = (TextAsset)Resources.Load(fname);
		string testoExtrapos = qAsset.text;
		extrapos = testoExtrapos.Split ("\r\n" [0]);
		
		fname = "Text/phrases_" + suffix;
		qAsset = (TextAsset)Resources.Load(fname);
		string testoPhrases = qAsset.text;
		phrases = testoPhrases.Split ("\r\n" [0]);
		//print("ple.loadSentences");
	}
	
	public void showQuestionCard(int cardType) 
	{
		//print("ple.showQuestionCard");
		WPlayerCTRL wp = null;
		if(gu.getCurrAvatar()!=null) 
			wp = gu.getCurrAvatar().GetComponent<WPlayerCTRL> ();

		if (gu.currentTurn == 1) 
		{
			wp = avatar1;
			actualCell=wp.finalDest;
			//print("Player1");
		}
		else
		{
			wp = avatar2;
			actualCell=wp.finalDest;
			//print("Player2");
		}
		
		oldWrongQ=false;

		for(int i=0; i<gu.wrongCell.Length; i++)
		{
			if(gu.wrongCell[i]!=0)
			{
				if(gu.wrongCell[i]==actualCell)
				{
					//print("found Wrong cell");
					IDcellStore=actualCell;
					nqStore=gu.wrongQ[i];
					oldWrongQ=true;
					break;
				}
			}
		}		

		if(oldWrongQ)
		{
			print("OldWrongQuestion");
			Question(cardType, false);
		}
		else
		{
			print("NewQuestion");
			Question(cardType);
		}
		
        //akg.AssignFocus(1);
	}

	/*This is the function more important and a bit difficult to understand:
    It's called in 3 cases:
    -when the player ends on a normal cell with a question
    -when the player ends on a red cell with a question that other player aswered wrong
    -when the user touch the red cell for simply read the question or for use a powercard for try to answer again and earn extra points
    So, depending on these 3 cases, it can be called in these 3 ways:
    -Question(cardType);
    -Question(cardType, false);
    -Question(cardType, false, #numberOfQuestion);
    */
    public void Question(int cardType, bool normalQ=true, int nq=-1)
	{	
        if(nq!=-1)
        {
            enoughPWC=false;
            WPlayerCTRL wp = null;
            if(gu.getCurrAvatar()!=null)
			{
				wp = gu.getCurrAvatar().GetComponent<WPlayerCTRL> ();
			}
            if (gu.currentTurn == 1) 
            {
                wp = avatar1;
                if(wp.pcOwned[cardType]>=2)
                {
                    enoughPWC=true;
                }
            }
            else
            {
                wp = avatar2;
                if(wp.pcOwned[cardType]>=2)
                {
                    enoughPWC=true;
                }
            }
        }

        view.switchTo3DView(false);
		gu.gameBoardRotation=false;
		//print("ple.NormalQuestion");
        if(nq==-1)
        {
            if(playSounds)
                sound.Play("challenge");
        }
		questionType = cardType;
		int playerColor = 0;
		if (gu.currentTurn == 1) 
		{
			playerColor = PlayerPrefs.GetInt ("p1color", 1);
		}
		else
		{
			playerColor = PlayerPrefs.GetInt ("p2color", 2);
		}
		
		selectedAnswer = 0;	

		Color clr_alpha=c.cc[cardType];
		clr_alpha.a = alphaClrLabel;

		QExa.sInnerColor=clr_alpha;
		AExa1.sInnerColor=clr_alpha;
		AExa2.sInnerColor=clr_alpha;
		AExa3.sInnerColor=clr_alpha;
		AExa4.sInnerColor=clr_alpha;

		QExa_2.sInnerColor=clr_alpha;
		AExa_2.sInnerColor=clr_alpha;

		QExa.colorInner=clr_alpha;
		AExa1.colorInner=clr_alpha;
		AExa2.colorInner=clr_alpha;
		AExa3.colorInner=clr_alpha;
		AExa4.colorInner=clr_alpha;

		QExa_2.colorInner=clr_alpha;
		AExa_2.colorInner=clr_alpha;

		//print("ct="+cardType+" color="+AExa1.colorInner);

		PWCExaObj.SetActive(false);
		PWCExaObj_2.SetActive(false);

		if (gu.currentTurn == 2 && gu.numPlayers == 1) 
		{
			QExa.sOuterColor=Color.black;
			AExa1.sOuterColor = Color.black;
			AExa2.sOuterColor = Color.black;
			AExa3.sOuterColor = Color.black;
			AExa4.sOuterColor = Color.black;

			QExa_2.sOuterColor=Color.black;
			AExa_2.sOuterColor = Color.black;

			QExa.colorOutline = Color.black;
			AExa1.colorOutline = Color.black;
			AExa2.colorOutline = Color.black;
			AExa3.colorOutline = Color.black;
			AExa4.colorOutline = Color.black;

			QExa_2.colorOutline = Color.black;
			AExa_2.colorOutline = Color.black;
		}
		else if(nq!=-1)
		{
			QExa.sOuterColor=new Color32(88,88,88,255);
			AExa1.sOuterColor = new Color32(88,88,88,255);
			AExa2.sOuterColor = new Color32(88,88,88,255);
			AExa3.sOuterColor = new Color32(88,88,88,255);
			AExa4.sOuterColor = new Color32(88,88,88,255);

			QExa_2.sOuterColor=new Color32(88,88,88,255);
			AExa_2.sOuterColor = new Color32(88,88,88,255);

			QExa.colorOutline = new Color32(88,88,88,255);
			AExa1.colorOutline = new Color32(88,88,88,255);
			AExa2.colorOutline = new Color32(88,88,88,255);
			AExa3.colorOutline = new Color32(88,88,88,255);
			AExa4.colorOutline = new Color32(88,88,88,255);

			QExa_2.colorOutline = new Color32(88,88,88,255);
			AExa_2.colorOutline = new Color32(88,88,88,255);
		}
		else
		{
			QExa.sOuterColor=Color.white;
			AExa1.sOuterColor = Color.white;
			AExa2.sOuterColor = Color.white;
			AExa3.sOuterColor = Color.white;
			AExa4.sOuterColor = Color.white;

			QExa_2.sOuterColor=Color.white;
			AExa_2.sOuterColor = Color.white;

			QExa.colorOutline = Color.white;
			AExa1.colorOutline = Color.white;
			AExa2.colorOutline = Color.white;
			AExa3.colorOutline = Color.white;
			AExa4.colorOutline = Color.white;

			QExa_2.colorOutline = Color.white;
			AExa_2.colorOutline = Color.white;
		}

		int qnum=0;
		print("SHOW cat="+cardType);
        int[] _qseq=qseq0;
        int _q=0;
        string[] _qCat=qCat0;
        int _nCat=0;

        for (int i = 0; i < nCatTot; i++)
		{
			if(i==cardType)
            {
                _qseq=(int[])qSeq[i];
                _q=q[i];
                _qCat=(string[])qCat[i];
                _nCat=nCat[i];
            }
		}
		
        if(normalQ)
        {
            qnum=_qseq[_q];
			akQuest.notAlways=false;
			print("Normal Question");
        }
        else if (nq!=-1)
        {
            qnum=nq;
			akQuest.notAlways=true;
			print("Old Question to Read");
        }
        else
        {
            qnum=nqStore;
			akQuest.notAlways=true;
			print("Old Question");
        }

        string[] couple = _qCat[qnum*5].Split ('=');
		txtQ.text = couple[1];
		txtQ_2.text = couple[1];
        for (int i = 0; i < nAnsTot; i++)
        {
            couple = _qCat[qnum*5+i+1].Split ('=');
            nTxtA[i].text = couple[1];
            nTxtA_2[i].text = couple[1];
			nTxtA[i].color = Color.white;
			nTxtA_2[i].color = Color.white;
			//print("txtA"+(i+1)+"="+couple[1]);
            if(int.Parse (couple [0].Substring (6, 1))==1)
            {
                answerWaited=i+1;
            }
        }

		rotatorQuest2.prepare=true;

		print("nDomanda=" + _q);
        _q++;
        if (_q >= _nCat)
        {
            _q=0;
        }

		q[cardType]=_q;

        switch (cardType) 
        {
            case 0:
                CatIconQ.sprite = Resources.Load<Sprite> ("Textures/Sprite_PlaySafely");
                CatTxtQ.text = getPhraseFromId("R00033");
				CatIconQ_2.sprite = Resources.Load<Sprite> ("Textures/Sprite_PlaySafely");
                CatTxtQ_2.text = getPhraseFromId("R00033");
                break;
            case 1:
                CatIconQ.sprite = Resources.Load<Sprite> ("Textures/Sprite_SocialLife");
				CatTxtQ.text = getPhraseFromId("R00034");
				CatIconQ_2.sprite = Resources.Load<Sprite> ("Textures/Sprite_SocialLife");
				CatTxtQ_2.text = getPhraseFromId("R00034");
                break;
            case 2:
                CatIconQ.sprite = Resources.Load<Sprite> ("Textures/Sprite_DontRisk");
				CatTxtQ.text = getPhraseFromId("R00035");
				CatIconQ_2.sprite = Resources.Load<Sprite> ("Textures/Sprite_DontRisk");
				CatTxtQ_2.text = getPhraseFromId("R00035");
                break;
            case 3:
				CatIconQ.sprite = Resources.Load<Sprite> ("Textures/Sprite_NoBull");
				CatTxtQ.text = getPhraseFromId("R00036");
				CatIconQ_2.sprite = Resources.Load<Sprite> ("Textures/Sprite_NoBull");
				CatTxtQ_2.text = getPhraseFromId("R00036");
                break;
            case 4:
				CatIconQ.sprite = Resources.Load<Sprite> ("Textures/Sprite_PrivacyMatters");
				CatTxtQ.text = getPhraseFromId("R00037");
				CatIconQ_2.sprite = Resources.Load<Sprite> ("Textures/Sprite_PrivacyMatters");
				CatTxtQ_2.text = getPhraseFromId("R00037");
                break;
            case 5:
				CatIconQ.sprite = Resources.Load<Sprite> ("Textures/Sprite_LetsChat");
				CatTxtQ.text = getPhraseFromId("R00038");
				CatIconQ_2.sprite = Resources.Load<Sprite> ("Textures/Sprite_LetsChat");
				CatTxtQ_2.text = getPhraseFromId("R00038");
                break;
            case 6:
				CatIconQ.sprite = Resources.Load<Sprite> ("Textures/Sprite_WatchOut");
				CatTxtQ.text = getPhraseFromId("R00039");
				CatIconQ_2.sprite = Resources.Load<Sprite> ("Textures/Sprite_WatchOut");
				CatTxtQ_2.text = getPhraseFromId("R00039");
                break;
            case 7:
				CatIconQ.sprite = Resources.Load<Sprite> ("Textures/Sprite_IsItFair");
				CatTxtQ.text = getPhraseFromId("R00040");
				CatIconQ_2.sprite = Resources.Load<Sprite> ("Textures/Sprite_IsItFair");
				CatTxtQ_2.text = getPhraseFromId("R00040");
                break;
        }

		currentAttempt = 1;
		
		btnA1.enabled=true;
		btnA2.enabled=true;
		btnA3.enabled=true;
		btnA4.enabled=true;

		btnA_2.enabled=true;
        
        if(normalQ)
        {
		    numQuestion=qnum;
        }
        else if(nq!=-1)
        {
            //enough PWC?
            PWCExaObj.SetActive(true);
            PWCExaObj_2.SetActive(true);
            if(enoughPWC)
            {
                PWCExa.colorOutline = Color.white;
                PWCExa_2.colorOutline = Color.white;
                ExaPowerBehaviour.connectByID=cardType;
            }
            else
            {
                PWCExa.colorOutline = Color.grey;
                PWCExa_2.colorOutline = Color.grey;
            }

            catStore=cardType;
            nqStore=nq;
            dice.Move.back=true;
            deviantClick=true;
        }
        else
        {
            catStore=cardType;
        }
        
		card.SetActive(true);
		QuestionMovement.resizeTxt=true;
		QuestionMovement.backIn=true;
		//print("question number=" + qnum);
		//akg.AssignFocus(1);
	}

	//Make the old Question Active
	public void showQtoReTry() 
	{
		if(playSounds)
				sound.Play("challenge");

		PWCExaObj.SetActive(false);
		PWCExaObj_2.SetActive(false);

		QExa.colorOutline = Color.white;
		AExa1.colorOutline = Color.white;
		AExa2.colorOutline = Color.white;
		AExa3.colorOutline = Color.white;
		AExa4.colorOutline = Color.white;

		QExa_2.colorOutline = Color.white;
		AExa_2.colorOutline = Color.white;

		currentAttempt = 1;

		btnA1.enabled=true;
		btnA2.enabled=true;
		btnA3.enabled=true;
		btnA4.enabled=true;
		
		btnA_2.enabled=true;
	}

	public void SelectAnswer(int which)
	 {
		if(playSounds)
			sound.Play("click");
		
		if(deviantClick)
		{
			Alarm.on=true;
		}
		else
		{
			selectedAnswer = which;
			print("selectedAnswer="+selectedAnswer+ "currentAttempt="+currentAttempt);
			ShowAnswerResultDefinitive();
		}
	}
	
	public void ShowAnswerResultDefinitive() 
	{
		print("ShowAnswerResultDefinitive");

		if (selectedAnswer == answerWaited) 
		{
			//Suono
			if(playSounds) 
			{
				sound.Play("PosAns");
			}

			if(currentAttempt==1) //1
			{
				//print ("right at first attempt");
				
				//Change Camera
				if(!enoughPWC)
				{
					view.switchTo3DPlusView();
				}
				//Go to Question
				QuestionMovement.resetTxt=true;
				QuestionMovement.moveAway = true;
				//Vs Computer
				if(computerTurn)
				{
					SettingScore.toAdd=300;

					int oppRes=oppResult();
					//print("reaction of wp="+oppRes);

					WinModule.who="opp_"+oppRes;
					WinModule.prepare=true;
					if(oldWrongQ)
					{
						gu.putBack(IDcellStore);
						//print("catStore"+catStore);
						gu.wrongGenCat[catStore]--;

						for(int i=0; i<gu.wrongCell.Length; i++)
						{
							if(gu.wrongCell[i]==IDcellStore)
							{
								gu.wrongCell[i]=0;
								gu.wrongQ[i]=0;
							}
						}

						//Reset cell		
						Click4Question.disactivateByID=IDcellStore;
					}
				}
				else
				{
					WPlayerCTRL wp = null;
					if (gu.currentTurn == 1) 
					{
						wp = avatar1;
						fx1.ar=true;
					}
					if (gu.currentTurn == 2) 
					{
						wp = avatar2;
						fx2.ar=true;
					}
					//OldWrongQ
					if(enoughPWC || oldWrongQ)
					{
						//Card earned with PWC
						gu.putBack(IDcellStore);

						//Reset PWC of utilities
						//print("catStore"+catStore);
						gu.wrongGenCat[catStore]--;

						for(int i=0; i<gu.wrongCell.Length; i++)
						{
							if(gu.wrongCell[i]==IDcellStore)
							{
								gu.wrongCell[i]=0;
								gu.wrongQ[i]=0;
							}
						}

						//Reset cell		
						Click4Question.disactivateByID=IDcellStore;
						if(enoughPWC)
						{
							//Reset PWC of Player
							wp.pcOwned[catStore]=0;
						}
					}

					if(!enoughPWC)
					{
						wp.pcOwned[questionType]++;
						if(wp.pcOwned[questionType]==2) //If PowerCards are 2
						{
							//print("2PWC");
							sc.showWinPowercard(questionType, 2);
						}
						else if(wp.pcOwned[questionType]==1) //If PowerCard is 1
						{
							//print("1PWC");
							sc.showWinPowercard(questionType, 1);
						}
						else
						{
							//print("changeTurn");
							showTurnCard();
						}
					}
					SettingScore.toAdd=firstAttemptPoints;
				}
				SettingScore.add=true;
			}
			else if(currentAttempt==2)//2
			{
				//print ("right at second attempt");
				//Change Camera
				if(!enoughPWC)
				{
					view.switchTo3DPlusView();
				}
				//Go to Question
				QuestionMovement.resetTxt=true;
				QuestionMovement.moveAway = true;
				//Vs Computer
				if(computerTurn)
				{
					SettingScore.toAdd=200;
					
					int oppRes=oppResult();
					//print("reaction of wp="+oppRes);

					WinModule.who="opp_"+oppRes;
					WinModule.prepare=true;

					if(oldWrongQ)
					{
						gu.putBack(IDcellStore);
						//print("catStore"+catStore);
						gu.wrongGenCat[catStore]--;

						for(int i=0; i<gu.wrongCell.Length; i++)
						{
							if(gu.wrongCell[i]==IDcellStore)
							{
								gu.wrongCell[i]=0;
								gu.wrongQ[i]=0;
							}
						}

						//Reset cell		
						Click4Question.disactivateByID=IDcellStore;
					}
				}
				else
				{
					WPlayerCTRL wp = null;
					if (gu.currentTurn == 1) 
					{
						wp = avatar1;
						fx1.ar=true;
					}
					if (gu.currentTurn == 2) 
					{
						wp = avatar2;
						fx2.ar=true;
					}
					//OldWrongQ
					if(enoughPWC || oldWrongQ)
					{
						//Card earned with PWC
						gu.putBack(IDcellStore);

						//Reset PWC of utilities
						//print("catStore"+catStore);
						gu.wrongGenCat[catStore]--;

						for(int i=0; i<gu.wrongCell.Length; i++)
						{
							if(gu.wrongCell[i]==IDcellStore)
							{
								gu.wrongCell[i]=0;
								gu.wrongQ[i]=0;
							}
						}

						//Reset cell		
						Click4Question.disactivateByID=IDcellStore;
						if(enoughPWC)
						{
							//Reset PWC of Player
							wp.pcOwned[catStore]=0;
							akg.AssignFocus(0);
						}
					}

					if(!enoughPWC)
					{
						wp.pcOwned[questionType]++;
						if(wp.pcOwned[questionType]==2) //If PowerCards are 2
						{
							//print("2PWC");
							sc.showWinPowercard(questionType, 2);
						}
						else if(wp.pcOwned[questionType]==1) //If PowerCard is 1
						{
							//print("1PWC");
							sc.showWinPowercard(questionType, 1);
						}
						else
						{
							//print("changeTurn");
							showTurnCard();
						}
					}
					SettingScore.toAdd=secondAttemptPoints;
				}
				SettingScore.add=true;
			}
			return;
		}
		//If wrong add another attempt
		++currentAttempt;

		if(playSounds)
		{
			sound.Play("NegAns");
		}

		if(currentAttempt==2)
		{
			switch(selectedAnswer)
			{
				case 1:
					AExa1.colorOutline = new Color32(200, 0, 0, 255);
					AExa1.colorInner = new Color32(115, 115, 115, 255);
					AExa_2.colorOutline = new Color32(200, 0, 0, 255);
					AExa_2.colorInner = new Color32(115, 115, 115, 255);
					txtA1.color = new Color32(60, 60, 60, 255);
					txtA1_2.color = new Color32(60, 60, 60, 255);
					rotatorQuest2.firstWrongAttempt=1;
					point1.wrong=true;
					break;
				case 2:
					AExa2.colorOutline = new Color32(200, 0, 0, 255);
					AExa2.colorInner = new Color32(115, 115, 115, 255);
					AExa_2.colorOutline = new Color32(200, 0, 0, 255);
					AExa_2.colorInner = new Color32(115, 115, 115, 255);
					txtA2.color = new Color32(60, 60, 60, 255);
					txtA1_2.color = new Color32(60, 60, 60, 255);
					rotatorQuest2.firstWrongAttempt=2;
					point2.wrong=true;
					break;
				case 3:
					AExa3.colorOutline = new Color32(200, 0, 0, 255);
					AExa3.colorInner = new Color32(115, 115, 115, 255);
					AExa_2.colorOutline = new Color32(200, 0, 0, 255);
					AExa_2.colorInner = new Color32(115, 115, 115, 255);
					txtA3.color = new Color32(60, 60, 60, 255);
					txtA1_2.color = new Color32(60, 60, 60, 255);
					rotatorQuest2.firstWrongAttempt=3;
					point3.wrong=true;
					break;
				case 4:
					AExa4.colorOutline = new Color32(200, 0, 0, 255);
					AExa4.colorInner = new Color32(115, 115, 115, 255);
					AExa_2.colorOutline = new Color32(200, 0, 0, 255);
					AExa_2.colorInner = new Color32(115, 115, 115, 255);
					txtA4.color = new Color32(60, 60, 60, 255);
					txtA1_2.color = new Color32(60, 60, 60, 255);
					rotatorQuest2.firstWrongAttempt=4;
					point4.wrong=true;
					break;
			}
		}	
		else if (currentAttempt > 2)
		{
			switch(selectedAnswer)
			{
				case 1:
					AExa1.colorOutline = new Color32(200, 0, 0, 255);
					AExa1.colorInner = new Color32(115, 115, 115, 255);
					AExa_2.colorOutline = new Color32(200, 0, 0, 255);
					AExa_2.colorInner = new Color32(115, 115, 115, 255);
					txtA1.color = new Color32(60, 60, 60, 255);
					txtA1_2.color = new Color32(60, 60, 60, 255);
					point1.wrong=true;
					break;
				case 2:
					AExa2.colorOutline = new Color32(200, 0, 0, 255);
					AExa2.colorInner = new Color32(115, 115, 115, 255);
					AExa_2.colorOutline = new Color32(200, 0, 0, 255);
					AExa_2.colorInner = new Color32(115, 115, 115, 255);
					txtA2.color = new Color32(60, 60, 60, 255);
					txtA1_2.color = new Color32(60, 60, 60, 255);
					point2.wrong=true;
					break;
				case 3:
					AExa3.colorOutline = new Color32(200, 0, 0, 255);
					AExa3.colorInner = new Color32(115, 115, 115, 255);
					AExa_2.colorOutline = new Color32(200, 0, 0, 255);
					AExa_2.colorInner = new Color32(115, 115, 115, 255);
					txtA3.color = new Color32(60, 60, 60, 255);
					txtA1_2.color = new Color32(60, 60, 60, 255);
					point3.wrong=true;
					break;
				case 4:
					AExa4.colorOutline = new Color32(200, 0, 0, 255);
					AExa4.colorInner = new Color32(115, 115, 115, 255);
					AExa_2.colorOutline = new Color32(200, 0, 0, 255);
					AExa_2.colorInner = new Color32(115, 115, 115, 255);
					txtA4.color = new Color32(60, 60, 60, 255);
					txtA1_2.color = new Color32(60, 60, 60, 255);
					point4.wrong=true;
					break;
			}
			QuestionMovement.resetTxt=true;
			QuestionMovement.moveAway = true;

			if(computerTurn)
			{
				int oppRes=oppResult();
				//print("reaction of wp="+oppRes);
				if(oppRes==4)
				{
					if(!oldWrongQ) 
					{
						gu.wrongGenCat[questionType]++;
						gu.wrongCell[wrongAnswers]=actualCell;
						gu.wrongQ[wrongAnswers]=numQuestion;

						Click4Question.passCat=questionType;
						Click4Question.passNQ=numQuestion;
						Click4Question.activateByID=actualCell;

						wrongAnswers++;
						gu.putAside();
						fx2.aw=true;
					}
					else
					{
						//print("changeTurn because Computer VS oldQuestion");
						showTurnCard();
					}
				}
				else
				{
					WinModule.who="opp_"+oppRes;
					WinModule.prepare=true;
				}
			}
			else
			{
				if(!enoughPWC && !oldWrongQ)//NormalQuestion
				{
					view.switchTo3DPlusView();
					//print("putAside");
					gu.wrongGenCat[questionType]++;
					gu.wrongCell[wrongAnswers]=actualCell;
					gu.wrongQ[wrongAnswers]=numQuestion;

					Click4Question.passCat=questionType;
					Click4Question.passNQ=numQuestion;
					Click4Question.activateByID=actualCell;

					wrongAnswers++;
					gu.putAside();

					if (gu.currentTurn == 2) 
					{
						fx2.aw=true;
					}
					else
					{
						fx1.aw=true;
					}
				}
				else if(enoughPWC)
				{
					WPlayerCTRL wp = null;
					if (gu.currentTurn == 1) 
					{
						wp = avatar1;
					}
					if (gu.currentTurn == 2) 
					{
						wp = avatar2;
					}
					//Reset PWC of Player
					wp.pcOwned[catStore]=0;
					dice.Move.move=true;
					enoughPWC=false;
				}
				else
				{
					//print("changeTurn because no alternative");
					showTurnCard();
				}
			}
		}
	}

	//This function set the result of game of Computer player
	int oppResult()
    {
		if (refill)
        {
            allRes = new int[]{4,4,4,2,2,1};
            refill = false;
        }
        else
        {
            allRes = allResPass;
        }

		int nRes=allRes.Length-1;
        
        int max = Mathf.Max(allRes.Length-1);
        int min = Mathf.Min(0);

		int rRes = Random.Range(min, max);
        
        if (allRes.Length - 1 > 0)
        {
            allResPass = new int[nRes];

			bool jumped = false;
            
            for (int i = 0; i < allRes.Length; i++)
            {
                if (i != rRes)
                {
                    if (jumped)
                    {
                        allResPass[i-1] = allRes[i];
                    }
                    else
                    {
                        allResPass[i] = allRes[i];
                    }
                }
                else
                {
                    jumped = true;
                }
            }
        }
        else
        {
            refill = true;
        }

        return allRes[rRes];
    }

	//void forcedMove() {
	//	gu.forcedMove(_forcedMove);
	//}

	public void showTurnCard() 
	{
		print("core.showTurnCard");
		btnVis = true;
		if(gu.numPlayers == 1 && !firstTime)//Single Player
		{
			//When starship arrive to finish
			if(gu.currentTurn == 1)//Player1 Turn
			{
				if(avatar2.finalDest>32)
				{
					dice.ActiveBtn(true, true);
				}
			}
			else//Pc Turn
			{
				if(avatar1.finalDest>32)
				{
					view.switchTo2DView();
					int pcGame = Random.Range (1, 7);
					gu.playTurn(pcGame);
					return;
				}
			}

			gu.changeTurnStep2();

			if(gu.currentTurn == 1)//Player1 Turn
			{
				view.switchTo3DView();
				ViewBtnMovement.back=true;
				WinModule.cPlayer=PlayerPrefs.GetInt("p1color");
				print("here");
				if(computerTurn)
				{
					print("and here");
					computerTurn=false;
					
					//dice.ActiveBtn(true);
					dice.Move.move=false;
					dice.Move.ok=true;
					ExaPowerManager.waveOn=true;
				}
			}
			else//Pc Turn
			{
				computerTurn=true;
				if(firstTimePC)
				{
					view.switchTo3DView();
					changeTurnN.phase4=true;
					dice.Move.ok=false;
					firstTimePC=false;
				}
				else
				{
					view.switchTo2DView();
					int pcGame = Random.Range (1, 7);
					gu.playTurn(pcGame);
				}
				ViewBtnMovement.move=true;
			}
		}
		else//Double Player
		{
			//print("showTurnCard avName="+gu.getCurrAvatar().transform.parent.name);
			int avatarCurPlace;
			if(gu.getCurrAvatar().transform.parent.name=="ContAvatar1")
			{
				avatarCurPlace=avatar2.finalDest;
				print("player2");
			}
			else
			{
				avatarCurPlace=avatar1.finalDest;
				print("player1");
			}

			if(avatarCurPlace>32)
			{
				dice.ActiveBtn(true, true);
			}
			else
			{
				if(gu.currentTurn == 1) 
				{
					WinModule.cPlayer=PlayerPrefs.GetInt("p2color");
				}
				else
				{
					WinModule.cPlayer=PlayerPrefs.GetInt("p1color");
					if(firstTime)
					{
						firstTime=false;
					}
				}

				changeTurnN.phase1=true;
				SettingScore.hideAS=true;
				SettingScore2.hideAS=true;

				Invoke ("hideTurnCard", 3.0f);
			}
		}

		akg.AssignFocus(0);
	}

	public void hideTurnCardOnClick() 
	{
		print("Should appear here and not after...");
		dice.ActiveBtn(true);
		btnVis = false;
		ExaPowerManager.waveOn=true;
		WinModule_Btn.able=false;
		WinModule.continu=true;
	}

	void hideTurnCard() 
	{print("Should..");
		if(btnVis)
		{
			dice.ActiveBtn(true);
			print("..appear here");
			btnVis = false;
			changeTurnN.phase3=true;
            ExaPowerManager.waveOn=true;
			WinModule_Btn.able=false;
        	WinModule.continu=true;
		}
	}
	
	public void BackToMenu()
	{
		endScore.Exit();
	}
}