﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SettingScore : MonoBehaviour
{
    public GenericUtils gu;

    public WPlayerCTRL wp1;
    public WPlayerCTRL wp2;

    private int score;
    private int displayScore;
    private int actScoreN;
    public Text actScoreT;
    private RectTransform addScoreR;
    public Text addScoreT;

    public static bool add;
    public static bool sub;
    public static bool showAS;
    public static bool show;
    public static bool show2;
    public static bool hideAS;
    public static bool hide;
    public static int toAdd;
    public static int toSub;

    private float t;
    private float _time;  
    private float _time2;  
    public AnimationCurve curve;

    private int step;

    public Sound sound;


    void Start()
    {
        wp1=gu.avatar1.GetComponent<WPlayerCTRL>();
		wp2=gu.avatar2.GetComponent<WPlayerCTRL>();
        
        t=0;
        _time=1.5f;
        _time2=0.5f;

        addScoreR=addScoreT.transform.GetComponent<RectTransform>();
        addScoreR.sizeDelta=new Vector2(0, addScoreR.sizeDelta.y);
        toAdd=1500;
    }

    void Update()
    {
        if(add)
        {
            if (gu.currentTurn == 1 || CoreEvents.computerTurn) 
            {
                score=wp1.currentScore;
                score+=toAdd;
                wp1.currentScore=score;
            }
            else 
            {
                score=wp2.currentScore;
                score+=toAdd;
                print(score);
                wp2.currentScore=score;
            }
            step=toAdd/10;
            addScoreT.text="+" + toAdd.ToString();
            _time=1.5f;
            show=true;
            add=false;
        }

        if(sub)
        {
            if (gu.currentTurn == 1) 
            {
                score=wp1.currentScore;
                score-=toSub;
                wp1.currentScore=score;
            }
            else 
            {
                score=wp2.currentScore;
                score-=toSub;
                wp2.currentScore=score;
            }
            step=-toSub/10;
            addScoreT.text="-" + toSub.ToString();
            _time=0.1f;
            show=true;
            sub=false;
        }

        if(show)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            addScoreR.sizeDelta=new Vector2(Mathf.Lerp(0, 500, curve.Evaluate(s)), addScoreR.sizeDelta.y);
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                StartCoroutine(ScoreShower());
                
                hide=true;
                sound.Play("points");
                t = 0;
                show=false;
            }
        }

        if(hide)
        {
            t += Time.deltaTime;
            float s = t / _time2;
            
            addScoreR.sizeDelta=new Vector2(Mathf.Lerp(180, 0, curve.Evaluate(s)), addScoreR.sizeDelta.y);
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                t = 0;
                hide=false;
            }
        }

        if(showAS)
        {
            if (gu.currentTurn == 1) 
            {
                score=wp1.currentScore;
            }
            else 
            {
                score=wp2.currentScore;
            }
            displayScore=0;
            if(score!=0)
            {
                step=score/20;
                StartCoroutine(ScoreShower());
            }
            showAS=false;
        }

        if(hideAS)
        {
            if (gu.currentTurn == 1) 
            {
                score=wp1.currentScore;
            }
            else 
            {
                score=wp2.currentScore;
            }

            if(score!=0)
            {
                step=displayScore/20;
                StartCoroutine(ScoreHider());
            }
            hideAS=false;
        }
    }

    private IEnumerator ScoreShower()
    {
        while(true)
        {
            if(displayScore != score)
            {
                displayScore +=step;
                actScoreT.text = displayScore.ToString();
            }
            else
            {
                StopAllCoroutines();
            }
            yield return new WaitForSeconds(0.001f);
        }
    }

    private IEnumerator ScoreHider()
    {
        while(true)
        {
            if(displayScore != 0)
            {
                displayScore -=step;
                actScoreT.text = displayScore.ToString();
            }
            else
            {
                StopAllCoroutines();
            }
            yield return new WaitForSeconds(0.001f);
        }
    }
}
