﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

using System.Collections;
using UnityEngine;

public class ExaPowerManager : MonoBehaviour
{
    public CoreEvents ce;
    public Dice dice;
    public static bool waveOn;
    public static bool waveOnly;
    private bool waveOff0;
    public bool waveOff3;
    public static bool waveOffAll;

    public float t;

    public int child;

    void Start()
    {
        child=transform.childCount;
    }

    IEnumerator Hide0()
    {
        for(int i=0; i<child; i++)
        {
            ExaPowerBehaviour.waveOff0Id=i;
            yield return new WaitForSeconds(t);
        }
        ExaPowerBehaviour.waveOff0Id=-1;
    }

    IEnumerator Hide3()
    {
        for(int i=0; i<child; i++)
        {
            ExaPowerBehaviour.waveOff3Id=i;
            yield return new WaitForSeconds(t);
        }
        ExaPowerBehaviour.waveOff3Id=-1;
    }

    IEnumerator HideAll()
    {
        yield return new WaitForSeconds(0.5f);
        for(int i=0; i<child; i++)
        {
            ExaPowerBehaviour.waveOffAll=i;
            yield return new WaitForSeconds(t);
        }
        ExaPowerBehaviour.waveOffAll=-1;
        //Qui lancia il changeTurn
        ce.showTurnCard();
    }
    
    IEnumerator Show()
    {
        for(int i=0; i<child; i++)
        {
            ExaPowerBehaviour.waveOnId=i;
            yield return new WaitForSeconds(t);
        }
        ExaPowerBehaviour.waveOnId=-1;
        yield return new WaitForSeconds(0.2f);

        if(!CoreEvents.deviantClick)
        {
            if(!waveOnly)
            {
                dice.Move.move=true;
            }
            else
            {
                waveOnly=false;
            }
        }

        waveOff0=true;
    }

    void Update()
    {
        if (waveOn)
        {
            StartCoroutine(Show());
            waveOn = false;
        }

        if (waveOff0)
        {
            StartCoroutine(Hide0());
            waveOff0 = false;
        }

        if (waveOff3)
        {
            StartCoroutine(Hide3());
            waveOff3 = false;
        }

        if (waveOffAll)
        {
            StartCoroutine(HideAll());
            waveOffAll = false;
        }
    }
}
