﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

using UnityEngine;
using UnityEngine.UI;

public class ViewBtnMovement : MonoBehaviour
{
    Transform ViewBTN;

    private Vector3 _fromVB;
    private Vector3 _toVB;
    
    private float t;
    private float duration;

    public static bool move;//va via
    public static bool back;//arriva
    
    public AnimationCurve curve;
    void Start()
    {
        ViewBTN=transform;
        _fromVB = ViewBTN.localPosition;
        _toVB = new Vector3(Screen.width+135*2, 0f, 0f);
        t = 0;
        duration = 0.5f;
    }

    void Update()
    {
        if (move)
        {
            t += Time.deltaTime;
            float s = t / duration;
            
            ViewBTN.localPosition=Vector3.Lerp(_toVB, _fromVB, curve.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                move = false;
                t = 0;
            }
        }

        if (back)
        {
            t += Time.deltaTime;
            float s = t / duration;

            ViewBTN.localPosition=Vector3.Lerp(_fromVB, _toVB, curve.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                print("spostato a "+ViewBTN.localPosition);
                back = false;
                t = 0;
            }
        }
    }
}
