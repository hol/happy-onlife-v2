/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhichQuestion : MonoBehaviour
{
    public bool quest1;
    public GameObject question1;
    public bool quest2;
    public GameObject question2;

    void Start() 
    {
        Active1();
    }
    
    void Active1()
    {
        question2.SetActive(false);
        question1.SetActive(true);
    }
    void Active2()
    {
        question1.SetActive(false);
        question2.SetActive(true);
    }

    void Update()
    {
        if(quest1)
        {
            Active1();
            quest1=false;
        }

        if(quest2)
        {
            Active2();
            quest2=false;
        }
        
    }
}
