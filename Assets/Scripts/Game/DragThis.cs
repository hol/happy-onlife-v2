﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragThis : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler 
{
    Vector3 startPos;
    Vector3 releasePos;
    private bool moveback;

    private bool check;

    void Start()
    {
        check = false;
    }

    #region IBeginDragHandler implementation

    public void OnBeginDrag (PointerEventData eventData)
    {
        startPos = transform.position;
    }

    #endregion

    #region IDragHandler implementation

    public void OnDrag (PointerEventData eventData)
    {
        transform.position =
            Camera.main.ScreenToWorldPoint(new Vector3(eventData.position.x, eventData.position.y, 30));
    }

    #endregion

    #region IEndDragHandler implementation

    public void OnEndDrag (PointerEventData eventData)
    {
        releasePos = transform.position;
        check = true;
    }

    #endregion

    void Update()
    {
        if (check)
        {
            //CheckinCat.CheckThis(transform, startPos,releasePos);
            check = false;
        }
    }
}