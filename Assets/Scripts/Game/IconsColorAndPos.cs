/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class IconsColorAndPos : MonoBehaviour
{
    public CoreEvents core;
    public Colors c;
    public GenericUtils gu;
    public ChangeTurnNew ctn;

	public GameObject[] Icon1;
	public GameObject[] Icon2;
	//public GameObject IconNPG;

	public Material mat1;
	public Material mat2;

    private GameObject IconPlayer3D1;
	private GameObject IconPlayer3D2;
	private GameObject IconPlayerBackup;

	private int type1;
	private int type2;

	private int type1store;
	private int type2store;

	private int numPlayers;
	public int enemyClrNum;

	bool firstTime=true;

    void Start()
    {
		type1=gu.type1;
		type2=gu.type2;
		numPlayers=gu.numPlayers;
		type1store=type1;
		type2store=type2;
		print("Icons:num Giocatori="+numPlayers+", type1="+type1+" e type2="+type2);
    }

	public void SetPlayers(GameObject p1, GameObject p2)
	{
		IconPlayer3D1=p1;
		IconPlayer3D2=p2;
		print("Assign Icons:IconPlayer3D1="+IconPlayer3D1.name+" e IconPlayer3D2="+IconPlayer3D2.name);
	}

    public void v2D()
    {
		IconPlayer3D1.transform.parent.gameObject.transform.localPosition=new Vector3(130,-79,20);
		IconPlayer3D1.transform.parent.gameObject.transform.eulerAngles=new Vector3(36f,50f,12f);
		IconPlayer3D2.transform.parent.gameObject.transform.localPosition=new Vector3(-94,-49,53);
		IconPlayer3D2.transform.parent.gameObject.transform.eulerAngles=new Vector3(-11.9f, 363.1f, 19.7f);
    }

    public void v3D()
    {
		IconPlayer3D1.transform.parent.gameObject.transform.localPosition=new Vector3(55,-35,5);
		IconPlayer3D1.transform.parent.gameObject.transform.eulerAngles=new Vector3(27.9f,115.7f,15.9f);
		IconPlayer3D2.transform.parent.gameObject.transform.localPosition=new Vector3(-13,-35,63);
		IconPlayer3D2.transform.parent.gameObject.transform.eulerAngles=new Vector3(18.6f, 1.4f, 33.1f);
    }

    public void v3DPlus()
    {
		IconPlayer3D1.transform.parent.gameObject.transform.localPosition=new Vector3(84,-50,10);
		IconPlayer3D1.transform.parent.gameObject.transform.eulerAngles=new Vector3(27.9f,115.7f,15.9f);
		IconPlayer3D2.transform.parent.gameObject.transform.localPosition=new Vector3(7,0,19);
		IconPlayer3D2.transform.parent.gameObject.transform.eulerAngles=new Vector3(18.6f, 1.4f, 33.1f);
    }

    // Update UI Render with the current player data
	public void updateControlPanel() 
	{
		if(firstTime)
		{
			Start();
			firstTime=false;
		}

		if(gu.getCurrAvatar()!=null)
		{
			
		}

		if(numPlayers==1) //Color of Enemy
		{
			PlayerPrefs.SetInt("p2color", enemyClrNum);
		}

		int pClr = PlayerPrefs.GetInt("p1color");
		int pClr2 = PlayerPrefs.GetInt("p2color");
		
		if (numPlayers == 1) 
		{
			SetPlayers(Icon1[type1], Icon2[type2]);
			mat1=gu.wpMat;
		}
		else if (gu.currentTurn == 1) 
		{
			SetPlayers(Icon1[type1], Icon2[type2]);
			mat1=gu.wpMat;
			mat2=gu.wpMat2;
			type1=type1store;
			type2=type2store;
			print("Colori coerenti");
		}
		else
		{
			SetPlayers(Icon1[type2], Icon2[type1]);
			mat1=gu.wpMat2;
			mat2=gu.wpMat;
			type1=type2store;
			type2=type1store;
			print("Colori invertiti");
		}
		
		//ColorOut
		if(type1==0)
		{
			print("IconPlayer3D1="+IconPlayer3D1.transform.GetChild(0).transform.GetChild(0).transform.name);
			print("IconPlayer3D1="+IconPlayer3D1.transform.GetChild(0).transform.GetChild(1).transform.name);
			IconPlayer3D1.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().material.CopyPropertiesFromMaterial(mat1);
			IconPlayer3D1.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Renderer>().material.CopyPropertiesFromMaterial(mat1);
		}
		else if(type1==1)
		{
			print("IconPlayer3D1="+IconPlayer3D1.transform.GetChild(0).transform.GetChild(0).transform.name);
			IconPlayer3D1.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].CopyPropertiesFromMaterial(mat1);
		}
		else if(type1==2)
		{
			print("IconPlayer3D1="+IconPlayer3D1.transform.GetChild(0).transform.GetChild(0).transform.name);
			IconPlayer3D1.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].CopyPropertiesFromMaterial(mat1);
		}
		
		if(type2==0 && pClr2 != enemyClrNum)
		{
			print("IconPlayer3D2="+IconPlayer3D2.transform.GetChild(0).transform.GetChild(0).transform.name);
			print("IconPlayer3D2="+IconPlayer3D2.transform.GetChild(0).transform.GetChild(1).transform.name);
			IconPlayer3D2.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().material.CopyPropertiesFromMaterial(mat2);
			IconPlayer3D2.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Renderer>().material.CopyPropertiesFromMaterial(mat2);
		}
		else if(type2==1)
		{
			print("IconPlayer3D2="+IconPlayer3D2.transform.GetChild(0).transform.GetChild(0).transform.name);
			IconPlayer3D2.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].CopyPropertiesFromMaterial(mat2);
		}
		else if(type2==2)
		{
			print("IconPlayer3D2="+IconPlayer3D2.transform.GetChild(0).transform.GetChild(0).transform.name);
			IconPlayer3D2.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].CopyPropertiesFromMaterial(mat2);
		}
	}
}
