﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class TransformPWC : MonoBehaviour
{
    public int ID;
    public Exa2Dnew Exa;
    public GameObject Icon;
    public Transform TxtPWC;
    //PowerCard Compiler
    public Image iconImg;
	public Text titleCardTxt;
	public Text content1Txt;
	public Text content2Txt;
	public Text content3Txt;

    Transform meT;
    RectTransform meRT;
    Button meBTN;
    public bool goForIt;
    bool active;
    public bool hideIt;
    bool hide;
    
    Vector2 _fRT;
    Vector2 _tRT;
    Vector3 _fT;
    Vector3 _tT;
    Vector3 _tT2;
    float _fWidth;
    float _tWidth;
    float _fHeight;
    float _tHeight;
    float _fLine;
    float _tLine;
    Vector3 _fTxt;
    Vector3 _tTxt;

    Vector3 _origTxt;

    private float t;
    private float _time;    
    public AnimationCurve curve;

    void Start()
    {
        goForIt=false;
        active=false;
        hideIt=false;
        hide=false;
        meT=transform;
        meRT=transform.GetComponent<RectTransform>();
        meBTN=transform.GetComponent<Button>();
        _fWidth=Exa.width;
        _tWidth=1250;
        _fHeight=Exa.width;
        _tHeight=580;
        _fLine=Exa.lineThickness;
        _tLine=12;
        _fRT=new Vector2(_fWidth, _fHeight);
        _tRT=new Vector2(_tWidth, _tHeight);
        _fTxt=TxtPWC.position;
        _tTxt=new Vector3(0,-30,0);
        t=0;
        _time=0.2f;
    }

    void Update()
    {
        if(goForIt)
        {
            t = 0;
            Exa.edit=true;
            _fT=meT.transform.localPosition;
            _tT=new Vector3(_fT.x,0,_fT.z);
            _tT2=new Vector3(-Screen.width,_tT.y,_fT.z);
            
            //Textify
            TextAsset qAsset = null;
            string suffix = PlayerPrefs.GetString ("linguaSuffix");
            string fname = "Text/power_" + suffix;
            qAsset = (TextAsset)Resources.Load(fname);
            string testoIntero = qAsset.text;
            string[] strgs = testoIntero.Split ("\r\n" [0]);
            string[] strg;     
            //pass content of that specific PWC       
            switch(ID){
                case 1 :
                    iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_PlaySafely");
                    strg = strgs [0].Split ('=');
                    titleCardTxt.text = strg[1];
                    strg = strgs [1].Split ('=');
                    content1Txt.text = strg[1];
                    strg = strgs [2].Split ('=');
                    content2Txt.text = strg[1];
                    strg = strgs [3].Split ('=');
                    content3Txt.text = strg[1];
                    break;
                case 2 :
                    iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_SocialLife");
                    strg = strgs [4].Split ('=');
                    titleCardTxt.text = strg[1];
                    strg = strgs [5].Split ('=');
                    content1Txt.text = strg[1];
                    strg = strgs [6].Split ('=');
                    content2Txt.text = strg[1];
                    strg = strgs [7].Split ('=');
                    content3Txt.text = strg[1];
                    break;
                case 3 :
                    iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_DontRisk");
                    strg = strgs [8].Split ('=');
                    titleCardTxt.text = strg[1];
                    strg = strgs [9].Split ('=');
                    content1Txt.text = strg[1];
                    strg = strgs [10].Split ('=');
                    content2Txt.text = strg[1];
                    strg = strgs [11].Split ('=');
                    content3Txt.text = strg[1];
                    break;
                case 4 :
                    iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_noBull");
                    strg = strgs [12].Split ('=');
                    titleCardTxt.resizeTextForBestFit=false;
                    titleCardTxt.text = strg[1];
                    strg = strgs [13].Split ('=');
                    content1Txt.resizeTextForBestFit=false;
                    content1Txt.text = strg[1];
                    strg = strgs [14].Split ('=');
                    content2Txt.text = strg[1];
                    strg = strgs [15].Split ('=');
                    content3Txt.text = strg[1];
                    break;
                case 5 :
                    iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_PrivacyMatters");
                    strg = strgs [16].Split ('=');
                    titleCardTxt.text = strg[1];
                    strg = strgs [17].Split ('=');
                    content1Txt.text = strg[1];
                    strg = strgs [18].Split ('=');
                    content2Txt.text = strg[1];
                    strg = strgs [19].Split ('=');
                    content3Txt.text = strg[1];
                    break;
                case 6 :
                    iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_LetsChat");
                    strg = strgs [20].Split ('=');
                    titleCardTxt.text = strg[1];
                    strg = strgs [21].Split ('=');
                    content1Txt.text = strg[1];
                    strg = strgs [22].Split ('=');
                    content2Txt.text = strg[1];
                    strg = strgs [23].Split ('=');
                    content3Txt.text = strg[1];
                    break;
                case 7 :
                    iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_WatchOut");
                    strg = strgs [24].Split ('=');
                    titleCardTxt.text = strg[1];
                    strg = strgs [25].Split ('=');
                    content1Txt.text = strg[1];
                    strg = strgs [26].Split ('=');
                    content2Txt.text = strg[1];
                    strg = strgs [27].Split ('=');
                    content3Txt.text = strg[1];
                    break;
                case 8 :
                    iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_IsItFair");
                    strg = strgs [28].Split ('=');
                    titleCardTxt.resizeTextForBestFit=false;
                    titleCardTxt.text = strg[1];
                    strg = strgs [29].Split ('=');
                    content1Txt.resizeTextForBestFit=false;
                    content1Txt.text = strg[1];
                    strg = strgs [30].Split ('=');
                    content2Txt.text = strg[1];
                    strg = strgs [31].Split ('=');
                    content3Txt.text = strg[1];
                    break;
            }
            active=true;
            goForIt=false;
        }

        if(active)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            meRT.sizeDelta=Vector2.Lerp(_fRT, _tRT, curve.Evaluate(s));
            meT.localPosition=Vector3.Lerp(_fT, _tT, curve.Evaluate(s));
            Exa.width=Mathf.Lerp(_fWidth, _tWidth, curve.Evaluate(s));
            Exa.height=Mathf.Lerp(_fHeight, _tHeight, curve.Evaluate(s));
            Exa.lineThickness=Mathf.Lerp(_fLine, _tLine, curve.Evaluate(s));
            float checker=Mathf.Lerp(0, 1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                t = 0;
                Icon.SetActive(false);
                meBTN.interactable=true;
                _origTxt=TxtPWC.localPosition;
                TxtPWC.SetParent(transform,false);
                TxtPWC.localPosition=_tTxt;
                active = false;
            }
        }

        if(hideIt)
        {
            t = 0;
            hide=true;
            hideIt=false;
            CoreEvents.activeQ=true;
        }

        if(hide)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            meT.localPosition=Vector3.Lerp(_tT, _tT2, curve.Evaluate(s));
            float checker=Mathf.Lerp(0, 1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                t = 0;
                TxtPWC.SetParent(transform.parent,false);
                TxtPWC.SetSiblingIndex(0);
                TxtPWC.localPosition=_origTxt;
                Icon.SetActive(true);
                meRT.sizeDelta=_fRT;
                Exa.width=_fWidth;
                Exa.height=_fHeight;
                Exa.lineThickness=_fLine;
                //Exa.edit=false;
                hide = false;
            }
        }
    }
}
