/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GetEndScore : MonoBehaviour
{
    public CoreEvents core;
    public Sound sound;
    public EndWinnerIs ewi;
    
 // Score card
	public Image scoreCard;
	public Button confirm;
	public Image confimImage;
	public Image logoScore;
	public Text textScore;
	public Text textPoints;
	public Text tynText;
	public InputField ifName;
	public Image ifLimits;
	public Text ifphText;
	public Text ifText;
	public Text disclaimerText;

	private void Start() 
    {
		ifName.enabled=false;
		textScore.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		textPoints.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		tynText.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		ifphText.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		ifText.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		disclaimerText.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
    }
 // Shows and hides score
	public void hideScore() 
	{
		scoreCard.enabled = false;
		confirm.enabled = false;
		confimImage.enabled = false;
		logoScore.enabled = false;
		textScore.enabled = false;
		textPoints.enabled = false;
		tynText.enabled = false;
		ifName.enabled = false;
		ifLimits.enabled = false;
		ifphText.enabled = false;
		ifText.enabled = false;
		disclaimerText.enabled = false;
		//print("hideScore");
	}
	
	public void showScore(string playerScore) 
	{
		if(core.playSounds)
			sound.Play("HighScore");
		scoreCard.enabled = true;
		confirm.enabled = true;
		confimImage.enabled = true;
		logoScore.enabled = true;
		textScore.enabled = true;
		textPoints.enabled = true;
		tynText.enabled = true;
		ifName.enabled = true;
		ifLimits.enabled = true;
		ifphText.enabled = true;
		ifText.enabled = true;

        textScore.text = core.getPhraseFromId("R00030");

        disclaimerText.enabled = true;

		textPoints.text = playerScore;
		if(playerScore==ewi.scorePWinText.text)
			ifphText.text = ewi.playerWinText.text;
		else
			ifphText.text = ewi.p2Text.text;
		tynText.text = core.getPhraseFromId ("R00008");
		disclaimerText.text = core.getPhraseFromId ("R00009");
		ifName.enabled=true;
		ifName.ActivateInputField ();
		ifName.Select ();
		ifText.text = "";
		ifName.text = "";
		//print("showScore attiva l'inputField e gli da il focus");
	}

    public void confirmName() 
	{
		if(core.playSounds)
			sound.Play("Click");
		int[] sc = new int[] {0,0,0,0,0};
		string[] scName = new string[] {"","","","",""};
		sc[0] = PlayerPrefs.GetInt ("sc1");
		sc[1] = PlayerPrefs.GetInt ("sc2");
		sc[2] = PlayerPrefs.GetInt ("sc3");
		sc[3] = PlayerPrefs.GetInt ("sc4");
		sc[4] = PlayerPrefs.GetInt ("sc5");
		scName[0] = PlayerPrefs.GetString ("nam1");
		scName[1] = PlayerPrefs.GetString ("nam2");
		scName[2] = PlayerPrefs.GetString ("nam3");
		scName[3] = PlayerPrefs.GetString ("nam4");
		scName[4] = PlayerPrefs.GetString ("nam5");
		
        if (ifphText.text.Substring(ifphText.text.Length - 1) == "1") 
		{
			if (int.Parse(textPoints.text) > sc [0]) {
				//print("bigger than 1°");
				for (int i=4; i>0; i--) {
					sc [i] = sc [i-1];
					scName [i] = scName [i-1];
				}
				sc [0] = int.Parse(textPoints.text);
				scName [0] = ifText.text;
			} else if (int.Parse(textPoints.text) > sc [1]) {
				//print("bigger than 2°");
                for (int i=4; i>1; i--)
                {
					sc [i] = sc [i-1];
					scName [i] = scName [i-1];
				}
				sc [1] = int.Parse(textPoints.text);
				scName [1] = ifText.text;
			} else if (int.Parse(textPoints.text) > sc [2]) {
				//print("bigger than 3°");
                for (int i = 4; i > 2; i--)
                {
                    sc[i] = sc[i - 1];
                    scName[i] = scName[i - 1];
                }
                sc[2] = int.Parse(textPoints.text);
                scName[2] = ifText.text;
			} else if (int.Parse(textPoints.text) > sc [3]) {
				//print("bigger than 4°");
                for (int i = 4; i > 3; i--)
                {
                    sc[i] = sc[i - 1];
                    scName[i] = scName[i - 1];
                }
                sc[3] = int.Parse(textPoints.text);
                scName[3] = ifText.text;
			} else if (int.Parse(textPoints.text) > sc [4]) {
				//print("bigger than 5°");
				sc [4] = int.Parse(textPoints.text);
				scName [4] = ifText.text;
			}

			for (int i=0; i<5; i++) {
				PlayerPrefs.SetInt ("sc" + (i+1).ToString (), sc [i]);
				PlayerPrefs.SetString ("nam" + (i+1).ToString (), scName [i]);
			}

			if (ewi.p2Text.text != "Computer" &&
                ewi.p2Text.text != "Player 1" &&
                int.Parse(ewi.scoreP2Text.text) > sc [4] &&
                int.Parse(ewi.scoreP2Text.text)!= sc [3] &&
                int.Parse(ewi.scoreP2Text.text)!= sc [2] &&
                int.Parse(ewi.scoreP2Text.text)!= sc [1] &&
                int.Parse(ewi.scoreP2Text.text)!= sc [0])
			{
				showScore(ewi.scoreP2Text.text);
				return;
			}
		}
        else if (ifphText.text.Substring(ifphText.text.Length - 1) == "2") 
		{
            if (int.Parse(textPoints.text) > sc[0])
            {
                //print("bigger than 1°");
                for (int i = 4; i > 0; i--)
                {
                    sc[i] = sc[i - 1];
                    scName[i] = scName[i - 1];
                }
                sc[0] = int.Parse(textPoints.text);
                scName[0] = ifText.text;
            }
            else if (int.Parse(textPoints.text) > sc[1])
            {
                //print("bigger than 2°");
                for (int i = 4; i > 1; i--)
                {
                    sc[i] = sc[i - 1];
                    scName[i] = scName[i - 1];
                }
                sc[1] = int.Parse(textPoints.text);
                scName[1] = ifText.text;
            }
            else if (int.Parse(textPoints.text) > sc[2])
            {
                //print("bigger than 3°");
                for (int i = 4; i > 2; i--)
                {
                    sc[i] = sc[i - 1];
                    scName[i] = scName[i - 1];
                }
                sc[2] = int.Parse(textPoints.text);
                scName[2] = ifText.text;
            }
            else if (int.Parse(textPoints.text) > sc[3])
            {
                //print("bigger than 4°");
                for (int i = 4; i > 3; i--)
                {
                    sc[i] = sc[i - 1];
                    scName[i] = scName[i - 1];
                }
                sc[3] = int.Parse(textPoints.text);
                scName[3] = ifText.text;
            }
            else if (int.Parse(textPoints.text) > sc[4])
            {
                //print("bigger than 5°");
                sc[4] = int.Parse(textPoints.text);
                scName[4] = ifText.text;
            }

			for (int i=0; i<5; i++) {
				PlayerPrefs.SetInt ("sc" + (i+1).ToString (), sc [i]);
				PlayerPrefs.SetString ("nam" + (i+1).ToString (), scName [i]);
			}

			if (ewi.p2Text.text != "Computer" &&
                ewi.p2Text.text != "Player 2" &&
                int.Parse(ewi.scoreP2Text.text) > sc [4] &&
                int.Parse(ewi.scoreP2Text.text)!= sc [3] &&
                int.Parse(ewi.scoreP2Text.text)!= sc [2] &&
                int.Parse(ewi.scoreP2Text.text)!= sc [1] &&
                int.Parse(ewi.scoreP2Text.text)!= sc [0])
			{
				showScore(ewi.scoreP2Text.text);
				return;
			}
		}
		//print("confirmName");
		Exit();
	}

	public void Exit()
	{
		SceneManager.LoadScene("MainMenu");
	}
}
