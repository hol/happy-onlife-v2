/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class Views : MonoBehaviour
{
	[Header("Modules")]
    public CoreEvents core;
    public Sound sound;
    public GenericUtils gu;
    public IconsColorAndPos icons;

	[Header("Touch Elements")]
	public GameObject Touch;
	public GameObject Touch1;
	public GameObject Touch2;
    
	[Header("Buttons")]
	public ViewBtn Btn_2d;
	public ViewBtn Btn_3d;
	public ViewBtn Btn_3dP;

	[Header("Cameras")]
    public Canvas mainCanvas;
	public Camera mc;
	public Camera ac1;
	public Camera ac2;
    public CameraRotationCustom crc;
	public GameObject cameraRotator;
	private Quaternion crInitRot;
	
	private bool its2dview = false;
	private BloomOptimized bloom;

	public Vector3 startRot;
    
    void Start()
    {
		crInitRot=cameraRotator.transform.rotation;
		bloom=mc.transform.GetComponent<BloomOptimized>();
    }
    
	// Switch to 2d view
	public void switchTo2DView() 
	{
		if (!its2dview) 
		{
			if(core.playSounds)
				sound.Play("Click");

			//CameraSettings
			gu.gameBoardRotation = false;
			mc.orthographic = true;
			mc.enabled = true;
			ac1.enabled = false;
			ac2.enabled = false;
			mainCanvas.worldCamera = mc;
			Vector3 pos = mc.transform.localPosition;
			Vector3 rot = transform.eulerAngles;
			pos.x = 0f;
			pos.y = 5f;
			pos.z = -0.4f;
			rot.x = 90f;
			rot.y = 0f;
			rot.z = -2f;
			mc.transform.localPosition = pos;
			mc.transform.eulerAngles = rot;
			crc.v2d=true;
			its2dview = true;
            icons.v2D();
		}
		//print("switchTo2DView");
        Btn_2d.SetMeOn();
	}
	
	//Automatic switch to 3D view
	private void internalSwitch3D() 
	{
		its2dview = false;
		gu.gameBoardRotation = true;
		mc.orthographic = false;
		mc.enabled = true;
		ac1.enabled = false;
		ac2.enabled = false;
		mainCanvas.worldCamera = mc;
		
		Vector3 pos = mc.transform.localPosition;
		Quaternion rot = Quaternion.Euler(startRot);
		float rap=(float)Screen.width/(float)Screen.height;
		pos.x= 0;
		pos.y= 14.2f;
		pos.z= -21.6f;
		mc.transform.localPosition = pos;
		mc.transform.localRotation = rot;
		crc.v3d=true;
		//print("internalSwitch3D");
	}
	
	// Switch to 3d view 
	public void switchTo3DView(bool bloomOptimized=true) 
	{
		if(core.playSounds)
			sound.Play("Click");
		internalSwitch3D();
        icons.v3D();
		cameraRotator.transform.rotation=crInitRot;
		Touch.SetActive(true);
		Touch1.SetActive(false);
		Touch2.SetActive(false);
		//print("switchTo3DView");
		Btn_3d.SetMeOn();
		//if(bloom.enabled!=bloomOptimized)
		//{
		//	bloom.enabled=bloomOptimized;
		//}
		
	}
	
	// Switch to 3d+ view
	public void switchTo3DPlusView() 
	{
		internalSwitch3D();
		if(core.playSounds)
			sound.Play("Click");
		its2dview = false;
		gu.gameBoardRotation = false;
		mc.orthographic = false;
		mc.enabled = false;
		ac1.enabled = false;
		ac2.enabled = false;
		if (gu.currentTurn == 1) 
		{
			ac1.enabled = true;
			mainCanvas.worldCamera = ac1;
			Touch1.SetActive(true);
			Touch2.SetActive(false);
		} else 
		{
			ac2.enabled = true;
			mainCanvas.worldCamera = ac2;
			Touch2.SetActive(true);
			Touch1.SetActive(false);
		}
		Touch.SetActive(false);

        icons.v3DPlus();
		//print("switchTo3DPlusView");
        Btn_3dP.SetMeOn();
	}
}
