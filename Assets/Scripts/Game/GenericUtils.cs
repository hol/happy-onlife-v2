﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

using UnityEngine;

public class GenericUtils : MonoBehaviour 
{
	[Header("Modules")]
	public CoreEvents ce;
	public Dice dice;
	public SecondaryCards sc;
	public Views view;
	public IconsColorAndPos ui3D;
    public EndWinnerIs ewi;
    public Colors c;
    public AssistKeyboardGroups akg;
	
	[Header("Avatars Containers")]
	public GameObject contAvatar1;
	public GameObject contAvatar2;
	private int p1color;
	private int p2color;
	public int types=3;
	public GameObject[] AvatarObject1;
	public GameObject[] AvatarObject2;

	[Header("Avatars\r\n(Automatically extracted at Awake)")]
	public GameObject avatar1;
	public GameObject avatar2;
	public Material wpMat;
	public Material wpMat2;

	public GameObject IconPC3D;

	public int currentTurn = 1;
	public int numPlayers = 1;
	public int type1;
	public int type2;


	public int firstAnswerOk = 1000;
	public int secondAnswerOk = 1000;
	public int firstClassified = 1500;
	public int secondClassified = 500;

	public bool gameBoardRotation = true;

	public int[] wrongGenCat = new int[] {0,0,0,0,0,0,0,0};

	public int[] wrongCell = new int[] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	public int[] wrongQ = new int[] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	//private string[] cellSequence = new string[] {"MStart","M01","M02","P01","M03","M04","M05","P02","M06","M07","G07","G06","G05","G04","G03","G02","G01","M08","M09","M10","M11",
	//											  "P03","M12","P04","M13","M14","P05","M15","M16","P06","M17","M18","M19","P07","M20","M21","M22","P08","M23","M24","P09","M25","M26",
	//											  "P10","M27","M28","M29","P11","M30","M31","M32","M33"};
	
	private string[] cellSequence = new string[] {"MStart","M01","M02", "M03","M04","M05", "M06","M07", "M08","M09","M10","M11","M12","M13","M14","M15","M16","M17","M18","M19","M20","M21","M22","M23","M24","M25","M26",
												  "M27","M28","M29","M30","M31","M32","M33"};//(M33=Finish)

	public static int[] catSequence = new int[32];
	public static int[] catStops = new int[32];

	private Vector3 destination;

	private GameObject currAvatar = null;

	void Awake() 
	{
		// Retrieve the number and type of players
		numPlayers = PlayerPrefs.GetInt ("numplayers");
		type1=PlayerPrefs.GetInt("p1type");
		type2=PlayerPrefs.GetInt("p2type");

		AvatarObject1=new GameObject[types];
		AvatarObject2=new GameObject[types];

		for(int i=0; i<types; i++)
		{
			AvatarObject1[i]=contAvatar1.transform.GetChild(i).transform.gameObject;
			AvatarObject2[i]=contAvatar2.transform.GetChild(i).transform.gameObject;
		}

		if(numPlayers==1)
		{
			for(int i=0; i<AvatarObject1.Length; i++)
			{
				if(i==type1)
				{
					avatar1=AvatarObject1[i];
				}
				else
				{
					AvatarObject1[i].SetActive(false);
				}
			}
			avatar2=AvatarObject2[0];
			AvatarObject2[1].SetActive(false);
			AvatarObject2[2].SetActive(false);

			//Assign here to players its proper opponent
			avatar1.GetComponent<MoveOnPath>().opponent=avatar2.GetComponent<MoveOnPath>();
			avatar2.GetComponent<MoveOnPath>().opponent=avatar1.GetComponent<MoveOnPath>();
		}
		else
		{
			print("Players are:");
			for(int i=0; i<AvatarObject1.Length; i++)
			{
				if(i==type1)
				{
					avatar1=AvatarObject1[i];
					print(avatar1.gameObject.name+";");
				}
				else
				{
					AvatarObject1[i].SetActive(false);
				}

				if(i==type2)
				{
					avatar2=AvatarObject2[i];
					print(avatar2.gameObject.name+";");
				}
				else
				{
					AvatarObject2[i].SetActive(false);
				}
			}
			//Assign here to players its proper opponent
			avatar1.GetComponent<MoveOnPath>().opponent=avatar2.GetComponent<MoveOnPath>();
			avatar2.GetComponent<MoveOnPath>().opponent=avatar1.GetComponent<MoveOnPath>();
		}
		
		// Change colors for the avatar players
		SelectColorMaterial ("p1type", "p1color", 1);
		SelectColorMaterial ("p2type", "p2color", 2);

		print("Gen:num Giocatri="+numPlayers+", type1="+type1+" e type2="+type2+", color1="+p1color+" e color2="+p2color);
	}

	//toDelete
	//public int currAvatarCell() 
	//{
	//	PlayerController pc = currAvatar.GetComponent<PlayerController> ();
	//	return pc.currentPlace;
	//}

	// This function will set the state for playerInMovement then all the necessary action will be taken
	//toDelete
	//public int destCellNum(int currentPos, int df) 
	//{
	//	PlayerController pc = currAvatar.GetComponent<PlayerController> ();
	//	int destCell = currentPos + df;
    //    if (destCell > 33)
    //    {
    //        destCell = 33;
    //    }
    //    string dcn = cellSequence [destCell];
	//	return destCell;
	//}

	//toDelete
	//public string destCellName(int currentPos, int df) 
	//{
	//	PlayerController pc = currAvatar.GetComponent<PlayerController> ();
	//	int destCell = currentPos + df;
    //    if (destCell > 33)
    //    {
    //        destCell = 33;
    //    }
    //     string dcn = cellSequence [destCell];
	//	dcn = cellSequence [destCell];
	//	return dcn;
	//}

	void movePlayer(int diceNumber) 
	{
		/*
		PlayerController pc = currAvatar.GetComponent<PlayerController> ();
		pc.diceNumber = df;
		pc.forced = false;
		pc.jumpstops = true;
		print("gu.movePlayer");
		*/
		
		if(diceNumber>0)
		{
			WPlayerCTRL wPlayer = currAvatar.transform.GetComponent<WPlayerCTRL>();
			wPlayer.Go(diceNumber);
			print("gu.movePlayer - WPlayerCTRL="+wPlayer.gameObject.name+" - diceNumb="+diceNumber);
		}
	}

	//void forcedMovePlayer(int df, bool jumpstops) 
	//{
	//	PlayerController pc = currAvatar.GetComponent<PlayerController> ();
	//	//pc.jumpstops = jumpstops;
	//}

	public void changeTurn() 
	{
		print("gu.changeTurn to ce.showTurnCard");
		ce.showTurnCard ();
	}

	public void changeTurnStep2() 
	{
		// Change the player in turn
		if (currentTurn == 1) 
		{
			currentTurn = 2;
			if(view.ac1.enabled)
			{
				view.ac1.enabled = false;
				view.ac2.enabled = true;
				view.mainCanvas.worldCamera = view.ac2;
			}
		}
		else 
		{
			currentTurn = 1;
			if(view.ac2.enabled)
			{
				view.ac1.enabled = true;
				view.ac2.enabled = false;
				view.mainCanvas.worldCamera = view.ac1;
			}
		}

		ui3D.updateControlPanel();
	}
	
	void hitDice() 
	{
		dice.hitDice();
	}

	public GameObject getCurrAvatar() 
	{
		return currAvatar;
	}

	public void WhoIsDisposable(int cat)
	{
		Click4Question.passCat=cat;
		Click4Question.answer=true;
	}

	public void updateStatus () 
	{
		print ("gu.updateStatus");
		WPlayerCTRL wPlayer = currAvatar.transform.GetComponent<WPlayerCTRL>();
		if (wPlayer.finalDest < cellSequence.Length-1) {
            //Debug.Log("if1");
            // Get the current position of player controller and if necessary display questions or informations
            if (wPlayer.finalDest >= 1 && wPlayer.finalDest <= 6 ||
				wPlayer.finalDest >= 8 && wPlayer.finalDest <= 11 ||
				wPlayer.finalDest >= 13 && wPlayer.finalDest <= 16 ||
				wPlayer.finalDest >= 18 && wPlayer.finalDest <= 22 ||
				wPlayer.finalDest >= 24 && wPlayer.finalDest <= 28 ||
				wPlayer.finalDest >= 30 && wPlayer.finalDest <= 32 ) 
			{
                int cardType = 0;
                cardType=catSequence[wPlayer.finalDest];

				print ("gu.updateStatus to ce.showQuestionCard(cardType)");
				ce.showQuestionCard(cardType);
			}
			else if (wPlayer.finalDest == 7)
			{
                // Display the antivirus card
				print("Antivirus");
				print("gu.updateStatus to ce.showAVCard");
                sc.showAVCard();
				wPlayer.numAntivirus++;
			}
			else if (wPlayer.finalDest == 17)
			{
                // Display the virus card or the used antivirus card
				print("Virus");
				print("gu.updateStatus to ce.showVirus");
				if(wPlayer.numAntivirus>0)
				{
					sc.showVirus(false);
				}
				else
				{
					sc.showVirus(true);
				}
			}
			else if(wPlayer.finalDest==12 || wPlayer.finalDest==23 || wPlayer.finalDest==29)
			{
                // Display the lucky card
				print("Lucky");
				print("gu.updateStatus to ce.showLucky");
                sc.showLucky();
            }
			else
			{
                // Display the Change Turn // There will be NOMORE!!! If the other player has t jump his turn, the actual player will play directly TWICE!!!
				print ("gu.updateStatus to gu.changeTurn");
                changeTurn();
            }
		}
		else
		{
            //Display the end of game
			GameObject otherPlayer=new GameObject();
			if (currentTurn == 1) 
			{
				otherPlayer = avatar2;
			}
			if (currentTurn == 2) 
			{
				otherPlayer = avatar1;
			}

			WPlayerCTRL otherPC = otherPlayer.GetComponent<WPlayerCTRL> ();

			if(otherPC.finalDest>32)
			{
				print("gu.updateStatus to ce.showWinner");
				view.switchTo3DView();
            	ewi.showWinner();
				akg.AssignFocus(3);
			}
			else
			{
				print("primo ad arrivare al traguardo");
				print("currentTurn="+currentTurn);
				print("gu.updateStatus to gu.changeTurn");
                changeTurn();
			}
		}
			
	}

	public void putAside() 
	{
		if (currentTurn == 1) {
			currAvatar = avatar1;
		}
		if (currentTurn == 2) {
			currAvatar = avatar2;
		}
		WPlayerCTRL wPlayer = currAvatar.transform.GetComponent<WPlayerCTRL>();
		Generizer.Generize(wPlayer.finalDest);
	}

	public void putBack(int IDcell) 
	{
		akg.AssignFocus(0);
		Generizer.Destroize(IDcell);
	}

	//public void forcedMove(int steps) 
	//{
	//	if (currentTurn == 1) {
	//		currAvatar = avatar1;
	//	}
	//	if (currentTurn == 2) {
	//		currAvatar = avatar2;
	//	}
	//	forcedMovePlayer(steps,true);
	//}

										//It decide who plays
	public void playTurn(int diceFace) 
	{
		if (currentTurn == 1) {
			currAvatar = avatar1;
		}
		if (currentTurn == 2) {
			currAvatar = avatar2;
		}
		print("currentTurn="+currentTurn);
		print("gu.playTurn to gu.movePlayer");
		movePlayer(diceFace);
	}

	//toDelete
	public Vector3 getCellCoordsByName(string cName) 
	{
		Vector3 cellPos = new Vector3 (0, 0, 0);
		if (cName.Contains("M")) {
			string cellName = "/PercorsoW/" + cName;
			cellPos = GameObject.Find(cellName).transform.position;
			print("gu.getCellCoordsByName vado "+cName);
		}
		return cellPos;
	}
	
	// Set color player avatar 
	void SelectColorMaterial(string prefsType, string prefsName, int player) 
	{
		int pClr = PlayerPrefs.GetInt(prefsName);
		int type = PlayerPrefs.GetInt(prefsType);

		GameObject p=new GameObject();

		if(player==1)
		{
			p=avatar1;
			p1color=pClr;
		}
		else
		{
			p=avatar2;
			p2color=pClr;
		}
		
		if(type==0)
		{
			if (numPlayers == 1 && player==2) 
			{
				print(p.transform.GetChild(0).transform.GetChild(0).transform.name);
				wpMat2=p.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0];
				wpMat2=p.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Renderer>().materials[0];
			}
			else
			{
				p.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].SetColor("_Color", c.hcs[pClr-1]);
				p.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].SetColor("_EmissionColor", c.em[pClr-1]);
				p.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Renderer>().materials[0].SetColor("_Color", c.hcs[pClr-1]);
				p.transform.GetChild(0).transform.GetChild(1).transform.GetComponent<Renderer>().materials[0].SetColor("_EmissionColor", c.em[pClr-1]);
			}
			if(player==1)
			{
				wpMat=p.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().material;
			}
			else
			{
				wpMat2=p.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().material;
			}
		}
		else if(type==1)
		{
			p.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].SetColor("_Color", c.hcs[pClr-1]);
            p.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].SetColor("_EmissionColor", (Color)c.em[pClr-1]);
			if(player==1)
			{
				wpMat=p.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0];
			}
			else
			{
				wpMat2=p.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0];
			}
		}
		else if(type==2)
		{
			p.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].SetColor("_Color", c.hcs[pClr-1]);
            p.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].SetColor("_EmissionColor", (Color)c.em[pClr-1]);
			if(player==1)
			{
				wpMat=p.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0];
			}
			else
			{
				wpMat2=p.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0];
			}
		}

		
	}
}
