﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class WinModule : MonoBehaviour
{
    [Header("Modules")]
    public CoreEvents ce;
    public Colors c;
    public Button BtnObjWin;
    public RectTransform objWin;
    public Image img;
    public ParticleSystem Worms;
    public ParticleSystem Circle;
    public Exa2Dnew label;
    public RectTransform labelTransform;
    public Text txt;
    public Exa2Dnew pwc;
    public Image Icon;
    public RectTransform target;
    public AssistKeyboardGroups akg;

    private Vector3 initPos;
    private Vector3 middlePos;
    private Vector3 finitPos;

    private Vector3 initSca;
    private Vector3 middleSca;
    private Vector3 finitSca;

    public static bool prepare;
    public bool init;
    public static bool continu;
    private bool reset;
    private bool consequence;

    public static string who;
    public static string whatSay;
    public int which;

    public static string what;
    public static int howMuch;

    public static int cPlayer;
    int cPlayerFix;

    private float t;
    private float _time;  
    public AnimationCurve curve;

    private float t2;
    private float _time2;    
    public AnimationCurve curve2;

    float labelH;

    public Sound sound;
    
    void Start()
    {
        img.transform.gameObject.SetActive(true);
        label.transform.gameObject.SetActive(true);
        pwc.transform.gameObject.SetActive(true);
        t=0;
        _time=1.5f;
        _time2=0.5f;

        initPos=new Vector3(0, Screen.height*0.75f, 0);
        middlePos=new Vector3(0, 0, 0);
        finitPos=target.transform.localPosition;

        initSca=new Vector3(0, 0, 0);
        middleSca=new Vector3(1, 1, 1);
        finitSca=new Vector3(0.1f, 0.1f, 0.1f);

        objWin.transform.localPosition=initPos;
        objWin.transform.localScale=initSca;
    }

    void Update()
    {
        if(prepare)
        {
            string[] splitArray =  who.Split(char.Parse("_"));
            who = splitArray[0];
            switch(who)
            {
                case "virus":
                    pwc.transform.gameObject.SetActive(false);
                    img.sprite=Resources.Load("Textures/virus", typeof(Sprite)) as Sprite;
                    labelH=398;
                    labelTransform.localPosition=new Vector3(labelTransform.localPosition.x,labelH,labelTransform.localPosition.z);
                    label.colorOutline=Color.white;
                    label.colorInner=new Color32(255,0,0,255);
                    whatSay=ce.getPhraseFromId("R00049");
                    txt.text=whatSay;//"Virus";
                    break;
                case "virusAttack":
                    pwc.transform.gameObject.SetActive(false);
                    img.sprite=Resources.Load("Textures/virus", typeof(Sprite)) as Sprite;
                    labelH=398;
                    labelTransform.localPosition=new Vector3(labelTransform.localPosition.x,labelH,labelTransform.localPosition.z);
                    label.colorOutline=Color.white;
                    label.colorInner=new Color32(255,0,0,255);
                    whatSay=ce.getPhraseFromId("R00049");
                    txt.text=whatSay;//"Virus";
                    Worms.gameObject.SetActive(true);
                    Worms.Play();
                    Circle.Play();
                    sound.Play("virus");
                    break;
                case "antivirus":
                    pwc.transform.gameObject.SetActive(false);
                    img.sprite=Resources.Load("Textures/antivirus_icon", typeof(Sprite)) as Sprite;
                    labelH=382;
                    labelTransform.localPosition=new Vector3(labelTransform.localPosition.x,labelH,labelTransform.localPosition.z);
                    label.colorOutline=Color.white;
                    label.colorInner=new Color32(0,123,255,255);
                    whatSay=ce.getPhraseFromId("R00048");
                    txt.text=whatSay;//"Anit-Virus";
                    break;
                case "lucky":
                    pwc.transform.gameObject.SetActive(false);
                    img.sprite=Resources.Load("Textures/Sprite_Lucky", typeof(Sprite)) as Sprite;
                    labelH=-229;
                    labelTransform.localPosition=new Vector3(labelTransform.localPosition.x,labelH,labelTransform.localPosition.z);
                    label.colorOutline=Color.white;
                    label.colorInner=new Color32(40,164,0,255);
                    txt.text=what;
                    break;
                case "pwc1":
                    _time=1.5f;
                    img.transform.gameObject.SetActive(false);
                    which=int.Parse(splitArray[1]);
                    labelH=398;
                    labelTransform.localPosition=new Vector3(labelTransform.localPosition.x,labelH,labelTransform.localPosition.z);
                    label.colorOutline=Color.white;
                    label.colorInner=c.cc[which];
                    whatSay=ce.getPhraseFromId("R00046");
                    txt.text=whatSay;//"PowerCard 1/2";
                    pwc.halfSx=true;
                    pwc.colorInner=c.cc[which];
                    switch(which)
                    {
                        case 0 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_PlaySafely");
                            break;
                        case 1 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_SocialLife");
                            break;
                        case 2 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_DontRisk");
                            break;
                        case 3 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_noBull");
                            break;
                        case 4 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_PrivacyMatters");
                            break;
                        case 5 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_LetsChat");
                            break;
                        case 6 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_WatchOut");
                            break;
                        case 7 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_IsItFair");
                            break;
                    }
                    break;
                case "pwc2":
                    _time=1.5f;
                    img.transform.gameObject.SetActive(false);
                    which=int.Parse(splitArray[1]);
                    labelH=398;
                    labelTransform.localPosition=new Vector3(labelTransform.localPosition.x,labelH,labelTransform.localPosition.z);
                    label.colorOutline=Color.white;
                    label.colorInner=c.cc[which];
                    whatSay=ce.getPhraseFromId("R00047");
                    txt.text=whatSay;//"PowerCard 2/2";
                    pwc.halfDx=true;
                    pwc.colorInner=c.cc[which];
                    switch(which)
                    {
                        case 0 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_PlaySafely");
                            break;
                        case 1 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_SocialLife");
                            break;
                        case 2 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_DontRisk");
                            break;
                        case 3 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_noBull");
                            break;
                        case 4 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_PrivacyMatters");
                            break;
                        case 5 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_LetsChat");
                            break;
                        case 6 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_WatchOut");
                            break;
                        case 7 :
                            Icon.sprite = Resources.Load<Sprite>("Textures/Sprite_IsItFair");
                            break;
                    }
                    break;
                case "changeTurn":
                    _time=1f;
                    pwc.transform.gameObject.SetActive(false);
                    img.transform.gameObject.SetActive(false);
                    labelH=-410.3f;
                    labelTransform.localPosition=new Vector3(labelTransform.localPosition.x,labelH,labelTransform.localPosition.z);
                    label.colorOutline=Color.white;
                    label.colorInner=c.hcs[cPlayer-1];
                    whatSay=ce.getPhraseFromId("R00012");
                    txt.text=whatSay;//"it' your turn";
                    break;
                case "changeTurnFirst":
                    _time=1f;
                    pwc.transform.gameObject.SetActive(false);
                    img.transform.gameObject.SetActive(false);
                    labelH=-410.3f;
                    labelTransform.localPosition=new Vector3(labelTransform.localPosition.x,labelH,labelTransform.localPosition.z);
                    label.colorOutline=Color.white;
                    label.colorInner=c.hcs[cPlayer-1];
                    whatSay=ce.getPhraseFromId("R00045");
                    txt.text=whatSay;//"Play and Learn!";
                    break;
                case "changeTurnPC":
                    _time=1f;
                    pwc.transform.gameObject.SetActive(false);
                    img.transform.gameObject.SetActive(false);
                    labelH=-410.3f;
                    labelTransform.localPosition=new Vector3(labelTransform.localPosition.x,labelH,labelTransform.localPosition.z);
                    label.colorOutline=Color.black;
                    label.colorInner=Color.grey;
                    whatSay=ce.getPhraseFromId("R00041");
                    txt.text=whatSay;//"Try to answer question of your opponent and earn some extra points!!!";
                    break;
                case "opp":
                    _time=1.5f;
                    img.transform.gameObject.SetActive(false);
                    pwc.transform.gameObject.SetActive(false);
                    labelH=-152.5f;
                    labelTransform.localPosition=new Vector3(labelTransform.localPosition.x,labelH,labelTransform.localPosition.z);
                    label.colorOutline=Color.white;
                    label.colorInner=c.hcs[cPlayer-1];//To change with color of Player
                    which=int.Parse(splitArray[1]);
                    switch (which)
                    {
                        case 1:
                            whatSay=ce.getPhraseFromId("R00044");
                            txt.text=whatSay;//"Your opponent wins 1000 pt.\r\nIt's your turn.";
                            SettingScore2.toAdd=1000;
                            SettingScore2.add=true;
                            break;
                        case 2:
                            whatSay=ce.getPhraseFromId("R00043");
                            txt.text=whatSay;//"Your opponent wins 500 pt.\r\nIt's your turn.";
                            SettingScore2.toAdd=500;
                            SettingScore2.add=true;
                            break;
                        case 4:
                            whatSay=ce.getPhraseFromId("R00042");
                            txt.text=whatSay;//"Your opponent went wrong.\r\nIt's your turn.";
                            break;
                    }
                    break;
            }
            cPlayerFix=cPlayer;
            BtnObjWin.interactable=true;
            Click4Question.free=false;
            init=true;
            prepare=false;
            akg.AssignFocus(2);
        }

        if(init)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            objWin.transform.localPosition=Vector3.Lerp(initPos, middlePos, curve.Evaluate(s));
            objWin.transform.localScale=Vector3.Lerp(initSca, middleSca, curve.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                init = false;
                WinModule_Btn.able=true;



                t = 0;
            }
        }

        if(continu)
        {
            t += Time.deltaTime;
            float s = t / _time2;
            
            objWin.transform.localPosition=Vector3.Lerp(middlePos, finitPos, curve2.Evaluate(s));
            objWin.transform.localScale=Vector3.Lerp(middleSca, finitSca, curve2.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                Click4Question.free=true;
                continu = false;
                t = 0;

                switch(who)
                {
                    case "virus":
                        Consequence.virus=true;
                        break;
                    case "virusAttack":
                        Worms.Stop();
                        Circle.Stop();
                        Consequence.virusAttack=true;
                        break;
                    case "antivirus":
                        Consequence.antivirus=true;
                        break;
                    case "lucky":
                        if(CoreEvents.computerTurn)
                        {
                            SettingScore2.toAdd=howMuch;
                            SettingScore2.add=true;
                        }
                        else
                        {
                            SettingScore.toAdd=howMuch;
                            SettingScore.add=true;
                        }
                        Consequence.lucky=true;
                        break;
                    case "pwc1":
                        Consequence.pwc=true;
                        Consequence.whichPwc=which;
                        break;
                    case "pwc2":
                        Consequence.pwc=true;
                        Consequence.whichPwc=which;
                        break;
                    case "changeTurn":
                        Consequence.changeTurn=true;
                        break;
                    case "changeTurnFirst":
                        Consequence.changeTurnFirst=true;
                        break;
                    case "changeTurnPC":
                        Consequence.changeTurnPC=true;
                        break;
                    case "opp":
                        Consequence.opp=true;
                        break;
                    }

                reset=true;
            }
            akg.AssignFocus(0);
        }

        if(reset)
        {
            Start();
            reset=false;
        }
    }
}
