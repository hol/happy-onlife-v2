/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class ControlOffsetTopByObjectDistance : MonoBehaviour
{
    public int offset = 2;
    public GameObject WorldObject;
    public RectTransform UI_Element;
    private RectTransform CanvasRect;
    public Canvas Canva;
    public Camera main;

    void Start()
    {
        CanvasRect=Canva.GetComponent<RectTransform>();
    }

    public void SetY()
    {
        Vector2 ViewportPosition=main.WorldToViewportPoint(WorldObject.transform.position);

        Vector2 WorldObject_ScreenPosition=new Vector2(
        ((ViewportPosition.x*CanvasRect.sizeDelta.x)-(CanvasRect.sizeDelta.x*0.5f)),
        ((ViewportPosition.y*CanvasRect.sizeDelta.y)-(CanvasRect.sizeDelta.y*0.5f)));

        //print("result="+WorldObject_ScreenPosition/20+" SCREENHeight="+Screen.height);

        UI_Element.transform.GetComponent<HorizontalLayoutGroup>().padding.top=Mathf.RoundToInt((WorldObject_ScreenPosition.y/2)/offset);
    }
    void Update()
    {
        SetY();
    }
}
