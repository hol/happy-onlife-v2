﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class AudioSettings2 : MonoBehaviour 
{
	public GameObject SliderSound;

	public Image s1;
	public Image s2;
	public Image s3;
	public Image s4;
	public Image s5;
	public Image s6;
	public Image s7;
	public Image s8;

	public GameObject ViewBtn;

	Image[] soundImg;

	float alphaIntensity;

	private Slider slider;

	private float vol;
	public static float maxHeight;
	public static float maxLength;
	public static float maxSize;
	
	bool sound;

	public GenericUtils gu;

	// Use this for initialization
	void Start () 
	{
		transform.GetComponent<Button> ().onClick.AddListener (delegate {Sound (); });

		slider = SliderSound.transform.GetComponent<Slider>();

		soundImg=new Image[8]{s1,s2,s3,s4,s5,s6,s7,s8};

		if(PlayerPrefs.GetString("mute")!=null)
		{
			vol = PlayerPrefs.GetFloat("vol");
			slider.value = vol;
		}
		else
		{
			vol = slider.value;
		}
		
		
		SliderSound.SetActive(false);
		
		OnValueChanged ();

		Intencity(vol);
	}

	void Intencity(float value)
	{
		for(int i=0; i<soundImg.Length; i++)
		{
			soundImg[i].color=new Color(1,1,1,value+0.2f);
		}
	}

	void Update()
	{
		if (sound) {
			OnValueChanged ();
		}
	}

	public void Sound()
	{
		print("Sound");

		if (sound)
		{
			SliderSound.SetActive(false);
			gu.gameBoardRotation=true;
			ViewBtn.SetActive(true);
		}
		else
		{
			SliderSound.SetActive(true);
			gu.gameBoardRotation=false;
			ViewBtn.SetActive(false);
		}

		sound = !sound;
	}

	public void OnValueChanged()
	{
		vol = slider.value;
		AudioListener.volume = vol;

		Intencity(vol);
		
		PlayerPrefs.SetFloat("vol", vol);
		if (vol == 0)
		{
			PlayerPrefs.SetString("mute", "yes");
		}
		else
		{
			PlayerPrefs.SetString("mute", "no");
		}
	}

}
