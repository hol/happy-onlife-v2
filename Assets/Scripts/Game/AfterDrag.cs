﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System;
using UnityEngine;

public class AfterDrag : MonoBehaviour
{   
    public RectTransform Target;
    public PowerTrailsSetting Trail;
    private static Transform _who;
    private static Vector3 _from;
    private static Vector3 _to;
    
    private static bool check;

    public float offsetX;
    public float offsetY;
    
    void Start()
    {
        check = false;
    }

    public static void CheckThis(Transform who,Vector3 from, Vector3 to)
    {
        _who = who;
        _from = from;
        _to = to;

        check = true;
    }

    void Update()
    {
        if (check)
        {
            CheckPos();
            check = false;
        }
    }

    void CheckPos()
    {
        float maxX = Target.position.x + Target.sizeDelta.x / 2 + offsetX;
        float minX = Target.position.x - Target.sizeDelta.x / 2 - offsetX;
        float maxY = Target.position.y + Target.sizeDelta.y / 2 + offsetY;
        float minY = Target.position.y - Target.sizeDelta.y / 2 - offsetY;

        if (_to.x <= maxX && _to.x > minX)
        {
            if (_to.y <= maxY && _to.y > minY)
            {
                float dist1 = Mathf.Abs((1-((Vector3.Distance(_to, Target.position))*0.001f))*0.5f);
                ProviderMovement.MoveThis(_who, _to, Target.position, dist1);
                Trail.connected = true;
            }
            else
            {
                float dist2 = Mathf.Abs((1-((Vector3.Distance(_to, _from))*0.001f))*0.5f);
                ProviderMovement.BackThat(_who, _to, _from, dist2);
            }
        }
        else
        {
            float dist2 = Mathf.Abs((1-((Vector3.Distance(_to, _from))*0.001f))*0.5f);
            ProviderMovement.BackThat(_who, _to, _from, dist2);
        }
    }
}
