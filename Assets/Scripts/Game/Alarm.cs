﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

using UnityEngine;
using UnityEngine.UI;

public class Alarm : MonoBehaviour
{
    public Exa2Dnew Exa;
    public GameObject X;

    public static bool on;

    private bool alarmOn;
    private bool alarmOff;

    private float t;
    private float _time;    
    public AnimationCurve curve;
    public AnimationCurve curve2;

    int counter;

    Color32 red;
    Color32 grey;

    float lLarge;
    float lOrig;

    private Image line1;
    private Image line2;

    public bool stopNow;

    void Start()
    {
        red=new Color32(255,0,0,255);
        grey=new Color32(88,88,88,255);
        lLarge=40;
        lOrig=Exa.lineThickness;
        line1=X.transform.GetChild(0).GetComponent<Image>();
        line2=X.transform.GetChild(1).GetComponent<Image>();
        X.SetActive(false);
        t=0;
        _time=0.2f;
    }

    void Update()
    {
        if(stopNow)
        {
            on=false;
            alarmOn=false;
            alarmOff=false;
            Exa.colorOutline=grey;
            line1.color=grey;
            line2.color=grey;
            Exa.lineThickness=lOrig;
            t = 0;
            X.SetActive(false);
            print("stoprightNow");
            stopNow=false;
        }

        if(on)
        {
            if(CoreEvents.enoughPWC)
            {
                grey=new Color32(255,255,255,255);
            }
            else
            {
                grey=new Color32(88,88,88,255);
            }
            counter=0;
            line1=X.transform.GetChild(0).transform.GetComponent<Image>();
            line2=X.transform.GetChild(1).transform.GetComponent<Image>();
            line1.color=grey;
            line2.color=grey;
            X.SetActive(true);
            alarmOn=true;
            on=false;
            print("on");
        }

        if(alarmOn)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            Exa.colorOutline=Color32.Lerp(grey, red,  curve.Evaluate(s));
            line1.color=Color32.Lerp(grey, red,  curve.Evaluate(s));
            line2.color=Color32.Lerp(grey, red,  curve.Evaluate(s));
            Exa.lineThickness=Mathf.Lerp(lOrig, lLarge, curve.Evaluate(s));
            float checker=Mathf.Lerp(0, 1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                t = 0;
                counter++;
                if(counter!=4)
                {
                    alarmOff=true;
                }
                alarmOn = false;
                print("alarmOn");
            }

        }

        if(alarmOff)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            Exa.colorOutline=Color32.Lerp(red, grey, curve2.Evaluate(s));
            line1.color=Color32.Lerp(red, grey, curve.Evaluate(s));
            line2.color=Color32.Lerp(red, grey, curve.Evaluate(s));
            Exa.lineThickness=Mathf.Lerp(lLarge, lOrig, curve2.Evaluate(s));
            float checker=Mathf.Lerp(0, 1, curve2.Evaluate(s));
            
            if (checker == 1)
            {
                t = 0;
                counter++;
                if(counter!=4)
                {
                    alarmOn=true;
                }
                else
                {
                    X.SetActive(false);
                }
                alarmOff = false;
                print("alarmOff");
            }
        }
    }
}