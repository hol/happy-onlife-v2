﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class ChangeTurnNew : MonoBehaviour
{
    [Header("Modules")]
    public Views ui3D;
    public GenericUtils gu;
    public ViewBtn vb;
    public RectTransform winObj;
    public Camera main;
    public float offset=0.5f;

    public GameObject ph1;
    public GameObject ph2;
    public GameObject ph0;
    public GameObject ph02;

    public GameObject[] IconObject1;
    public GameObject[] IconObject2;
    public GameObject PCavatarBig;

    public GameObject iconPlayer;
    public GameObject iconNotPlayer;
    public GameObject iconBackUp;

    public bool phase1;
    public bool phase2;
    public bool phase3;
    public bool phase4;
    public bool phase5;
    public bool phase6;

    private float t;
    private float _time1;   
    private float _time2;   
    private float _time3;   
    public AnimationCurve curve1;
    public AnimationCurve curve2;
    public AnimationCurve curve3;

    public float posXcenter;

    private Vector3 sPos1;
    private Vector3 ePos1;
    private Vector3 sPos2;
    private Vector3 ePos2;
    private Vector3 sPos3;
    private Vector3 ePos3;

    private Vector3 sRot2;
    private Vector3 eRot2;
    private Vector3 sRot3;
    private Vector3 eRot3;

    private Vector3 s2Pos1;
    private Vector3 e2Pos1;
    private Vector3 s2Pos2;
    private Vector3 e2Pos2;
    private Vector3 s2Rot1;
    private Vector3 e2Rot1;
    private Vector3 s2Rot2;
    private Vector3 e2Rot2;
    private bool firstTime;
    private int type1;
	private int type2;
    private int numPlayers;
    private bool invert=false;

    public GameObject middleTarget;
    public bool preparePhase2;

    void Start()
    {
        firstTime=true;

		type1 = gu.type1;
		type2 = gu.type2;
        numPlayers  = gu.numPlayers;

        WhoIsWho(type1, type2);

        iconPlayer=IconObject1[type1].transform.parent.gameObject;

        if(numPlayers==1)
        {
            iconNotPlayer=PCavatarBig.transform.parent.gameObject;
            PCavatarBig.SetActive(false);
        }
        else
        {
            iconNotPlayer=IconObject2[type2].transform.parent.gameObject;
            PCavatarBig.SetActive(false);
        }
        

        print("ptp1="+type1+" ptp2="+type2);

        t=0;
        
        _time1=1f;
        _time2=1f;
        _time3=1f;

        sPos1=new Vector3(55,-35,5);
        ePos1=new Vector3(55,150,5);

        sPos3=ePos2;
        ePos3=new Vector3(84,-50,10);
		eRot3=new Vector3(0,180, 0);

        s2Pos1=new Vector3(-13,-35,63);
        e2Pos1=new Vector3(-13,150,63);
        s2Rot1=new Vector3(20,75,0);
        e2Rot1=new Vector3(0,0,0);        
    }

    void WhoIsWho(int p1, int p2)
    {
        for(int i=0; i<IconObject1.Length; i++)
		{
			if(i!=p1)
			{
				IconObject1[i].SetActive(false);
			}
            else
            {
                IconObject1[i].SetActive(true);
            }
	
			if(i!=p2)
			{
				IconObject2[i].SetActive(false);
			}
            else
            {
                IconObject2[i].SetActive(true);
            }
		}
    }
    
    void Update()
    {
        //Middle Screen Icon Position
        ph0.transform.position=ph02.transform.position;

        //5 Movement Phases
        if(phase1)
        {
            if(!firstTime)//Icons go up;
            {
                t += Time.deltaTime;
                float s = t / _time1;
                
                iconPlayer.transform.localPosition=Vector3.Lerp(sPos1, ePos1, curve1.Evaluate(s));
                iconNotPlayer.transform.localPosition=Vector3.Lerp(s2Pos1, e2Pos1, curve1.Evaluate(s));
                float checker=Mathf.Lerp(0,  1, curve1.Evaluate(s));
                
                if (checker == 1)
                {
                    phase1 = false;
                    print("ChangeTurn:PHASE1 not firstTime anymore ui3D.switchTo3DView wm.ct e gu.changeTurnStep2");
                    ui3D.switchTo3DView();
                    vb.SetMeOn();
                    WinModule.who="changeTurn";
                    WinModule.prepare=true;
                    t = 0;
                    gu.changeTurnStep2 ();
                    preparePhase2=true;

                    //Invert IconMeshes (Change Turn New)
                    if(numPlayers==2)
                    {
                        if(invert)
                        {
                            WhoIsWho(type1, type2);

                            iconPlayer=IconObject1[type1].transform.parent.gameObject;
                            iconNotPlayer=IconObject2[type2].transform.parent.gameObject;

                            invert=false;
                        }
                        else
                        {
                            WhoIsWho(type2, type1);

                            iconPlayer=IconObject1[type2].transform.parent.gameObject;
                            iconNotPlayer=IconObject2[type1].transform.parent.gameObject;

                            invert=true;
                        }
                    }
                    phase2=true;
                    SettingScore2.showAS=true;
                }
            }
            else
            {
                phase1 = false;
                firstTime=false;
                print("ChangeTurn:PHASE1 firstTime");
                WinModule.who="changeTurn";
                WinModule.prepare=true;
                t = 0;
                gu.changeTurnStep2();
                preparePhase2=true;
                phase2=true;
            }
        }

        if(phase2)//Icon1 in the middle; Icon2 at its place
        {
            if(preparePhase2)
            {
                //from
                sPos2=iconPlayer.transform.position;
                sRot2=iconPlayer.transform.eulerAngles;
                //to
                ePos2=middleTarget.transform.position;
                eRot2=middleTarget.transform.eulerAngles;
                preparePhase2=false;
            }
            t += Time.deltaTime;
            float s = t / _time2;

            ePos2=middleTarget.transform.position;
            eRot2=middleTarget.transform.eulerAngles;

            iconPlayer.transform.position=Vector3.Lerp(sPos2, ePos2, curve2.Evaluate(s));
            iconPlayer.transform.eulerAngles=Vector3.Lerp(sRot2, eRot2, curve2.Evaluate(s));

            iconNotPlayer.transform.localPosition=Vector3.Lerp(s2Pos2, e2Pos2, curve2.Evaluate(s));
            iconNotPlayer.transform.eulerAngles=Vector3.Lerp(s2Rot2, e2Rot2, curve2.Evaluate(s));

            float checker=Mathf.Lerp(0,  1, curve2.Evaluate(s));
            
            if (checker == 1)
            {
                phase2 = false;
                t = 0;
                print("ChangeTurn:PHASE2 Who plays");
            }
        }

        if(phase3)//Icon1 from the middle to its place;
        {
            t += Time.deltaTime;
            float s = t / _time3;
            
            sPos3=iconPlayer.transform.localPosition;
            sRot3=iconPlayer.transform.eulerAngles;

            iconPlayer.transform.localPosition=Vector3.Lerp(sPos3, ePos3, curve3.Evaluate(s));
            iconPlayer.transform.eulerAngles=Vector3.Lerp(sRot3, eRot3, curve3.Evaluate(s));

            float checker=Mathf.Lerp(0,  1, curve3.Evaluate(s));
            
            if (checker == 1)
            {
                phase3 = false;
                if(!CoreEvents.computerTurn)
                {
                    SettingScore.showAS=true;
                    Click4Question.free=true;
                }
                t = 0;
                print("ChangeTurn:PHASE3 Dice On Stage");
            }
        }

        if(phase4)
        {
            
            t += Time.deltaTime;
            float s = t / _time1;
            
            iconNotPlayer.transform.localPosition=Vector3.Lerp(s2Pos1, e2Pos1, curve1.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve1.Evaluate(s));
            
            if (checker == 1)
            {
                phase4 = false;
                
                if(numPlayers==1)
                {
                    PCavatarBig.SetActive(true);
                }

                print("ChangeTurn:PHASE4   wm.ctPC");
                WinModule.who="changeTurnPC";
                WinModule.prepare=true;
                t = 0;
                phase5=true;
            }
        }

        if(phase5)
        {
            t += Time.deltaTime;
            float s = t / _time2;
            
            PCavatarBig.transform.localPosition=Vector3.Lerp(sPos2, ePos2, curve2.Evaluate(s));
            PCavatarBig.transform.eulerAngles=Vector3.Lerp(sRot2, eRot2, curve2.Evaluate(s));

            float checker=Mathf.Lerp(0,  1, curve2.Evaluate(s));
            
            if (checker == 1)
            {
                phase5 = false;
                t = 0;
                print("ChangeTurn:PHASE5");
            }
        }

        if(phase6)
        {
            t += Time.deltaTime;
            float s = t / _time3;
            
            iconNotPlayer.transform.localPosition=Vector3.Lerp(e2Pos1, s2Pos1, curve1.Evaluate(s));
            PCavatarBig.transform.localPosition=Vector3.Lerp(ePos2, sPos2, curve2.Evaluate(s));
            PCavatarBig.transform.eulerAngles=Vector3.Lerp(eRot2, sRot2, curve2.Evaluate(s));

            float checker=Mathf.Lerp(0,  1, curve3.Evaluate(s));
            
            if (checker == 1)
            {
                CoreEvents.challangeAccepted=true;
                phase6 = false;
                t = 0;
                print("ChangeTurn:PHASE6 ce.challangeAccepted=true");
            }
        }
    }
}
