﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SettingScore2 : MonoBehaviour
{
    public GenericUtils gu;

    private WPlayerCTRL wp1;
    private WPlayerCTRL wp2;

    private int score;
    private int displayScore;
    private int actScoreN;
    public Text actScoreT;
    private RectTransform addScoreR;

    public static bool add;
    public static bool sub;
    public static bool showAS;
    public static bool show;
    public static bool show2;
    public static bool hideAS;
    public static bool hide;
    public static int toAdd;
    public static int toSub;
    public AnimationCurve curve;

    private int step;

    void Start()
    {
        wp1=gu.avatar1.GetComponent<WPlayerCTRL>();
		wp2=gu.avatar2.GetComponent<WPlayerCTRL>();
        
        toAdd=1500;
    }

    void Update()
    {
        if(add)
        {
            if (gu.currentTurn == 1) 
            {
                score=wp1.currentScore;
                score+=toAdd;
                wp1.currentScore=score;
            }
            else 
            {
                score=wp2.currentScore;
                score+=toAdd;
                print(score);
                wp2.currentScore=score;
            }
            step=toAdd/10;
            show=true;
            add=false;
        }

        if(sub)
        {
            score=wp2.currentScore;
            score-=toSub;
            wp2.currentScore=score;
            step=-toSub/10;
            show=true;
            sub=false;
        }

        if(show)
        {   
            StartCoroutine(ScoreShower());
            show=false;
        }

        if(showAS)
        {
            if (gu.currentTurn == 1) 
            {
                score=wp2.currentScore;
            }
            else 
            {
                score=wp1.currentScore;
            }
            displayScore=0;
            if(score!=0)
            {
                step=score/20;
                StartCoroutine(ScoreShower());
            }
            showAS=false;
        }

        if(hideAS)
        {
            if (gu.currentTurn == 1) 
            {
                score=wp2.currentScore;
            }
            else 
            {
                score=wp1.currentScore;
            }

            if(score!=0)
            {
                step=displayScore/20;
                StartCoroutine(ScoreHider());
            }
            hideAS=false;
        }
    }

    private IEnumerator ScoreShower()
    {
        while(true)
        {
            if(displayScore != score)
            {
                displayScore +=step;
                actScoreT.text = displayScore.ToString();
            }
            else
            {
                StopAllCoroutines();
            }
            yield return new WaitForSeconds(0.001f);
        }
    }

    private IEnumerator ScoreHider()
    {
        while(true)
        {
            if(displayScore != 0)
            {
                displayScore -=step;
                actScoreT.text = displayScore.ToString();
            }
            else
            {
                StopAllCoroutines();
            }
            yield return new WaitForSeconds(0.001f);
        }
    }
}
