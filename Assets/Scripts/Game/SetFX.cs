﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetFX : MonoBehaviour
{
    [Header("Spaceship ID")]
    public int ID;

    [Header("Audio Connection")]
    public Sound sound;

    [Header("Particles FX")]
    public ParticleSystem StartingDust;
    public ParticleSystem MotorRay1;
    public ParticleSystem MotorRay2;
    public ParticleSystem MotorRay3;
    public ParticleSystem MotorRay4;
    public ParticleSystem MotorRay5;
    public ParticleSystem MotorRay6;
    public ParticleSystem MotorRay7;
    public ParticleSystem AnswerRight;
    public ParticleSystem AnswerWrong;
    public ParticleSystem AntivirusFX;
    public ParticleSystem VirusRejected;
    public ParticleSystem VirusAttackSpark;
    public ParticleSystem VirusAttackSmoke;
    public ParticleSystem LuckyFXCircle;
    public ParticleSystem LuckyFXLeaf;

    [Header("Switches\r\n-Stardust")]
    public bool sd;
    [Header("-MotorRay")]
    public bool mr;
    [Header("-No MotorRay")]
    public bool noMr;
    [Header("-Up MotorRay")]
    public bool upMr;
    [Header("-Down MotorRay")]
    public bool downMr;
    [Header("-Answer Right")]
    public bool ar;
    [Header("-Answer Wrong")]
    public bool aw;
    [Header("-Antivirus FX")]
    public bool afx;
    [Header("-Virus Rejected")]
    public bool vr;
    [Header("-Virus Attack")]
    public bool va;
    [Header("-Lucky FX")]
    public bool lfx;

    [Header("Motor Scale")]
    public float minScale;
    public float maxScale;
    private float motorScale;
    
    [Header("Animation Settings")]
    public float duration;
    public AnimationCurve curve;
    private float t=0;

    void Start() 
    {
        downMr=true;
    }
    // Update is called once per frame
    void Update()
    {
        if(sd)
        {
            StartingDust.Play();
            sound.Play("startSS");
            sd=false;
        }
        
        if(mr)
        {
            switch(ID)
            {
                case 1:
                    MotorRay1.Play();
                    break;
                case 2:
                    MotorRay1.Play();
                    MotorRay2.Play();
                    MotorRay3.Play();
                    break;
                case 3:
                    MotorRay1.Play();
                    MotorRay2.Play();
                    MotorRay3.Play();
                    MotorRay4.Play();
                    MotorRay5.Play();
                    MotorRay6.Play();
                    MotorRay7.Play();
                    break;
            }
            mr=false;
        }

        if(noMr)
        {
            switch(ID)
            {
                case 1:
                    MotorRay1.Stop();
                    break;
                case 2:
                    MotorRay1.Stop();
                    MotorRay2.Stop();
                    MotorRay3.Stop();
                    break;
                case 3:
                    MotorRay1.Stop();
                    MotorRay2.Stop();
                    MotorRay3.Stop();
                    MotorRay4.Stop();
                    MotorRay5.Stop();
                    MotorRay6.Stop();
                    MotorRay7.Stop();
                    break;
            }
            noMr=false;
        }

        if(upMr)
        {
            t += Time.deltaTime;
            float s = t / duration;

            motorScale = Mathf.Lerp(minScale, maxScale, curve.Evaluate(s));
            
            float checker = Mathf.Lerp(0f, 1f, curve.Evaluate(s));

            switch(ID)
            {
                case 1:
                    IncreaseMotorPower(MotorRay1, motorScale);
                    break;
                case 2:
                    IncreaseMotorPower(MotorRay1, motorScale);
                    IncreaseMotorPower(MotorRay2, motorScale);
                    IncreaseMotorPower(MotorRay3, motorScale);
                    break;
                case 3:
                    IncreaseMotorPower(MotorRay1, motorScale);
                    IncreaseMotorPower(MotorRay2, motorScale);
                    IncreaseMotorPower(MotorRay3, motorScale);
                    IncreaseMotorPower(MotorRay4, motorScale);
                    IncreaseMotorPower(MotorRay5, motorScale);
                    IncreaseMotorPower(MotorRay6, motorScale);
                    IncreaseMotorPower(MotorRay7, motorScale);
                    break;
            }

            if(checker == 1)
            {
                t=0;
                upMr=false;
                print("upMr");
            }
        }

        if(downMr)
        {
            t += Time.deltaTime;
            float s = t / duration;

            motorScale = Mathf.Lerp(maxScale, minScale, curve.Evaluate(s));
            
            float checker = Mathf.Lerp(0f, 1f, curve.Evaluate(s));

            switch(ID)
            {
                case 1:
                    DecreaseMotorPower(MotorRay1, motorScale);
                    break;
                case 2:
                    DecreaseMotorPower(MotorRay1, motorScale);
                    DecreaseMotorPower(MotorRay2, motorScale);
                    DecreaseMotorPower(MotorRay3, motorScale);
                    break;
                case 3:
                    DecreaseMotorPower(MotorRay1, motorScale);
                    DecreaseMotorPower(MotorRay2, motorScale);
                    DecreaseMotorPower(MotorRay3, motorScale);
                    DecreaseMotorPower(MotorRay4, motorScale);
                    DecreaseMotorPower(MotorRay5, motorScale);
                    DecreaseMotorPower(MotorRay6, motorScale);
                    DecreaseMotorPower(MotorRay7, motorScale);
                    break;
            }

            if(checker == 1)
            {
                t=0;
                downMr=false;
                print("downMr");
            }
        }

        if(ar)
        {
            AnswerRight.Play();
            ar=false;
        }

        if(aw)
        {
            AnswerWrong.Play();
            sound.Play("electrifySS");
            aw=false;
        }

        if(afx)
        {
            AntivirusFX.Play();
            sound.Play("antivirusSS");
            afx=false;
        }

        if(vr)
        {
            VirusRejected.Play();
            sound.Play("virusSS");
            vr=false;
        }

        if(va)
        {
            VirusAttackSpark.Play();
            VirusAttackSmoke.Play();
            sound.Play("explosionSS");
            va=false;
        }

        if(lfx)
        {
            LuckyFXCircle.Play();
            LuckyFXLeaf.Play();
            sound.Play("luckySS");
            lfx=false;
        }
    }

    public void MotorOn() 
    {
        upMr=true;
    }

    public void MotorOff() 
    {
        downMr=true;
    }

    private void IncreaseMotorPower(ParticleSystem ps, float scale) 
    {
        var sz = ps.sizeOverLifetime;
        sz.enabled = true;

        AnimationCurve curve = new AnimationCurve();
        curve.AddKey(0.0f, 0.1f);
        curve.AddKey(0.75f, 1.0f);

        sz.size = new ParticleSystem.MinMaxCurve(scale, curve);
    }

    private void DecreaseMotorPower(ParticleSystem ps, float scale) 
    {
        var sz = ps.sizeOverLifetime;
        sz.enabled = true;

        AnimationCurve curve = new AnimationCurve();
        curve.AddKey(0.0f, 0.1f);
        curve.AddKey(0.75f, 1.0f);

        sz.size = new ParticleSystem.MinMaxCurve(scale, curve);
    }
}
