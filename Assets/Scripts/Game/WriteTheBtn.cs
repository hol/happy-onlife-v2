﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WriteTheBtn : MonoBehaviour
{
    private Text me;
    private string sentence;

    public bool write;
    public bool erase;

    string passingSentence;

    string suffix="";
    void Start()
    {
        me=transform.GetComponent<Text>();
        suffix=PlayerPrefs.GetString("linguaSuffix");
        print(suffix);
        sentence=PrepareTheSentence("Text/menu_" + suffix, 18);
        print(sentence);
    }

    string PrepareTheSentence (string fromWhere,  int n) 
	{
		//Textify from MENU
		TextAsset qAsset = null;
		string suffix = PlayerPrefs.GetString ("linguaSuffix");
		string fname = fromWhere;
		qAsset = (TextAsset)Resources.Load(fname);
		string testoIntero = qAsset.text;
		string[] strgs = testoIntero.Split ("\r\n" [0]);
		string[] strg = strgs [n].Split ('=');
		string toPass = strg[1];
		
        passingSentence = "";

		return toPass;
    }



    IEnumerator Write()
    {
        for(int i=0; i<sentence.Length; i++)
        {
            passingSentence += sentence[i].ToString();
            me.text = passingSentence;
            yield return new WaitForSeconds(.01f);
        }
    }

    IEnumerator Erase()
    {
        for(int i=0; i<sentence.Length; i++)
        {
            me.text = sentence.Substring(0, sentence.Length - (i+1));
            yield return new WaitForSeconds(.001f);
        }
        passingSentence = "";
        transform.gameObject.SetActive(false);
    }

    void Update()
    {
        if(write)
        {
            StartCoroutine(Write());
            write=false;
        }

        if(erase)
        {
            StartCoroutine(Erase());
            erase=false;
        }
    }
}
