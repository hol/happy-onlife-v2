/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class EndWinnerIs : MonoBehaviour
{
    public CoreEvents core;
    public Sound sound;
    public GetEndScore endScore;
    public GenericUtils gu;
	public AssistKeyboardGroups akg;

    // Winner card
	public Image winnerCard;
	public Image logo;
	public Image cup;
	public Text  congText;
	public Text  playerWinText;
	public Text  scorePWinText;
	public Text  p1Text;
	public Text  scoreP1Text;
	public Text  p2Text;
	public Text  scoreP2Text;
	public Text  p3Text;
	public Text  scoreP3Text;
	public Button okButtonWin;
	public Image  okBtnWinImage;
	public ParticleSystem fireworks;

	private string plr1;
	private string plr2;
    
	private void Start() {
        congText.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		playerWinText.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		scorePWinText.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		p1Text.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		scoreP1Text.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		p2Text.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		scoreP2Text.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		p3Text.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		scoreP3Text.font =  (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
        
        // Random Seed initialization
        fireworks.Stop();
    }
	// Shows and hides winner card
	public void hideWinner() {
		winnerCard.enabled = false;
		logo.enabled = false;
		congText.enabled = false;
		playerWinText.enabled = false;
		scorePWinText.enabled = false;
		cup.enabled = false;
		okButtonWin.enabled = false;
		okBtnWinImage.enabled = false;
		p1Text.enabled = false;
		scoreP1Text.enabled = false;
		p2Text.enabled = false;
		scoreP2Text.enabled = false;
		p3Text.enabled = false;
		scoreP3Text.enabled = false;
		fireworks.Stop ();
		//print("hideWinner");
	}
	
	public void showWinner() 
	{
		plr1=core.getPhraseFromId ("R00031")+" 1";
		plr2=core.getPhraseFromId ("R00031")+" 2";
		winnerCard.enabled = true;
		logo.enabled = true;
		congText.enabled = true;
		cup.enabled = true;
		playerWinText.enabled = true;
		scorePWinText.enabled = true;
		p2Text.enabled = true;
		scoreP2Text.enabled = true;
		okButtonWin.enabled = true;
		okBtnWinImage.enabled = true;

		//Capture scores
		p1Text.text = plr1;
		string p1score = core.avatar1.currentScore.ToString();
		string p2score = core.avatar2.currentScore.ToString();

		//WHO IS THE WINNER?
		if(int.Parse(p1score)==int.Parse(p2score))//Par
		{
			cup.enabled = false;
			congText.text = core.getPhraseFromId ("R00026");
			playerWinText.text="";
			playerWinText.text = plr1;
			scorePWinText.text=p1score;
			if (PlayerPrefs.GetInt("numplayers") == 1)
				p2Text.text = "Computer";
			else 
				p2Text.text = plr2;
			scoreP2Text.text=p2score;
		}
		else if(int.Parse(p1score)>int.Parse(p2score))//Player1 wins
		{
			congText.text = core.getPhraseFromId ("R00027");
			playerWinText.text = plr1;
			scorePWinText.text=p1score;
			if (PlayerPrefs.GetInt("numplayers") == 1)
			{
				p2Text.text = "Computer";
			}
			else
			{
				p2Text.text = plr2;
			}
			scoreP2Text.text=p2score;
			fireworks.Play ();
		}
		else if(int.Parse(p1score)<int.Parse(p2score))//Player2 wins
		{
			if (PlayerPrefs.GetInt("numplayers") == 1)
			{
				cup.enabled = false;
				playerWinText.text = "Computer";
				scorePWinText.text=p2score;
				p2Text.text = plr1;
				scoreP2Text.text=p1score;
				congText.text = core.getPhraseFromId ("R00028");
			}
			else 
			{
				playerWinText.text = plr2;
				scorePWinText.text=p2score;
				p2Text.text = plr1;
				scoreP2Text.text=p1score;
				congText.text = core.getPhraseFromId ("R00027");
				fireworks.Play ();
			}
		}
		if(core.playSounds)
			sound.Play("EndWinnerIs");
		if (gu.currentTurn == 2 && gu.numPlayers == 1)
			Invoke ("okWinButtonPressed", 5.0f);
	}

	public void okWinButtonPressed() 
	{
		print("X btn clicked");
		if(core.playSounds)
			sound.Play("Click");
		hideWinner();
		int s5 = PlayerPrefs.GetInt ("sc5", 0);
		print("sc1="+PlayerPrefs.GetInt ("sc1", 0));
		print("sc2="+PlayerPrefs.GetInt ("sc2", 0));
		print("sc3="+PlayerPrefs.GetInt ("sc3", 0));
		print("sc4="+PlayerPrefs.GetInt ("sc4", 0));
		print("sc5="+PlayerPrefs.GetInt ("sc5", 0));
		if (gu.currentTurn == 2 && gu.numPlayers == 1) 
		{
			if(int.Parse(scorePWinText.text)>s5) 
			{
				endScore.showScore(scorePWinText.text);
				akg.AssignFocus(4);
			}
			else
			{
				endScore.Exit();
			}
		} 
		else 
		{
			// Checks for hi-scores
			if(playerWinText.text==plr1)
			{
				if(int.Parse(scorePWinText.text)>s5)
				{
					endScore.showScore(scorePWinText.text);
					akg.AssignFocus(4);
				}
				else if(int.Parse(scoreP2Text.text)>s5)
				{
					endScore.showScore(scoreP2Text.text);
					akg.AssignFocus(4);
				}
				else
				{
					endScore.Exit();
				}
			}
			else
			{
				if(int.Parse(scorePWinText.text)>s5)
				{
					endScore.showScore(scorePWinText.text);
					akg.AssignFocus(4);
				}
				else if(int.Parse(scoreP2Text.text)>s5)
				{
					endScore.showScore(scoreP2Text.text);
					akg.AssignFocus(4);
				}
				else
				{
					endScore.Exit();
				}
			}
		}
	}
}
