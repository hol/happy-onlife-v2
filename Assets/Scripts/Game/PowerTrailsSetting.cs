﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class PowerTrailsSetting : MonoBehaviour
{
    public bool connected;
    public static bool config2=false;
    private float t;
    private float duration;

    public Image ExaMiddle;
    public Image ExaMiddle2;
    public Image Icon;
    public float offset2;
    
    public AnimationCurve curve;

    private bool go;
    private bool back;

    private Color clr;

    private Vector3 iconPos;
    private Vector3 exaPos;

    float normal;
    
    private void Start()
    {
        normal=Icon.transform.GetComponent<RectTransform>().sizeDelta.x;
        duration = 0.2f;
        go = true;
        back = false;
    }

    
    void Update()
    {
        if (connected)
        {
            if(config2)
            {
                exaPos = ExaMiddle2.transform.position;
            }
            else
            {
                exaPos = ExaMiddle.transform.position;
            }
        }
        else
        {
            offset2 = (normal) * 0.00005f;
        }
        
        if (go)
        {
            t += Time.deltaTime;
            float s = t / duration;

            iconPos = Icon.transform.position;

            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));

            if (connected)
            {
                transform.position = Vector3.Lerp(exaPos, iconPos, curve.Evaluate(s));
            }
            else
            {
                float offsetX = Mathf.Lerp(offset2, -offset2, curve.Evaluate(s));
                transform.position = new Vector3(iconPos.x + offsetX, iconPos.y, iconPos.z);
            }

            if (checker == 1)
            {
                go = false;
                t = 0;
                back = true;
            }
        }
        
        if (back)
        {
            t += Time.deltaTime;
            float s = t / duration;

            iconPos = Icon.transform.position;
            
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (connected)
            {
                transform.position = Vector3.Lerp(iconPos, exaPos, curve.Evaluate(s));
            }
            else
            {
                float offsetX = Mathf.Lerp(-offset2, offset2, curve.Evaluate(s));
                transform.position = new Vector3(iconPos.x + offsetX, iconPos.y, iconPos.z);
            }

            if (checker == 1)
            {
                back = false;
                t = 0;
                go = true;
            }
        }
    }
}
