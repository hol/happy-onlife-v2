﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewBtn : MonoBehaviour
{
    public RectTransform Img; 
    public bool on;
    private bool moveOut;
    private bool moveIn;
    private float t;
    private float duration;
    public AnimationCurve curve;
    public float xIn;

    public static bool check;

    //public RectTransform[] btnImg;
    void Start()
    {
        t = 0;
        duration = 0.2f;
        xIn=90f;
        if(on)
        {
            Img.transform.localPosition = new Vector3(xIn, 0, 0);
        }
        else
        {
            Img.transform.localPosition = new Vector3(0, 0, 0);
        }
    }

    public void SetMeOn()
    {
        if(!on)
        {
            on=true;
            moveIn=true;
            check=true;
        }
    }

    void CheckMeIn()
    {
        if(on)
        {
            on=false;
            moveOut = true;
            check = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(moveOut)
        {
            t += Time.deltaTime;
            float s = t / duration;

            float checker = Mathf.Lerp(0, 1, curve.Evaluate(s));

            float outPos = Mathf.Lerp(xIn, 0, curve.Evaluate(s));
            Img.transform.localPosition = new Vector3(outPos, 0, 0);

            if (checker == 1)
            {
                moveOut = false;
                t = 0;
            }
        }
        

        if(moveIn)
        {
            t += Time.deltaTime;
            float s = t / duration;

            float checker = Mathf.Lerp(0, 1, curve.Evaluate(s));

            float outPos = Mathf.Lerp(0, xIn, curve.Evaluate(s));
            Img.transform.localPosition = new Vector3(outPos, 0, 0);

            if (checker == 1)
            {
                moveIn = false;
                t = 0;
            }
        }
        else
        {
            if(check)
            {
                CheckMeIn();
            }
        }
    }


}