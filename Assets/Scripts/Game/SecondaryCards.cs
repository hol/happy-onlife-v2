/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class SecondaryCards : MonoBehaviour
{//Logic behind Consequense script
    [Header("Modules")]
	public CoreEvents ce;
	public Sound sound;
	public Views view;
	public GenericUtils gu;

	public void showVirus(bool virusAttack) 
	{
		if(ce.playSounds)
			sound.Play("virus");
		view.switchTo3DPlusView();

		if(!virusAttack)
		{
			WinModule.who="virus";
		}
		else
		{
			WinModule.who="virusAttack";
		}
		WinModule.prepare=true;
		//print("showVirus");
	}

	public void showLucky() 
	{
		if(ce.playSounds)
			sound.Play("Lucky");
		int tipmsg = ce.lseq [ce.l1];
		int nummsg = Mathf.Abs(tipmsg)-1;
		view.switchTo3DPlusView();
		WinModule.who="lucky";
		WinModule.what=ce.getPhraseFromId2(ce.extrapos[nummsg]);
		WinModule.howMuch=ce.extrapospoints[nummsg];
		WinModule.prepare=true;
	}
	
	public void showAVCard() 
	{
		if(ce.playSounds)
			sound.Play("AntiVirus");
		view.switchTo3DPlusView();
		WinModule.who="antivirus";
		WinModule.prepare=true;
		//print("showAVCard");
	}

    public void showWinPowercard(int cardType, int howMany) 
	{
		if(ce.playSounds)
		{
			sound.Play("loose");
		}

		WinModule.who="pwc"+howMany+"_"+cardType;
		WinModule.prepare=true;
	}
}
