﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

//Set procedural animation of appear and disappear
using UnityEngine;

public class QuestionMovement : MonoBehaviour
{
    public CoreEvents ce;
    public ViewBtn vb;
    public Transform sfumaUp;

    public Transform ViewBTN;

    public AssistKeyboardGroups akg;
    public static int rightConnection=1;

    public SizeMe txtQ;
    public SizeMe txtA1;
    public SizeMe txtA2;
    public SizeMe txtA3;
    public SizeMe txtA4;

    public static bool resizeTxt=false;
    public static bool resetTxt=false;

    private static Transform me;
    private Vector3 _from;
    private Vector3 _to;

    private Vector3 _fromSU;
    private Vector3 _toSU;

    private Vector3 _fromVB;
    private Vector3 _toVB;
    
    private float t;
    private float duration;
    private float duration2;

    public static bool moveAway;
    public static bool backIn;

    private bool firstTime=true;
    
    public AnimationCurve curve;
    void Start()
    {
        me = transform;
        _from = me.localPosition;
        _to = new Vector3(0f, -550f, -1700f);
        _fromSU = new Vector3(0f, +Screen.height*1.5f, 0f);
        _toSU = sfumaUp.localPosition;
        //sfumaUp.localPosition=_fromSU;
        _fromVB = ViewBTN.localPosition;
        _toVB = new Vector3(Screen.width+135*2, 0f, 0f);
        t = 0;
        duration = 0.5f;
        duration2 = 0.8f;
        me.localPosition = _to;
    }

    void Update()
    {
        if (resizeTxt)
        {
            txtQ.resizeH=true;
            txtA1.resizeH=true;
            txtA2.resizeH=true;
            txtA3.resizeH=true;
            txtA4.resizeH=true;
            resizeTxt=false;
        }

        if (resetTxt)
        {
            txtQ.resetH=true;
            txtA1.resetH=true;
            txtA2.resetH=true;
            txtA3.resetH=true;
            txtA4.resetH=true;
            resetTxt=false;
        }

        if (moveAway)
        {
            t += Time.deltaTime;
            float s = t / duration;
            
            me.localPosition=Vector3.Lerp(_from, _to, curve.Evaluate(s));
            sfumaUp.localPosition=Vector3.Lerp(_toSU, _fromSU, curve.Evaluate(s));
            ViewBTN.localPosition=Vector3.Lerp(_toVB, _fromVB, curve.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                Click4Question.free=true;
                moveAway = false;
                txtQ.resetH=true;
                txtA1.resetH=true;
                txtA2.resetH=true;
                txtA3.resetH=true;
                txtA4.resetH=true;
                if(!firstTime)
                {
                    transform.gameObject.SetActive(false);
                }
                else
                {
                    firstTime=false;
                }
                t = 0;
            }
        }

        if (backIn)
        {
            t += Time.deltaTime;
            float s = t / duration2;
            
            me.localPosition=Vector3.Lerp(_to, _from, curve.Evaluate(s));
            sfumaUp.localPosition=Vector3.Lerp(_fromSU, _toSU, curve.Evaluate(s));
            ViewBTN.localPosition=Vector3.Lerp(_fromVB, _toVB, curve.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                Click4Question.free=false;
                backIn = false;
                t = 0;
                if(CoreEvents.deviantClick)
                {
                    ExaPowerManager.waveOn=true;
                }
                akg.AssignFocus(rightConnection);
            }
        }
    }
}
