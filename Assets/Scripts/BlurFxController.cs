/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class BlurFxController : MonoBehaviour
{
    [Header("Blur Material")]
    public Color screenTint;
    public Shader blurShader;
    Material blurMat;
    public GameObject targetBlurry;
    RenderTexture postRenderTexture;
    public bool applyBlurMaterial;
    bool setBlurMaterial=true;

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        if(blurMat==null)
        {
            blurMat=new Material(blurShader);
        }

        if(postRenderTexture==null)
        {
            postRenderTexture=new RenderTexture(src.width, src.height, 0, src.format);
        }

        blurMat.SetColor("_ScreenTint", screenTint);

        Graphics.Blit(src, postRenderTexture, blurMat, 0);

        Shader.SetGlobalTexture("_GlobalRenderTexture", postRenderTexture);

        Graphics.Blit(src, dest);
    }

    void Update()
    {
        if(applyBlurMaterial)
        {
            if(setBlurMaterial)
            {
                if(blurMat==null)
                {
                    blurMat=new Material(blurShader);
                }

                targetBlurry.SetActive(true);
                targetBlurry.GetComponent<Renderer>().material = blurMat;

                setBlurMaterial=false;
            }
        }
        else
        {
            if(!setBlurMaterial)
            {
                targetBlurry.SetActive(false);
                setBlurMaterial=true;
            }
        }
    }
}
