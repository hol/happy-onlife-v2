/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHideSettings : MonoBehaviour
{
    public AudioSettings audioSettings;
    public GameObject target;
    public AssistKeyboardGroups asg;
    public int addressToShow=5;
    private bool boole = true;
    
    public bool stopRotation;
    public CameraRotationCustom crc;

    void Start() 
    {
        ShowHide();
    }
    
    void StopRot(bool booole)
    {
        crc.enabled=!booole;
    }

    public void ShowHide()
    {
        boole=!boole;

        target.SetActive(boole);

        audioSettings.active=boole;

        if(boole)
        {
            asg.AssignFocus(addressToShow);
        }
        else
        {
            asg.AssignFocus(0);
        }
    }
}
