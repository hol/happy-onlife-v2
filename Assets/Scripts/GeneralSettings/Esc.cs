﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

using UnityEngine;
using System.Collections;

public class Esc : MonoBehaviour {

    void Start()
    {
        //ONLY FOR DEVELOPERS
        //Reset PlayerPrefs
        //PlayerPrefs.DeleteAll();
    }

	void Update () 
	{
		if (Input.GetKey(KeyCode.Escape))
		{
            switch (PlayerPrefs.GetString("env"))
            {
                case "desk":
                    Screen.SetResolution(1024, 768, false);
                    break;
                case "mob":
                    break;
                case "web":
                    //Screen.SetResolution(Screen.width, Screen.height, false);
                    break;
            }
		}
	}
}
