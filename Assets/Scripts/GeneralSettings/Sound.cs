/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class Sound : MonoBehaviour
{
    public AudioSettings audioSettings;
    [Header("Audio Sources Game FX")]
	public AudioSource diceSound;
	public AudioSource luckyCard;
	public AudioSource clickSound;
	public AudioSource virusCard;
	public AudioSource posansCard;
	public AudioSource negansCard;
	public AudioSource moveCard;
	public AudioSource powerCard;
	public AudioSource antivirusCard;
	public AudioSource hiscoreCard;
	public AudioSource winnerCard;
	public AudioSource winPC;
	public AudioSource challengeSound;
	public AudioSource points;
	public AudioSource redCell;

    [Header("Audio Sources SpaceShip FX")]
    public AudioSource antivirus;
    public AudioSource electrify;
    public AudioSource explosion;
    public AudioSource lucky;
    public AudioSource virusVsAntivirus;
    public AudioSource startFlight;

    public AudioSource[] allAudioMagazine;
    public AudioSource[] allAudio;

    void Start () 
	{
		AudioListener.volume=1;
        allAudioMagazine=new AudioSource[21];

        allAudioMagazine[0]=diceSound;
        allAudioMagazine[1]=luckyCard;
        allAudioMagazine[2]=clickSound;
        allAudioMagazine[3]=virusCard;
        allAudioMagazine[4]=posansCard;
        allAudioMagazine[5]=negansCard;
        allAudioMagazine[6]=moveCard;
        allAudioMagazine[7]=powerCard;
        allAudioMagazine[8]=antivirusCard;
        allAudioMagazine[9]=hiscoreCard;
        allAudioMagazine[10]=winnerCard;
        allAudioMagazine[11]=winPC;
        allAudioMagazine[12]=challengeSound;
        allAudioMagazine[13]=points;
        allAudioMagazine[14]=redCell;
        allAudioMagazine[15]=antivirus;
        allAudioMagazine[16]=electrify;
        allAudioMagazine[17]=explosion;
        allAudioMagazine[18]=lucky;
        allAudioMagazine[19]=virusVsAntivirus;
        allAudioMagazine[20]=startFlight;

        int counter=0;

        for(int i=0; i<allAudioMagazine.Length; i++)
		{
            if(allAudioMagazine[i]!=null)
            {
                counter++;
            }
		}

        print("Volume counter="+counter);
        allAudio=new AudioSource[counter];
        int incr=0;

        for(int i=0; i<allAudioMagazine.Length; i++)
		{
            if(allAudioMagazine[i]!=null)
            {
                allAudio[incr]=allAudioMagazine[i];
                incr++;
            }
		}
        
        audioSettings.audioSourceFX=allAudio;
    }

    public void PlayClick()
    {
        clickSound.Play();
    }
    
    public void Play(string what)
    {
        switch (what)
        {
            case "Click":
                clickSound.Play();
                break;
            case "HighScore":
                hiscoreCard.Play();
                break;
            case "loose":
                winPC.Play();
                break;
            case "EndWinnerIs":
                winnerCard.Play();
                break;
            case "dice":
                diceSound.Play();
                break;
            case "virus":
                virusCard.Play();
                break;
            case "antivirus":
                antivirusCard.Play();
                break;
            case "lucky":
                luckyCard.Play();
                break;
            case "challenge":
                challengeSound.Play();
                break;
            case "PosAns":
                posansCard.Play();
                break;
            case "NegAns":
                negansCard.Play();
                break;
            case "antivirusSS":
                antivirus.Play();
                break;
            case "electrifySS":
                electrify.Play();
                break;
            case "explosionSS":
                explosion.Play();
                break;
            case "luckySS":
                lucky.Play();
                break;
            case "virusSS":
                virusVsAntivirus.Play();
                break;
            case "startSS":
                startFlight.Play();
                break;
            case "points":
                points.Play();
                break;
            case "power":
                powerCard.Play();
                break;
            case "redCell":
                redCell.Play();
                break;
        }
    }
}
