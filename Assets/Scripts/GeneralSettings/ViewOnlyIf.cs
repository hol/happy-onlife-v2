/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewOnlyIf : MonoBehaviour
{
    private Text textModule;
    public static bool permission=false;
    public static bool denied=false;
    public Textify txtf;
    public static bool textify=false;

    // Start is called before the first frame update
    void Start()
    {
        textModule=transform.GetComponent<Text>();
    #if UNITY_WEBGL && !UNITY_EDITOR
            permission=true;
    #endif
    #if !UNITY_WEBGL
        print("Language="+PlayerPrefs.GetString("linguaSuffix"));
        if(PlayerPrefs.GetString("linguaSuffix")=="en")
        {
            permission=true;
        }
    #endif
    }

    void Textify()
    {
        txtf.OnDemand ("reader", 0, true);
    }

    // Update is called once per frame
    void Update()
    {
        if(permission)
        {
        #if UNITY_WEBGL && !UNITY_EDITOR
            Textify();
            textModule.enabled=true;
        #endif
            permission=false;
        }

        if(denied)
        {
            textModule.enabled=false;
            denied=false;
        }
    }
}
