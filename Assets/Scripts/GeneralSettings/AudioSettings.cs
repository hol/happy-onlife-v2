﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class AudioSettings : MonoBehaviour 
{
	[Header("Audio Sources")]
	public AudioSource audioSourceMusic;
	public Sound sound;
	public AudioSource[] audioSourceFX;

	[Header("Slider")]
	public Slider musicSlider;
	public Slider fxSlider;
	public bool active;

	[Header("Volume")]
	public float musicVol;
	public float fxVol;
	public static float maxHeight;
	public static float maxLength;
	public static float maxSize;

	void Start () 
	{
		AudioListener.volume=1;
		//audioSourceFX=sound.allAudio;

		//Music
		if(PlayerPrefs.GetString("muteM")!=null)
		{
			musicVol = PlayerPrefs.GetFloat("volM");
			musicSlider.value = musicVol;
		}
		else
		{
			musicVol = musicSlider.value;
		}

		//SoundFX
		if(PlayerPrefs.GetString("muteFX")!=null)
		{
			fxVol = PlayerPrefs.GetFloat("volFX");
			fxSlider.value = fxVol;
		}
		else
		{
			fxVol = fxSlider.value;
		}
		
		OnValueChanged ();
	}

	void Update() 
	{
		if(active)
		{
			musicVol = musicSlider.value;
			fxVol = fxSlider.value;

			OnValueChanged();
		}
	}

	public void OnValueChanged()
	{
		//Music
		audioSourceMusic.volume=musicVol;
		//print("Music.volume="+musicVol);
		
		PlayerPrefs.SetFloat("volM", musicVol);
		if (musicVol == 0)
		{
			PlayerPrefs.SetString("muteM", "yes");
		}
		else
		{
			PlayerPrefs.SetString("muteM", "no");
		}

		//SoundFX
		for(int i=0; i<audioSourceFX.Length; i++)
		{
			audioSourceFX[i].volume=fxVol;
		}
		//print("FX.volume="+fxVol);

		PlayerPrefs.SetFloat("volFX", fxVol);
		if (fxVol == 0)
		{
			PlayerPrefs.SetString("muteFX", "yes");
		}
		else
		{
			PlayerPrefs.SetString("muteFX", "no");
		}
	}
}
