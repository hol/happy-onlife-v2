/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class AssistKeyboardGroups : MonoBehaviour
{
    [Header("Pages")]
    public AssistKeyboard[] grps;
    public int focus;
    public bool popup;
    private int focusStore;
    private int whoStore=-1;
    void Start()
    {
        focus=0;
        focusStore=focus;
        AssignFocus(focus);
    }

    public void AssignFocus(int who)
    {
        if(whoStore!=-1)
        {
            who=whoStore;
            whoStore=-1;
        }

        if(popup)
        {
            whoStore=focus;
            popup=false;
        }

        for(int i=0; i<grps.Length; i++)
        {
            if(i!=who)
            {
                grps[i].focusPage=false;
                grps[i].activ=false;
            }
            else
            {
                grps[who].focusPage=true;
            }
        }
        //print("focus on "+grps[who].name);
    }

    void Update()
    {
        //MenuMovement will provide every change of focus//
        if(focus!=focusStore)
        {
            AssignFocus(focus);
            print("focus="+focus+" focusStore="+focusStore);
            focusStore=focus;
        }
    }
}
