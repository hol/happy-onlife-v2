/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class AssistKeyboardElement : MonoBehaviour
{
    private Text txt;
    private RectTransform rt;
    [Header("Button (only if it can disabled)")]
    private Button buttonTarget;
    public GameObject buttonTargetObj;
    public bool sameBtnSize;
    public float offsetPadding;

    [Header("Outline Model")]
    public GameObject instance;
    private GameObject target;
    private Outline outline;
    private float xSize;
    private float xSizeStore;
    private float ySize;
    private float ySizeStore;
    private float zSize;
    public bool customPos;
    public Vector3 customPosition;
    public float zDefault=100f;
    public bool customScale;
    public Vector3 customSize;

    [Header("InEditMode (Change value then check)")]
    public bool check;

    [Header("Action")]
    public UnityEvent whatToDo;

    [Header("Camera Status")]
    public bool prospective;

    [Header("Behaviour Settings")]
    public bool notUI;
    public bool selectable;
    public bool disabled;
    private bool disabledStore;
    public bool multiple;
    public bool focusElement;
    public bool noMoreThanHalf;
    private bool interact=true;
    public bool popup;
    public bool scrollerUp;
    public bool scrollerDown;
    public ScrollRect scrollRect;
    public bool slider;
    public AudioSettings audioMSettings;

    [Header("Outline")]
    public Color color;
    public float width;

    void Start()
    {
        target=Instantiate (instance, Vector3.zero , Quaternion.identity);
        target.transform.parent = transform;
        target.transform.localPosition=Vector3.zero;
        target.transform.localRotation=Quaternion.identity;

        outline=target.GetComponent<Outline>();

        if(buttonTargetObj!=null)
        {
            buttonTarget=buttonTargetObj.GetComponent<Button>();
        }

        if(prospective)
        {
            zSize=10;
        }
        else
        {
            zSize=100;
        }

        if(!notUI)
        {
            txt=transform.GetComponent<Text>();
            rt=transform.GetComponent<RectTransform>();
            FindDimension();
            FindPosition();
        }
        else
        {
            if(customScale)
            {
                target.transform.localScale=customSize;
            }
            else
            {
                target.transform.localScale=new Vector3(10, 10, 10);
            }

            if(customPos)
            {
                target.transform.localPosition=customPosition;
            }
        }

        outline.OutlineColor=color;
        outline.OutlineWidth=width;
        
        target.SetActive(false);
        
    }

    void FindDimension()
    {
        if(sameBtnSize && buttonTargetObj!=null)
        {
            target.transform.localScale=new Vector3(
                buttonTargetObj.GetComponent<RectTransform>().sizeDelta.x+offsetPadding, 
                buttonTargetObj.GetComponent<RectTransform>().sizeDelta.y+offsetPadding, 
                10);
        }
        else if(customScale)
        {
            target.transform.localScale=customSize;
        }
        else if(notUI)
        {
            customSize=transform.GetComponent<BoxCollider>().size;
            target.transform.localScale=customSize;
        }
        else
        {
            xSize=rt.sizeDelta.x;
            if(txt!=null)
            {
                //print("Fix Xsize");
                xSize=txt.preferredWidth;
                if(noMoreThanHalf)
                {
                    if(xSize>Screen.width/5)
                    {
                        xSize=Screen.width/5;
                        //print("noMoreThanHalf.name="+transform.name);
                    }
                }
            }
            xSizeStore=xSize;
            
            ySize=rt.sizeDelta.y;
            if(txt!=null)
            {
                //print("Fix Ysize");
                ySize=txt.preferredHeight;
                if(noMoreThanHalf)
                {
                    if(ySize<Screen.height/8)
                    {
                        ySize=Screen.height/8;
                        //print("noMoreThanHalf.name="+transform.name);
                    }
                }
            }
            ySizeStore=ySize;
            
            target.transform.localScale=new Vector3(xSize, ySize, zSize);
        }
    }

    void FindPosition()
    {
        if(customPos)
        {
            target.transform.localPosition=customPosition;
        }
        else if(notUI)
        {
            customPosition=transform.GetComponent<BoxCollider>().center;
            target.transform.localPosition=customPosition;
        }
        else
        {
            if(txt==null)
                target.transform.localPosition=new Vector3(0, 0, zDefault);
            else if(txt.alignment==TextAnchor.MiddleCenter)
                target.transform.localPosition=new Vector3(0, 0, zDefault);
            else if(txt.alignment==TextAnchor.MiddleRight)
                target.transform.localPosition=new Vector3(-xSize*0.5f, 0, zDefault);
            else if(txt.alignment==TextAnchor.MiddleLeft)
                target.transform.localPosition=new Vector3(xSize*0.5f, 0, zDefault);
        }
    }

    void ShowMe()
    {
        target.SetActive(true);
    }

    void HideMe()
    {
        target.SetActive(false);
    }

    void Update()
    {
        if(check)
        {
            FindDimension();
            FindPosition();
            outline.OutlineColor=color;
            outline.OutlineWidth=width;
            check=false;
        }

        if(buttonTargetObj!=null && !sameBtnSize)
        {
            disabled=!buttonTarget.interactable;
        }

        if(selectable)
        {
            if(focusElement)
            {
                if(interact)
                {
                    if(!notUI)
                    {
                        FindDimension();
                        FindPosition();
                    }
                    ShowMe();
                }
                interact=false;
            }
            else
            {
                if(!interact)
                {
                    HideMe();
                }
                interact=true;
            }
        }
    }
}
