/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class AssistKeyboard : MonoBehaviour
{
    public AssistKeyboardGroups father;
    [Header("Passive Elements")]
    public GameObject[] unselectable;

    [Header("Always Active Elements")]
    public AssistKeyboardElement alwaysFirst;
    public AssistKeyboardElement[] selectable;

    [Header("Not Always Active Elements")]
    public bool notAlways;
    public bool before;
    public AssistKeyboardElement[] selectable2;
    public bool[] when;
    public AssistKeyboardElement[] grpSel;

    [Header("Calculate Variables")]
    public bool focusPage;
    public bool activ;
    public bool rightShape3D;
    private bool rightShaped;
    private bool checkd;
    private bool switchy=true;
    private bool activStore;
    public int nTarget;

    private void Start() 
    {
        Calculate();
    }

    public void Calculate()
    {
        if(notAlways)
        {
            int many=0;

            for(int i=0; i<selectable2.Length; i++)
            {
                if(when[i])
                {
                    many++;
                }
            }

            int manyTot=selectable2.Length+selectable.Length;
            int manyValid=many+selectable.Length;

            if(alwaysFirst!=null)
            {
                manyTot=selectable2.Length+selectable.Length+1;
                manyValid=many+selectable.Length+1;
            }

            grpSel=new AssistKeyboardElement[manyValid];

            int incr=0;

            if(before)
            {
                for(int i=0; i<manyTot; i++)
                {
                    if(alwaysFirst!=null)
                    {
                        if(i==0)
                        {
                            grpSel[incr]=alwaysFirst;
                            grpSel[incr].check=true;
                            incr++;
                        }
                        else if(i<selectable2.Length+1)
                        {
                            if(when!=null)
                            {
                                if(when[i-1])
                                {
                                    grpSel[incr]=selectable2[i-1];
                                    grpSel[incr].check=true;
                                    incr++;
                                }
                            }
                        }
                        else
                        {
                            grpSel[incr]=selectable[i-1-selectable2.Length];
                            grpSel[incr].check=true;
                            incr++;
                        }
                    }
                    else
                    {
                        if(i<selectable2.Length)
                        {
                            if(when!=null)
                            {
                                if(when[i])
                                {
                                    grpSel[incr]=selectable2[i];
                                    grpSel[incr].check=true;
                                    incr++;
                                }
                            }
                        }
                        else
                        {
                            grpSel[incr]=selectable[i-selectable2.Length];
                            grpSel[incr].check=true;
                            incr++;
                        }
                    }
                }
            }
            else
            {
                for(int i=0; i<manyTot; i++)
                {
                    if(alwaysFirst!=null)
                    {
                        if(i==0)
                        {
                            grpSel[incr]=alwaysFirst;
                            grpSel[incr].check=true;
                            incr++;
                        }
                        else if(i<selectable.Length+1)
                        {
                            grpSel[incr]=selectable[i-1];
                            grpSel[incr].check=true;
                            incr++;
                        }
                        else
                        {
                            if(when!=null)
                            {
                                if(when[i-selectable.Length])
                                {
                                    grpSel[incr]=selectable2[i-1-selectable.Length];
                                    grpSel[incr].check=true;
                                    incr++;
                                }
                            }
                        }
                    }
                    else
                    {
                        if(i<selectable.Length)
                        {
                            grpSel[incr]=selectable[i];
                            grpSel[incr].check=true;
                            incr++;
                        }
                        else
                        {
                            if(when!=null)
                            {
                                if(when[i-selectable.Length])
                                {
                                    grpSel[incr]=selectable2[i-selectable.Length];
                                    grpSel[incr].check=true;
                                    incr++;
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            grpSel=selectable;
        }
        //print("CALCULATED "+transform.name);
    }

    public void Switchy()
    {
        if(switchy)
        {
            switchy=false;
        }
        else
        {
            switchy=true;
        }
    }

    void FixShapeOnBoxCollider()
    {
        for(int i=0; i<selectable.Length; i++)
        {
            if(selectable[i].notUI)
            {
                selectable[i].check=true;
            }
        }
        rightShaped=true;
    }
    
    void Update()
    {
        if(focusPage)
        {
            if(rightShape3D)
            {
                if(!rightShaped)
                {
                    FixShapeOnBoxCollider();
                }
            }
            if (Input.GetKeyDown("return"))
            {
                if(nTarget!=-1 && grpSel[nTarget].focusElement)
                {
                    if(!grpSel[nTarget].multiple)
                    {
                        Reset();
                    }
                    if(grpSel[nTarget].popup)
                    {
                        if(switchy)
                        {
                            father.popup=true;
                        }
                    }
                    print("Return on nTarget="+nTarget+" of grpSel="+grpSel[nTarget].name);
                    if(grpSel[nTarget].scrollerUp)
                    {
                        grpSel[nTarget].scrollRect.verticalNormalizedPosition+=0.1f;
                    }
                    else if(grpSel[nTarget].scrollerDown)
                    {
                        grpSel[nTarget].scrollRect.verticalNormalizedPosition-=0.1f;
                    }
                    else
                    {
                        if(!grpSel[nTarget].disabled)
                        {
                            grpSel[nTarget].whatToDo.Invoke();
                        }
                    }
                }
            }

            if (Input.GetKeyDown("right") || Input.GetKeyDown(KeyCode.Tab))
            {
                if(!checkd)
                {
                    Calculate();
                }

                if(!activ)
                {
                    activ=true;
                }
                
                if(nTarget!=-1)
                {
                    grpSel[nTarget].focusElement=false;
                }
                
                nTarget++;
                
                if(nTarget>grpSel.Length-1)
                {
                    nTarget=0;
                }

                if(!grpSel[nTarget].gameObject.activeSelf)
                {
                    nTarget++;
                }
                
                print("nTarget="+nTarget+" of grpSel="+grpSel[nTarget].name);
                
                grpSel[nTarget].focusElement=true;
            }

            if (Input.GetKeyDown("left"))
            {
                if(!checkd)
                {
                    Calculate();
                }

                if(!activ)
                {
                    activ=true;
                }
                
                if(nTarget!=-1)
                {
                    grpSel[nTarget].focusElement=false;
                }
                
                nTarget--;
                
                if(nTarget<0)
                {
                    nTarget=grpSel.Length-1;
                }
                print("nTarget="+nTarget+" of grpSel="+grpSel[nTarget].name);
                grpSel[nTarget].focusElement=true;
            }

            if(Input.GetKey("down"))
            {
                if(grpSel[nTarget].slider)
                {
                    grpSel[nTarget].audioMSettings.musicVol -= 0.01f;
                }
            }
            else if(Input.GetKey("up"))
            {
                if(grpSel[nTarget].slider)
                {
                    grpSel[nTarget].audioMSettings.fxVol += 0.01f;
                }
            }
        }
        else
        {
            if(checkd)
            {
                checkd=false;
                print("ready to recheck "+transform.name);
            }
        }

        if(!activ)
        {
            nTarget=-1;
        }
    }

    public void AddNASelectable(AssistKeyboardElement oneMore)
    {
        print("Trying to ADD "+oneMore);
        AssistKeyboardElement[] grpTemp=new AssistKeyboardElement[selectable2.Length+1];
        when=new bool[grpTemp.Length];

        for(int i=0; i<grpTemp.Length; i++)
        {
            if(i==0)
            {
                grpTemp[i]=oneMore;
            }
            else
            {
                grpTemp[i]=selectable2[i-1];
            }
            when[i]=true;
        }

        selectable2=grpTemp;
    }

    public void RemoveNASelectable(string oneLess)
    {
        print("Trying to REMOVE " + oneLess);
        AssistKeyboardElement[] grpTemp=new AssistKeyboardElement[selectable2.Length-1];
        when=new bool[grpTemp.Length];
        int incr=0;

        for(int i=0; i<selectable2.Length; i++)
        {
            if(selectable2[i].name!=oneLess)
            {
                grpTemp[incr]=selectable2[incr];
                when[incr]=true;
                incr++;
            }
            else
            {
                print(oneLess + " REMOVED from list");
            }
        }

        selectable2=grpTemp;
        Calculate();
    }

    public void Reset()
    {
        for(int i=0; i<grpSel.Length; i++)
        {
            grpSel[i].focusElement=false;
        }
    }
}
