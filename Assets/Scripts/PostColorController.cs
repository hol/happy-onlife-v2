/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class PostColorController : MonoBehaviour
{
    [Header("Color Filter")]
    public Color screenTint;
    public Shader postColorShader;
    Material postColorMat;
    RenderTexture postRenderTexture;
    public bool applyColorFilter;

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        if(applyColorFilter)
        {
            if(postColorMat==null)
            {
                postColorMat=new Material(postColorShader);
            }

            if(postRenderTexture==null)
            {
                postRenderTexture=new RenderTexture(src.width, src.height, 0, src.format);
            }

            postColorMat.SetColor("_ScreenTint", screenTint);

            Graphics.Blit(src, postRenderTexture, postColorMat, 0);

            Shader.SetGlobalTexture("_GlobalRenderTexture", postRenderTexture);
        
            Graphics.Blit(postRenderTexture, dest);
        }
        else
        {
            Graphics.Blit(src, dest);
        }
    }
}
