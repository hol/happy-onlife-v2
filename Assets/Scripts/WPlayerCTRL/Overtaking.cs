/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using UnityEngine;

public class Overtaking : MonoBehaviour
{
    private MoveOnPath MOP;
    private Transform target;
    private Idle IDLE;
    public float highness;
    private float storePosY;
    private float startPosY;
    private float endPosY;
    
    [Header("Switchers")]
    public bool moving;
    public bool safe;
    public bool lastStep;
    public bool up;
    public bool go;
    private bool prepare;
    public bool back;
    private bool prepareBack;

    [Header("Animation Settings")]
    private float t;
    private float t2;
    public float duration=1.14f;
    public float duration2=1.5f;
    public AnimationCurve curve;
    public AnimationCurve curve2;

    void Start()
    {
        MOP=transform.GetComponent<MoveOnPath>();
        target=MOP.target;

        IDLE=transform.GetComponent<Idle>();

        safe=true;
        prepare=true;
        prepareBack=true;
    }

    public void Moving()
    {
        lastStep=false;
        moving=true;
        
        if(up)
        {
            safe=MOP.CheckOpponentOnNextCell();

            if(safe)
            {
                IDLE.ResetBase();
                back=true;
            }
        }
    }

    public void Stopping()
    {
        moving=false;
    }

    void OnTriggerStay(Collider col)
    {
        if(moving)
        {
            if(!go)
            {
                safe=MOP.TriggeredOpponentOnPath();

                if(!safe)
                {
                    print("WARNING! Start Overtaking NOW!!!");

                    if(back)
                    {
                        back = false;
                        prepareBack=true;
                        t2 = 0;
                        print("Overtaking(Back)Blocked!");
                        up=false;
                    }
                    
                    go=true;
                }

                Debug.Log("Collinding official with "+col.gameObject.name);
            }
            //Debug.Log("Collision with "+col.gameObject.name);
        }
    }

    public void EmergencyOverTaking()
    {
        go=true;
        print("EmergencyOverTaking");
    }

    void Update()
    {
        if(go)
        {
            if(prepare)
            {
                startPosY=target.position.y;
                endPosY=target.position.y+highness;
                prepare=false;
                print("Overtaking(Go)Started!");
            }

            //Time
            t += Time.deltaTime;
            float s = t / duration;

            //Y Position
            float transPosY = Mathf.Lerp(startPosY, endPosY, curve.Evaluate(s));
            target.position = new Vector3(target.position.x, transPosY, target.position.z);
            
            float checker = Mathf.Lerp(0f, 1f, curve.Evaluate(s));            
            
            if (checker == 1)
            {
                go = false;
                print("Overtaking(Go)Arrived!");
                lastStep=MOP.CheckLastStep();
                if(lastStep)
                {
                    back=false;
                    IDLE.baseY=target.position.y;
                    up=true;
                }
                else
                {
                    back=true;
                    up=false;
                }
                t = 0;
                prepare=true;
            }
        }

        if(back)
        {
            if(prepareBack)
            {
                startPosY=target.position.y;
                endPosY=target.position.y-highness;
                prepareBack=false;
                print("Overtaking(Back)Started!");
            }

            //Time
            t2 += Time.deltaTime;
            float s2 = t2 / duration2;

            //Position
            float transPosY = Mathf.Lerp(startPosY, endPosY, curve2.Evaluate(s2));
            target.position = new Vector3(target.position.x, transPosY, target.position.z);
            
            float checker = Mathf.Lerp(0f, 1f, curve2.Evaluate(s2));            
            
            if (checker == 1)
            {
                back = false;
                prepareBack=true;
                t2 = 0;
                print("Overtaking(Back)Arrived!");
                up=false;
            }
        }
    }
}
