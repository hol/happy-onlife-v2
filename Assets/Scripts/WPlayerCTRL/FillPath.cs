/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillPath : MonoBehaviour
{
    private MoveOnPath MOP;
    public Transform exaStart;
    public Transform exaFinish;
    public Transform[] exaPath;

    public bool passAll;
    void Start()
    {
        MOP=transform.GetComponent<MoveOnPath>();
    }

    // Update is called once per frame
    void Update()
    {
        if(passAll)
        {
            MOP.startPoint=exaStart;
            MOP.finishPoint=exaFinish;
            MOP.points=exaPath;
            MOP.Initialize();
            passAll=false;
        }
    }
}
