/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.Events;

public class WPlayerCTRL : MonoBehaviour
{
    public MoveOnPath target;

    [Header("Specific Variables")]
    public int numAntivirus=0;
    public bool virus;
    public int[] pcOwned;
    public int currentScore=0;

    [Header("Switchers")]
    public bool invokeAtTheEnd;

    [Header("CallBack Event and Data")]
    public UnityEvent endAction;
    public int finalDest;

    void Start() 
    {
        pcOwned = new int[8] {0,0,0,0,0,0,0,0};
    }

    public void Go(int howMany)
    {
        target.steps=howMany;
        target.go=true;
    }

    public void Stop()
    {
        finalDest=target.destination;

        if(invokeAtTheEnd)
        {
            endAction.Invoke();
        }
    }


}
