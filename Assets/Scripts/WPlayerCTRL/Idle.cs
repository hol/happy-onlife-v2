/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Idle : MonoBehaviour
{
    private MoveOnPath MOP;
    private Transform target;

    [Header("Movement")]
    public bool xAxis;
    public float axisX;
    public bool yAxis;
    public float axisY;
    public bool zAxis;
    public float axisZ;
    
    [Header("Base Data\r\n(If they remain to 0,\r\ninitial position will be stored)")]
    public float baseX;
    public float baseY;
    public float baseZ;
    public float baseXstore;
    public float baseYstore;
    public float baseZstore;
    private Vector3 actualPos;
    private Vector3 transPos;
    private float startPosX;
    private float endPosX;
    private float transPosX;
    private float startPosY;
    private float endPosY;
    private float transPosY;
    private float startPosZ;
    private float endPosZ;
    private float transPosZ;
    

    [Header("Switchers")]
    public bool fromStart;
    public bool go;
    private bool prepare;
    public bool back;
    private bool prepareBack;
    public bool stop;
    public bool start;

    [Header("Animation Settings")]
    private float t;
    private float t2;
    public float duration;
    private float storeDuration;
    public AnimationCurve curve;
    public AnimationCurve curve2;

    void Start()
    {
        MOP=transform.GetComponent<MoveOnPath>();
        target=MOP.target;

        if(baseX==0)
        {
            baseX=transform.position.x;
        }
        if(baseY==0)
        {
            baseY=transform.position.y;
        }
        if(baseZ==0)
        {
            baseZ=transform.position.z;
        }

        //Storing Base
        baseXstore=baseX;
        baseYstore=baseY;
        baseZstore=baseZ;

        prepare=true;
        prepareBack=true;

        if(fromStart)
        {
            go=true;
        }

    }
    public void Stop()
    {
        stop=true;
        ResetAnimationSettings();
    }

    void ResetAnimationSettings()
    {
        go=false;
        prepare=true;
        t=0;
        back=false;
        prepareBack=true;
        t2=0;
    }

    public void Restart()
    {
        start=true;
        print("Restart IDLE");
    }

    public void ResetBase()
    {
        baseX=baseXstore;
        baseY=baseYstore;
        baseZ=baseZstore;
    }

    // Update is called once per frame
    void Update()
    {
        if(start)
        {
            stop=false;
            go=true;
            start=false;
        }

        if(!stop)
        {
            if(go)
            {
                if(prepare)
                {
                    if(xAxis)
                    {
                        startPosX=transform.position.x;
                        endPosX=baseX+axisX;
                    }
                    if(yAxis)
                    {
                        startPosY=transform.position.y;
                        endPosY=baseY+axisY;
                    }
                    if(zAxis)
                    {
                        startPosZ=transform.position.z;
                        endPosZ=baseZ+axisZ;
                    }
                    prepare=false;
                    //print("Idle(Go)Started!");
                }

                //Time
                t += Time.deltaTime;
                float s = t / duration;

                //Position
                if(xAxis)
                {
                    transPosX = Mathf.Lerp(startPosX, endPosX, curve.Evaluate(s));
                    transform.position = new Vector3(transPosX, transform.position.y, transform.position.z);
                }
                if(yAxis)
                {
                    transPosY = Mathf.Lerp(startPosY, endPosY, curve.Evaluate(s));
                    transform.position = new Vector3(transform.position.x, transPosY, transform.position.z);
                }
                if(zAxis)
                {
                    transPosZ = Mathf.Lerp(startPosZ, endPosZ, curve.Evaluate(s));
                    transform.position = new Vector3(transform.position.x, transform.position.y, transPosZ);
                }
                
                float checker = Mathf.Lerp(0f, 1f, curve.Evaluate(s));            
                
                if (checker == 1)
                {
                    go = false;
                    ////print("Now ActualPoint="+actualPoint);
                    prepare=true;
                    t = 0;
                    //print("Idle(Go)Arrived!");
                    back=true;
                }
            }

            if(back)
            {
                if(prepareBack)
                {
                    if(xAxis)
                    {
                        startPosX=baseX+axisX;
                        endPosX=baseX;
                    }
                    if(yAxis)
                    {
                        startPosY=baseY+axisY;
                        endPosY=baseY;
                    }
                    if(zAxis)
                    {
                        startPosZ=baseZ+axisZ;
                        endPosZ=baseZ;
                    }
                    prepareBack=false;
                    //print("Idle(Back)Started!");
                }

                //Time
                t2 += Time.deltaTime;
                float s2 = t2 / duration;

                //Position
                if(xAxis)
                {
                    transPosX = Mathf.Lerp(startPosX, endPosX, curve.Evaluate(s2));
                    transform.position = new Vector3(transPosX, transform.position.y, transform.position.z);
                }
                if(yAxis)
                {
                    transPosY = Mathf.Lerp(startPosY, endPosY, curve.Evaluate(s2));
                    transform.position = new Vector3(transform.position.x, transPosY, transform.position.z);
                }
                if(zAxis)
                {
                    transPosZ = Mathf.Lerp(startPosZ, endPosZ, curve.Evaluate(s2));
                    transform.position = new Vector3(transform.position.x, transform.position.y, transPosZ);
                }
                
                float checker = Mathf.Lerp(0f, 1f, curve.Evaluate(s2));            
                
                if (checker == 1)
                {
                    back = false;
                    ////print("Now ActualPoint="+actualPoint);
                    prepareBack=true;
                    t2 = 0;
                    //print("Idle(Back)Arrived!");

                    if(!stop)
                    {
                        back=false;
                        go=true;
                    }
                }
            }
        }
    }
}
