/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

//Animation on auto rotation on Y axe
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimRot : MonoBehaviour
{
    public bool go;
    public bool stop;
    public float incr;
    public float incrMax;
    public float incremental;
    public AnimationCurve rotCurve;
    public AnimationCurve stopCurve;

    public void Go()
    {
        go=true;
    }

    public void Stop()
    {
        stop=true;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, incremental*Time.deltaTime, 0);
        if(go)
        {
            if(incremental<=incrMax)
            {
                incremental+=rotCurve.Evaluate(Time.time)*incr;
            }
            else
            {
                go=false;
            }
        }
        if(stop)
        {
            if(incremental>=0.0f)
            {
                incremental-=stopCurve.Evaluate(Time.time)*incr;
            }
            else
            {
                incremental=0f;
                stop=false;
            }
        }
    }
}
