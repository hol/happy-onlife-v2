﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class AvatarYAxisRotation : MonoBehaviour {
	/*
	 * This function is assigned to the disk instance
	 * It makes the smooth rotation along the disk Y axis.
	 * Using the transform.Rotate function.
	 */
	[Header("Rotation")]
	public float intensity=20f;
	void Update () 
	{
		transform.Rotate(0,intensity*Time.deltaTime,0);
	}
}
