/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Inclination : MonoBehaviour
{
    private MoveOnPath MOP;
    private Transform target;
    private Overtaking OT;
    private Vector3 actualRot;
    private float startRot;
    private float endRot;
    private float transRot;

    [Header("Inclination")]
    public float leave;
    public float arrive;

    [Header("Switchers")]
    public bool go;
    public bool prepare;
    public bool back;
    public bool prepareBack;
    public bool invokeAtTheEnd;

    [Header("Animation Settings")]
    private float t;
    private float t_2;
    private float t2;
    private float t2_2;
    public float startDuration;
    public float startReleaseDuration;
    public float endDuration;
    public float endReleaseDuration;
    public AnimationCurve curve;
    public AnimationCurve curve_2;
    public AnimationCurve curve2;
    public AnimationCurve curve2_2;

    [Header("End Event")]
    public UnityEvent endAction;

    void Start()
    {
        MOP=transform.GetComponent<MoveOnPath>();
        target=MOP.target;

        OT=transform.GetComponent<Overtaking>();

        prepare=true;
        prepareBack=true;

        t=0;
        t_2=0;
        t2=0;
        t2_2=0;
    }

    public void Forward()
    {
        go=true;
    }

    public void Back()
    {
        back=true;
    }

    // Update is called once per frame
    void Update()
    {
        if(go)
        {
            if(prepare)
            {
                startRot=0f;
                //print("startRot="+startRot);
                endRot=arrive;
                prepare=false;
                //print("Inclination(Forward)Started!");
            }

            //Time Influence Path
            t += Time.deltaTime;
            float s = t / startDuration;

            //Rotation
            //Principal Path
            transRot = Mathf.Lerp(startRot, endRot, curve.Evaluate(s));
            target.eulerAngles= new Vector3(transRot, target.eulerAngles.y, 0f);
            
            float checker = Mathf.Lerp(0f, 1f, curve.Evaluate(s));            
            
            if (checker == 1)
            {
                //Time Influence Path
                t_2 += Time.deltaTime;
                float s_2 = t_2 / startReleaseDuration;

                //Rotation
                //Principal Path
                transRot = Mathf.Lerp(endRot, startRot, curve_2.Evaluate(s_2));
                target.eulerAngles= new Vector3(transRot, target.eulerAngles.y, 0f);
                
                float checker_2 = Mathf.Lerp(0f, 1f, curve.Evaluate(s_2));

                if (checker_2 == 1)
                {
                    go = false;
                    ////print("Now ActualPoint="+actualPoint);
                    prepare=true;
                    t = 0;
                    t_2 = 0;
                    print("Inclination(Forward)Arrived!");

                    if(invokeAtTheEnd)
                    {
                        endAction.Invoke();
                    }
                }
            }
        }

        if(back)
        {
            if(prepareBack)
            {
                startRot=0f;
                //print("startRot="+startRot);
                endRot=leave;
                prepareBack=false;
                //print("Inclination(Back)Started!");
            }

            //Time Influence Path
            t2 += Time.deltaTime;
            float s2 = t2 / endDuration;

            //Rotation
            //Principal Path
            transRot = Mathf.Lerp(startRot, endRot, curve.Evaluate(s2));
            target.eulerAngles= new Vector3(transRot, target.eulerAngles.y, 0f);
            
            float checker2 = Mathf.Lerp(0f, 1f, curve.Evaluate(s2));            
            
            if (checker2 == 1)
            {
                //Time Influence Path
                t2_2 += Time.deltaTime;
                float s2_2 = t2_2 / endReleaseDuration;

                //Rotation
                //Principal Path
                transRot = Mathf.Lerp(endRot, startRot, curve_2.Evaluate(s2_2));
                target.eulerAngles= new Vector3(transRot, target.eulerAngles.y, 0f);
                
                float checker2_2 = Mathf.Lerp(0f, 1f, curve.Evaluate(s2_2));

                if (checker2_2 == 1)
                {
                    back = false;
                    ////print("Now ActualPoint="+actualPoint);
                    prepareBack=true;
                    t2 = 0;
                    t2_2 = 0;
                    //print("Inclination(Back)Arrived!");
                }
            }
        }
    }
}
