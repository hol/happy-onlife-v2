/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentOnPath : MonoBehaviour
{
    [Header("Opponent")]
    public MoveOnPath MOP2;
    private MoveOnPath MOP;
    private Overtaking OT;

    [Header("Switchers")]
    public bool safe;

    private int opPos;
    private int actPos;
    private int destPos;

    void Start()
    {
        MOP=transform.GetComponent<MoveOnPath>();
        OT=transform.GetComponent<Overtaking>();
    }
    
    public bool SafePath()
    {
        //opPos=MOP2.actualPoint;
        //actPos=MOP.actualPoint;
        //destPos=MOP.destinationPoint;
        if(opPos<actPos || opPos>destPos)
        {
            safe=true;
        }
        else
        {
            safe=false;
        }

        return safe;
    }
}
