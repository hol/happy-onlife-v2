/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MoveOnPath : MonoBehaviour
{
    [Header(
        "Script for a complex path\r\ncomposed by a mix of simple path from A to B\r\nand path with more than 2 points\r\n-Target (by default is scriptOwner)"
    )]
    public Transform target;
    [Header("-Point A")]
    public Transform startPoint;
    [Header("-Point B")]
    public Transform finishPoint;

    [Header("Tranforms to derivate other points")]
    public Transform[] points;
    private Vector3[] pointsPos;

    [Header("Steps to do")]
    public int steps;
    public int destination;
    private Overtaking OT;
    private int origPoint;
    private int actualPoint;
    private int destinationPoint;
    private int partialPoint;
    private Vector3 startPartPos;
    private Vector3 stopPartPos;
    private Vector3 influencePos;
    private Vector3 startWPos;
    private Vector3 stopWPos;
    private Vector3 transitionPos;
    private float yoyo;
    public int currentPos;

    [Header("During the movement this setting\r\ncomunicate with Overtaking for its management.")]
    public bool lastStep;
    public int lastStepNum;
    public int lastCheckedNum;

    [Header("Opponet\r\n-Auto-filled at Start")]
    public MoveOnPath opponent;
    
    [Header("Switchers")]
    public bool startOffset;
    public bool finishOffset;
    public bool go;
    private bool prepare;
    private bool safe=true;
    private bool autoRot;
    public float speedRot=0.003f;

    [Header("Only for circular Starship")]
    public bool additionalRot;
    public AnimRot animRot;
    
    [Header("Animation Type:\r\nDefault animation is Sliced\r\nIf number remain -1\r\nit will be only of one kind.\r\nSet step for switch to Mixed")]
    public int switchAt=-1;

    [Header("Principal\r\n(Only A to Z)")]
    public bool direct;

    [Header("Sliced\r\n(Every steps)")]
    public bool sliced;

    [Header("Both\r\n-Sliced 100%,\r\n-Direct 0% - 50% - 0%")]
    public bool mixed;
    
    [Header("Animation Settings")]
    private float t;
    private float t2;
    public float duration=2f;
    public float partialDuration;
    public AnimationCurve curve;
    public AnimationCurve curve2;

    [Header("Event Switchers")]
    public bool invokeForced;
    public bool invokeAtTheStart;
    public bool invokeAtTheEnd;

    [Header("Forced Event")]
    public UnityEvent forcedAction;

    [Header("Start Event\r\n-delay goes from 0 to 1")]
    public float startDelay;
    public UnityEvent startAction;
    [Header("End Event\r\n-delay goes from 0 to 1")]
    public float endDelay;
    public UnityEvent endAction;

    [Header("ToolBox - Only for Developers")]
    public bool reset;
    
    void Start()
    {
        if(target==null)
        {
            target=transform;
        }

        OT=transform.GetComponent<Overtaking>();
        
        //If path is passed manually (if not will be passed and initialized by FillPath script)
        if(startPoint!=null)
        {
            Initialize();
        }

        if(!direct && !sliced && !mixed)
        {
            sliced=true;
        }

        actualPoint=0;
        origPoint=0;
        autoRot=true;
        prepare=true;
    }

    public void Initialize()
    {
        //Pass all V3 from Transforms
        pointsPos=new Vector3[points.Length+2];
        for (int i = 0; i < points.Length+2; i++)
        {
            if(i==0)
            {//Set startPoint at the beginning
                if(startOffset)//Automatically use starship position as offset
                {
                    float diff=target.position.x-startPoint.position.x;
                    //print(transform.name+" diff="+diff);
                    pointsPos[i]=new Vector3(startPoint.position.x+diff, startPoint.position.y, startPoint.position.z);
                }
                else
                {
                    pointsPos[i]=startPoint.position;
                }
            }
            else if(i==points.Length+1)
            {//Set finishPoint at the end
                if(finishOffset)//Automatically use startPoint's offset 
                {
                    float diff=startPoint.position.x-target.position.x;
                    pointsPos[i]=new Vector3(finishPoint.position.x+diff, finishPoint.position.y, finishPoint.position.z);
                }
                else
                {
                    pointsPos[i]=finishPoint.position;
                }
            }
            else
            {//Set the rest in the middle
                pointsPos[i]=points[i-1].position;
            }
        }
    }

    public int SafeCheckForOpponent()
    {
        return opponent.currentPos;
    }

    public bool CheckOpponentOnPath()
    {
        //SafeCheckForOpponent
        int opponentPos=SafeCheckForOpponent();
        print("CheckOppOnPath: opponentPos="+opponentPos+" act="+actualPoint);
        if(opponentPos==actualPoint)
        {
            safe=false;
            print("Opponent On Path from Check!!!");
        }

        return safe;
    }

    public bool CheckOpponentOnNextCell()
    {
        int opponentPos=SafeCheckForOpponent();
        print("CheckOpponentOnNextCell: opponentPos="+opponentPos+" nextCell="+(actualPoint+1));
        if(opponentPos==(actualPoint+1))
        {
            safe=false;
            print("Opponent On NextCell!!!");
        }
        else
        {
            safe=true;
            print("NO Opponent on NextCell!!!");
        }

        return safe;
    }

    public bool CheckLastStep()
    {
        int opponentPos=SafeCheckForOpponent();
        print("CheckLastStep: opponentPos="+opponentPos+" nextCell="+actualPoint);
        if(opponentPos==actualPoint)
        {
            lastStep=true;
        }
        else
        {
            lastStep=false;
        }

        print("CheckLastStep: lastStep="+lastStep);
        return lastStep;
    }

    public bool TriggeredOpponentOnPath()
    {
        if(actualPoint==partialPoint)
        {
            print("Seems all ok, last Check:");
            safe=CheckOpponentOnPath();
            //lastCheckedNum=actualPoint;
        }
        else
        {
            safe=false;
            lastCheckedNum=actualPoint;
            print("Opponent On Path from Triggered!!!");
        }
        print("TriggeredOppOnPath: orig="+origPoint+" act="+actualPoint+" part="+partialPoint+" safe="+safe);
        
        return safe;
    }

    public void FirstIncrement()
    {
        //print("Too Late");
        actualPoint++;
    }

    // Update is called once per frame
    void Update()
    {
        if(reset)
        {
            target.position=new Vector3(startWPos.x, 
                                        target.position.y, 
                                        startWPos.z);
            actualPoint=0;
            reset=false;
        }

        if(go)
        {
            if(prepare)
            {
                //To this only if startDelay==0
                if(invokeAtTheStart)
                {
                    if(startDelay==0)
                    {
                        startAction.Invoke();
                    }

                    if(additionalRot)
                    {
                        animRot.Go();
                        //print("animrot go");
                    }
                    
                    invokeAtTheStart=false;
                }
                print("Preparation");
                origPoint=actualPoint;
                partialPoint=actualPoint+steps;
                destinationPoint=actualPoint+steps;
                destination=destinationPoint;
                print("actual="+actualPoint+" partial="+partialPoint);
                //OT.Moving();
                //prepare principal path
                startWPos=pointsPos[actualPoint];
                if(destinationPoint>pointsPos.Length-1)
                {
                    partialPoint=pointsPos.Length-1;
                    destinationPoint=pointsPos.Length-1;
                }
                stopWPos=pointsPos[destinationPoint];
                
                //prepare first partial path
                startPartPos=pointsPos[actualPoint];
                
                stopPartPos=pointsPos[actualPoint+1];
                
                t2=0;
                
                autoRot=true;
                prepare=false;
                //print("ActualPoint="+actualPoint);
                //print("Started...");

                if(switchAt!=-1)
                {
                    if(steps<switchAt)
                    {
                        direct=false;
                        sliced=true;
                        mixed=false;
                    }
                    else
                    {
                        direct=false;
                        sliced=false;
                        mixed=true;
                    }
                }

                //SafeCheckForOpponent
                //int opponentPos=SafeCheckForOpponent();
                //if(opponentPos==(actualPoint+1))
                //{
                //    OT.EmergencyOverTaking();
                //}
            }

            //Time Influence Path
            t2 += Time.deltaTime;
            float s2 = t2 / duration;
            partialDuration=s2*steps;

            //Rotation
            if(autoRot)
            {
                target.forward = Vector3.RotateTowards(
                                        target.forward, 
                                        stopPartPos - startPartPos, 
                                        speedRot, 
                                        10f);
            }
            
            //Influence Path
            influencePos = Vector3.Lerp(startPartPos, stopPartPos, curve2.Evaluate(partialDuration));

            float influChecker = Mathf.Lerp(0f, 1f, curve2.Evaluate(partialDuration));

            //Forced Action
            if (influChecker > 0.25f)
            {
                if(invokeForced)
                {
                    forcedAction.Invoke();
                    invokeForced=false;
                }
            }
            
            if (influChecker == 1)
            {
                //prepare start for partial path
                //print("actual="+actualPoint+" partial="+partialPoint);
                if(actualPoint<=partialPoint)
                {
                    //print("destination="+destinationPoint+" length="+pointsPos.Length);
                    //print("start="+startPartPos+" actual="+actualPoint);
                    if(actualPoint<=pointsPos.Length-1)
                    {
                        startPartPos=pointsPos[actualPoint];
                        actualPoint++;
                    }
                    else
                    {
                        //go = false;
                        //actualPoint=destinationPoint;
                        //print("Now ActualPoint="+actualPoint+" and parialPoint="+partialPoint);
                        //prepare=true;
                        
                        //To this only if endDelay==0
                        //if(invokeAtTheEnd)
                        //{
                        //    if(endDelay==0)
                        //    {
                        //        endAction.Invoke();
                        //        invokeAtTheEnd=false;
                        //    }
                        //}
//
                        //if(additionalRot)
                        //{
                        //    animRot.Stop();
                        //    //print("animrot stop");
                        //}
                        //
                        //invokeAtTheStart=true;
                        //invokeForced=true;
                        //invokeAtTheEnd=true;
                        //t = 0;
//
                        print("Ended!");
                    }
                    
                }

                //prepare end for partial path
                if(actualPoint<=partialPoint)
                {
                    //print("stop="+stopPartPos+" actual="+actualPoint);
                    if(destinationPoint<=pointsPos.Length-1)
                    {
                        stopPartPos=pointsPos[actualPoint];
                        t2=0;
                    }
                }
                else
                {
                    autoRot=false;
                }
            }

            //Time Principal Path
            t += Time.deltaTime;
            float s = t / duration;

            //Principal Path
            transitionPos = Vector3.Lerp(startWPos, stopWPos, curve.Evaluate(s));
            
            float checker = Mathf.Lerp(0f, 1f, curve2.Evaluate(s));

            //Position
            if(sliced)
            {
                target.position=new Vector3(influencePos.x, 
                                                target.position.y, 
                                                influencePos.z);
            }
            else if(direct)
            {            
                target.position=new Vector3(transitionPos.x, 
                                                target.position.y, 
                                                transitionPos.z);
            }
            else if(mixed)
            {
                if(checker<0.5f)
                {
                    yoyo = Mathf.Lerp(0f, 1f, curve2.Evaluate(s));
                }
                else
                {
                    yoyo = Mathf.Lerp(1f, 0f, curve2.Evaluate(s));
                }
            
                target.position=new Vector3((startWPos.x-transitionPos.x)*yoyo+influencePos.x, 
                                                target.position.y, 
                                                (startWPos.z-transitionPos.z)*yoyo+influencePos.z);
            }

            //To this only if startDelay!=0
            if(invokeAtTheStart)
            {
                if(checker >= startDelay && startDelay!=0)
                {
                    startAction.Invoke();
                    invokeAtTheStart=false;
                }
                
                if(additionalRot)
                {
                    animRot.Go();
                    //print("animrot go2");
                }
                
                invokeAtTheStart=false;
            }

            //To this only if endDelay!=0
            if(invokeAtTheEnd && endDelay!=0)
            {
                if(checker >= endDelay)
                {
                    //print("endAction");
                    endAction.Invoke();
                    invokeAtTheEnd=false;
                }
            }
            
            if(checker == 1)
            {
                go = false;
                actualPoint=destinationPoint;
                print("Now ActualPoint="+actualPoint+" and parialPoint="+partialPoint);
                currentPos=destinationPoint;
                print("currentPos="+currentPos);
                prepare=true;
                
                //To this only if endDelay==0
                if(invokeAtTheEnd)
                {
                    if(endDelay==0)
                    {
                        endAction.Invoke();
                        invokeAtTheEnd=false;
                    }
                }

                if(additionalRot)
                {
                    animRot.Stop();
                    //print("animrot stop");
                }
                
                invokeAtTheStart=true;
                invokeForced=true;
                invokeAtTheEnd=true;
                t = 0;

                //print("Arrived!");
            }
        }
    }
}
