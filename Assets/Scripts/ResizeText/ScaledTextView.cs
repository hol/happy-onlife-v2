/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaledTextView : MonoBehaviour
{
    [Header("Input Texts and Rects")]
    public List<Text> txts;
    private int[] txtsMaxStore;
    private float[] txtsLineMaxStore;
    public RectTransform[] rts;
    private float[] rtsMaxStore;

    public int maxTxtScale=120;
    public int maxTxtContentScale=90;
    public int maxRtWidth=210;

    [Header("Questions")]
    public GameObject question;
    private WhichQuestion wq;

    [Header("Bools")]
    public bool officially;
    public bool scalemup;
    public bool scalemdown;

    void Start()
    {
        if(question!=null)
        {
            wq=question.GetComponent<WhichQuestion>();
        }
        Protocol();
    }

    private void Protocol()
    {
        txtsMaxStore=new int [txts.Count];
        txtsLineMaxStore=new float [txts.Count];
        rtsMaxStore=new float [rts.Length];
        print("testi da ridimensionare="+txts.Count);
    }

    public void AddMe(Text newone, int scale)
    {
        txts.Add(newone);
        Protocol();
    }

    public void ScaleThem()
    {
        if(officially)
        {
            scalemdown=true;
            if(question!=null)
            {
                wq.quest1=true;
                PowerTrailsSetting.config2=false;
                CoreEvents.rightConnection=1;
                QuestionMovement.rightConnection=1;
            }
            print("Scale Them Down");
            officially=false;
        }
        else
        {
            scalemup=true;
            if(question!=null)
            {
                wq.quest2=true;
                PowerTrailsSetting.config2=true;
                CoreEvents.rightConnection=14;
                QuestionMovement.rightConnection=14;
            }
            print("Scale Them Up");
            officially=true;
        }
        
    }
    
    void Update()
    {
        if(scalemup)
        {
            for(int i = 0; i < txts.Count; i++)
            {
                txtsMaxStore[i]=txts[i].fontSize;
                if(txts[i].name=="TextContent")
                {
                    txts[i].resizeTextMaxSize=maxTxtContentScale;
                    txts[i].fontSize=maxTxtContentScale;
                }
                else
                {
                    txts[i].resizeTextMaxSize=maxTxtScale;
                    txts[i].fontSize=maxTxtScale;
                }
                txtsLineMaxStore[i]=txts[i].lineSpacing;
                txts[i].lineSpacing=1;
            }
            
            for(int i = 0; i < rts.Length; i++)
            {
                rtsMaxStore[i]=rts[i].sizeDelta.x;
                rts[i].sizeDelta=new Vector2(maxRtWidth, rts[i].sizeDelta.y);
            }
            scalemup=false;
        }

        if(scalemdown)
        {
            for(int i = 0; i < txts.Count; i++)
            {
                txts[i].resizeTextMaxSize=txtsMaxStore[i];
                txts[i].fontSize=txtsMaxStore[i];
                txts[i].lineSpacing=txtsLineMaxStore[i];
            }

            for(int i = 0; i < rts.Length; i++)
            {
                rts[i].sizeDelta=new Vector2(rtsMaxStore[i], rts[i].sizeDelta.y);
            }
            scalemdown=false;
        }
    }
}
