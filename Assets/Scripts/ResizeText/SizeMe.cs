/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

//Set size of text inside EXA
using UnityEngine;
using UnityEngine.UI;

public class SizeMe : MonoBehaviour
{
    public Transform target;
    private RectTransform rt;
    public int maxFontSize;
    public int minFontSize;
    private Text tt;
    public bool resizeH;
    public bool noResizeExa;
    public bool resetH;
    private LayoutElement le;
    private SizeMeExa sme;
    public float pw;
    public float ph;
    public float lw;
    public float offsetw=0f;
    public float lh;
    public float offseth=0f;

    public GameObject question;

    public static int initialCounter=0;
    public static bool first=true;
    
    void Start()
    {
        sme=target.GetComponent<SizeMeExa>();
        rt=GetComponent<RectTransform>();
        tt=GetComponent<Text>();
        CreateLE();
        if(first)
        {
            initialCounter++;
            //print(initialCounter);
            if(initialCounter>=5)
            {
                question.SetActive(false);
                first=false;
            }
        }
    }

    void CreateLE()
    {
        if(le==null)
        {
            le=rt.gameObject.AddComponent<LayoutElement>();
            tt.fontSize=maxFontSize;
            tt.resizeTextForBestFit=false;
            lw=sme.maxScale.x-sme.horiPad*2;
            le.preferredWidth=Mathf.Abs(lw);
        }
    }

    void Update()
    {
        if(resizeH)
        {
            CreateLE();
            lw=sme.maxScale.x-sme.horiPad*2;
            le.preferredWidth=lw;

            ph=LayoutUtility.GetPreferredHeight(rt)+sme.vertPad;
            
            lh=sme.maxScale.y-sme.vertPad*2;
            
            if(ph<lh)
            {
                le.preferredHeight=ph;
            }
            else
            {
                le.preferredHeight=lh-offseth ;
                tt.resizeTextForBestFit=true;
                tt.resizeTextMinSize=minFontSize;
                tt.resizeTextMaxSize=maxFontSize;
            }

            pw=LayoutUtility.GetPreferredWidth(rt)-offsetw;
            
            if(!noResizeExa)
            {
                sme.resize=true;
            }
            
            resizeH=false;
        }
        //For reset LayoutElement we need to destroy it and recreate it
        if(resetH)
        {
            Destroy(le);
            CreateLE();
            resetH=false;
        }
    }
}
