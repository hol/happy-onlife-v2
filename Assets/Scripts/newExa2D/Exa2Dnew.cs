﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]

public class Exa2Dnew : MonoBehaviour
{
    [Header("-Start-")]
    public float sFactor;
    public float sWidth;
    public float sHeight;
    public float sThick;
    public Color32 sOuterColor;
    public Color32 sInnerColor;
    public bool sEmpty;
    public bool sFull;
    public bool sHalfSx;
    public bool sHalfDx;
    public bool sNoLeft;
    public bool sNoRight;
    public bool sNoUp;
    public bool sNoDown;

    [Header("-Inputs General-")]
    private RectTransform myRT;
    public GameObject ExaArrowL;
    public GameObject ExaMiddle;
    public GameObject ExaArrowR;
    private RectTransform arrowlRT;
    private RectTransform middleRT;
    private RectTransform arrowrRT;
    
    [Header("-Inputs ArrowL-")]
    public GameObject AlFilled;
    private Image AlFilledImg;
    public GameObject AlLine1;
    private RectTransform AlLine1RT;
    private Image AlLine1Img;
    public GameObject AlCorner1;
    private RectTransform AlCorner1RT;
    private Image AlCorner1Img;
    public GameObject AlLine2;
    private RectTransform AlLine2RT;
    private Image AlLine2Img;
    public GameObject AlCorner2;
    private RectTransform AlCorner2RT;
    private Image AlCorner2Img;
    
    [Header("-Inputs Middle-")]
    public GameObject MFilled;
    private Image MFilledImg;
    public GameObject MLine1;
    private RectTransform MLine1RT;
    private Image MLine1Img;
    public GameObject MLine2;
    private RectTransform MLine2RT;
    private Image MLine2Img;
    
    [Header("-Inputs ArrowR-")]
    public GameObject ArFilled;
    private Image ArFilledImg;
    public GameObject ArLine1;
    private RectTransform ArLine1RT;
    private Image ArLine1Img;
    public GameObject ArCorner1;
    private RectTransform ArCorner1RT;
    private Image ArCorner1Img;
    public GameObject ArLine2;
    private RectTransform ArLine2RT;
    private Image ArLine2Img;
    public GameObject ArCorner2;
    private RectTransform ArCorner2RT;
    private Image ArCorner2Img;
    
    private float AtotalScale;
    private float MlineThickness;
    
    [Header("-Switchers-")]
    public bool heredity;
    public bool edit;
    public bool anima;
    private bool move;
    private bool back;

    [Header("-Rect Settings-")] 
    public float factor;
    public float width;
    private float storeW;
    public float height;
    private float storeH;

    [Header("-Color Settings-")] 
    public bool colorNow;
    public Color32 colorOutline;
    private Color32 outlineColor;
    public Color32 colorInner;
    private Color32 innerColor;
    
    [Header("-OutLine Settings-")]
    [Range(1.0f, 100.0f)]
    public float lineThickness;
    
    [Header("-Display Settings-")]
    public bool noInner;
    private bool innerOff;
    public bool halfDx;
    public bool halfSx;
    public bool full;

    public bool noLeft;
    private bool leftOff;
    public bool noRight;
    private bool rightOff;
    public bool noUp;
    private bool upOff;
    public bool noDown;
    private bool downOff;
    private float storeThick;
    
    [Header("-Animation Settings-")]
    private float t;
    public float duration;
    public AnimationCurve curve;
    private float startWidth;
    private float endWidth;
    
    void Start()
    {
        myRT = transform.GetComponent<RectTransform>();
        arrowlRT = ExaArrowL.transform.GetComponent<RectTransform>();
        middleRT = ExaMiddle.transform.GetComponent<RectTransform>();
        arrowrRT = ExaArrowR.transform.GetComponent<RectTransform>();
        
        AlFilledImg = AlFilled.transform.GetComponent<Image>();
        AlLine1RT = AlLine1.transform.GetComponent<RectTransform>();
        AlLine1Img = AlLine1.transform.GetComponent<Image>();
        AlCorner1RT = AlCorner1.transform.GetComponent<RectTransform>();
        AlCorner1Img = AlCorner1.transform.GetComponent<Image>();
        AlLine2RT = AlLine2.transform.GetComponent<RectTransform>();
        AlLine2Img = AlLine2.transform.GetComponent<Image>();
        AlCorner2RT = AlCorner2.transform.GetComponent<RectTransform>();
        AlCorner2Img = AlCorner2.transform.GetComponent<Image>();
        
        MFilledImg = MFilled.transform.GetComponent<Image>();
        MLine1RT = MLine1.transform.GetComponent<RectTransform>();
        MLine1Img = MLine1.transform.GetComponent<Image>();
        MLine2RT = MLine2.transform.GetComponent<RectTransform>();
        MLine2Img = MLine2.transform.GetComponent<Image>();
        
        ArFilledImg = ArFilled.transform.GetComponent<Image>();
        ArLine1RT = ArLine1.transform.GetComponent<RectTransform>();
        ArLine1Img = ArLine1.transform.GetComponent<Image>();
        ArCorner1RT = ArCorner1.transform.GetComponent<RectTransform>();
        ArCorner1Img = ArCorner1.transform.GetComponent<Image>();
        ArLine2RT = ArLine2.transform.GetComponent<RectTransform>();
        ArLine2Img = ArLine2.transform.GetComponent<Image>();
        ArCorner2RT = ArCorner2.transform.GetComponent<RectTransform>();
        ArCorner2Img = ArCorner2.transform.GetComponent<Image>();

        //Initialize from start
        factor=sFactor;
        width=sWidth;
        height=sHeight;
        lineThickness=sThick;
        colorOutline=sOuterColor;
        colorInner=sInnerColor;
        noInner=sEmpty;
        full=sFull;
        halfSx=sHalfSx;
        halfDx=sHalfDx;
        noLeft=sNoLeft;
        noRight=sNoRight;
        noUp=sNoUp;
        noDown=sNoDown;

        if (heredity)
        {
            Transform father = transform.parent;
            width = (Screen.width - (father.GetComponent<HorizontalLayoutGroup>().spacing -
                                     father.GetComponent<HorizontalLayoutGroup>().padding.left -
                                     father.GetComponent<HorizontalLayoutGroup>().padding.right)) / father.childCount;
            height = father.GetComponent<RectTransform>().sizeDelta.y;
        }
        SetHeight();
        SetThickness();
        SetInnerColor();
        SetOutlineColor();
        WhatDisplay();
        t = 0;
    }
    
    void Update()
    {
        if (edit)
        {
            //WIDTH & HEIGHT
            if (storeH != height || storeW != width)
            {
                SetHeight();
            }
            //LINE THICKNESS
            if (storeThick != lineThickness)
            {
                SetThickness();
            }
            //INNER COLOR
            if (colorInner.r != innerColor.r || colorInner.g != innerColor.g || colorInner.b != innerColor.b || colorInner.a != innerColor.a)
            {
                SetInnerColor();
            }
            //OUTLINER COLOR
            if (colorOutline.r != outlineColor.r || colorOutline.g != outlineColor.g || colorOutline.b != outlineColor.b || colorOutline.a != outlineColor.a)
            {
                SetOutlineColor();
            }
            //WHAT DISPLAY
            if (innerOff != noInner || innerOff != full || innerOff != halfDx || innerOff != halfSx  || leftOff != noLeft || rightOff != noRight || upOff != noUp || downOff != noDown)
            {
                WhatDisplay();
            }
        }

        if (colorNow)
        {
            SetInnerColor();
            SetOutlineColor();
            colorNow = false;
        }

        if (anima)
        {
            startWidth = width;
            endWidth = height * 1.1f;
            edit = true;
            noInner = true;
            move = true;
            anima = false;
        }
        
        if (move)
        {
            t += Time.deltaTime;
            float s = t / duration;
            
            float checker = Mathf.Lerp(0, 1, curve.Evaluate(s));
            width = Mathf.Lerp(startWidth, endWidth, curve.Evaluate(s));
            
            if (checker == 1)
            {
                move = false;
                t = 0;
            }
        }

        if (back)
        {
            t += Time.deltaTime;
            float s = t / duration;
            
            float checker = Mathf.Lerp(0, 1, curve.Evaluate(s));
            width = Mathf.Lerp(endWidth, startWidth, curve.Evaluate(s));

            if (checker == 1)
            {
                back = false;
                t = 0;
            }
        }
    }

    void SetHeight()
    {
        //width & height of main object
        myRT.sizeDelta = new Vector2(width, height);
        //scale of both Arrow objects
        AtotalScale = height / 512;
        arrowlRT.localScale = new Vector3(AtotalScale,AtotalScale, AtotalScale);
        arrowrRT.localScale = new Vector3(AtotalScale,AtotalScale, AtotalScale);
        //lateral offset of Middle object
        middleRT.offsetMin = new Vector2(((lineThickness * 0.57f + 146) * height) / 512, middleRT.offsetMin.y);
        middleRT.offsetMax = new Vector2(-((lineThickness * 0.57f + 146) * height) / 512, middleRT.offsetMax.y);
        //thickness of Middle Lines
        MlineThickness = (lineThickness*AtotalScale);
        MLine1RT.sizeDelta = new Vector2(MLine1RT.sizeDelta.x, MlineThickness);
        MLine2RT.sizeDelta = new Vector2(MLine2RT.sizeDelta.x, MlineThickness);
                
        storeH = height;
    }
    
    void SetThickness()
    {
        //thickness of ArrowL Line
        AlLine1RT.sizeDelta = new Vector2(AlLine1RT.sizeDelta.x, lineThickness);
        AlLine2RT.sizeDelta = new Vector2(AlLine2RT.sizeDelta.x, lineThickness);
        AlCorner1RT.sizeDelta = new Vector2(lineThickness, lineThickness);
        AlCorner1RT.localPosition = new Vector3(-AlLine1RT.sizeDelta.x, -lineThickness, 0);
        AlCorner2RT.sizeDelta = new Vector2(lineThickness, lineThickness);
        AlCorner2RT.localPosition = new Vector3(-AlLine2RT.sizeDelta.x, lineThickness, 0);
        //thickness of ArrowR Line
        ArLine1RT.sizeDelta = new Vector2(ArLine1RT.sizeDelta.x, lineThickness);
        ArLine2RT.sizeDelta = new Vector2(ArLine2RT.sizeDelta.x, lineThickness);
        ArCorner1RT.sizeDelta = new Vector2(lineThickness, lineThickness);
        ArCorner1RT.localPosition = new Vector3(-ArLine1RT.sizeDelta.x, -lineThickness, 0);
        ArCorner2RT.sizeDelta = new Vector2(lineThickness, lineThickness);
        ArCorner2RT.localPosition = new Vector3(-ArLine2RT.sizeDelta.x, lineThickness, 0);
        //scale of both Arrow objects
        AtotalScale = height / 512;
        //lateral offset of Middle object
        middleRT.offsetMin = new Vector2(((lineThickness * 0.57f + 146) * height) / 512, middleRT.offsetMin.y);
        middleRT.offsetMax = new Vector2(-((lineThickness * 0.57f + 146) * height) / 512, middleRT.offsetMax.y);
        //thickness of Middle Lines
        MlineThickness = (lineThickness*AtotalScale);
        MLine1RT.sizeDelta = new Vector2(MLine1RT.sizeDelta.x, MlineThickness);
        MLine2RT.sizeDelta = new Vector2(MLine2RT.sizeDelta.x, MlineThickness);
        
        storeThick = lineThickness;
    }

    void SetInnerColor()
    {
        AlFilledImg.color = colorInner;
        MFilledImg.color = colorInner;
        ArFilledImg.color = colorInner;
            
        innerColor = colorInner;
    }
    
    void SetOutlineColor()
    {
        AlLine1Img.color = colorOutline;
        AlCorner1Img.color = colorOutline;
        AlLine2Img.color = colorOutline;
        AlCorner2Img.color = colorOutline;
        
        MLine1Img.color = colorOutline;
        MLine2Img.color = colorOutline;
        
        ArLine1Img.color = colorOutline;
        ArCorner1Img.color = colorOutline;
        ArLine2Img.color = colorOutline;
        ArCorner2Img.color = colorOutline;
            
        innerColor = colorInner;
    }

    void WhatDisplay()
    {
        if (noInner)
        {
            AlFilled.SetActive(false);
            MFilled.SetActive(false);
            ArFilled.SetActive(false);
            noInner=false;
        }
        
        if(full)
        {
            AlFilled.SetActive(true);
            MFilled.SetActive(true);
            MFilledImg.fillAmount=1f;
            ArFilled.SetActive(true);
            full=false;
        }

        if (halfSx)
        {
            AlFilled.SetActive(true);
            MFilled.SetActive(true);
            MFilledImg.fillOrigin=(int) Image.OriginHorizontal.Left;
            MFilledImg.fillAmount=0.5f;
            ArFilled.SetActive(false);
            halfSx=false;
        }

        if (halfDx)
        {
            AlFilled.SetActive(false);
            MFilled.SetActive(true);
            MFilledImg.fillOrigin=(int) Image.OriginHorizontal.Right;
            MFilledImg.fillAmount=0.5f;
            ArFilled.SetActive(true);
            halfDx=false;
        }
        
        if (noLeft)
        {
            ExaArrowL.SetActive(false);
            leftOff = noLeft;
        }
        else
        {
            ExaArrowL.SetActive(true);
            leftOff = noLeft;
        }
        
        if (noRight)
        {
            ExaArrowR.SetActive(false);
            rightOff = noRight;
        }
        else
        {
            ExaArrowR.SetActive(true);
            rightOff = noRight;
        }
        
        if (noUp)
        {
            MLine1.SetActive(false);
            upOff = noUp;
        }
        else
        {
            MLine1.SetActive(true);
            upOff = noUp;
        }
        
        if (noDown)
        {
            MLine2.SetActive(false);
            downOff = noDown;
        }
        else
        {
            MLine2.SetActive(true);
            downOff = noDown;
        }
    }
}
