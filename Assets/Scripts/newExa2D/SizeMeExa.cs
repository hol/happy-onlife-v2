/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

//Set size of EXA around content (text)
using UnityEngine;
using UnityEngine.UI;

public class SizeMeExa : MonoBehaviour
{
    public RectTransform target;
    private SizeMe sm;
    public float horiPad;
    public float vertPad;
    private Exa2Dnew exa;
    public bool blockSize;
    public bool resize;
    public Vector2 maxScale;
    public Vector2 contScale;
    public Vector2 resultingScale;

    void Start()
    {
        exa=GetComponent<Exa2Dnew>();
        maxScale=new Vector2(exa.width, exa.height);
        sm=target.GetComponent<SizeMe>();
    }
    
    void Update()
    {
        if(resize && !blockSize)
        {
            contScale=new Vector2((LayoutUtility.GetPreferredWidth(target))+horiPad*2, (LayoutUtility.GetPreferredHeight(target))+vertPad*2);
            exa.width=contScale.x;
            exa.height=contScale.y;

            if (exa.width>maxScale.x)
            {
                exa.width=maxScale.x;
            }

            if (exa.height>maxScale.y)
            {
                exa.height=maxScale.y;
            }

            resultingScale=new Vector2(exa.width, exa.height);
            resize=false;
        }
    }
}