/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SizeMeGeneral : MonoBehaviour
{
    public SizeMeExa target;
    public LayoutElement[] le;
    public Text ex;
    public float gw=0f;
    public float gh=0f;
    
    void Update()
    {
        //Width
        for(int i=0; i<le.Length; i++)
        {
            if(le[i].preferredWidth<gw)
                gw=le[i].preferredWidth;
        }

        if(target.maxScale.x<gw)
            gw=target.maxScale.x;

        for(int i=0; i<le.Length; i++)
        {
            le[i].preferredWidth=gw;
        }
        
        //Height
        for(int i=0; i<le.Length; i++)
        {
            if(le[i].preferredHeight<gh)
                gh=le[i].preferredHeight;
        }

        if(target.maxScale.y<gh)
            gh=target.maxScale.y;
            
        for(int i=0; i<le.Length; i++)
        {
            le[i].preferredHeight=gh;
        }
    }
}
