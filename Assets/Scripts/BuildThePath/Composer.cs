﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class Composer : MonoBehaviour
{
    Vector3 tStore;
    Vector3 tNow;
    string where;
    float offsetO = 1.8f;
    float offsetV = 1.55f;
    
    public GameObject starter;
    public GameObject Mattonella;
    GameObject target;
    GameObject storeTarget;
    GameObject nuTarget;

    public static GameObject reActivated;

    public static bool door;
    public static bool deepening;
    public static bool reActive;
    public static bool rezet;

    void Start()
    {
        target = starter;
        storeTarget = target;
        tStore = Vector3.zero;
        tNow = Vector3.zero;
        door = false;
        deepening = false;
        reActive = false;
    }
    
    void Update()
    {
        if (rezet)
        {
            Start();
            rezet = false;
        }

        if (reActive)
        {
            target = reActivated;
            storeTarget = reActivated;
            reActive = false;
        }

        if (door)
        {
            tNow = Right.mp;
        }
        else if (deepening)
        {
            tNow = Middle.mp;
        }
        else
        {
            tNow = Left.mp;
            target = storeTarget;
        }

        if (tStore != tNow)
        {
            where = "";

            Vector3 sP = Camera.main.WorldToScreenPoint(target.transform.position);

            if (Rotate90.rot)
            {
                if (tNow.x > sP.x + 50)
                {
                    where = "D";
                }
                else if (tNow.x < sP.x - 50)
                {
                    where = "S";
                }
                else
                {
                    where = "L";
                }

                if (tNow.y > sP.y)
                {
                    where += "S";
                }
                else if (tNow.y < sP.y)
                {
                    where += "G";
                }

                switch (where)
                {
                    case "DS":
                        nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x + offsetV, target.transform.position.y, target.transform.position.z + offsetO /2), target.transform.rotation);
                        break;
                    case "LS":
                        nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z + offsetO), target.transform.rotation);
                        break;
                    case "SS":
                        nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x - offsetV, target.transform.position.y, target.transform.position.z + offsetO / 2), target.transform.rotation);
                        break;
                    case "DG":
                        nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x + offsetV, target.transform.position.y, target.transform.position.z - offsetO / 2), target.transform.rotation);
                        break;
                    case "LG":
                        nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z - offsetO), target.transform.rotation);
                        break;
                    case "SG":
                        nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x - offsetV, target.transform.position.y, target.transform.position.z - offsetO / 2), target.transform.rotation);
                        break;
                }
            }
            else
            {
                if (tNow.x > sP.x)
                {
                    where = "D";
                }
                else
                {
                    where = "S";
                }

                if (tNow.y > sP.y + 50)
                {
                    where += "S";
                }
                else if (tNow.y < sP.y - 50)
                {
                    where += "G";
                }
                else
                {
                    where += "L";
                }

                switch (where)
                {
                    case "DS":
                        nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x + (offsetO / 2), target.transform.position.y, target.transform.position.z + offsetV), Quaternion.identity);
                        break;
                    case "DL":
                        nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x + offsetO, target.transform.position.y, target.transform.position.z), Quaternion.identity);
                        break;
                    case "DG":
                        nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x + (offsetO / 2), target.transform.position.y, target.transform.position.z - offsetV), Quaternion.identity);
                        break;
                    case "SS":
                        nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x - (offsetO / 2), target.transform.position.y, target.transform.position.z + offsetV), Quaternion.identity);
                        break;
                    case "SL":
                        nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x - offsetO, target.transform.position.y, target.transform.position.z), Quaternion.identity);
                        break;
                    case "SG":
                        nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x - (offsetO / 2), target.transform.position.y, target.transform.position.z - offsetV), Quaternion.identity);
                        break;
                }
            }

            nuTarget.transform.parent = target.transform;

            if (door)
            {
                nuTarget.GetComponent<MeshRenderer>().material.color = Color.black;
                nuTarget.name = "P";
                door = false;
                UICounters.cP++;
                if (deepening)
                {
                    tNow = Middle.mp;
                }
                else
                {
                    tNow = Left.mp;
                }

            }
            else if (deepening)
            {
                nuTarget.GetComponent<MeshRenderer>().material.color = Color.white;
                nuTarget.name = "D";
                UICounters.cD++;
                target = nuTarget;
            }
            else
            {
                nuTarget.name = "Q";
                UICounters.cQ++;
                storeTarget = nuTarget;
            }

            tStore = tNow;
        }

    }
}
