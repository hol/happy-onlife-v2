﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class Left2 : MonoBehaviour
{
    public static Vector3 mp;
    public static bool on;
    public static bool rec;
    public static bool block;
    public static bool rezet;

    void Start()
    {
        mp = new Vector3();
        on = false;
        rec = false;
        block = true;
    }

    void Update()
    {
        if (rezet)
        {
            Start();
            rezet = false;
        }

        if (on)
        {
            if (rec)
            {
                mp = Input.mousePosition;
            }
            else
            {
                mp = new Vector3();
            }

            if (Input.GetMouseButtonDown(0))
            {

                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    if (hit.transform != null)
                    {
                        string name = hit.transform.gameObject.name;
                        if (hit.transform.childCount == 0 && name[0] == 'Q')
                        {
                            print("Ultimo");
                            block = false;
                            hit.transform.GetComponent<MeshRenderer>().material.color = Color.green;
                            Modifier.target = hit.transform.parent.gameObject;
                            Modifier.nuTarget = hit.transform.gameObject;
                            Del.on = true;
                            Composer.reActivated = hit.transform.gameObject;
                            Composer.reActive = true;
                            Left.doubleCheck = true;
                            Left.on = true;
                            Right.block = true;
                            Right.on = true;
                            Middle.on = true;
                            rec = true;
                            Modifier.move = true;
                        }
                        else
                        {
                            if (hit.transform.GetComponent<MeshRenderer>().material.color == Color.green)
                            {
                                hit.transform.GetComponent<MeshRenderer>().material.color = Color.grey;
                                Left.on = false;
                                Right.on = false;
                                Middle.on = false;
                            }
                            else if (hit.transform.GetComponent<MeshRenderer>().material.color != Color.yellow)
                            {
                                block = false;
                                hit.transform.GetComponent<MeshRenderer>().material.color = Color.yellow;
                                Modifier.target = hit.transform.parent.gameObject;
                                Modifier.nuTarget = hit.transform.gameObject;
                                Del.on = true;
                                Composer.reActivated = hit.transform.gameObject;
                                Composer.reActive = true;
                                Left.doubleCheck = true;
                                Left.on = true;
                                Right.block = true;
                                Right.on = true;
                                Middle.on = true;
                                rec = true;
                                Modifier.move = true;
                            }
                            else
                            {
                                //Modifier.move = false;
                                //rec = false;
                                if (name[0] == 'Q')
                                {
                                    hit.transform.GetComponent<MeshRenderer>().material.color = Color.grey;
                                }
                                else if (name[0] == 'D')
                                {
                                    hit.transform.GetComponent<MeshRenderer>().material.color = Color.white;
                                }
                                else if (name[0] == 'P')
                                {
                                    hit.transform.GetComponent<MeshRenderer>().material.color = Color.black;
                                }
                            }
                        }
                    }
                }
            }

            if (Input.GetMouseButton(0))
            {
                if (!block)
                {
                    rec = true;
                    Modifier.move = true;
                    print("clicked");
                }
                
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (!block)
                {
                    rec = false;
                    Modifier.move = false;
                    block = true;
                    print("clickedUp");
                }
                
            }
        }
    }
}
