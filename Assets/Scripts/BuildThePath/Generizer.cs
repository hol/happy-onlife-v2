﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Linq;
using System.Xml.Linq;
using UnityEngine;

public class Generizer : MonoBehaviour
{
    public static string fileName;
    public static bool load;
    public static bool rezet;
    public GameObject firstCell;
    public GameObject Mattonella;
    public GameObject Starter;
    public GameObject Finisher;
    private GameObject target;
    private GameObject nuTarget;
    private GameObject storeTarget;
    private float offsetO;
    private float offsetV;
    private string type;
    private bool deep;

    public Texture ImgAV;
    public Texture ImgVirus;
    public Texture ImgLucky;

    private Color32[] cs;
    private Color32 lt;
    private Color32 sf;
    private static float em;
    public float multiEmission;


    private float yFactorValue;
    private float yFactor;

    private int categories;
    private string whitchCat;
    private int[] allCat;
    private bool refill;
    private int[] allCatPass;
    private int storeCat;

    private int[] dispositionCat;
    private static Vector3[] cellOrig;
    private static Vector3[] cellStops;

    private static Vector3 cellPosFrom;
    private static Vector3 cellPosFromY;
    private static Vector3 cellPosTo;

    private static GameObject mover;
    
    private float t;
    private float duration;

    public static bool move;
    public static bool move2;
    public static bool back;
    public static bool create;
    public static bool delete;
    public static bool backHome;
    
    public AnimationCurve curve;

    public Material metalMat;

    public GenericUtils gu;
    public Dice dice;
    //public CoreEvents ple;

    private Color32 red;

    public static string cellName;

    public FillPath fp1_1;
    public FillPath fp1_2;
    public FillPath fp1_3;
    public FillPath fp2_1;
    public FillPath fp2_2;
    public FillPath fp2_3;
    
    void Start()
    {
        em=multiEmission;
        deep=false;
        load=false;
        rezet=false;
        move=false;
        move2=false;
        back=false;
        create=false;
        delete=false;
        backHome=false;
        refill=false;
        offsetO = 1.8f;
        offsetV = 1.55f;
        fileName = "Orig2";
        type = "";
        target = firstCell;
        storeTarget = target;
        
        cs = new[]
        {
            new Color32(100,73,167,255), //1
            new Color32(128,60,157,255), //2
            new Color32(142,50,151,255), //3
            new Color32(161,8,137,255), //4
            new Color32(177,5,133,255), //5
            new Color32(191,0,119,255), //6
            new Color32(210,0,113,255), //7
            new Color32(223,0,104,255), //8
            new Color32(237,23,99,255), //9
            new Color32(238,71,100,255), //10
            new Color32(245,98,103,255), //11
            new Color32(246,123,104,255), //12
            new Color32(249,145,105,255), //13
            new Color32(255,168,103,255), //14
            new Color32(252,175,97,255), //15
            new Color32(250,189,83,255), //16
            new Color32(246,204,67,255), //17
            new Color32(241,216,46,255), //18
            new Color32(238,224,19,255), //19
            new Color32(233,234,0,255), //20
            new Color32(220,229,0,255), //21
            new Color32(197,218,0,255), //22
            new Color32(168,208,3,255), //23
            new Color32(143,198,18,255), //24
            new Color32(106,185,36,255), //25
            new Color32(68,176,46,255), //26
            new Color32(50,173,75,255), //27
            new Color32(22,172,117,255), //28
            new Color32(3,172,157,255), //29
            new Color32(0,172,195,255), //30
            new Color32(0,169,228,255), //31
            new Color32(0,165,255,255) //32
        };

        lt = new Color32(173, 173, 173, 255);
        
        sf = new Color32(0, 40, 114, 255);
        
        Starter.transform.GetChild(1).GetComponent<MeshRenderer>().material.color = sf;
        Starter.transform.GetChild(1).GetComponent<MeshRenderer>().material.SetVector("_EmissionColor", (Color)sf * multiEmission);
        Starter.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
            Resources.Load<Texture>("Textures/Start");
        Finisher.transform.GetChild(1).GetComponent<MeshRenderer>().material.color = sf;
        Finisher.transform.GetChild(1).GetComponent<MeshRenderer>().material.SetVector("_EmissionColor", (Color)sf * multiEmission);
        Finisher.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
            Resources.Load<Texture>("Textures/Finish");

        yFactorValue = 0.1f;

        categories = PlayerPrefs.GetInt("nCat");
        print("Gen:cat="+categories);
        whitchCat = PlayerPrefs.GetString("cats");
        print("Gen:whitchCat="+whitchCat);
        allCat = new int[categories];
        dispositionCat = new int[33];
        dispositionCat[0]=0;
        cellOrig = new Vector3[33];
        cellOrig[0] = Vector3.zero;
        cellStops = new Vector3[33];
        cellStops[0] = Vector3.zero;

        //Passing Path Start and Fininsh to Players
        fp1_1.exaStart=Starter.transform;
        fp1_1.exaFinish=Finisher.transform;
        fp1_2.exaStart=Starter.transform;
        fp1_2.exaFinish=Finisher.transform;
        fp1_3.exaStart=Starter.transform;
        fp1_3.exaFinish=Finisher.transform;

        fp2_1.exaStart=Starter.transform;
        fp2_1.exaFinish=Finisher.transform;
        fp2_2.exaStart=Starter.transform;
        fp2_2.exaFinish=Finisher.transform;
        fp2_3.exaStart=Starter.transform;
        fp2_3.exaFinish=Finisher.transform;
        
        refill = true;
        parseFile();

        t = 0;
        duration = 1f;

        red=new Color32(255,0,0,255);
    }
    
    int cat()
    {
        int nCat = -1;
        
        if (refill)
        {
            allCat = new int[categories];
            for (int i = 0; i < categories; i++)
            {
                allCat[i] = int.Parse(whitchCat[i].ToString());
            }

            refill = false;
        }
        else
        {
            allCat = allCatPass;
        }
        
        int max = Mathf.Max(allCat);
        int min = Mathf.Min(allCat);

        while (!allCat.Contains(nCat))
        {
            nCat = Random.Range(min, max);
        }
        
        if (allCat.Length - 1 > 0)
        {
            allCatPass = new int[allCat.Length - 1];

            bool jumped = false;
            
            for (int i = 0; i < allCat.Length; i++)
            {
                if (allCat[i] != nCat)
                {
                    if (jumped)
                    {
                        allCatPass[i-1] = allCat[i];
                    }
                    else
                    {
                        allCatPass[i] = allCat[i];
                    }
                }
                else
                {
                    jumped = true;
                }
            }
        }
        else
        {
            refill = true;
        }
        
        storeCat = nCat;
        return nCat;
    }

    void parseFile()
    {
        //Preparing reading XML file
        TextAsset textAsset = (TextAsset) Resources.Load(fileName);
        
        XDocument doc = XDocument.Parse(textAsset.text);
        
        var head = doc.Element("content");
        var rows = doc.Element("content").Elements("row");

        //init counter
        int counter = 0;
        
        //Prepare first cell
        firstCell.transform.GetChild(1).GetComponent<MeshRenderer>().material.color = cs[counter];
        firstCell.transform.GetChild(1).GetComponent<MeshRenderer>().material.SetVector("_EmissionColor", (Color)cs[counter] * multiEmission);
        
        //Randomizing Category Order
        int randomCat = cat();

        dispositionCat[counter+1] = randomCat;
        cellOrig[counter+1] = firstCell.transform.position;
        cellStops[counter+1] = new Vector3(firstCell.transform.position.x + offsetV, yFactor, firstCell.transform.position.z - offsetO / 2);

        switch (randomCat)
        {
            case 0:
                firstCell.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                    Resources.Load<Texture>("Textures/Icon_PlaySafely");
                break;
            case 1:
                firstCell.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                    Resources.Load<Texture>("Textures/Icon_SocialLife");
                break;
            case 2:
                firstCell.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                    Resources.Load<Texture>("Textures/Icon_DontRisk");
                break;
            case 3:
                firstCell.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                    Resources.Load<Texture>("Textures/Icon_NoBull");
                break;
            case 4:
                firstCell.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                    Resources.Load<Texture>("Textures/Icon_PrivacyMatters");
                break;
            case 5:
                firstCell.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                    Resources.Load<Texture>("Textures/Icon_LetsChat");
                break;
            case 6:
                firstCell.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                    Resources.Load<Texture>("Textures/Icon_WatchOut");
                break;
            case 7:
                firstCell.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                    Resources.Load<Texture>("Textures/Icon_IsItFair");
                break;
        }
        
        firstCell.transform.GetChild(0).transform.localEulerAngles=new Vector3(0,180,0);
        
        //Starting creation of new cells
        counter++;

        int deepN = 0;
        
        foreach (var row in rows)
        {
            yFactor = Random.Range(-yFactorValue, yFactorValue);
            
            string where = row.Value;
            
            if (row.Attribute("id").Value == "deepening" && !deep)
            {
                target = storeTarget;
                type = "D";
                deep = true;
            }
            else if (row.Attribute("id").Value == "portkey" && deep)
            {
                type = "P";
                deep = false;
            }
            else if (row.Attribute("id").Value == "portkey")
            {
                target = storeTarget;
                type = "P";
                deep = false;
            }
            else if (row.Attribute("id").Value == "question")
            {
                target = storeTarget;
                //type = "Q";
                type = "M" + (counter+1).ToString("00");
                deep = false;
            }

            //Positioning
            switch (where)
            {
                case "DS":
                    nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x + offsetV, yFactor, target.transform.position.z + offsetO / 2), target.transform.rotation);
                    break;
                case "LS":
                    nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x,yFactor, target.transform.position.z + offsetO), target.transform.rotation);
                    break;
                case "SS":
                    nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x - offsetV, yFactor, target.transform.position.z + offsetO / 2), target.transform.rotation);
                    break;
                case "DG":
                    nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x + offsetV, yFactor, target.transform.position.z - offsetO / 2), target.transform.rotation);
                    break;
                case "LG":
                    nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x, yFactor, target.transform.position.z - offsetO), target.transform.rotation);
                    break;
                case "SG":
                    nuTarget = Instantiate(Mattonella, new Vector3(target.transform.position.x - offsetV, yFactor, target.transform.position.z - offsetO / 2), target.transform.rotation);
                    break;
            }

            if (row.Attribute("id").Value == "question")
            {
                switch(row.Attribute("stop").Value)
                {
                    case "DS":
                        cellStops[counter+1] = new Vector3(nuTarget.transform.position.x + offsetV, yFactor, nuTarget.transform.position.z + offsetO / 2);
                        break;
                    case "LS":
                        cellStops[counter+1] = new Vector3(nuTarget.transform.position.x,yFactor, nuTarget.transform.position.z + offsetO);
                        break;
                    case "SS":
                        cellStops[counter+1] = new Vector3(nuTarget.transform.position.x - offsetV, yFactor, nuTarget.transform.position.z + offsetO / 2);
                        break;
                    case "DG":
                        cellStops[counter+1] = new Vector3(nuTarget.transform.position.x + offsetV, yFactor, nuTarget.transform.position.z - offsetO / 2);
                        break;
                    case "LG":
                        cellStops[counter+1] = new Vector3(nuTarget.transform.position.x, yFactor, nuTarget.transform.position.z - offsetO);
                        break;
                    case "SG":
                        cellStops[counter+1] = new Vector3(nuTarget.transform.position.x - offsetV, yFactor, nuTarget.transform.position.z - offsetO / 2);
                        break;
                }
            }

            cellOrig[counter+1] = nuTarget.transform.position;

            nuTarget.name = type;
            nuTarget.transform.parent = target.transform.parent.transform;

            //Old kind of cells, no more used from V2.0
            if (row.Attribute("id").Value == "portkey")
            {
                nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture>("Textures/ArrowStop");
                nuTarget.transform.GetChild(0).transform.localEulerAngles=new Vector3(0,int.Parse(row.Attribute("rot").Value),0);
                nuTarget.transform.GetChild(1).GetComponent<MeshRenderer>().material.color = lt;
            }
            else if (row.Attribute("id").Value == "deepening")
            {
                if (deepN == 0)
                {
                    nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture>("Textures/ArrowDouble");
                    nuTarget.transform.GetChild(0).transform.localEulerAngles=new Vector3(0,int.Parse(row.Attribute("rot").Value),0);
                    deepN++;
                }
                else if (deepN == 1)
                {
                    nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture>("Textures/Jump");
                    nuTarget.transform.GetChild(0).transform.localEulerAngles=new Vector3(0,int.Parse(row.Attribute("rot").Value),0);
                    deepN++;
                }
                else if (deepN == 2)
                {
                    nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture>("Textures/ArrowStop");
                    nuTarget.transform.GetChild(0).transform.localEulerAngles=new Vector3(0,int.Parse(row.Attribute("rot").Value),0);
                    deepN--;
                }
                nuTarget.transform.GetChild(1).GetComponent<MeshRenderer>().material.color = lt;
                target = nuTarget;
            }
            else//Antivirus, Virus, Lucky
            {
                if (counter == 6)
                {
                    nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture>("Textures/Icon_AV");
                }
                else if (counter == 11 || counter == 22 || counter == 28)
                {
                    nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture>("Textures/Icon_Lucky");
                }
                else if (counter == 16)
                {
                    nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture>("Textures/Icon_Virus");
                }
                else//Questions
                {
                    randomCat = cat();

                    dispositionCat[counter+1] = randomCat;
                    switch (randomCat)
                    {
                        case 0:
                            nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                                Resources.Load<Texture>("Textures/Icon_PlaySafely");
                            break;
                        case 1:
                            nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                                Resources.Load<Texture>("Textures/Icon_SocialLife");
                            break;
                        case 2:
                            nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                                Resources.Load<Texture>("Textures/Icon_DontRisk");
                            break;
                        case 3:
                            nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                                Resources.Load<Texture>("Textures/Icon_NoBull");
                            break;
                        case 4:
                            nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                                Resources.Load<Texture>("Textures/Icon_PrivacyMatters");
                            break;
                        case 5:
                            nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                                Resources.Load<Texture>("Textures/Icon_LetsChat");
                            break;
                        case 6:
                            nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                                Resources.Load<Texture>("Textures/Icon_WatchOut");
                            break;
                        case 7:
                            nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture =
                                Resources.Load<Texture>("Textures/Icon_IsItFair");
                            break;
                    }
                }
                //Rotation
                nuTarget.transform.GetChild(0).transform.localEulerAngles=new Vector3(0,int.Parse(row.Attribute("rot").Value),0);
                //Material colors
                nuTarget.transform.GetChild(1).GetComponent<MeshRenderer>().material.color = cs[counter];
                nuTarget.transform.GetChild(1).transform.GetComponent<Renderer>().material.SetVector("_EmissionColor", (Color)cs[counter] * em);

                counter++;
                storeTarget = nuTarget;
            }
            
        }
        fileName = "";
        type = "";
        deep = false;

        //Passing Category Disposition tu GU
        GenericUtils.catSequence = dispositionCat;

        //Passing Path to Players
        fp1_1.exaPath=new Transform[counter];
        fp1_2.exaPath=new Transform[counter];
        fp1_3.exaPath=new Transform[counter];
        fp2_1.exaPath=new Transform[counter];
        fp2_2.exaPath=new Transform[counter];
        fp2_3.exaPath=new Transform[counter];

        target=firstCell.transform.parent.gameObject;

        int children = target.transform.childCount;
        int passer=0;

        for (int i = 0; i < children; ++i)
        {
            if(target.transform.GetChild(i).gameObject.name!="M33")
            {
                fp1_1.exaPath[passer]=target.transform.GetChild(i).transform;
                fp1_2.exaPath[passer]=target.transform.GetChild(i).transform;
                fp1_3.exaPath[passer]=target.transform.GetChild(i).transform;
                fp2_1.exaPath[passer]=target.transform.GetChild(i).transform;
                fp2_2.exaPath[passer]=target.transform.GetChild(i).transform;
                fp2_3.exaPath[passer]=target.transform.GetChild(i).transform;
                passer++;
            }
        }

        fp1_1.passAll=true;
        fp1_2.passAll=true;
        fp1_3.passAll=true;
        fp2_1.passAll=true;
        fp2_2.passAll=true;
        fp2_3.passAll=true;
    }


    void Update()
    {
        if(delete)
        {
            Destroy(mover.transform.GetChild(5).gameObject);
            move = true;
            backHome=true;
            delete = false;
        }

        //Creation of new cell (type "Think About That")
        if(create)
        {
            cellPosFromY = new Vector3(cellPosFrom.x, cellPosFrom.y - 0.2f, cellPosFrom.z);
            nuTarget = Instantiate(Mattonella, cellPosFromY, mover.transform.rotation);
            nuTarget.transform.GetChild(1).GetComponent<Renderer>().material = metalMat;
            nuTarget.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture>("Textures/Icon_Space4Knowledge");
            nuTarget.transform.GetChild(0).transform.localEulerAngles=new Vector3(0,180,0);
            nuTarget.name = "TAT";
            nuTarget.transform.parent = mover.transform.parent.transform;
            move = true;
            create = false;
        }

        if (move)
        {
            t += Time.deltaTime;
            float s = t / duration;
            
            mover.transform.position=Vector3.Lerp(cellPosFrom, cellPosTo, curve.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                move = false;
                t = 0;
                if(backHome)
                {
                    print("backHome");
                    if(CoreEvents.enoughPWC)
                    {
                        dice.Move.move=true;
                        gu.gameBoardRotation=true;
                        CoreEvents.enoughPWC=false;
                        print("here comes dice");
                    }
                    else if(CoreEvents.oldWrongQ)
                    {
                        print("change turn");
                        gu.gameBoardRotation=true;
                    }
                    
                    backHome=false;
                }
                else
                {
                    print("move2");
                    move2 = true;
                }
            }
        }

        if (move2)
        {
            t += Time.deltaTime;
            float s = t / duration;
            
            nuTarget.transform.position=Vector3.Lerp(cellPosFromY, cellPosFrom, curve.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                move2 = false;
                t = 0;
                nuTarget.transform.parent = mover.transform;
                gu.changeTurn();
            }
        }

        if (back)
        {
            t += Time.deltaTime;
            float s = t / duration;
            
            mover.transform.position=Vector3.Lerp(cellPosFrom, cellPosTo, curve.Evaluate(s));
            float checker=Mathf.Lerp(0, 1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                back = false;
                t = 0;
            }
        }
    }

    //On wrong answer by new question
    public static void Generize(int id)
    {
        print("generize " + id);
        cellName = "M" + (id).ToString("00");
        cellPosFrom = GameObject.Find(cellName).transform.position;
        cellPosTo = cellStops[id];
        mover = GameObject.Find(cellName);
        //Coloring in red
        mover.transform.GetChild(1).transform.GetComponent<Renderer>().material.SetColor("_Color", new Color32(255,0,0,255));
        mover.transform.GetChild(1).transform.GetComponent<Renderer>().material.SetVector("_EmissionColor", new Color(255,0,0) * em);
        mover.transform.GetChild(2).transform.GetComponent<Renderer>().material.SetColor("_Color", new Color32(255,0,0,255));
        
        create = true;
    }

    //On right answer on old question
    public static void Destroize(int id)
    {
        print("destroy " + id);
        cellName = "M" + (id).ToString("00");
        cellPosFrom = GameObject.Find(cellName).transform.position;
        cellPosTo = cellOrig[id];
        mover = GameObject.Find(cellName);
        mover.transform.GetChild(1).transform.GetComponent<Renderer>().material.SetColor("_Color", new Color32(0,255,0,255));
        mover.transform.GetChild(1).transform.GetComponent<Renderer>().material.SetVector("_EmissionColor", new Color(0,255,0) * em);
        mover.transform.GetChild(2).transform.GetComponent<Renderer>().material.SetColor("_Color", new Color32(0,255,0,255));
        delete = true;
    }
}
