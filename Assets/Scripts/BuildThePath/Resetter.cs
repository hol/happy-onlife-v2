﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using UnityEngine;

public class Resetter : MonoBehaviour
{
    public GameObject starter;
    public static bool rezet = false;
    
    void Update()
    {
        if (rezet)
        {
            StartCoroutine(Rezet());
            rezet = false;
        }
    }

    IEnumerator Rezet()
    {
        RezetAll();

        yield return 0;

        RezetAll();

        yield return 0;

        DestroyAll();
    }

    void RezetAll()
    {
        Xmlizer.rezet = true;
        Modifier.rezet = true;
        Composer.rezet = true;
        Generizer.rezet = true;
        Left.rezet = true;
        Left2.rezet = true;
        Right.rezet = true;
        Middle.rezet = true;
        Del.rezet = true;
        Rotate90.rezet = true;
        UICounters.rezet = true;
        UI.rezet = true;
    }

    void DestroyAll()
    {
        foreach (Transform child in starter.transform)
        {
            Destroy(child.gameObject);
        }
        starter.transform.rotation = Quaternion.identity;
    }
}
