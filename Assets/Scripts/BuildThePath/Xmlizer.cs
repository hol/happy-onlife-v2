﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.IO;
using UnityEngine;

public class Xmlizer : MonoBehaviour
{
    public GameObject starter;
    GameObject father;
    GameObject child;
    GameObject nephew;

    public static string fileName;
    string xml;
    public static bool build;
    public static bool rezet;

    void Start()
    {
        fileName = "";
        xml = "";
        build = false;
    }

    void Update()
    {
        if (rezet)
        {
            Start();
            rezet = false;
        }

        if (build)
        {
            BuildXml();
            build = false;
        }
    }

    string FindPos(GameObject c, GameObject f)
    {
        string where = "";
        print("cx="+c.transform.position.x+" fx="+f.transform.position.x);
        if (Rotate90.rot)
        {
            if (System.Math.Round(c.transform.position.x, 2) > System.Math.Round(f.transform.position.x, 2))
            {
                where = "D";
            }
            else if (System.Math.Round(c.transform.position.x, 2) < System.Math.Round(f.transform.position.x, 2))
            {
                where = "S";
            }
            else
            {
                where = "L";
            }

            if (System.Math.Round(c.transform.position.z, 2) > System.Math.Round(f.transform.position.z, 2))
            {
                where += "S";
            }
            else
            {
                where += "G";
            }
        }
        else
        {
            if (System.Math.Round(c.transform.position.x, 2) > System.Math.Round(f.transform.position.x, 2))
            {
                where = "D";
            }
            else
            {
                where = "S";
            }

            if (System.Math.Round(c.transform.position.z, 2) > System.Math.Round(f.transform.position.z, 2))
            {
                where += "S";
            }
            else if (System.Math.Round(c.transform.position.z, 2) < System.Math.Round(f.transform.position.z, 2))
            {
                where += "G";
            }
            else
            {
                where += "L";
            }
        }

        print(where);
        return where;
    }

    string WriteXml(string objName, GameObject c, GameObject f)
    {
        string where = FindPos(c, f);
        print(objName);
        switch (objName)
        {
            case "P":
                xml += "\t<row id=\"portkey\">" + where + "</row>\n";
                break;
            case "D":
                xml += "\t<row id=\"deepening\">" + where + "</row>\n";
                break;
            case "Q":
                xml += "\t<row id=\"question\">" + where + "</row>\n";
                break;
        }

        return xml;
    }

    int TotA(GameObject firstA)
    {
        int totA = 0;
        GameObject firstChild = firstA;
        for (int o = 0; o < 20; o++)
        {
            if (firstChild.transform.childCount > 0)
            {
                GameObject newChild = firstChild.transform.GetChild(0).gameObject;
                totA++;
                print(totA);
                firstChild = newChild;
            }
        }
        return totA;
    }

    void BuildXml()
    {
        father = starter;

        print("starterTree = " + starter.transform.hierarchyCount);

        for (int i = 0; i < starter.transform.hierarchyCount; i++)
        {
            for (int e = 0; e < father.transform.childCount; e++)
            {
                child = father.transform.GetChild(e).gameObject;
                
                string objName = child.transform.gameObject.name;
        
                xml = WriteXml(objName, child, father);
        
                if (objName == "D")
                {
                    int totA = TotA(child);

                    for (int a = 1; a < totA + 1; a++)
                    {
                        nephew = child.transform.GetChild(0).gameObject;
                    
                        string objName2 = nephew.transform.gameObject.name;
                        
                        xml = WriteXml(objName2, nephew, child);
                    
                        child = nephew;
                    
                        i++;
                    }
                }
                
            }
        
            father = child;
        }

        SaveXml();
    }

    void SaveXml()
    {
        StreamWriter File = new StreamWriter(Application.dataPath+ "/Resources/" + fileName + ".xml");
        if (Rotate90.rot)
        {
            File.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<content rot=\"yes\">\n");
        }
        else
        {
            File.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<content rot=\"no\">\n");
        }
        File.Write(xml);
        File.Write("</content>");
        File.Close();
    }
}
