﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class UICounters : MonoBehaviour
{
    
    public Text counterQ;
    public Text counterD;
    public Text counterP;

    public static int cQ;
    public static int cD;
    public static int cP;
    public static bool rezet;

    void Start()
    {
        cQ = 1;
        cD = 0;
        cP = 0;
    }

    void Update()
    {
        if (rezet)
        {
            Start();
            rezet = false;
        }

        counterQ.text = cQ.ToString();
        counterD.text = cD.ToString();
        counterP.text = cP.ToString();
    }
}
