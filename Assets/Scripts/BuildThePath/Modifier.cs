﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class Modifier : MonoBehaviour
{
    Vector3 tStore;
    Vector3 tNow;
    string where;
    float offsetO = 1.8f;
    float offsetV = 1.55f;
    
    public static GameObject target;
    public GameObject Mattonella;
    GameObject storeTarget;
    public static GameObject nuTarget;

    public static bool door;
    public static bool deepening;
    public static bool move;
    public static bool delete;
    public static bool rezet;

    void Start()
    {
        door = false;
        deepening = false;
        move = false;
        delete = false;
        tStore = Vector3.zero;
        tNow = Vector3.zero;
    }

    void DelCounters(GameObject ToDel)
    {
        print(ToDel.transform.gameObject.name);
        switch (ToDel.transform.gameObject.name)
        {
            case "P":
                UICounters.cP--;
                break;
            case "D":
                UICounters.cD--;
                break;
            case "Q":
                UICounters.cQ--;
                break;
        }
    }
    
    void Update()
    {
        if (rezet)
        {
            Start();
            rezet = false;
        }

        if (delete)
        {
            Del.on = false;
            Left2.rec = false;
            move = false;
            tNow = tStore;

            GameObject cChild = nuTarget;
            DelCounters(cChild);
            int tot = 1;
            print("0 - " + tot);
            for (int o = 0; o < 80; o++)
            {
                if (cChild.transform.childCount > 0)
                {
                    GameObject newChild = cChild.transform.GetChild(0).gameObject;
                    DelCounters(newChild);
                    tot++;
                    print("1 - "+tot);

                    if (newChild.transform.gameObject.name == "D")
                    {
                        GameObject childD = newChild;
                        for (int u = 0; u < 30; u++)
                        {
                            if (childD.transform.childCount > 0)
                            {
                                GameObject childD2 = childD.transform.GetChild(0).gameObject;
                                DelCounters(childD2);
                                tot++;
                                print("3 - " + tot);
                                childD = childD2;
                            }
                        }
                    }

                    if (cChild.transform.childCount > 1)
                    {
                        GameObject newChild2 = cChild.transform.GetChild(1).gameObject;
                        DelCounters(newChild2);
                        tot++;
                        print("2 - " + tot);
                        cChild = newChild2;
                    }
                    else
                    {
                        cChild = newChild;
                    }
                }
            }

            Destroy(nuTarget.transform.gameObject);
            delete = false;
        }
        
        if (move)
        {
            tNow = Left2.mp;
        }

        if (tStore != tNow)
        {
            //genCounter++;
            where = "";

            Vector3 sP = Camera.main.WorldToScreenPoint(target.transform.position);

            if (Rotate90.rot)
            {
                if (tNow.x > sP.x + 50)
                {
                    where = "D";
                }
                else if (tNow.x < sP.x - 50)
                {
                    where = "S";
                }
                else
                {
                    where = "L";
                }

                if (tNow.y > sP.y)
                {
                    where += "S";
                }
                else if (tNow.y < sP.y)
                {
                    where += "G";
                }
                
                switch (where)
                {
                    case "DS":
                        nuTarget.transform.position = new Vector3(target.transform.position.x + offsetV, target.transform.position.y, target.transform.position.z + offsetO /2);
                        break;
                    case "LS":
                        nuTarget.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z + offsetO);
                        break;
                    case "SS":
                        nuTarget.transform.position = new Vector3(target.transform.position.x - offsetV, target.transform.position.y, target.transform.position.z + offsetO / 2);
                        break;
                    case "DG":
                        nuTarget.transform.position = new Vector3(target.transform.position.x + offsetV, target.transform.position.y, target.transform.position.z - offsetO / 2);
                        break;
                    case "LG":
                        nuTarget.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z - offsetO);
                        break;
                    case "SG":
                        nuTarget.transform.position = new Vector3(target.transform.position.x - offsetV, target.transform.position.y, target.transform.position.z - offsetO / 2);
                        break;
                }
            }
            else
            {
                if (tNow.x > sP.x)
                {
                    where = "D";
                }
                else
                {
                    where = "S";
                }

                if (tNow.y > sP.y + 50)
                {
                    where += "S";
                }
                else if (tNow.y < sP.y - 50)
                {
                    where += "G";
                }
                else
                {
                    where += "L";
                }

                print(where);

                switch (where)
                {
                    case "DS":
                        nuTarget.transform.position = new Vector3(target.transform.position.x + (offsetO / 2), target.transform.position.y, target.transform.position.z + offsetV);
                        break;
                    case "DL":
                        nuTarget.transform.position = new Vector3(target.transform.position.x + offsetO, target.transform.position.y, target.transform.position.z);
                        break;
                    case "DG":
                        nuTarget.transform.position = new Vector3(target.transform.position.x + (offsetO / 2), target.transform.position.y, target.transform.position.z - offsetV);
                        break;
                    case "SS":
                        nuTarget.transform.position = new Vector3(target.transform.position.x - (offsetO / 2), target.transform.position.y, target.transform.position.z + offsetV);
                        break;
                    case "SL":
                        nuTarget.transform.position = new Vector3(target.transform.position.x - offsetO, target.transform.position.y, target.transform.position.z);
                        break;
                    case "SG":
                        nuTarget.transform.position = new Vector3(target.transform.position.x - (offsetO / 2), target.transform.position.y, target.transform.position.z - offsetV);
                        break;
                }
            }

            if (door)
            {
                nuTarget.GetComponent<MeshRenderer>().material.color = Color.black;
                nuTarget.name = "P";
                door = false;
                UICounters.cP++;
                if (deepening)
                {
                    tNow = Middle.mp;
                }
                else
                {
                    tNow = Left.mp;
                }
            }

            tStore = tNow;
        }

    }
}
