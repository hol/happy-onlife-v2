﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public Image firstPage;
    public Image openPage;
    public Image savePage;
    public InputField inputOpenName;
    public InputField inputSaveName;

    public static bool save;
    public static bool rezet;

    void Start()
    {
        firstPage.transform.gameObject.SetActive(true);
        openPage.transform.gameObject.SetActive(true);
        savePage.transform.gameObject.SetActive(false);
        save = false;
    }

    void Update()
    {
        if (rezet)
        {
            Start();
            rezet = false;
        }

        if (save)
        {
            SaveName();
            save = false;
        }
    }

    public void Hide1()
    {
        firstPage.transform.gameObject.SetActive(false);
    }

    public void HideAllandCompose()
    {
        firstPage.transform.gameObject.SetActive(false);
        openPage.transform.gameObject.SetActive(false);
        Left.on = true;
        Right.on = true;
        Middle.on = true;
    }

    public void Load()
    {
        Generizer.fileName = inputOpenName.text;
        Generizer.load = true;
        openPage.transform.gameObject.SetActive(false);
        Left2.on = true;
    }

    public void Save()
    {
        Xmlizer.fileName = inputSaveName.text;
        Xmlizer.build = true;
        savePage.transform.gameObject.SetActive(false);
        Resetter.rezet = true;
    }

    void SaveName()
    {
        Left.on = false;
        Left2.on = false;
        savePage.transform.gameObject.SetActive(true);
    }

    public void Rezet()
    {
        Resetter.rezet = true;
    }
}
