﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class Left : MonoBehaviour
{
    public static Vector3 mp;
    public static bool on;
    public static bool doubleCheck;
    public static bool rezet;

    void Start()
    {
        mp = new Vector3();
        on = false;
        doubleCheck = false;
    }

    void Update()
    {
        if (rezet)
        {
            Start();
            rezet = false;
        }

        if (doubleCheck)
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    on = false;
                }
            }
        }

        if (on)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Composer.deepening = false;
                mp = Input.mousePosition;
                Right.block = true;
            }
        }
    }
}
