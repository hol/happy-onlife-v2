﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System;
using UnityEngine;
using UnityEngine.UI;

public class Textify : MonoBehaviour
{
	[Serializable] public struct Txt 
	{ 
		public Text txt;
		public int strgN;
	};

	[Serializable] public struct GroupText
	{ 
		public string id;
		public Txt[] nTxt;
		public bool whole;
	};

    [Header("TEXTIFY")]
	public GroupText[] groupTexts;
    
    public bool lang;
    public bool more;

	private TextAsset qAsset = null;
	private string suffix;
	private string fname="anything";
	private string fnameStore="";
	private string testoIntero;
	private string[] strgs;
	private string[] strg;

    void Update()
    {
        if (lang)
        {
            LangTxt();
            lang = false;
        }
    }

	void LangTxt () 
	{
		for(int e=0; e<groupTexts.Length; e++)
		{
            //Load & Split texts from file (id=file.name & suffix=file.lang)
			suffix = PlayerPrefs.GetString ("linguaSuffix");
			fname = "Text/"+groupTexts[e].id+"_" + suffix;
			fnameStore=fname;
			qAsset = (TextAsset)Resources.Load(fname);
			testoIntero = qAsset.text;
			if(!groupTexts[e].whole)
			{
				strgs = testoIntero.Split ("\r\n" [0]);
			}
            //

			for(int i=0; i<groupTexts[e].nTxt.Length; i++)
			{
                //Distribute text, font to UI.Text
				if(groupTexts[e].whole)
				{
					groupTexts[e].nTxt[i].txt.text=testoIntero;
				}
				else
				{
					strg = strgs[groupTexts[e].nTxt[i].strgN].Split ('=');
					groupTexts[e].nTxt[i].txt.text=strg[1];
				}
				groupTexts[e].nTxt[i].txt.font = (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
                //
			}
		}
	}

	public string OnDemand (string from, int which, bool entire=false) 
	{
		fname = "Text/" + from + "_" + suffix;
		print("fileName="+fname+"; fnameStore="+fnameStore);
		if(fnameStore!=fname)
		{
			fnameStore=fname;
			qAsset = (TextAsset)Resources.Load(fname);
			print("fileName="+fname);
			testoIntero = qAsset.text;
			if(entire)
			{
				return testoIntero;
			}
			strgs = testoIntero.Split ("\r\n" [0]);
		}
		strg = strgs[which].Split ('=');
		return strg[1];
	}


}
