﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class DisposeCircular : MonoBehaviour
{
    public static bool initMove;
    
    public RectTransform CenterElement;

    private Languages l;
    public Sound sound;
    private int totLang;
    private string[] languages;

    public GameObject defaultBtn;
    public MenuMovement mm;
    public AssistKeyboard assistKeyboard;
    public AssistKeyboardElement hamburger;
    public AssistKeyboardElement quitBtn;

    public ScaledTextView stv;
    
    private RectTransform[] buttons;

    private int totBtn;

    Vector3 center;
    private int ang;
    private int angtot;
    private int angstart;
    [Header("Genereal Settings")]
    public float scale;
    public float radius;
    [Header("Initial Animation Radius")]
    public float fromRadius=200f;
    public float toRadius=300f;
    
    private float t;
    private float _time;

    public AnimationCurve angleRotCurve;
    public AnimationCurve radiusLengthCurve;
    
    void Start()
    {
        l=transform.GetComponent<Languages>();
        totLang=l.languages.Length;
        totBtn=l.languages.Length;
        t = 0;
        _time = 2f;
        radius = 204;
        angtot = 360;
        buttons=new RectTransform[totLang];
        CreateBtns();
        Dispose();
        initMove = true;
    }

    void CreateBtns()
    {
        languages=l.languages;
        
        for (int i = 0; i < languages.Length; i++)
        {
            GameObject newBtn=Instantiate(defaultBtn, Vector3.zero, Quaternion.identity);
            newBtn.name=languages[i]+"_Btn";
            newBtn.transform.SetParent(transform);
            newBtn.transform.localPosition=Vector3.zero;
            newBtn.transform.GetChild(1).transform.GetComponent<Text>().text=languages[i].ToUpper();
            newBtn.transform.GetChild(1).name=""+i;
            
            newBtn.GetComponent<Button>().onClick.AddListener(delegate {
                l.SelectLanguage(int.Parse(newBtn.transform.GetChild(1).name));
                mm.Next(1);
                sound.PlayClick();
            });
            buttons[i]=newBtn.transform.GetComponent<RectTransform>();
            assistKeyboard.selectable[i]=newBtn.transform.GetComponent<AssistKeyboardElement>();
            assistKeyboard.selectable[i].whatToDo.AddListener(delegate {
                l.SelectLanguage(int.Parse(newBtn.transform.GetChild(1).name));
                mm.Next(1);
                sound.PlayClick();
            });

            //stv.AddMe(newBtn.transform.GetChild(1).GetComponent<Text>(), 70);
        }
        assistKeyboard.selectable[languages.Length]=hamburger;
        assistKeyboard.selectable[languages.Length+1]=quitBtn;
        //
        defaultBtn.SetActive(false);
    }

    void Dispose()
    {
        center = CenterElement.localPosition;
        
        for (int i = 0; i < totBtn; i++)
        {
            ang = angstart+(angtot / totBtn * i);
            Vector3 pos = RandomCircle();
            buttons[i].localPosition=pos;
            buttons[i].localScale=new Vector3(scale, scale, scale);
        }
    }

    Vector3 RandomCircle ()
    {
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = center.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.z = center.z;
        return pos;
    }

    void Update()
    {
        Dispose();
        
        if (initMove)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            radius = Mathf.Lerp(fromRadius, toRadius, radiusLengthCurve.Evaluate(s));
            angstart = (int)Mathf.Lerp(0, 360, angleRotCurve.Evaluate(s));

            float checker = Mathf.Lerp(0, 1, angleRotCurve.Evaluate(s));
            if (checker == 1)
            {
                initMove = false;
                t = 0;
            }
        }
    }
}
