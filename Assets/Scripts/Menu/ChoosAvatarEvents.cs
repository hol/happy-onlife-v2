﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChoosAvatarEvents : MonoBehaviour 
{
	public Text playerTxt;
	private RectTransform playerTxtRT;
	private float playerTxtRTY;
	public Text loadingTxt;
	public GameObject loadingBracket;

	public GameObject ssType;

	public GameObject ss1;
	public GameObject ss2;
	public GameObject ss3;
	private GameObject ssAct;
	private GameObject ssStore;
	public SSBtnCom ssbc;
	public MenuMovement mm;
	public AssistKeyboardGroups akg;

	public bool type=true;
	public int selectedID;

	Transform[] objects1;
	Transform[] objects2;
	Transform[] objects3;

	Transform[] ss;
	DisposeCircular3D dc;
	DisposeCircular3D dcStore;
	DisposeCircular3D dc1;
	DisposeCircular3D dc2;
	DisposeCircular3D dc3;
	Vector3[] origPos = new Vector3 [8];

	//VAR 4 Textify
	private string[] strgs;
	
	private string plr;
	private int players;
	public static bool check;
	private int turn;
	private int backTo;

	public static bool lang;

	public Textify textify;

	void Start ()
	{
		objects1=ss1.GetComponent<DisposeCircular3D>().objects;
		objects2=ss2.GetComponent<DisposeCircular3D>().objects;
		objects3=ss3.GetComponent<DisposeCircular3D>().objects;
		dc1=ss1.GetComponent<DisposeCircular3D>();
		dc2=ss2.GetComponent<DisposeCircular3D>();
		dc3=ss3.GetComponent<DisposeCircular3D>();
		
		turn = 1;
		
		loadingTxt.enabled = false;
		loadingBracket.SetActive(false);
		ssType.SetActive(true);
		ss1.SetActive(false);
		ss2.SetActive(false);
		ss3.SetActive(false);
		playerTxtRT=playerTxt.GetComponent<RectTransform>();
		playerTxtRTY=200f;
		playerTxtRT.localPosition = new Vector3(playerTxtRT.localPosition.x, playerTxtRTY, playerTxtRT.localPosition.z);
		//Reset();
	}

	void SetType(int id)
	{
		switch(id)
		{
			case 1:
				ss=objects1;
				ss1.gameObject.SetActive(true);
				ssAct=ss1;
				dc=dc1;
				if (players == 2 && turn==1)
				{
					backTo=5;
				}
				akg.AssignFocus(5);
				break;
			case 2:
				ss=objects2;
				ss2.gameObject.SetActive(true);
				ssAct=ss2;
				dc=dc2;
				if (players == 2 && turn==1)
				{
					backTo=6;
				}
				akg.AssignFocus(6);
				break;
			case 3:
				ss=objects3;
				ss3.gameObject.SetActive(true);
				ssAct=ss3;
				dc=dc3;
				if (players == 2 && turn==1)
				{
					backTo=7;
				}
				akg.AssignFocus(7);
				break;
		}
		
		if (players == 2 && turn==1)
		{
			dcStore=dc;
			ssStore=ssAct;
		}
		
		for(int i=0; i<origPos.Length; i++)
		{
			origPos[i]=ss[i].localPosition;
		}
	}

	public void Reset()
	{
		for(int i=0; i<origPos.Length; i++)
		{
			if(ss[i].gameObject!=null)
			{
				ss[i].gameObject.SetActive(true);
				ss[i].transform.localPosition=origPos[i];
				ss[i].transform.localScale=new Vector3(0.15f,0.15f,0.15f);
			}
		}

		ssType.SetActive(true);
		ss1.SetActive(false);
		ss2.SetActive(false);
		ss3.SetActive(false);

		playerTxt.text = plr + " 1";
		ContinueAva.dis = true;
		turn = 1;
		loadingTxt.enabled = false;
		loadingBracket.SetActive(false);
		DisposeCircular3D.totUfo=8;
		dc.rezet=true;
	}

	public void AndNow()
	{
		if (players == 2 && turn==1)
		{
			if(type)
			{
				ssType.SetActive(false);
				SetType(selectedID);
				type=false;
				print("Continue1");
			}
			else
			{
				ContinueAva.dis = true;
				playerTxt.text = plr + " 2";
				dc.taken = dc.selected;
				ssbc.focusOnID=0;
				dc.rezet = true;
				ss1.gameObject.SetActive(false);
				ss2.gameObject.SetActive(false);
				ss3.gameObject.SetActive(false);
				ssType.SetActive(true);
				type=true;
				PlayerPrefs.SetInt ("curAvatarPlayer", 2);
				playerTxtRT.localPosition = new Vector3(playerTxtRT.localPosition.x, playerTxtRTY, playerTxtRT.localPosition.z);
				turn++;
				print("Continue2");
				akg.AssignFocus(4);
			}
		}
		else
		{
			if(type)
			{
				ssType.SetActive(false);
				SetType(selectedID);
				type=false;
				print("Continue3");
			}
			else
			{
				playerTxt.enabled = false;
				loadingTxt.enabled = true;
				loadingBracket.SetActive(true);
				print("Continue4");
				BeginGame();
			}
		}
	}

	public void BackNow()
	{
		if (players == 2 && turn==1)
		{
			if(type)
			{
				mm.Back(3);
				Reset();
				ssbc.focusOnID=0;
				print("Back1");
			}
			else
			{
				dc.rezet = true;
				ssStore.SetActive(false);
				ssbc.focusOnID=0;
				ssType.SetActive(true);
				type=true;
				playerTxtRT.localPosition = new Vector3(playerTxtRT.localPosition.x, playerTxtRTY, playerTxtRT.localPosition.z);
				//ssbc.reset=true;
				akg.AssignFocus(4);
				print("Back2");
			}
		}
		else if (players == 2 && turn==2)
		{
			if(type)
			{
				Reset();
				dcStore.taken=-1;
				dcStore.rezet=true;
				dc=dcStore;
				ssType.SetActive(false);
				type=false;
				ssStore.SetActive(true);
				playerTxt.text = plr + " 1";
				PlayerPrefs.SetInt ("curAvatarPlayer", 1);
				turn=1;
				print("Back3");
				akg.AssignFocus(backTo);
			}
			else
			{
				dc.rezet = true;
				ssAct.gameObject.SetActive(false);
				ssbc.focusOnID=0;
				ssType.SetActive(true);
				type=true;
				playerTxtRT.localPosition = new Vector3(playerTxtRT.localPosition.x, playerTxtRTY, playerTxtRT.localPosition.z);
				print("Back4");
				//ssbc.reset=true;
				akg.AssignFocus(4);
			}
		}
		else
		{
			if(type)
			{
				mm.Back(3);
				Reset();
				ssbc.focusOnID=0;
				print("Back1");
			}
			else
			{
				dc.rezet = true;
				ssAct.gameObject.SetActive(false);
				ssType.SetActive(true);
				type=true;
				ssbc.reset=true;
				print("Back2");
				akg.AssignFocus(4);
			}
		}
	}

	void Update()
	{
		if (lang)
		{
			LangTxt();
			lang = false;
		}

		if (check)
		{
			players = PlayerPrefs.GetInt ("numplayers");
			print("Arrivato! Players:"+players);
			check = false;
		}
	}

	//Textify
	void LangTxt () 
	{
		plr = textify.OnDemand("menu", 23);
		playerTxt.text = plr + " 1";
    }
	
    //Load main game scene
	void BeginGame() 
	{
		int p1 = PlayerPrefs.GetInt ("p1color");
		int p2 = PlayerPrefs.GetInt ("p2color", 2);
		
		if(p1==p2)
		{
			p2++;
			if(p2==9)
				p2=1;
			PlayerPrefs.SetInt ("p2color",p2);
		}

		SceneManager.LoadScene("Game");
	}
}
