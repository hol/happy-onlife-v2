﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisposinGrillExaC : MonoBehaviour//
{
    public static int Num;
    public Transform Obj1;
    public Transform Obj2;
    public Transform Obj3;
    public Transform Obj4;
    public Transform Obj5;
    public Transform Obj6;
    public Transform Obj7;
    public Transform Obj8;

    private int totObj;
    
    private Transform[] container;
    private ExaC[] exac;
    public static Vector3[] startMove;
    public static Vector3[] finishMove;
    public static Vector3[] actualPos;

    private int numStore;
    private float offsetX;
    private float offsetY;
    
    private float sw;
    private float sh;
    private float ow;
    private float oh;
    
    public static int counter;
    private bool stop;
    private bool dispose;
    
    void Start()
    {
        Num = -1;
        numStore = Num;
        container = new []{Obj1, Obj2, Obj3, Obj4, Obj5, Obj6, Obj7, Obj8};
        
        exac = new[]
        {
            Obj1.GetComponent<ExaC>(), Obj2.GetComponent<ExaC>(), Obj3.GetComponent<ExaC>(), Obj4.GetComponent<ExaC>(),
            Obj5.GetComponent<ExaC>(), Obj6.GetComponent<ExaC>(), Obj7.GetComponent<ExaC>(), Obj8.GetComponent<ExaC>()
        };
        
        startMove = new Vector3[]
        {
            Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero,Vector3.zero
        };
        finishMove = new Vector3[]
        {
            Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero,Vector3.zero
        };
        totObj = container.Length;
        actualPos = new Vector3[]
        {
            Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero,Vector3.zero
        };

        sw = transform.GetComponent<RectTransform>().sizeDelta.x;
        sh = transform.GetComponent<RectTransform>().sizeDelta.y;
        ow = container[0].transform.GetComponent<RectTransform>().sizeDelta.x;
        oh = container[0].transform.GetComponent<RectTransform>().sizeDelta.y;
        
        counter = 0;
        stop = false;
        dispose = false;
    }

    void WhosOnStage()
    {
        Debug.Log("Avviso chi è di scena");
        //hide useless
        for (int e = 0; e < totObj; e++)
        {
            if (Num <= e)
            {
                if (container[e].gameObject.activeSelf)
                {
                    exac[e].off = true;
                }
            }
            else
            {
                if (!container[e].gameObject.activeSelf)
                {
                    container[e].gameObject.SetActive(true);
                }
            }
        }
        dispose = true;
        
        //This shouldn't br here but...
        SineWave.init = true;
    }

    void DisposeThem()
    {
        Debug.Log("Dispongo la griglia");
        ExaC.unlock = true;
        ExaC.checkMove = true;
        bool odd = false;
        int horizRow = 2;
        if (Num % 2 == 1) //odd
        {
            odd = true;
        }
        int horizPad = 2;
            
        offsetX = (sw - (ow * horizRow)) / (horizRow * horizPad);
        
        float midNum = (float)Num / 2;
        int vertRow = (int)Mathf.Ceil(midNum);
        int vertPad = 2;
        
        offsetY = (sh - (oh * vertRow)) / (vertRow * vertPad);
        
        for (int i = 0; i < totObj; i++)
        {
            if (odd && i == Num - 1)
            {
                horizRow = 1;
                offsetX = (sw - (ow * horizRow)) / (horizRow * horizPad);
            }

            float x = offsetX + ow*0.5f;
            float y = -(offsetY + oh*0.5f + offsetY*2*((float)i*0.5f) + oh*((float)i*0.5f));
            
            startMove[i] = container[i].localPosition;
            finishMove[i] = new Vector3(x, y, 0);
            actualPos[i] = container[i].position;
            
            if (odd && i == Num - 1)
            {

            }
            else
            {
                float x2 = offsetX * 3 + ow * 0.5f + ow;
                startMove[i + 1] = container[i + 1].localPosition;
                finishMove[i + 1] = new Vector3(x2, y, 0);
                actualPos[i + 1] = container[i + 1].position;
            }
            i++;
        }
        ExaC.idChecking = Num;
    }
    
    void Update()
    {
        if (Num != -1)
        {
            Num = SnappinScrollerCat.result;
            //Pass the num of Categories
            CheckinCat.nCat = Num;
        }

        if(dispose)
        {
            //Debug.Log("dispose");
            CheckinCat.repos=true;
            DisposeThem();
            CheckinCat.checkCat = true;
            dispose = false;
        }
        
        if (numStore != Num)
        {
            numStore = Num;
            WhosOnStage();
            CheckinCat.control=true;
        }

        if (counter == Num)
        {
            Debug.Log("Ferma ExaC");
            if (!stop)
            {
                Debug.Log("Passa GreenLight");
                CheckinCat.greenLight = true;
                stop = true;
            }
            ExaC.checkMove = false;
            ExaC.idChecking = -1;
            counter = 0;
            CheckinCat.repos=false;
        }
    }
}
