﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class PowercardsEvents : MonoBehaviour 
{
	public static int currentCard = 1;

	//VAR 4 Textify
	private string[] strgs;

	//public Image cardImg;
	public Exa2Dnew cardImg;
	public Image iconImg;
	public Text titleCardTxt;
	public Text content1Txt;
	public Text content2Txt;
	public Text content3Txt;

	public static bool lang;
	public static bool changeNext;
	public static bool changePrev;

	public Textify textify;
	
	void Update()
	{
		if (lang)
		{
			showCard();
			lang = false;
		}

		if (changeNext)
		{
			nextPowercard();
			changeNext = false;
		}
		
		if (changePrev)
		{
			prevPowercard();
			changePrev = false;
		}
	}

	void showCard() 
	{
		switch(currentCard){
		case 1 :
			cardImg.colorInner = new Color32(40,163,206,255);
			cardImg.colorNow=true;
			iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_PlaySafely");
			titleCardTxt.text = textify.OnDemand("power", 0);
			content1Txt.text = textify.OnDemand("power", 1);
			content2Txt.text = textify.OnDemand("power", 2);
			content3Txt.text = textify.OnDemand("power", 3);
			break;
		case 2 :
			cardImg.colorInner = new Color32(4,62,132,255);
			cardImg.colorNow=true;
			iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_SocialLife");
			titleCardTxt.text = textify.OnDemand("power", 4);
			content1Txt.text = textify.OnDemand("power", 5);
			content2Txt.text = textify.OnDemand("power", 6);
			content3Txt.text = textify.OnDemand("power", 7);
			break;
		case 3 :
			cardImg.colorInner = new Color32(0,167,155,255);
			cardImg.colorNow=true;
			iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_DontRisk");
			titleCardTxt.text = textify.OnDemand("power", 8);
			content1Txt.text = textify.OnDemand("power", 9);
			content2Txt.text = textify.OnDemand("power", 10);
			content3Txt.text = textify.OnDemand("power", 11);
			break;
		case 4 :
			cardImg.colorInner = new Color32(106,172,35,255);
			cardImg.colorNow=true;
			iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_noBull");
			titleCardTxt.text = textify.OnDemand("power", 12);
			content1Txt.text = textify.OnDemand("power", 13);
			content2Txt.text = textify.OnDemand("power", 14);
			content3Txt.text = textify.OnDemand("power", 15);
			break;
		case 5 :
			cardImg.colorInner = new Color32(235,163,29,255);
			cardImg.colorNow=true;
			iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_PrivacyMatters");
			titleCardTxt.text = textify.OnDemand("power", 16);
			content1Txt.text = textify.OnDemand("power", 17);
			content2Txt.text = textify.OnDemand("power", 18);
			content3Txt.text = textify.OnDemand("power", 19);
			break;
		case 6 :
			cardImg.colorInner = new Color32(231,102,44,255);
			cardImg.colorNow=true;
			iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_LetsChat");
			titleCardTxt.text = textify.OnDemand("power", 20);
			content1Txt.text = textify.OnDemand("power", 21);
			content2Txt.text = textify.OnDemand("power", 22);
			content3Txt.text = textify.OnDemand("power", 23);
			break;
		case 7 :
			cardImg.colorInner = new Color32(191,39,45,255);
			cardImg.colorNow=true;
			iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_WatchOut");
			titleCardTxt.text = textify.OnDemand("power", 24);
			content1Txt.text = textify.OnDemand("power", 25);
			content2Txt.text = textify.OnDemand("power", 26);
			content3Txt.text = textify.OnDemand("power", 27);
			break;
		case 8 :
			cardImg.colorInner = new Color32(194,35,128,255);
			cardImg.colorNow=true;
			iconImg.sprite = Resources.Load<Sprite>("Textures/Sprite_IsItFair");
			titleCardTxt.text = textify.OnDemand("power", 28);
			content1Txt.text = textify.OnDemand("power", 29);
			content2Txt.text = textify.OnDemand("power", 30);
			content3Txt.text = textify.OnDemand("power", 31);
			break;
		}
		
		RotatorCardP.cardN = currentCard;
		Point.sectionCalling = "power";
		Point.on = currentCard;
		Point.check = true;
	}
	
    //Go to next card
	public void nextPowercard() {
		++currentCard;
		if(currentCard>8) {
			currentCard=1;
		} 
		showCard ();
	}
	
    //Go to previous card
    public void prevPowercard() {
		--currentCard;
		if(currentCard<1) {
			currentCard=8;
		} 
		showCard ();
	}
}
