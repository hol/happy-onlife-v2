﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class ComeBackHome : MonoBehaviour
{
    public CatBtn myBtn;
    private Vector3 _from;
    public Vector3 _to;
    private float t;
    private float _time;

    public bool comeBackHome=false;
    private bool move=false;
    public AnimationCurve curve;
    void Awake()
    {
        _to=transform.position;
        t=0;
        _time=0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if(comeBackHome)
        {
            _from=transform.position;
            move=true;
            myBtn.clicked=false;
            comeBackHome=false;
        }

        if (move)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            transform.position=Vector3.Lerp(_from, _to, curve.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                move = false;
                t = 0;
            }
        }
    }
}
