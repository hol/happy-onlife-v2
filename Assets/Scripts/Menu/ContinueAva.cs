﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinueAva : MonoBehaviour
{
    private Button me;
    public Text myTxt;

    public static bool able;
    
    public static bool dis;
    
    void Start()
    {
        me = transform.GetComponent<Button>();
        me.interactable = false;
        myTxt.color = new Color(1, 1, 1, 0.2f );
        able = false;
        dis = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (able)
        {
            me.interactable = true;
            myTxt.color = new Color(1, 1, 1, 1 );
            able = false;
        }
        
        if (dis)
        {
            me.interactable = false;
            myTxt.color = new Color(1, 1, 1, 0.2f );
            dis = false;
        }
    }
}
