﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

//Settings of Players page
using UnityEngine;
using UnityEngine.UI;

public class ChoosePlayers : MonoBehaviour 
{
	public Button continueBtn;
	private Text continueTxt;

	public Transform Avatar;
	public Transform Avatar1;
	public Transform Avatar2;

	Vector3 avaPos;
	Vector3 avaPos1;
	Vector3 avaPos2;
	Vector3 ava2Pos;
	Vector3 ava2Pos1;
	Vector3 ava2Pos2;

	private SetFX fX;
	private SetFX fX1;
	private SetFX fX2;

	private AnimRot ar;
	private AnimRot ar1;
	private AnimRot ar2;

	

	bool singleP;
	bool singlePA;
	bool singlePS;
	bool doubleP;
	bool doublePA;
	bool doublePS;

	public float hOffset = 110;

	private float t;
    private float duration;    
    public AnimationCurve curveUp;
    public AnimationCurve curveDown;

	Color32 txtClr;
	void Start ()
	{
		continueTxt=continueBtn.GetComponentInChildren<Text>();
		
		fX=Avatar.GetComponentInChildren<SetFX>();
		fX1=Avatar1.GetComponentInChildren<SetFX>();
		fX2=Avatar2.GetComponentInChildren<SetFX>();

		ar=Avatar.GetComponentInChildren<AnimRot>();
		ar1=Avatar1.GetComponentInChildren<AnimRot>();
		ar2=Avatar2.GetComponentInChildren<AnimRot>();

		avaPos=Avatar.localPosition;
		//print(avaPos);
		avaPos1=Avatar1.localPosition;
		avaPos2=Avatar2.localPosition;
		ava2Pos=new Vector3 (avaPos.x,avaPos.y+hOffset,avaPos.z);
		//print(ava2Pos);
		ava2Pos1=new Vector3 (avaPos1.x,avaPos1.y+hOffset,avaPos1.z);
		ava2Pos2=new Vector3 (avaPos2.x,avaPos2.y+hOffset,avaPos2.z);

		continueBtn.interactable=false;
		txtClr = new Color(1, 1, 1, 0.2f );
		continueTxt.color=txtClr;

		t=0;

		duration=1;
	}
	
	void Update()
	{

		if(singlePS)
		{
			fX.sd=true;
			fX.mr=true;
			ar.go=true;

			if (doublePA)
            {
				fX1.noMr=true;
				fX2.noMr=true;
				ar1.stop=true;
				ar2.stop=true;
            }

			singleP=true;
			singlePS=false;
		}

		if(singleP)
		{
			t += Time.deltaTime;

            float s = t / duration;
            
            float checker = Mathf.Lerp(0, 1, curveUp.Evaluate(s));
            
            Avatar.localPosition = Vector3.Lerp(avaPos, ava2Pos, curveUp.Evaluate(s));

            if (doublePA)
            {
                Avatar1.localPosition = Vector3.Lerp(ava2Pos1, avaPos1, curveDown.Evaluate(s));
                Avatar2.localPosition = Vector3.Lerp(ava2Pos2, avaPos2, curveDown.Evaluate(s));
            }
            
            if (checker == 1)
            {
                singleP = false;
                singlePA = true;
				if (doublePA)
            	{
					doublePA=false;
				}
                t = 0;
            }
		}

		if(doublePS)
		{
			fX1.sd=true;
			fX1.mr=true;
			fX2.sd=true;
			fX2.mr=true;
			ar1.go=true;
			ar2.go=true;

			if (singlePA)
            {
				fX.noMr=true;
				ar.stop=true;
            }

			doubleP=true;
			doublePS=false;
		}

		if(doubleP)
		{
			t += Time.deltaTime;
            float s = t / duration;
            
            float checker = Mathf.Lerp(0, 1, curveUp.Evaluate(s));
            
            Avatar1.localPosition = Vector3.Lerp(avaPos1, ava2Pos1, curveUp.Evaluate(s));
            Avatar2.localPosition = Vector3.Lerp(avaPos2, ava2Pos2, curveUp.Evaluate(s));

            if (singlePA)
            {
                Avatar.localPosition = Vector3.Lerp(ava2Pos, avaPos, curveDown.Evaluate(s));
            }
            
            if (checker == 1)
            {
                doubleP = false;
                doublePA = true;
				if (singlePA)
				{
					singlePA=false;
				}
                t = 0;
            }
		}
	}

	/*
	 * The function chooseAvatar is connected to the One Player button
	 * When the user press the button the function writes into PlayerPrefs the numPlayers property equal to 1
	 * And the curAvatarPlayer to 1 (this is to tell the level that it actually is to choose the first player avatar)
	 */
	public void chooseAvatar()
	{
		PlayerPrefs.SetInt ("numplayers", 1);
		PlayerPrefs.SetInt ("p2type", 0);
		ChoosAvatarEvents.check = true;
		PlayerPrefs.SetInt ("curAvatarPlayer", 1);
		singlePS=true;
		continueBtn.interactable=true;
		continueTxt.color=Color.white;
	}

	/*
	 * The function chooseAvatar2 is connected to the Two Players button
	 * When the user press the button the function writes into PlayerPrefs the numPlayers property equal to 2
	 * And the curAvatarPlayer to 1 (this is to tell the level that it actually is to choose the first player avatar)
	 */
	public void chooseAvatar2() 
	{
		PlayerPrefs.SetInt ("numplayers", 2);
		ChoosAvatarEvents.check = true;
		PlayerPrefs.SetInt ("curAvatarPlayer", 1);
		doublePS=true;
		continueBtn.interactable=true;
		continueTxt.color=Color.white;
	}

	public void Reset()
	{
		Avatar.localPosition=avaPos;
		Avatar1.localPosition=avaPos1;
		Avatar2.localPosition=avaPos2;
		continueBtn.interactable=false;
		continueTxt.color=txtClr;
	}

	public void Continue()
	{
		DisposeCircular3D.initMove = true;
	}
}
