﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class ProviderMovement : MonoBehaviour
{
    public static Transform me;
    public static Vector3 _from;
    public static Vector3 _to;
    
    public static float t;
    public static float _time;

    public static bool move;
    
    
    public static Transform me2;
    public static Vector3 _from2;
    public static Vector3 _to2;
    
    public static float t2;
    public static float _time2;

    public static bool back;
    
    public AnimationCurve curve;
    
    void Start()
    {
        t = 0;
        t2 = 0;
        move = false;
        back = false;
    }

    public static void MoveThis(Transform obj, Vector3 from, Vector3 to, float time)
    {
        me = obj;
        _from = from;
        _to = to;
        _time = time;
        
        move = true;
    }
    
    public static void BackThat(Transform obj2, Vector3 from2, Vector3 to2, float time2)
    {
        me2 = obj2;
        _from2 = from2;
        _to2 = to2;
        _time2 = time2;
        
        back = true;
    }

    void Update()
    {
        if (move)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            me.position=Vector3.Lerp(_from, _to, curve.Evaluate(s));
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                move = false;
                t = 0;
            }
        }
        
        if (back)
        {
            t2 += Time.deltaTime;
            float s2 = t2 / _time2;
            
            me2.position=Vector3.Lerp(_from2, _to2, curve.Evaluate(s2));
            float checker2=Mathf.Lerp(0,  1, curve.Evaluate(s2));
            
            if (checker2 == 1)
            {
                back = false;
                t2 = 0;
            }
        }
    }
}
