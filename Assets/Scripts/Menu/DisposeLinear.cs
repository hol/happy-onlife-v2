﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
[ExecuteInEditMode]
public class DisposeLinear : MonoBehaviour
{
    [Tooltip("Single Target: it provides fixedValue")]
    public Transform targetA;
    [Tooltip("Multiple Target: together with Target A they provide the line")]
    public Transform targetB;
    [Tooltip("If present, it provides start Value")]
    public bool disposeLinear;
    public bool editMode;
    
    public RectTransform[] container;
    
    private int totObj;
    
    public bool horizontal;
    public bool vertical;
    
    public float fixedValue;
    public float startValue;
    public float endValue;
    
    private float totValue;
    private float finalValue;
    private float remainigValue;
    
    void Start()
    {
        totObj = container.Length;
        
        if (!horizontal && !vertical)
        {
            vertical = true;
        }
        
        disposeLinear = true;
    }

    void Dispose()
    {
        if (horizontal)
        {
            if (targetB)
            {
                
            }
            else if (targetA)
            {
                fixedValue = targetA.localPosition.y;
            }
            
            totValue = Screen.width;
        }
        else if (vertical)
        {
            if (targetB)
            {
                
            }
            else if (targetA)
            {
                fixedValue = targetA.localPosition.x;
            } 
            
            totValue = Screen.height;
        }

        finalValue = (totValue - startValue - endValue);
        
        if (targetB)
        {
            for (int i = 0; i < totObj; i++)
            {
                if (container[i])
                {
                    Vector3 pos = Vector3.zero;

                    float missingValue = (totValue / 2) - startValue - (finalValue / (totObj-1)) * i;
                    float missingValue2 = (totValue / 2) - startValue - (finalValue / (totObj-1)) * i;

                    if (horizontal)
                    {
                        pos = new Vector3(missingValue, fixedValue, 0);
                    }
                    else if (vertical)
                    {
                        pos = new Vector3(fixedValue, missingValue, 0);
                    }
                
                    container[i].localPosition = pos;
                }
            }
        }
        else
        {
            for (int i = 0; i < totObj; i++)
            {
                if (container[i])
                {
                    Vector3 pos = Vector3.zero;

                    float missingValue = (totValue / 2) - startValue - (finalValue / (totObj-1)) * i;

                    if (horizontal)
                    {
                        pos = new Vector3(missingValue, fixedValue, 0);
                    }
                    else if (vertical)
                    {
                        pos = new Vector3(fixedValue, missingValue, 0);
                    }
                
                    container[i].localPosition = pos;
                }
            }
        }
    }
    
    void Update()
    {
        if (disposeLinear)
        {
            totObj = container.Length;
            
            Dispose();
            
            if (!editMode)
            {
                disposeLinear = false;
            }
        }
    }
}
