﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
public class MenuMovement : MonoBehaviour
{
    public static int actualPage;
    private AssistKeyboardGroups akg;
    
    public Camera cam;
    public RectTransform FixedUI;
    public RectTransform FixedBG;
    public RectTransform Languages;
    public RectTransform MainMenu;
    public RectTransform Categories;
    public RectTransform Players;
    public Transform Player3D;
    public RectTransform Avatars;
    public Transform Avatar3DType;
    public Transform Avatar3DHufol;
    public Transform Avatar3DHearthol;
    public Transform Avatar3DSpeedhol;
    public RectTransform Activities;
    public RectTransform Powercards;
    public RectTransform Instructions;
    public RectTransform About;
    public RectTransform Score;

    private RectTransform[] pages;

    private RectTransform prevP;
    private RectTransform actualP;
    private RectTransform nextP;

    public Vector3 prevPos;
    public Vector3 actualPos;
    public Vector3 nextPos;

    public bool next;
    public bool prev;
    
    private float t;
    private float duration;
    
    private int w;
    private int wStore;
    
    public AnimationCurve curve;

    private float zIndex;

    public static float initialOffset;
    
    void Start()
    {
        SetCamSize();
        
        FixedUI.transform.gameObject.SetActive(true);
        
        t = 0;
        duration = 0.8f;
        pages=new []{Languages, MainMenu, Categories, Players, Avatars, Instructions, Activities, Powercards, About, Score};
        actualP = pages[0];
        akg=transform.GetComponent<AssistKeyboardGroups>();

        Measure();
        initialOffset = w;
        ShowAll();
        
        PrepareAll();
    }
    
    void SetCamSize()
    {
        float camSize = 404f;
        cam.orthographicSize = camSize;
    }

    void Measure()
    {
        zIndex = Vector3.Distance(actualP.position, cam.transform.position);
        zIndex = 0;
        w = 3480;
        wStore = w;
        nextPos = new Vector3(w,0,zIndex);
        actualPos = new Vector3(0,0,zIndex);
        prevPos = new Vector3(-w,0,zIndex);
        print("w="+w);
    }
    
    void ShowAll()
    {
        for (int i = 0; i < pages.Length; i++)
        {
            pages[i].transform.gameObject.SetActive(true);
        }
        Player3D.gameObject.SetActive(true);
        Avatar3DType.gameObject.SetActive(true);
        Avatar3DHufol.gameObject.SetActive(true);
        Avatar3DHearthol.gameObject.SetActive(true);
        Avatar3DSpeedhol.gameObject.SetActive(true);
    }

    void PrepareAll()
    {
        for (int i = 0; i < pages.Length; i++)
        {
            if (pages[i] != actualP)
            {
                pages[i].localPosition = new Vector3(initialOffset, 0, zIndex);
            }
        }
        Player3D.position =  new Vector3(initialOffset, 84.56f, 635.88f);
        Avatar3DType.position =  new Vector3(initialOffset, 50f, 612.6f);
        Avatar3DHufol.position =  new Vector3(initialOffset, -64f, -2891f);
        Avatar3DHearthol.position =  new Vector3(initialOffset, -74f, -2891f);
        Avatar3DSpeedhol.position =  new Vector3(initialOffset, -64f, -2891f);
    }
    
    public void Next(int me)
    {
        actualPage = me;
        nextP = pages[me];
        Debug.Log("sto per mandare un "+me);
        
        if(me==5)
        {
            akg.focus=8;
        }
        else if(me==6)
        {
            akg.focus=9;
        }
        else if(me==7)
        {
            akg.focus=10;
        }
        else if(me==9)
        {
            akg.focus=12;
        }
        else if(me==8)
        {
            akg.focus=11;
        }
        else
        {
            akg.focus=me;
        }
        
        if(me==0)
        {
            DisposeCircular.initMove=true;
        }
        next = true;
    }
    
    public void Back(int me)
    {
        actualPage = me;
        prevP = pages[me];
        print("sto per mandare un "+me);
        
        akg.focus=me;
        
        prev = true;
    }
    
    void Update()
    {
        if (next)
        {
            t += Time.deltaTime;
            float s = t / duration;
            
            float checker = Mathf.Lerp(0, 1, curve.Evaluate(s));
            
            actualP.localPosition = Vector3.Lerp(actualPos, prevPos, curve.Evaluate(s));
            nextP.localPosition = Vector3.Lerp(nextPos, actualPos, curve.Evaluate(s));

            if (actualPage == 2)
            {
                FixedBG.localPosition = Vector3.Lerp(actualPos, prevPos, curve.Evaluate(s));
            }

            if (actualPage == 3)
            {
                Player3D.position = Vector3.Lerp(new Vector3(nextPos.x, 84.56f, 635.88f), new Vector3(0, 84.56f, 635.88f), curve.Evaluate(s));
            }
            
            if (actualPage == 4)
            {
                Player3D.position = Vector3.Lerp(new Vector3(0, 84.56f, 635.88f), new Vector3(prevPos.x, 84.56f, 635.88f), curve.Evaluate(s));
                Avatar3DType.position = Vector3.Lerp(new Vector3(nextPos.x, 50f, 612.6f), new Vector3(0, 50f, 612.6f), curve.Evaluate(s));
                Avatar3DHufol.position = Vector3.Lerp(new Vector3(nextPos.x, -64f, -2891f), new Vector3(0, -64f, -2891f), curve.Evaluate(s));
                Avatar3DHearthol.position = Vector3.Lerp(new Vector3(nextPos.x, -74f, -2891f), new Vector3(0, -74f, -2891f), curve.Evaluate(s));
                Avatar3DSpeedhol.position = Vector3.Lerp(new Vector3(nextPos.x, -64f, -2891f), new Vector3(0, -64f, -2891f), curve.Evaluate(s));
            }
            
            if (checker == 1)
            {
                next = false;
                actualP = nextP;
                t = 0;
            }
        }
        
        if (prev)
        {
            t += Time.deltaTime;
            float s = t / duration;
            
            float checker = Mathf.Lerp(0, 1, curve.Evaluate(s));
            
            actualP.localPosition = Vector3.Lerp(actualPos, nextPos, curve.Evaluate(s));
            prevP.localPosition = Vector3.Lerp(prevPos, actualPos, curve.Evaluate(s));

            if (actualPage == 1 && nextP==Categories || actualPage == 1 && nextP==Players || actualPage == 1 && nextP==Avatars)
            {
                FixedBG.localPosition = Vector3.Lerp(prevPos, actualPos, curve.Evaluate(s));
            }

            if (actualPage == 2)
            {
                Player3D.position = Vector3.Lerp(new Vector3(0, 84.56f, 635.88f), new Vector3(nextPos.x, 84.56f, 635.88f), curve.Evaluate(s));
            }
            
            if (actualPage == 3)
            {
                Player3D.position = Vector3.Lerp(new Vector3(prevPos.x, 84.56f, 635.88f), new Vector3(0, 84.56f, 635.88f),  curve.Evaluate(s));
                Avatar3DType.position = Vector3.Lerp(new Vector3(0, 50f, 612.6f), new Vector3(nextPos.x, 50f, 612.6f), curve.Evaluate(s));
                Avatar3DHufol.position = Vector3.Lerp(new Vector3(0, -64f, -2891f), new Vector3(nextPos.x, -64f, -2891f), curve.Evaluate(s));
                Avatar3DHearthol.position = Vector3.Lerp(new Vector3(0, -74f, -2891f), new Vector3(nextPos.x, -74f, -2891f), curve.Evaluate(s));
                Avatar3DSpeedhol.position = Vector3.Lerp(new Vector3(0, -64f, -2891f), new Vector3(nextPos.x, -64f, -2891f), curve.Evaluate(s));
            }
            
            if (checker == 1)
            {
                prev = false;
                actualP = prevP;
                t = 0;
            }
        }
    }
}
