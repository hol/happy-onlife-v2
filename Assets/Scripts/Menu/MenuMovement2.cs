﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class MenuMovement2 : MonoBehaviour
{
    public RectTransform Languages;
    public RectTransform CommonUI;
    public RectTransform LogoUI;
    public RectTransform BackUI;
    public RectTransform MainMenu;
    public RectTransform Categories;
    public RectTransform Players;
    public RectTransform Avatars;
    public RectTransform Activities;
    public RectTransform Powercards;
    public RectTransform Instructions;
    public RectTransform About;
    public RectTransform Score;

    private RectTransform[] pages;

    private RectTransform prevP;
    private RectTransform actualP;
    private RectTransform nextP;

    public Transform Avatar3D;

    public int prevN;
    public int actualN;
    public static int nextN;
    
    private bool move;
    private bool move2;
    private bool move2back;
    private bool move3;
    private bool move3back;
    private bool moveAvatar3D;
    private bool moveAvatar3Dback;
    private bool back;
    private bool logo;
    public static bool next;
    
    private float t;
    private float duration;

    private float origAX;
    private float origNX;
    private float passAX;
    private float passNX;
    private float destAX;
    private float destNX;
    
    private float w;
    
    public AnimationCurve curve;

    private Vector3 A3DorigPos;
    private Vector3 A3DdestPos;
    
    // Start is called before the first frame update
    void Start()
    {
        w = Screen.width;
        move = false;
        move2 = false;
        move2back = false;
        move3 = false;
        move3back = false;
        back = false;
        logo = false;
        next = false;
        t = 0;
        duration = 0.7f;
        pages=new RectTransform[]{Languages, CommonUI, LogoUI, BackUI, MainMenu, Categories, Players, Avatars, Instructions, Activities, Powercards, About, Score};
        actualP = pages[0];
        origAX=actualP.localPosition.x;
        destAX=origAX-w;
        origNX=origAX+w;
        destNX=origAX;
        actualN = 0;

        A3DorigPos = Avatar3D.localPosition;
        A3DdestPos = new Vector3(Screen.width,A3DdestPos.y, A3DorigPos.z);
        
        AllVisible();
        AllTheirPos();
    }
    
    void AllVisible()
    {
        for (int i = 0; i < pages.Length; i++)
        {
            pages[i].transform.gameObject.SetActive(true);
        }
    }

    void AllTheirPos()
    {
        for (int i = 2; i < pages.Length; i++)
        {
            pages[i].localPosition = new Vector3(origNX, pages[i].localPosition.y, pages[i].localPosition.z);
        }

        Avatar3D.localPosition = A3DdestPos;
    }
    
    void Next(int me)
    {
        nextP = pages[me];
        nextN = me;
        CheckAndGo(nextN);
    }
    
    public void Back(int me = 0)
    {
        if(me==0)
        nextP = prevP;
        nextN = prevN;
        CheckAndGo(prevN);
    }
    
    void CheckAndGo(int me)
    {
        if (me == 0)
        {
            destAX=origAX+w;
            origNX=origAX-w;
            if (logo)
            {
                move2back = true;
                logo = false;
            }
            if (back)
            {
                move3back = true;
                back = false;
            }
        }
        else
        {
            if (me == 4)
            {
                if (!logo)
                {
                    move2 = true;
                    logo = true;
                }
                
                if (back)
                {
                    move3back = true;
                    back = false;
                }
            }
            else if (me >= 5)
            {
                if (!back)
                {
                    move3 = true;
                    back = true;
                    //if (me == 6)
                    //{
                    //    moveAvatar3Dback = true;
                    //}
                }
                if (me == 7)
                {
                    moveAvatar3D = true;
                }
            }
            destAX=origAX-w;
            origNX=origAX+w;
        }
        
        t = 0;
        move = true;
    }
    
    void Update()
    {
        if (next)
        {
            Next(nextN);
            next = false;
        }

        if (move)
        {
            t += Time.deltaTime;
            float s = t / duration;
            passAX=Mathf.Lerp(origAX, destAX, curve.Evaluate(s));
            passNX=Mathf.Lerp(origNX,  destNX, curve.Evaluate(s));
            
            actualP.localPosition = new Vector3(passAX, 0, 0);
            nextP.localPosition = new Vector3(passNX, 0, 0);
            
            if (move2)
            {
                pages[2].localPosition=nextP.localPosition;
            }
            else if (move2back)
            {
                pages[2].localPosition=actualP.localPosition;
            }
            
            if (move3)
            {
                pages[3].localPosition=nextP.localPosition;
                pages[2].localPosition=actualP.localPosition;
                
            }
            else if (move3back)
            {
                pages[3].localPosition=actualP.localPosition;
                pages[2].localPosition=nextP.localPosition;
            }

            if (moveAvatar3D)
            {
                Avatar3D.localPosition = Vector3.Lerp(A3DdestPos, A3DorigPos, curve.Evaluate(s));
            }
            else if (moveAvatar3Dback)
            {
                Avatar3D.localPosition = Vector3.Lerp(A3DorigPos, A3DdestPos, curve.Evaluate(s));
            }

            if (passNX == destNX)
            {
                move = false;
                move2 = false;
                move2back = false;
                move3 = false;
                move3back = false;
                moveAvatar3D = false;
                moveAvatar3Dback = false;
                prevP=actualP;
                prevN=actualN;
                actualP=nextP;
                actualN=nextN;
                //Wave.initMove = true;
            }
        }
    }
}
