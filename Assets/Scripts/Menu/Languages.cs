﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System;
using UnityEngine;

public class Languages : MonoBehaviour 
{
	[Header("Languages (insert here new language)")]
	public string[] languages;
	public string defaultLang;
	public string[] alternativeFontLang;

	[Header("Readable (insert here ScreenReader languages)")]
	public string[] readable;
	public string[] readableWeb;

	[Header("Font")]
	public string resourceFolder;
	public Font[] fonts = new Font[2];

	[Header("Pages")]
	public Textify[] pages;

	void Awake()
	{
		//Alphabetical Order
        Array.Sort(languages);
	}

	void Start()
	{
		//Default if first time playing
		if (PlayerPrefs.GetString("linguaSuffix") == "")
        {
	        PlayerPrefs.SetString("linguaSuffix", defaultLang);
			print("DefaultLang="+defaultLang);
        }

		//Save font.names
		for(int i=0; i<fonts.Length; i++)
		{
			PlayerPrefs.SetString("fontName"+i, fonts[i].name);
		}
	}

	//Btns.onTouch (n language)
	public void SelectLanguage(int num)
	{
		PlayerPrefs.SetInt("lingua", num);
		PlayerPrefs.SetString("linguaSuffix", languages[num]);

		print("LangChange="+languages[num]);

		//For activate the Reader
	#if !UNITY_WEBGL
		//(For now ONLY this language can access to the Reader feature: EN).
		for (int i = 0; i < readable.Length; i++)
		{
			if(languages[num]==readable[i])
			{
				Reader.permission=true;
				ViewOnlyIf.permission=true;
			}
			else
			{
				Reader.permission=false;
				ViewOnlyIf.denied=true;
			}
		}
	#endif
	#if UNITY_WEBGL && !UNITY_EDITOR
		//(For now ONLY this language can access to the Reader feature from web: EN,ES,FR,IT,NL,PT,RO).
		for (int i = 0; i < readableWeb.Length; i++)
		{
			if(languages[num]==readableWeb[i])
			{
				Reader.permission=true;
				ViewOnlyIf.permission=true;
			}
			else
			{
				Reader.permission=false;
				ViewOnlyIf.denied=true;
			}
		}
	#endif
		

		//Font Check
		for(int i=0; i<fonts.Length-1; i++)
		{
			PlayerPrefs.SetString("curFont", resourceFolder + "/" + PlayerPrefs.GetString("fontName"+(0).ToString()));
			if(PlayerPrefs.GetString("linguaSuffix") == alternativeFontLang[i])
			{
				PlayerPrefs.SetString("curFont", resourceFolder + "/" + PlayerPrefs.GetString("fontName"+(i+1).ToString()));
			}
			print("fontCheck="+ PlayerPrefs.GetString("linguaSuffix") +" font="+ PlayerPrefs.GetString("curFont"));
		}

		TextifyAll();
	}

    void TextifyAll()
    {
		for(int i=0; i<pages.Length; i++)
		{
	    	pages[i].lang = true;
			if(pages[i].more)
			{
				TextifyExtra(i);
			}
		}
    }

	void TextifyExtra(int num)
    {
        switch (num)
        {
            case 3:
                ChoosAvatarEvents.lang = true;
                break;
            case 4:
                ScoreEvents.lang = true;
                break;
            case 5:
                PowercardsEvents.lang = true;
                break;
            case 6:
                ActivitiesEvents.lang = true;
                break;
        }
    }
}
