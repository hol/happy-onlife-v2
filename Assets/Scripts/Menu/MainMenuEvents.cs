﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuEvents : MonoBehaviour {
	//VAR 4 Textify
	private string[] strgs;

	public Text pageTitle;
	public Button startBtn;
	public Button scoreBtn;
	public Button langBtn;
	public Button powerBtn;
	public Button extraBtn;
	public Button instruBtn;
	public Button aboutBtn;

	private Text t1;
	private Text t2;
	private Text t3;
	private Text t4;
	private Text t5;
	private Text t6;
	private Text t7;

	public static bool lang;

	void Start () 
	{
		t1 = startBtn.GetComponentInChildren<Text>();
		t2 = scoreBtn.GetComponentInChildren<Text>();
		t3 = langBtn.GetComponentInChildren<Text>();
		t4 = powerBtn.GetComponentInChildren<Text>();
		t5 = extraBtn.GetComponentInChildren<Text>();
		t6 = instruBtn.GetComponentInChildren<Text>();
		t7 = aboutBtn.GetComponentInChildren<Text>();
    }

	void Update()
	{
		if (lang)
		{
			LangTxt();
			lang = false;
		}
	}

	//Textify
    void LangTxt () 
	{
		TextAsset qAsset = null;
		string suffix = PlayerPrefs.GetString ("linguaSuffix");
		string fname = "Text/menu_" + suffix;
		qAsset = (TextAsset)Resources.Load(fname);
		string testoIntero = qAsset.text;
		strgs = testoIntero.Split ("\r\n" [0]);
		
		string[] strg = strgs [0].Split ('=');
		t1.text = strg[1];
		
		strg = strgs [1].Split ('=');
		t2.text=strg[1];
		
		strg = strgs [2].Split ('=');
		t3.text=strg[1];
		
		strg = strgs [3].Split ('=');
		t4.text=strg[1];
		
		strg = strgs [4].Split ('=');
		t5.text=strg[1];
		
		strg = strgs [5].Split ('=');
		t6.text=strg[1];
		
		strg = strgs [6].Split ('=');
		t7.text=strg[1];
		
		strg = strgs [13].Split ('=');
		pageTitle.text=strg[1];

		if (PlayerPrefs.GetString("linguaSuffix") == "ge" && Application.platform == RuntimePlatform.WebGLPlayer)
		{
            t1.font = (Font)Resources.Load("Fonts/micross");
            t1.fontStyle = FontStyle.Normal;
            t2.font = (Font)Resources.Load("Fonts/micross");
            t2.fontStyle = FontStyle.Normal;
            t3.font = (Font)Resources.Load("Fonts/micross");
            t3.fontStyle = FontStyle.Normal;
            t5.font = (Font)Resources.Load("Fonts/micross");
            t5.fontStyle = FontStyle.Normal;
            t6.font = (Font)Resources.Load("Fonts/micross");
            t6.fontStyle = FontStyle.Normal;
            t7.font = (Font)Resources.Load("Fonts/micross");
            t7.fontStyle = FontStyle.Normal;
            pageTitle.font = (Font)Resources.Load("Fonts/micross");
            pageTitle.fontStyle = FontStyle.Bold;
			print(t1.font);
        }
		else
		{
			t1.font = (Font)Resources.Load("Fonts/ECSquareSansPro-Regular");
			t1.fontStyle = FontStyle.Bold;
			t2.font = (Font)Resources.Load("Fonts/ECSquareSansPro-Regular");
			t2.fontStyle = FontStyle.Bold;
			t3.font = (Font)Resources.Load("Fonts/ECSquareSansPro-Regular");
			t3.fontStyle = FontStyle.Bold;
			t5.font = (Font)Resources.Load("Fonts/ECSquareSansPro-Regular");
			t5.fontStyle = FontStyle.Bold;
			t6.font = (Font)Resources.Load("Fonts/ECSquareSansPro-Regular");
			t6.fontStyle = FontStyle.Bold;
			t7.font = (Font)Resources.Load("Fonts/ECSquareSansPro-Regular");
			t7.fontStyle = FontStyle.Normal;
			pageTitle.font = (Font)Resources.Load("Fonts/ECSquareSansPro-Regular");
			pageTitle.fontStyle = FontStyle.Bold;
		}
    }

}
