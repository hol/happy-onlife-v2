﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class ScoreEvents : MonoBehaviour {

	public Text score1;
	public Text score2;
	public Text score3;
	public Text score4;
	public Text score5;

	public Text name1;
	public Text name2;
	public Text name3;
	public Text name4;
	public Text name5;

	public static bool lang;
	
	void Start ()
	{
		int s1 = PlayerPrefs.GetInt ("sc1", 0);
		int s2 = PlayerPrefs.GetInt ("sc2", 0);
		int s3 = PlayerPrefs.GetInt ("sc3", 0);
		int s4 = PlayerPrefs.GetInt ("sc4", 0);
		int s5 = PlayerPrefs.GetInt ("sc5", 0);
		string n1 = PlayerPrefs.GetString ("nam1", "None");
		string n2 = PlayerPrefs.GetString ("nam2", "None");
		string n3 = PlayerPrefs.GetString ("nam3", "None");
		string n4 = PlayerPrefs.GetString ("nam4", "None");
		string n5 = PlayerPrefs.GetString ("nam5", "None");
		score1.text = s1.ToString ("#,000");
		score2.text = s2.ToString ("#,000");
		score3.text = s3.ToString ("#,000");
		score4.text = s4.ToString ("#,000");
		score5.text = s5.ToString ("#,000");
		name1.text = n1;
		name2.text = n2;
		name3.text = n3;
		name4.text = n4;
		name5.text = n5;
	}
	
	void Update()
	{
		if (lang)
		{
			LangTxt();
			lang = false;
		}
	}
	
	void LangTxt () 
	{
		score1.font = (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		score1.fontStyle = FontStyle.Normal;
		score2.font = (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		score2.fontStyle = FontStyle.Normal;
		score3.font = (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		score3.fontStyle = FontStyle.Normal;
		score4.font = (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		score4.fontStyle = FontStyle.Normal;
		score5.font = (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		score5.fontStyle = FontStyle.Normal;
		name1.font = (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		name1.fontStyle = FontStyle.Normal;
		name2.font = (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		name2.fontStyle = FontStyle.Normal;
		name3.font = (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		name3.fontStyle = FontStyle.Normal;
		name4.font = (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		name4.fontStyle = FontStyle.Normal;
		name5.font = (Font)Resources.Load(PlayerPrefs.GetString("curFont"));
		name5.fontStyle = FontStyle.Normal;
    }
}
