﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(ScrollRect))]
public class SnappinScrollerCat : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler {

    public static int startPage = -1;
    public int width = 120;
    public float speedSwipe = 0.3f;
    private int speedSwipeDist = 0;
    public float decelerationRate = 0.5f;

    private int widthStore;
    
    private int speedSwipeMax;

    private ScrollRect scroller;
    private RectTransform scrollC;

    private bool hor;
    
    private int totPage;
    private int curPage;

    private bool move;
    private Vector2 dest;

    private List<Vector2> pagePos = new List<Vector2>();

    private bool drag;
    private float _timeStamp;
    private Vector2 startPos;
    
    private int pageStore;
    
    private List<Image> pageImg;

    public static int result = 4;
    
    private RectTransform[] child;

    //4F@ckin Georgian
    public Text num5;
    public Text num6;
    public Text num7;
    public Text num8;

    public static bool check4Lang;
    
    void Start()
    {
        scroller = GetComponent<ScrollRect>();
        scrollC = scroller.content;
        totPage = scrollC.childCount;
        //Debug.Log("totPage"+totPage);
        
        if (scroller.horizontal && !scroller.vertical)
        {
            hor = true;
        }
        else if (!scroller.horizontal && scroller.vertical)
        {
            hor = false;
        }
        else
        {
            hor = true;
        }

        move = false;
        
        child = new RectTransform[scrollC.childCount];

        ChildFactory();
        SetPagePositions();
        
        widthStore = width;
    }

    private void ChildFactory()
    {
        for (int i = 0; i < totPage; i++)
        {
            child[i] = scrollC.GetChild(i).GetComponent<RectTransform>();
        }
    }

    private void SetPagePositions() 
    {
        int height = 0;
        int offsetX = 0;
        int offsetY = 0;
        int containerWidth = 0;
        int containerHeight = 0;

        if (hor) 
        {
            //Debug.Log("width="+width);
            offsetX = (int)(width * 0.5f);
            containerWidth = width * totPage + offsetX * totPage + offsetX;
            //Debug.Log("containerWidth="+containerWidth);
            speedSwipeMax = width;
        } 
        else 
        {
            height = (int)scrollC.sizeDelta.y;
            offsetY = height / 2;
            containerHeight = height * totPage;
            speedSwipeMax = height;
        }
        
        Vector2 scrollCSize = new Vector2(containerWidth, containerHeight);
        scrollC.sizeDelta = scrollCSize;
        Vector2 scrollCPos = new Vector2(scrollCSize.x / 2, scrollCSize.y / 2);
        scrollC.anchoredPosition = scrollCPos;

        pagePos.Clear();

        for (int i = 0; i < totPage; i++) 
        {
            Vector2 childPosition;
            if (hor) 
            {
                childPosition = new Vector2((i+1) * width - containerWidth * 0.5f + offsetX, 0f);
            }
            else 
            {
                childPosition = new Vector2(0f, -(i * height - containerHeight / 2 + offsetY));
            }
            child[i].anchoredPosition = childPosition;
            pagePos.Add(-childPosition);
        }
    }
    
    private void SetPage(int startPageIndex) 
    {
        startPageIndex = Mathf.Clamp(startPageIndex, 0, totPage - 1);
        scrollC.anchoredPosition = pagePos[startPageIndex];
        curPage = startPageIndex;
    }
    
    void Update() 
    {
        if (startPage != -1)
        {
            SetPage(startPage);
            Debug.Log("Scroller parte da "+ startPage);
            result = startPage + 2;
            Debug.Log("Passo a DisponingGrill "+ result);
            DisposinGrillExaC.Num = result;
            startPage = -1;
        }
        
        if (widthStore != width)
        {
            SetPagePositions();
            widthStore = width;
        }
        
        if (move) 
        {
            float decelerate = Mathf.Min(decelerationRate * Time.deltaTime, 1f);
            scrollC.anchoredPosition = Vector2.Lerp(scrollC.anchoredPosition, dest, decelerate);
            
            if (Vector2.SqrMagnitude(scrollC.anchoredPosition - dest) < 0.25f) 
            {
                // snap to target and stop lerping
                scrollC.anchoredPosition = dest;
                move = false;
                scroller.velocity = Vector2.zero;
            }
        }
    }
    
    private int GetNearestPage() {
        // based on dist from current position, find nearest page
        Vector2 curPos = scrollC.anchoredPosition;

        float dist = float.MaxValue;
        int nearestPage = curPage;

        for (int i = 0; i < totPage; i++) //BEFORE: pagePos.Count
        {
            float testDist = Vector2.SqrMagnitude(curPos - pagePos[i]);
            if (testDist < dist) 
            {
                dist = testDist;
                nearestPage = i;
            }
        }

        return nearestPage;
    }
    
    public void LerpToPage(int startPageIndex) 
    {
        startPageIndex = Mathf.Clamp(startPageIndex, 0, totPage - 1);
        dest = pagePos[startPageIndex];
        move = true;
        curPage = startPageIndex;
        result = curPage + 2;
        Debug.Log("Chiamata:"+result);
    }
    
    public void OnBeginDrag(PointerEventData aEventData) 
    {
        move = false;
        drag = false;
    }
    
    public void OnDrag(PointerEventData aEventData) 
    {
        if (!drag) 
        {
            drag = true;
            _timeStamp = Time.unscaledTime;
            startPos = scrollC.anchoredPosition;
        }
    }
    
    public void OnEndDrag(PointerEventData aEventData) 
    {
        float difference;
        
        if (hor) 
        {
            difference = startPos.x - scrollC.anchoredPosition.x;
        }
        else
        {
            difference = - (startPos.y - scrollC.anchoredPosition.y);
        }

        if (Time.unscaledTime - _timeStamp < speedSwipe &&
            Mathf.Abs(difference) > speedSwipeDist &&
            Mathf.Abs(difference) < speedSwipeMax) 
        {
            if (difference > 0) 
            {
                //next
                LerpToPage(curPage + 1);
            } 
            else
            {
                //previous
                LerpToPage(curPage - 1);
            }
        }
        else
        {
            int where = GetNearestPage();
            LerpToPage(where);
        }

        drag = false;
    }
}
