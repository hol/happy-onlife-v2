﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class CatBtn : MonoBehaviour
{
    public int ID;
    public DraggerMasterCat dmc;
    public bool clicked=false;
    private bool statusStore=false;

    public static bool locked=false;

    public void Click()
    {
        if(!locked || clicked)
        {
            clicked=!clicked;
            locked=false;
        }
    }

    void Unlock()
    {
        transform.GetComponent<Button>().interactable=true;
    }

    void Update()
    {
        if(clicked!=statusStore)
        {
            if(clicked)
            {
                CheckinCat.CheckThis(ID);
                transform.GetComponent<Button>().interactable=false;
                dmc.DisableThis(ID);
                Invoke("Unlock", 0.5f);
                print("clicked CheckThis");
            }
            else
            {
                CheckinCat.FreeThis(ID);
                transform.GetComponent<Button>().interactable=false;
                dmc.EnableThis(ID);
                Invoke("Unlock", 0.5f);
                print("clicked FreThis");
            }
            statusStore=clicked;
        }
    }
}
