/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class SSSelected : MonoBehaviour
{
    [Header("Button Connection")]
    public int ID;
    public SSBtnCom btnKnx;

    [Header("FX Connection")]
    public GameObject[] fxKnx;

    [Header("Selected")]
    public bool selected;
    public bool go;
    public bool right;
    public bool left=true;

    [Header("Unselected")]
    public bool unselected;
    public bool back;

    [Header("Position")]
    public bool pos;
    public Vector3 posS;
    public Vector3 posE;
    public Vector3 posB;

    [Header("Rotation")]
    public bool rot;
    public Vector3 rotS;
    public Vector3 rotE;
    public Vector3 rotB;

    [Header("Scale")]
    public bool scale;
    public Vector3 scaleS;
    public Vector3 scaleE;
    public Vector3 scaleB;
    
    [Header("Motion")]
    private float t;
    public float durationS;
    public float durationE;
    public float durationB;
    public AnimationCurve curveS;
    public AnimationCurve curveE;
    public AnimationCurve curveB;
    private SetFX fx;
    
    void Start()
    {
        posS=transform.localPosition;
        rotS=transform.eulerAngles;
        scaleS=transform.localScale;
        fx=transform.GetComponent<SetFX>();
    }

    void Activate(bool right=true)
    {
        if(right)
        {
            for(int i=0; i<fxKnx.Length; i++)
            {
                fxKnx[i].SetActive(true);
            }
        }
        else
        {
            for(int i=0; i<fxKnx.Length; i++)
            {
                fxKnx[i].SetActive(false);
            }
        }
    }

    public void Select()
    {
        selected=true;
    }
    
    void Update()
    {
        if(selected)
        {
            btnKnx.focusOnID=ID;
            Activate();
            fx.StartingDust.Play();
            fx.mr=true;
            go=true;
            selected=false;
        }

        if(go)
        {
            if (left)
            {
                t += Time.deltaTime;
                float s = t / durationS;
                
                float checker = Mathf.Lerp(0, 1, curveE.Evaluate(s));
                
                if(pos)
                    transform.localPosition = Vector3.Lerp(posS, posS+posE, curveE.Evaluate(s));
                if(rot)
                    transform.eulerAngles= Vector3.Lerp(rotS, rotS+rotE,  curveE.Evaluate(s));
                if(scale)
                    transform.localScale = Vector3.Lerp(scaleS, scaleS+scaleE,  curveE.Evaluate(s));
                
                if (checker == 1)
                {
                    left = false;
                    right = true;
                    t = 0;
                }
            }

            if (right)
            {
                t += Time.deltaTime;
                float s = t / durationE;
                
                float checker = Mathf.Lerp(0, 1, curveS.Evaluate(s));
                
                if(pos)
                    transform.localPosition = Vector3.Lerp(posS+posE, posS, curveS.Evaluate(s));
                if(rot)
                    transform.eulerAngles= Vector3.Lerp(rotS+rotE, rotS,  curveS.Evaluate(s));
                if(scale)
                    transform.localScale = Vector3.Lerp(scaleS+scaleE, scaleS,  curveS.Evaluate(s));
                
                if (checker == 1)
                {
                    right = false;
                    left = true;
                    t = 0;
                }
            }

            if(unselected)
            {
                go=false;
                left=true;
                right=false;

                posB=posS-transform.localPosition;
                posB=new Vector3(-Mathf.Abs(posB.x), -Mathf.Abs(posB.y), -Mathf.Abs(posB.z));
                rotB=rotS-transform.eulerAngles;
                scaleB=scaleS-transform.localScale;
                fx.noMr=true;

                go=false;
                left=true;
                right=false;
                back=true;
                unselected=false;

                Activate(false);
            }
        }

        if (back)
        {
            t += Time.deltaTime;
            float s = t / durationB;
            
            float checker = Mathf.Lerp(0, 1, curveB.Evaluate(s));
            
            if(pos)
                transform.localPosition = Vector3.Lerp(posS-posB, posS, curveB.Evaluate(s));
            if(rot)
                transform.eulerAngles= Vector3.Lerp(rotS-rotB, rotS, curveB.Evaluate(s));
            if(scale)
                transform.localScale = Vector3.Lerp(scaleS-scaleB, scaleS, curveB.Evaluate(s));
            
            if (checker == 1)
            {
                back = false;
                t = 0;
            }
        }
    }
}
