﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class TrailSetting : MonoBehaviour
{
    private float t;
    public float duration;

    public ScrollRect Scroll;

    public float yPos;

    private float offset;
    
    public AnimationCurve curve;

    private bool go;
    private bool back;

    private Color clr;

    private Vector3 origPos;
    private Vector3 secPos;
    
    private void Start()
    {
        t = 0;
        go = true;
        back = false;
    }

    
    void Update()
    {
        offset = Scroll.content.sizeDelta.x * 0.33f;
        Vector3 iconPos = Scroll.content.position;
        secPos = new Vector3(iconPos.x-(offset*0.25f), yPos, -66);

        if (go)
        {
            t += Time.deltaTime;
            float s = t / duration;
            
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));

            float offsetX=Mathf.Lerp(offset,  -offset, curve.Evaluate(s));
            transform.localPosition = new Vector3(secPos.x + offsetX, secPos.y, secPos.z);
            
            if (checker == 1)
            {
                go = false;
                t = 0;
                back = true;
            }
        }
        
        if (back)
        {
            t += Time.deltaTime;
            float s = t / duration;
            
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            float offsetX=Mathf.Lerp(-offset,  offset, curve.Evaluate(s));
            transform.localPosition = new Vector3(secPos.x + offsetX, secPos.y, secPos.z);
            
            if (checker == 1)
            {
                back = false;
                t = 0;
                go = true;
            }
        }
    }
}
