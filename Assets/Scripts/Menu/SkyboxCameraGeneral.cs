﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class SkyboxCameraGeneral : MonoBehaviour 
{
	[Header("Cameras")]
	// set the main camera in the inspector
	public Camera MainCamera;
	
	// set the sky box camera in the inspector
	public Camera SkyCamera;

    // set the sky box camera in the inspector
    public Camera AgainstCamera;
    public GameObject Stars;


    Rect norm = new Rect(0,0,1,1);
    Rect plus = new Rect(0.5F, 0, 1, 1);
	
    // the additional rotation to add to the skybox
    // can be set during game play or in the inspector
	[Header("Rotation Options")]
	public bool rotationActive;
    public Vector3 skyBoxRotation;
	public bool x;
    public float intensityX;
	public bool y;
    public float intensityY;
	public bool z;
    public float intensityZ;
	
	// Use this for initialization
	void Start()
	{
		if (SkyCamera.depth >= MainCamera.depth)
		{
			Debug.Log("Set skybox camera depth lower "+
			          " than main camera depth in inspector");
		}
		if (MainCamera.clearFlags != CameraClearFlags.Nothing)
		{
			Debug.Log("Main camera needs to be set to dont clear" +
			          "in the inspector");
		}
	}
	
	void Update()
	{
		SkyCamera.transform.position = MainCamera.transform.position;
		SkyCamera.transform.rotation = MainCamera.transform.rotation;

		SkyCamera.orthographic = MainCamera.orthographic; //Same Projection to avoid flickering

		AgainstCamera.rect = norm;

		if (MainCamera.orthographic)
		{
			AgainstCamera.orthographic = false;
		}
		else
		{
			AgainstCamera.orthographic=true;
		}
		
		Matrix4x4 p = AgainstCamera.projectionMatrix;
		SkyCamera.projectionMatrix = p;

		if(rotationActive)
		{
			if(x)
			{
				skyBoxRotation.x += intensityX;
			}
			if(y)
			{
				skyBoxRotation.y += intensityY;
			}
			if(z)
			{
				skyBoxRotation.z += intensityZ;
			}
			SkyCamera.transform.Rotate(skyBoxRotation);
			Stars.transform.Rotate(skyBoxRotation*0.00001f);
		}
	}

	// if you need to rotate the skybox during gameplay
	// rotate the skybox independently of the main camera
	public void SetSkyBoxRotation(Vector3 rotation)
	{
		skyBoxRotation = rotation;
	}
}

