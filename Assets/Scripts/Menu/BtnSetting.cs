﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;
[ExecuteInEditMode]
public class BtnSetting : MonoBehaviour
{
    public bool editMode;
    public GameObject Txt;
    public GameObject UnderLine;
    public bool underlined;
    [Tooltip("Text position: dx = right of Icon | sx = left of Icon")]
    public string where;
    private string whereStore;
    [Tooltip("Text distance from Icon")]
    public float distance;
    [Tooltip("Scale of all")]
    public float scale;
    
    
    private Transform TxtTr;
    private RectTransform TxtRt;
    private Text Txxt;

    private bool dx;
    private bool sx;
    
    private float t;
    private float duration;

    private float from;
    private float to;
    
    public AnimationCurve curve;
    
    void Start()
    {
        TxtTr = Txt.transform;
        TxtRt = Txt.transform.GetComponent<RectTransform>();
        Txxt = Txt.transform.GetComponent<Text>();
        if (where == "")
        {
            if (TxtTr.localPosition.x == distance)
            {
                where = "dx";
                whereStore = where;
            }
            else
            {
                where = "sx";
                whereStore = where;
            }
        }

        dx = false;
        sx = false;

        duration = 0.2f;

        if (scale != 0)
        {
            transform.localScale=new Vector3(scale,scale,scale);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (editMode)
        {
            //Underlined
            if (underlined)
            {
                UnderLine.SetActive(true);
            }
            else
            {
                UnderLine.SetActive(false);
            }

            //Where
            if (whereStore != where)
            {
                from = TxtTr.localPosition.x;
                t = 0;

                switch (where)
                {
                    case "dx":
                        TxtRt.anchorMin = new Vector2(1, 0.5f);
                        TxtRt.anchorMax = new Vector2(1, 0.5f);
                        TxtRt.pivot = new Vector2(0, 0.5f);
                        Txxt.alignment = TextAnchor.MiddleRight;
                        dx = true;
                        break;
                    case "sx":
                        TxtRt.anchorMin = new Vector2(0, 0.5f);
                        TxtRt.anchorMax = new Vector2(0, 0.5f);
                        TxtRt.pivot = new Vector2(1, 0.5f);
                        Txxt.alignment = TextAnchor.MiddleLeft;
                        sx = true;
                        break;
                }

                whereStore = where;
            }

            if (dx)
            {
                t += Time.deltaTime;
                float s = t / duration;

                float valX = Mathf.Lerp(from, distance, curve.Evaluate(s));
                float checker = Mathf.Lerp(0, 1, curve.Evaluate(s));

                TxtTr.localPosition = new Vector3(valX, TxtTr.localPosition.y, 0);

                if (checker == 1)
                {
                    dx = false;
                }
            }

            if (sx)
            {
                t += Time.deltaTime;
                float s = t / duration;

                float valX = Mathf.Lerp(from, -distance, curve.Evaluate(s));
                float checker = Mathf.Lerp(0, 1, curve.Evaluate(s));

                TxtTr.localPosition = new Vector3(valX, TxtTr.localPosition.y, 0);

                if (checker == 1)
                {
                    sx = false;
                }
            }

            //Distance
            if (where == "dx")
            {
                TxtTr.localPosition = new Vector3(distance, TxtTr.localPosition.y, 0);
            }
            else
            {
                TxtTr.localPosition = new Vector3(-distance, TxtTr.localPosition.y, 0);
            }

            //Scale
            transform.localScale = new Vector3(scale, scale, scale);
        }
    }
}
