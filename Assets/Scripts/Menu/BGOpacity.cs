﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class BGOpacity : MonoBehaviour
{
    private float t;
    private float _time;
    
    public AnimationCurve curve;

    private bool go;
    private bool back;

    private Image me;
    private Color clr;
    
    void Start()
    {
        me = transform.GetComponent<Image>();
        _time = 10;
        go = true;
        back = false;
    }

    
    void Update()
    {
        if (go)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            float alpha=Mathf.Lerp(1,  0, curve.Evaluate(s));
            clr=new Color(1,1,1,alpha);
            me.color = clr;
            
            if (checker == 1)
            {
                go = false;
                t = 0;
                back = true;
            }
        }
        
        if (back)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            float alpha=Mathf.Lerp(0,  1, curve.Evaluate(s));
            clr=new Color(1,1,1,alpha);
            me.color = clr;
            
            if (checker == 1)
            {
                back = false;
                t = 0;
                go = true;
            }
        }
    }
}
