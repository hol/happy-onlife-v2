﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.EventSystems;

public class Dragger : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler 
{
    public CatBtn catBtn;
    public CheckinCat checat;
    public ComeBackHome cbh;
    public Vector3 startPos;
    Vector3 releasePos;

    private bool check;

    private Underline ul;
    public Transform myLine;

    void Awake()
    {
        check = false;

        ul = myLine.GetComponent<Underline>();

        startPos = transform.position;
    }

    #region IBeginDragHandler implementation

    public void OnBeginDrag (PointerEventData eventData)
    {
        startPos = new Vector3(transform.position.x, transform.position.y, 1);
        if (!ul.locked)
        {
            ul.go = true;
        }
    }

    #endregion

    #region IDragHandler implementation

    public void OnDrag (PointerEventData eventData)
    {
        transform.position =
            Camera.main.ScreenToWorldPoint(new Vector3(eventData.position.x, eventData.position.y, 1));
    }

    #endregion

    #region IEndDragHandler implementation

    public void OnEndDrag (PointerEventData eventData)
    {
        if(!checat.allok)
        {
            releasePos = new Vector3(transform.position.x, transform.position.y, 1);
            check = true;
        }
        else
        {
            cbh.comeBackHome=true;
        }
        
    }

    #endregion

    void Update()
    {
        if (check)
        {
            //CheckinCat.CheckThis(transform, startPos,releasePos);
            catBtn.Click();
            check = false;
        }
    }
}