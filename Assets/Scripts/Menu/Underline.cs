﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Underline : MonoBehaviour
{
    public bool hided;
    public bool playable;
    
    private float t;
    private float _time;
    public AnimationCurve curve;

    public bool go;
    public bool back;
    public bool locked;
    void Start()
    {
        if (hided)
        {
            transform.gameObject.SetActive(false);
        }
        
        if (playable)
        {
            if (!locked)
            {
                transform.localScale = new Vector3(0, 1, 1);
            }
        }

        t = 0;
        _time = 0.2f;
    }

    // Update is called once per frame
    void Update()
    {
        if (playable)
        {
            if (go)
            {
                t += Time.deltaTime;
                float s = t / _time;

                float xScale = Mathf.Lerp(0, 1, curve.Evaluate(s));
                transform.localScale = new Vector3(xScale, 1, 1);

                float checker = Mathf.Lerp(0, 1, curve.Evaluate(s));

                if (checker == 1)
                {
                    go = false;
                    t = 0;
                }
            }

            if (!locked)
            {
                if (back)
                {
                    t += Time.deltaTime;
                    float s = t / _time;

                    float xScale = Mathf.Lerp(1, 0, curve.Evaluate(s));
                    transform.localScale = new Vector3(xScale, 1, 1);

                    float checker = Mathf.Lerp(0, 1, curve.Evaluate(s));

                    if (checker == 1)
                    {
                        back = false;
                        t = 0;
                    }
                }
            }
        }
    }
}
