﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class SelectSSType : MonoBehaviour 
{
	public int ID;

	public ChoosAvatarEvents cae;
	private SSSelected sss;

	void Start() 
	{
		sss=GetComponent<SSSelected>();
	}
	
	void OnMouseDown() {
		Debug.Log ("SpaceShip selezionata:" + ID.ToString ());
		int curPlayer = PlayerPrefs.GetInt ("curAvatarPlayer");
		cae.selectedID=ID;
		if (curPlayer == 1) 
		{
			PlayerPrefs.SetInt ("p1type", ID-1);
		} 
		else
		{
			PlayerPrefs.SetInt ("p2type", ID-1);
		}

		ContinueAva.able = true;
		sss.selected = true;
	}

	public void ClickOnMe()
	{
		OnMouseDown();
	}
}
