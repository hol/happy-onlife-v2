﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

//Auto rotation on Y axe
using UnityEngine;
using System.Collections;

public class ItemYAxisRotation : MonoBehaviour {

	//private float dirSpeed = +0.10f;
	private Vector3 startPos;
	private float   direction = +10f;
    public int numDisk = 0;
    public float incremental;
    float incr;

	// Use this for initialization
	void Start () {

        startPos = transform.position;

        switch (numDisk)
        {
            case 1:
                transform.position = new Vector3(transform.position.x, transform.position.y + 10, transform.position.z);
                incr=1.1f;
                break;
            case 2:
                transform.position = new Vector3(transform.position.x, transform.position.y + 20f, transform.position.z);
                incr=1.2f;
                break;
            case 3:
                transform.position = new Vector3(transform.position.x, transform.position.y + 30f, transform.position.z);
                incr=1.3f;
                break;
            case 4:
	            transform.position = new Vector3(transform.position.x, transform.position.y + 40f, transform.position.z);
                incr=1.4f;
	            break;
            case 5:
                transform.position = new Vector3(transform.position.x, transform.position.y - 50f, transform.position.z);
                incr=1.5f;
                break;
            case 6:
                transform.position = new Vector3(transform.position.x, transform.position.y - 60f, transform.position.z);
                incr=1.6f;
                break;
            case 7:
                transform.position = new Vector3(transform.position.x, transform.position.y - 70f, transform.position.z);
                incr=1.7f;
                break;
            case 8:
	            transform.position = new Vector3(transform.position.x, transform.position.y - 80f, transform.position.z);
                incr=1.8f;
	            break;
        }
        if (transform.position.y < startPos.y)
            direction = -direction;
	}
    
	void Update ()
    {
		transform.Rotate(0,incremental*Time.deltaTime*incr,0);
	}
}
