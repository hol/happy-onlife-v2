﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class DisposeCircular3D : MonoBehaviour
{
    public static bool initMove;
    
    public Transform CenterElement;
    
    public Transform[] objects;
    private Transform[] ufos;

    public static int totUfo;

    Vector3 center;
    private float ang;
    private float ang2;
    private float angtot;
    private float angstart;
    [Header("General Settings")]
    public float radiusY;
    public float radiusXMultiplier;
    private Vector3 pos;
    [Header("Animation Settings")]
    public float fromRadius=1f;
    public float toRadius=1.24f;

    private float t;
    public float durationRot;
    public float durationMove;
    public AnimationCurve generalCurve;
    public AnimationCurve radiusLengthCurve;

    public int selected;
    private int selectedStore;
    public int deSelected;
    public int taken = -1;
    
    private bool select;

    private bool animOn;
    private bool animOff;

    private Vector3 goingOn;
    private Vector3 gOnFrom;
    private Vector3 gOnTo;
    private Vector3 goingOff;
    private Vector3 gOffFrom;
    private Vector3 gOffTo;
    public float scale;
    public float maxScale;
    
    private Vector3 origScale;
    private Vector3 selectedScale;
    private Vector3 sSFrom;
    private Vector3 sSnTo;
    private Vector3 deSelectedScale;
    private Vector3 dSFrom;
    private Vector3 dSTo;
    
    public RectTransform playerTxtRT;
    private float playerTxtRTY;

    public bool rezet;
    
    void Start()
    {
        t = 0;
        angtot = 360;
        
        ComposeCircle();
        
        //radiusXMultiplier = (float)Screen.width / (float)Screen.height + 0.75f;
        selected = -1;
        deSelected = -1;
        playerTxtRT.localPosition = new Vector3(playerTxtRT.localPosition.x, 0, playerTxtRT.localPosition.z);
        playerTxtRTY = -156f;
        origScale = new Vector3(scale, scale, scale);
        sSFrom = origScale;
        dSTo = origScale;
        sSnTo = new Vector3(maxScale, maxScale, maxScale);
        dSFrom = sSnTo;
        initMove = true;
        select=false;  
        animOn=false;
        animOff=false;
        
    }

    void ComposeCircle()
    {
        ufos=new Transform[objects.Length];
        
        if (taken != -1)
        {
            ufos=new Transform[objects.Length-1];
        }
        
        for (int i = 0; i < objects.Length; i++)
        {
            if (i < taken || taken == -1)
            {
                ufos[i] = objects[i];
            }
            else if (i == taken)
            {
                objects[i].gameObject.SetActive(false);
            }
            else if (i > taken)
            {
                ufos[i-1] = objects[i];
            }
        }
        
        totUfo = ufos.Length;
    }

    void PrintAll()
    {
        for (int i = 0; i < totUfo; i++)
        {
            print(ufos[i].gameObject.name);
        }
    }

    void Dispose()
    {
        center = CenterElement.localPosition;
        for (int i = 0; i < totUfo; i++)
        {
            if (i == selected)
            {
            }
            else if (i == deSelected)
            {
            }
            else
            {
                ang = angstart+(angtot / totUfo * i);
                pos = RandomCircle();
                if (rezet)
                {
                    objects[i].localPosition = pos;
                    objects[i].localScale = origScale;
                }
                else
                {
                    ufos[i].localPosition = pos;
                    ufos[i].localScale = origScale;
                }
            }
        }
    }

    Vector3 RandomCircle ()
    {
        Vector3 randPos;
        randPos.x = center.x + radiusY * Mathf.Sin(ang * Mathf.Deg2Rad)* radiusXMultiplier;
        randPos.y = center.y + radiusY * Mathf.Cos(ang * Mathf.Deg2Rad);
        randPos.z = center.z;
        return randPos;
    }
    
    Vector3 RandomCircle2 ()
    {
        Vector3 randPos;
        randPos.x = center.x + radiusY * Mathf.Sin(ang2 * Mathf.Deg2Rad)* radiusXMultiplier;
        randPos.y = center.y + radiusY * Mathf.Cos(ang2 * Mathf.Deg2Rad);
        randPos.z = center.z;
        return randPos;
    }

    void Update()
    {
        origScale = new Vector3(scale, scale, scale);
        sSFrom = origScale;
        dSTo = origScale;
        
        if (rezet)
        {
            Start();
            rezet = false;
        }
        else
        {
            if (selected != -1)
            {
                if (!select)
                {
                    gOnFrom = ufos[selected].localPosition;
                    gOnTo = new Vector3(0, 0.5f, 22.6f);
                    gOffFrom = gOnTo;
                    animOn = true;
                    playerTxtRT.localPosition = new Vector3(playerTxtRT.localPosition.x, playerTxtRTY, playerTxtRT.localPosition.z);
                    selectedStore = selected;
                    select = true;
                }

                if (selectedStore != selected)
                {
                    deSelected = selectedStore;
                    selectedStore = selected;
                    
                    animOff = true;
                    select = false;
                }
            }
            else
            {
                playerTxtRT.localPosition = new Vector3(playerTxtRT.localPosition.x, 0, playerTxtRT.localPosition.z);
            }
            

            if (initMove)
            {
                t += Time.deltaTime;
                float s = t / durationRot;
                
                radiusY = Mathf.Lerp(fromRadius, toRadius, radiusLengthCurve.Evaluate(s));
                angstart = (int)Mathf.Lerp(0, 360, generalCurve.Evaluate(s));

                float checker = Mathf.Lerp(0, 1, generalCurve.Evaluate(s));

                if (checker == 1)
                {
                    initMove = false;
                    t = 0;
                }
            }
            
            if (animOn)
            {
                t += Time.deltaTime;
                float s = t / durationMove;
                
                goingOn = Vector3.Lerp(gOnFrom, gOnTo, generalCurve.Evaluate(s));
                selectedScale =  Vector3.Lerp(sSFrom, sSnTo, generalCurve.Evaluate(s));
                
                ufos[selected].localPosition = goingOn;
                ufos[selected].localScale = selectedScale;

                if (animOff)
                {
                    ang2 = angstart+(angtot / totUfo * deSelected);
                    gOffTo = RandomCircle2();
                    
                    goingOff = Vector3.Lerp(gOffFrom, gOffTo, generalCurve.Evaluate(s));
                    deSelectedScale = Vector3.Lerp(dSFrom, dSTo, generalCurve.Evaluate(s));
                    
                    ufos[deSelected].localPosition = goingOff;
                    ufos[deSelected].localScale = deSelectedScale;
                }

                float checker = Mathf.Lerp(0, 1, generalCurve.Evaluate(s));
                
                if (checker == 1)
                {
                    animOn = false;
                    animOff = false;
                    deSelected = -1;
                    t = 0;
                }
            }
            
            Dispose();
            angstart += durationRot;
        }
    }
}
