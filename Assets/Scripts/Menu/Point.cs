﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System.Collections;
using UnityEngine;

public class Point : MonoBehaviour
{
    public string section;
    public int totForSection;
    private string sectionStore;
    public static string sectionCalling;
    private int ownSection;
    public static int ownSectionPass;
    public int ID;
    
    public static ArrayList sections = new ArrayList();
    
    public static int counter;
    public static int counterMax;
    private int ownCounter;
    
    private bool full;
    public static bool empty;
    public static bool allEmpty;
    public bool locked;
    
    public Exa2Dnew exa2D;

    public Color32 myColor;
    private Color32[] cs;

    public static bool check;
    public static int on;
    public bool wrong;
    
    void Start()
    {
        exa2D = transform.GetChild(1).transform.GetComponent<Exa2Dnew>();
        //exa2D.edit = true;

        //exa2D.colorInner = myColor;
        //exa2D.colorNow = true;
        
        cs = new[]
        {
            new Color32(40,163,206,255), //1
            new Color32(4,62,132,255), //2
            new Color32(0,167,155,255), //3
            new Color32(106,172,35,255), //4
            new Color32(235,163,29,255),//5
            new Color32(231,102,44,255), //6
            new Color32(191,39,45,255), //7
            new Color32(194,35,128,255), //8
            new Color32(100,45,140,255) //9
        };
        
        sectionStore = section;

        if(section=="question")
        {
            sectionCalling=section;
            sectionCalling=section;
        }
        
        SectionCount();
    }

    void SectionCount()
    {
        if (sections.Count == 0)
        {
            sections.Add(section);
        }
        else
        {
            bool newOne = false;
            
            for (int i = 0; i < sections.Count; i++)
            {
                if (section != sections[i].ToString())
                {
                    newOne = true;
                }
                else
                {
                    newOne = false;
                }
            }

            if (newOne)
            {
                sections.Add(section);
            }
        }
        counterMax++;

        if (transform.gameObject.name == "ExaPoint1")
        {
            SetFull();
            on=1;
            check=true;
        }
    }

    void Update()
    {
        if(wrong)
        {
            if(exa2D.colorOutline == Color.white)
            {
                exa2D.colorInner = new Color32(115, 115, 115, 255);
                exa2D.colorOutline = new Color32(200, 0, 0, 255);
            }
        }

        if(allEmpty)
        {
            AllEmpty();
            print("EmptyAll "+transform.name);
            allEmpty=false;
        }

        if (sectionCalling == sectionStore)
        {
            if (empty)
            {
                if (!locked)
                {
                    exa2D.edit = true;
                    exa2D.full=false;
                    exa2D.noInner = true;
                    counter++;
                    locked = true;
                }
            }

            if (full)
            {
                exa2D.edit = true;
                exa2D.noInner = false;
                if(section=="power")
                {
                    exa2D.colorInner=cs[ID-1];
                }
                else
                {
                    exa2D.colorInner=Color.white;
                    exa2D.colorOutline=Color.white;
                    if(wrong)
                    {
                        exa2D.colorInner = new Color32(115, 115, 115, 255);
                        exa2D.colorOutline = new Color32(200, 0, 0, 255);
                    }

                }
                print("FULL "+transform.name);
                exa2D.full=true;
                full = false;
            }

            if (counter == totForSection)
            {
                counter = 0;
                empty = false;
                full = false;
                sectionCalling = "";
            }
        }
        else
        {
            if (locked)
            {
                print("LOCKED "+transform.name);
                locked = false;
            }
        }

        if (check)
        {
            //print("Checking "+transform.name);
            if (sectionCalling == sectionStore)
            {
                print("ON="+on);
                if (ID == on)
                {
                    AllEmpty();
                    print("EmptyAll "+transform.name);
                    SetFull();
                    check = false;
                }
            }
        }
    }

    public void AllEmpty()
    {
        for (int i = 0; i < totForSection; i++)
        {
            if (ID == i+1)
            {
                empty = true;
            }
        }
    }

    public void SetFull()
    {
        print("SendFull "+transform.name);
        full = true;
    }
}
