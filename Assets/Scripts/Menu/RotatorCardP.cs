﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class RotatorCardP : MonoBehaviour
{
    public static int cardN;
    public int cardMax;
    
    public RectTransform card;
    
    public RectTransform titleRT;
    public RectTransform txt1RT;
    public RectTransform txt2RT;
    public RectTransform txt3RT;
    
    private float t;
    private float _time;

    public bool turn;
    public bool turnBack;
    
    public AnimationCurve curve;

    public static float startPY;
    public static float startPYB;
    private float endPY;
    private float endPYB;
    private float endPZ;
    private float endPZB;
    private bool changeNext;
    private bool changePrev;
    private bool locked;

    public string direction;
    void Start()
    {

        startPY = 0;
        endPY = 180;
        endPZ = -8;
        
        startPYB = 180;
        endPYB = 0;
        
        t = 0;
        _time = 0.5f;
        direction = "sx";
        cardN = 1;
    }

    void Update()
    {
        if (turn)
        {
            t += Time.deltaTime;
            float s = t / _time;
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            float y = Mathf.Lerp(startPY,  endPY, curve.Evaluate(s));
            
            float y2 = 0;
            float z = 0;
            float z2 = 0;
            
            if (y < endPY*0.5f)
            {
                z = (y * endPZ) / endPY;
                y2 = y;
                z2 = z;
            }
            else
            {
                changeNext = true;
                z = (endPZ)-((y * endPZ) / endPY);
                y2 = endPY - y;
                z2 = -z;
            }
            
            card.localEulerAngles = new Vector3(0, y, z);
            titleRT.localEulerAngles = new Vector3(0, y2, z2);
            txt1RT.localEulerAngles = new Vector3(0, y2, z2);
            txt2RT.localEulerAngles = new Vector3(0, y2, z2);
            txt3RT.localEulerAngles = new Vector3(0, y2, z2);
            
            if (checker == 1)
            {
                turn = false;
                t = 0;
                changeNext = false;
                locked = false;
            }
        }
        
        if (turnBack)
        {
            t += Time.deltaTime;
            float s = t / _time;
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            float y = Mathf.Lerp(startPYB,  endPYB, curve.Evaluate(s));
            
            float y2 = 0;
            float z = 0;
            float z2 = 0;
            
            if (y < endPY*0.5f)
            {
                changePrev = true;
                z = (y * endPZ) / endPY;
                y2 = y;
                z2 = z;
            }
            else
            {
                z = (endPZ)-((y * endPZ) / endPY);
                y2 = endPY - y;
                z2 = -z;
            }
            
            card.localEulerAngles = new Vector3(0, y, z);
            titleRT.localEulerAngles = new Vector3(0, y2, z2);
            txt1RT.localEulerAngles = new Vector3(0, y2, z2);
            txt2RT.localEulerAngles = new Vector3(0, y2, z2);
            txt3RT.localEulerAngles = new Vector3(0, y2, z2);
            
            if (checker == 1)
            {
                turnBack = false;
                t = 0;
                changePrev = false;
                locked = false;
            }
        }
        
        if (!locked)
        {
            if (changeNext)
            {
                print("changedNext");
                PowercardsEvents.changeNext = true;
                locked = true;
            }
            
            if (changePrev)
            {
                print("changedPrev");
                PowercardsEvents.changePrev = true;
                locked = true;
            }
        }
    }

    public void SetNewCard(int newCard)
    {
        if (newCard < cardN)
        {
            direction = "dx";
            PowercardsEvents.currentCard = newCard+1;
            turnBack = true;
        }
        else if (newCard > cardN)
        {
            direction = "sx";
            PowercardsEvents.currentCard = newCard-1;
            turn = true;
        }
        else
        {
            return;
        }
    }
    
    public void NextCard()
    {
        direction = "sx";
        turn = true;
    }
    
    public void PrevCard()
    {
        direction = "dx";
        turnBack = true;
    }
}
