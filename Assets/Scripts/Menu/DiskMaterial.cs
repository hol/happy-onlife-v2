/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class DiskMaterial : MonoBehaviour
{
    public Transform[] objects;
    public Color32[] cs;
    public Color32[] em;
    public Color32[] em2;
    public float[] emMulti2;
    
    void Start()
    {
        for (int i = 0; i < objects.Length; i++)
        {
            objects[i].GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_Color", cs[i]);
            objects[i].GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em[i]);
            objects[i].GetChild(1).transform.GetComponent<Renderer>().material.SetColor("_Color", cs[i]);
            objects[i].GetChild(1).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em[i]);
            objects[i].GetChild(2).transform.GetComponent<Renderer>().material.SetColor("_Color", cs[i]);
            objects[i].GetChild(2).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em2[i]);
            objects[i].GetChild(2).transform.GetComponent<Renderer>().material.SetVector("_EmissionColor", (Color)em2[i] * 2);
            objects[i].GetChild(3).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em2[i]);
            objects[i].GetChild(3).transform.GetComponent<Renderer>().material.SetVector("_EmissionColor", (Color)em2[i] * emMulti2[i]);
            objects[i].GetChild(4).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em2[i]);
            objects[i].GetChild(4).transform.GetComponent<Renderer>().material.SetVector("_EmissionColor", (Color)em2[i] * emMulti2[i]);
            objects[i].GetChild(5).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em2[i]);
            objects[i].GetChild(5).transform.GetComponent<Renderer>().material.SetVector("_EmissionColor", (Color)em2[i] * emMulti2[i]);
            objects[i].GetChild(6).transform.GetComponent<Renderer>().material.SetColor("_Color", cs[i]);
            objects[i].GetChild(6).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em2[i]);
            objects[i].GetChild(6).transform.GetComponent<Renderer>().material.SetVector("_EmissionColor", (Color)em2[i] * emMulti2[i]);
        }
    }

    void Update()
    {
        for (int i = 0; i < objects.Length; i++)
        {
            objects[i].GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_Color", cs[i]);
            objects[i].GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em[i]);
            objects[i].GetChild(1).transform.GetComponent<Renderer>().material.SetColor("_Color", cs[i]);
            objects[i].GetChild(1).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em[i]);
            objects[i].GetChild(2).transform.GetComponent<Renderer>().material.SetColor("_Color", cs[i]);
            objects[i].GetChild(2).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em2[i]);
            objects[i].GetChild(2).transform.GetComponent<Renderer>().material.SetVector("_EmissionColor", (Color)em2[i] * 2);
            objects[i].GetChild(3).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em2[i]);
            objects[i].GetChild(3).transform.GetComponent<Renderer>().material.SetVector("_EmissionColor", (Color)em2[i] * emMulti2[i]);
            objects[i].GetChild(4).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em2[i]);
            objects[i].GetChild(4).transform.GetComponent<Renderer>().material.SetVector("_EmissionColor", (Color)em2[i] * emMulti2[i]);
            objects[i].GetChild(5).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em2[i]);
            objects[i].GetChild(5).transform.GetComponent<Renderer>().material.SetVector("_EmissionColor", (Color)em2[i] * emMulti2[i]);
            objects[i].GetChild(6).transform.GetComponent<Renderer>().material.SetColor("_Color", cs[i]);
            objects[i].GetChild(6).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", em2[i]);
            objects[i].GetChild(6).transform.GetComponent<Renderer>().material.SetVector("_EmissionColor", (Color)em2[i] * emMulti2[i]);
        }
    }
}
