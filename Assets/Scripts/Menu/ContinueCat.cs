﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class ContinueCat : MonoBehaviour
{
    private Button me;
    public Text myTxt;

    static bool able;
    static bool able2;
    static bool dis2;
    
    void Start()
    {
        me = transform.GetComponent<Button>();
        me.interactable = false;
        myTxt.color = new Color(1, 1, 1, 0.2f );
        able = false;
        able2 = false;
        dis2 = false;
    }

    public void System()
    {
        if(able)
        {
            CheckinCat.system = true;
        }
    }
    
    void Update()
    {
        if (CheckinCat.settedCat == CheckinCat.nCat && CheckinCat.nCat!=0)
        {
            if (!able2)
            {
                myTxt.color = new Color(1, 1, 1, 1 );
                me.interactable = true;
                able = true;
                dis2 = false;
                CatBtn.locked=true;
            }
        }
        else
        {
            if (!dis2)
            {
                myTxt.color = new Color(1, 1, 1, 0.2f );
                me.interactable = false;
                able2 = false;
                CatBtn.locked=false;
            }
        }
    }


}
