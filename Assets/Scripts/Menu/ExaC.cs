﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class ExaC : MonoBehaviour
{
    public int ID;
    public int tot;
    public static int idChecking;
    public static bool checkMove;
    public static bool unlock;
    private bool locked;
    private bool move;
    
    public bool isOnStage;
    public bool isFull;
    public bool isEmpty;
    public  GameObject child;
    public Vector3 origPos;
    
    bool onStage;
    bool notOnStage;
    
    private Vector3 startScale;
    private Vector3 finalScale;
    
    private float t;
    private float _time;
    public AnimationCurve curve;

    
    private Vector3 startMove;
    private Vector3 finishMove;

    public static int exaCounter;

    public bool off;
    
    void Start()
    {
        locked = false;
        checkMove = false;
        unlock = false;
        move = false;

        exaCounter = 0;
        
        _time = 0.3f;

        startScale = Vector3.zero;
        finalScale = new Vector3(1f, 1f, 1f);

        transform.localScale = startScale;
        transform.gameObject.SetActive(false);
    }

    void OnDisable()//Visual Utility
    {
        isOnStage = false;
    }
    
    private void OnEnable()//Functional
    {
        onStage = true;
    }

    private void Move()
    {
        if (!locked)
        {
            locked = true;
            startMove = DisposinGrillExaC.startMove[ID];
            finishMove = DisposinGrillExaC.finishMove[ID];
            move = true;
            
            Debug.Log("Move:"+ID);
        }
    }

    void Update()
    {
        if (exaCounter == tot)
        {
            unlock = false;
            exaCounter = 0;
            //Debug.Log(ID + " stop unlock");
        }

        if (unlock)
        {
            locked = false;
            exaCounter++;
            //Debug.Log(ID + " unlock");
        }
//
        if (checkMove && ID < idChecking)
        {
            if (!move)
            {
                //Move();
                if (!locked)
                {
                    locked = true;
                    startMove = DisposinGrillExaC.startMove[ID];
                    finishMove = DisposinGrillExaC.finishMove[ID];
                    move = true;
            
                    Debug.Log("Move:"+ID);
                }
            }
        }
        
        if (move)
        {
            t += Time.deltaTime;
            float s = t / _time;

            if (onStage)
            {
                transform.localScale = Vector3.Lerp(startScale, finalScale, curve.Evaluate(s));
            }

            transform.localPosition=Vector3.Lerp(startMove, finishMove, curve.Evaluate(s));
            
            if (isFull)
            {
                child.transform.position = transform.position;
            }

            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                move = false;
                
                if (onStage)
                {
                    isOnStage = true;
                    onStage = false;
                }

                t = 0;
                //Debug.Log(ID + " incremento counter");
                DisposinGrillExaC.counter++;
            }
        }

        if (off)
        {
            transform.localScale = startScale;
            notOnStage = true;
            off = false;
        }

        if (notOnStage)
        {
            t += Time.deltaTime;
            float s = t / _time;
            
            transform.localScale=Vector3.Lerp(finalScale, startScale, curve.Evaluate(s));
            
            float checker=Mathf.Lerp(0,  1, curve.Evaluate(s));
            
            if (checker == 1)
            {
                notOnStage = false;
                if(isFull)
                {
                    isEmpty = true;
                    CheckinCat.empty = true;
                }
                
                t = 0;
                transform.gameObject.SetActive(false);
            }
        }

        if (!isFull)
        {
            if (child)
            {
                isFull = true;
            }
        }
    }
}
