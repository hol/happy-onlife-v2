﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;
using UnityEngine.UI;

public class ActivitiesEvents : MonoBehaviour 
{
	public static int currentCard = 1;

	//VAR 4 Textify
	private string[] strgs;
	
	public Text title1Txt;
	public Text title2Txt;
	public Image iconImg;
	public Text content1Txt;
	public Text content2Txt;

	public static bool lang;
	public static bool changeNext;
	public static bool changePrev;

	public Textify textify;
	
	void Update()
	{
		if (lang)
		{
			showCards();
			lang = false;
		}

		if (changeNext)
		{
			nextActivity();
			changeNext = false;
		}
		
		if (changePrev)
		{
			prevActivity();
			changePrev = false;
		}
	}

	void showCards() 
	{
		if (currentCard == 1)
		{
			//print("accendo l'1");
			iconImg.enabled=false;
			title2Txt.enabled=false;
			content2Txt.enabled=false;
			title1Txt.enabled=true;
			content1Txt.enabled=true;
		}
		else if(!iconImg.enabled)
		{
			iconImg.enabled=true;
			title1Txt.enabled=false;
			content1Txt.enabled=false;
			title2Txt.enabled=true;
			content2Txt.enabled=true;
		}

		switch (currentCard) {
		case 1: 
			title1Txt.text = textify.OnDemand("extracards", 0);
			content1Txt.text = textify.OnDemand("extracards", 1);
			break;
		case 2: 
			iconImg.sprite = Resources.Load<Sprite>("Textures/ea_icon02");
			title2Txt.text = textify.OnDemand("extracards", 2);
			content2Txt.text = textify.OnDemand("extracards", 3);
			break;
		case 3: 
			iconImg.sprite = Resources.Load<Sprite>("Textures/ea_icon03");
			title2Txt.text = textify.OnDemand("extracards", 4);
			content2Txt.text = textify.OnDemand("extracards", 5);
			break;
		case 4: 
			iconImg.sprite = Resources.Load<Sprite>("Textures/ea_icon04");
			title2Txt.text = textify.OnDemand("extracards", 6);
			content2Txt.text = textify.OnDemand("extracards", 7);
			break;
		case 5: 
			iconImg.sprite = Resources.Load<Sprite>("Textures/ea_icon05");
			title2Txt.text = textify.OnDemand("extracards", 8);
			content2Txt.text = textify.OnDemand("extracards", 9);
			break;
		case 6: 
			iconImg.sprite = Resources.Load<Sprite>("Textures/ea_icon06");
			title2Txt.text = textify.OnDemand("extracards", 10);
			content2Txt.text = textify.OnDemand("extracards", 11);
			break;
		case 7: 
			iconImg.sprite = Resources.Load<Sprite>("Textures/ea_icon07");
			title2Txt.text = textify.OnDemand("extracards", 12);
			content2Txt.text = textify.OnDemand("extracards", 13);
			break;
		case 8: 
			iconImg.sprite = Resources.Load<Sprite>("Textures/ea_icon08");
			title2Txt.text = textify.OnDemand("extracards", 14);
			content2Txt.text = textify.OnDemand("extracards", 15);
			break;
		case 9: 
			iconImg.sprite = Resources.Load<Sprite>("Textures/ea_icon09");
			title2Txt.text = textify.OnDemand("extracards", 16);
			content2Txt.text = textify.OnDemand("extracards", 17);
			break;
		case 10: 
			iconImg.sprite = Resources.Load<Sprite>("Textures/ea_icon10");
			title2Txt.text = textify.OnDemand("extracards", 18);
			content2Txt.text = textify.OnDemand("extracards", 19);
			break;
		case 11: 
			iconImg.sprite = Resources.Load<Sprite>("Textures/ea_icon11");
			title2Txt.text = textify.OnDemand("extracards", 20);
			content2Txt.text = textify.OnDemand("extracards", 21);
			break;
		case 12: 
			iconImg.sprite = Resources.Load<Sprite>("Textures/ea_icon12");
			title2Txt.text = textify.OnDemand("extracards", 22);
			content2Txt.text = textify.OnDemand("extracards", 23);
			break;
		case 13: 
			iconImg.sprite = Resources.Load<Sprite>("Textures/ea_icon13");
			title2Txt.text = textify.OnDemand("extracards", 24);
			content2Txt.text = textify.OnDemand("extracards", 25);
			break;
		case 14: 
			iconImg.sprite = Resources.Load<Sprite>("Textures/ea_icon14");
			title2Txt.text = textify.OnDemand("extracards", 26);
			content2Txt.text = textify.OnDemand("extracards", 27);
			break;
		}

		//print("currentCard"+currentCard);
		RotatorCardA.cardN = currentCard;
		Point.sectionCalling = "act";
		Point.on = currentCard;
		Point.check = true;
	}
	
    //Go to next powercard
    public void nextActivity() {
		++currentCard;
		if(currentCard>14) {
			currentCard = 1;
		}
		showCards ();
	}
	
    //Go to previous powercard
    public void prevActivity() {
		--currentCard;
		if(currentCard<1) {
			currentCard = 14;
		}
		showCards ();
	}
}
