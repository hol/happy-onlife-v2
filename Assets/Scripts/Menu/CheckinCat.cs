﻿/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using System;
using UnityEngine;

public class CheckinCat : MonoBehaviour
{
    public static bool greenLight;
    public DraggerMasterCat dmc;
    public bool allok;
    public static  int nCat;
    public static int settedCat;
    private int scInit;
    private string cats;
    
    public RectTransform Space0;
    public RectTransform Space1;
    public RectTransform Space2;
    public RectTransform Space3;
    public RectTransform Space4;
    public RectTransform Space5;
    public RectTransform Space6;
    public RectTransform Space7;
    
    private RectTransform[] spaces;
    private Vector3[] spacePos;
    private Vector3[] objPos;
    
    public Transform Obj0;
    public Transform Obj1;
    public Transform Obj2;
    public Transform Obj3;
    public Transform Obj4;
    public Transform Obj5;
    public Transform Obj6;
    public Transform Obj7;
    
    private Transform[] objs;
    
    private Vector3[] objsBack;

    public  bool[] itsPlace;
    int[] whoInside=new int[8]{-1,-1,-1,-1,-1,-1,-1,-1};
    
    public TrailsSetting Trails0;
    public TrailsSetting Trails1;
    public TrailsSetting Trails2;
    public TrailsSetting Trails3;
    public TrailsSetting Trails4;
    public TrailsSetting Trails5;
    public TrailsSetting Trails6;
    public TrailsSetting Trails7;
    
    private TrailsSetting[] trails;
    
    private Underline[] uls;

    private ExaC[] exas;

    private static int _who;
    private static Vector3 _from;
    private static Vector3 _to;

    private Vector3 spaceCheck;
    
    private static bool check;
    private static bool free;
    public static bool checkCat;
    public static bool empty;
    public static bool control;
    public static bool repos;

    public float offsetX;
    public float offsetY;
    
    public static bool system;
    
    void Start()
    {
        //initialization
        scInit = 0;//categorie selezionate al primo giro assoulto
        nCat = 4;
        cats = "";
        PlayerPrefs.SetInt("nCat", nCat);
        PlayerPrefs.SetString("cats", cats);
        SnappinScrollerCat.startPage = nCat - 2;
        Debug.Log("CheckinCat START from 0");
        
        greenLight = false;
        
        //initial definition
        check = false;
        checkCat = false;
        empty = false;
        spaces = new [] {Space0, Space1, Space2, Space3, Space4, Space5, Space6, Space7};
        objs = new [] {Obj0, Obj1, Obj2, Obj3, Obj4, Obj5, Obj6, Obj7};
        spacePos = new [] {Space0.position, Space1.position, Space2.position, Space3.position, Space4.position, Space5.position, Space6.position, Space7.position};
        objPos = new [] {Obj0.position, Obj1.position, Obj2.position, Obj3.position, Obj4.position, Obj5.position, Obj6.position, Obj7.position};
        objsBack = new []{Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero};
        
        itsPlace = new [] {true, true, true, true, true, true, true, true};
        
        uls = new []
        {
            Obj0.GetComponent<Dragger>().myLine.GetComponent<Underline>(),
            Obj1.GetComponent<Dragger>().myLine.GetComponent<Underline>(),
            Obj2.GetComponent<Dragger>().myLine.GetComponent<Underline>(),
            Obj3.GetComponent<Dragger>().myLine.GetComponent<Underline>(),
            Obj4.GetComponent<Dragger>().myLine.GetComponent<Underline>(),
            Obj5.GetComponent<Dragger>().myLine.GetComponent<Underline>(),
            Obj6.GetComponent<Dragger>().myLine.GetComponent<Underline>(),
            Obj7.GetComponent<Dragger>().myLine.GetComponent<Underline>()
        };
        
        exas = new []
        {
            Space0.gameObject.GetComponent<ExaC>(),
            Space1.gameObject.GetComponent<ExaC>(),
            Space2.gameObject.GetComponent<ExaC>(),
            Space3.gameObject.GetComponent<ExaC>(),
            Space4.gameObject.GetComponent<ExaC>(),
            Space5.gameObject.GetComponent<ExaC>(),
            Space6.gameObject.GetComponent<ExaC>(),
            Space7.gameObject.GetComponent<ExaC>()
        };
        
        trails = new []
        {
            Trails0.gameObject.GetComponent<TrailsSetting>(),
            Trails1.gameObject.GetComponent<TrailsSetting>(),
            Trails2.gameObject.GetComponent<TrailsSetting>(),
            Trails3.gameObject.GetComponent<TrailsSetting>(),
            Trails4.gameObject.GetComponent<TrailsSetting>(),
            Trails5.gameObject.GetComponent<TrailsSetting>(),
            Trails6.gameObject.GetComponent<TrailsSetting>(),
            Trails7.gameObject.GetComponent<TrailsSetting>()
        };
    }

    void InitialDisposition()
    {
        for (int i = 0; i < scInit; i++)
        {
            char childName =cats[i];
            int childNum=int.Parse(childName.ToString());
            Debug.Log(childNum);
            objsBack[childNum] = new Vector3(objs[childNum].position.x-MenuMovement.initialOffset, objs[childNum].position.y, objs[childNum].position.z);
            itsPlace[childNum] = false;
            uls[childNum].go = true;
            uls[childNum].locked = true;
            exas[i].child = objs[childNum].gameObject;
            objs[childNum].position = spaces[i].position;
            trails[childNum].connected = true;
            checkCat = true;
        }
        print("InitialDisposition");
    }

    void Update()
    {     
        if (repos)
        {
            //print("Repos");
            Repos();
        }

        if (control)
        {
            print("Control");
            Control();
            control = false;
        }

        if (check)
        {
            print("CheckPos");
            CheckPos();
            checkCat = true;
            check = false;
        }

        if (free)
        {
            print("FreePos");
            FreePos();
            checkCat = true;
            free = false;
        }

        if (empty)
        {
            print("WhosEmpty");
            WhosEmpty();
            empty = false;
        }
        
        if (checkCat)
        {
            print("CheckCat");
            CheckCat();
            checkCat = false;
        }
        
        if (greenLight)
        {
            Debug.Log("GREENLIGHT");
            InitialDisposition();
            greenLight = false;
        }
    }

    void Repos()
    {
        for(int i = 0;i<itsPlace.Length;i++)
        {
            if(!itsPlace[i])
            {
                print(i);
                spacePos = new [] {Space0.position, Space1.position, Space2.position, Space3.position, Space4.position, Space5.position, Space6.position, Space7.position};
                
                if(objs[whoInside[i]].position!=spacePos[i])
                {
                    objs[whoInside[i]].position=spacePos[i];
                }
            }
        }
    }

    void Control()
    {
        for(int i = 0;i<itsPlace.Length;i++)
        {
            if(i>nCat-1)
            {
                if(!itsPlace[i])
                {
                    itsPlace[i]=true;
                    trails[whoInside[i]].connected = false;
                    uls[whoInside[i]].back = true;
                    objs[whoInside[i]].GetComponent<ComeBackHome>().comeBackHome=true;
                    whoInside[i]=-1;
                }
            }
        }
    }

    public static void FreeThis(int who)
    {
        _who = who;
        free = true;
        print("FreeThis");
    }

    void FreePos()
    {
        spacePos = new [] {Space0.position, Space1.position, Space2.position, Space3.position, Space4.position, Space5.position, Space6.position, Space7.position};
        
        _to = objsBack[_who];
        for(int i = 0;i<whoInside.Length;i++)
        {
            if(whoInside[i]==_who)
            {
                whoInside[i]=-1;
                itsPlace[i]=true;
                _from = spacePos[i];
                trails[_who].connected = false;
                uls[_who].back = true;
                break;
            }
        }
        objs[_who].GetComponent<ComeBackHome>().comeBackHome=true;
        //float dist = Mathf.Abs((1-((Vector3.Distance(_to, _from))*0.001f))*0.5f);
        //ProviderMovement.BackThat(objs[_who], _from, _to, dist);
    }

    public static void CheckThis(int who)
    {
        _who = who;
        check = true;
        print("CheckThis");
    }

    void CheckPos()
    {
        spacePos = new [] {Space0.position, Space1.position, Space2.position, Space3.position, Space4.position, Space5.position, Space6.position, Space7.position};
        objPos = new [] {Obj0.position, Obj1.position, Obj2.position, Obj3.position, Obj4.position, Obj5.position, Obj6.position, Obj7.position};
        _from = objPos[_who];
        objsBack[_who] = _from;
        for(int i = 0;i<itsPlace.Length;i++)
        {
            if(itsPlace[i])
            {
                itsPlace[i]=false;
                whoInside[i]=_who;
                _to=spacePos[i];
                break;
            }
        }
        float dist = Mathf.Abs((1-((Vector3.Distance(_to, _from))*0.001f))*0.5f);
        ProviderMovement.MoveThis(objs[_who], _from, _to, dist);
        trails[_who].connected = true;
        uls[_who].go = true;
    }

    //For avoid double number in cats
    void CheckDouble()
    {
        for (int i = 0; i < cats.Length; i++)
        {
            string single = cats[i].ToString();
            for (int e = 0; e < cats.Length; e++)
            {
                if (i != e)
                {
                    if (single == cats[e].ToString())
                    {
                        string[] strgs = cats.Split (single [0]);
                        cats = "";
                        for (int k = 0; k < strgs.Length; k++)
                        {
                            cats += strgs[k];
                        }
                        cats += single;
                    }
                }
            }
        }
        print("CheckDouble");
    }

    void CheckCat()
    {
        int scStore = 0;
        cats="";
        for (int i = 0; i < itsPlace.Length; i++)
        {
            if (!itsPlace[i])
            {
                scStore++;
                cats+=whoInside[i];
            }
        }

        settedCat = scStore;

        if(settedCat>nCat)
        {
            control=true;
            settedCat-=1;
        }

        print("CheckCat:setted="+settedCat+" nCat="+nCat);

        if(settedCat==nCat)
        {
            WhosEmpty();
            allok=true;
        }
        else
        {
            allok=false;
        }
    }
    
    void WhosEmpty()
    {
        for (int i = 0; i < spaces.Length; i++)
        {
            if (exas[i].isEmpty)
            {
                float dist3 =
                    Mathf.Abs(
                        (1 - ((Vector3.Distance(exas[i].child.transform.position, objsBack[i])) * 0.001f)) * 0.5f
                    );
                Transform obj = exas[i].child.transform;
                char objName = obj.name[obj.name.Length - 1];
                int who=int.Parse(objName.ToString());
                
                obj.position = objsBack[who];
                
                exas[i].child = null;
                exas[i].isEmpty = false;
                exas[i].isFull = false;
                
                itsPlace[who] = true;
                uls[who].locked = false;
                uls[who].back = true;
                trails[who].connected = false;
                checkCat = true;
                //Delete old Cat
                string me = who.ToString();
                for (int j = 0; j < cats.Length; j++)
                {
                    if (cats[j].ToString() == me)
                    {
                        string[] strgs = cats.Split (me [0]);
                        cats = "";
                        for (int k = 0; k < strgs.Length; k++)
                        {
                            cats += strgs[k];
                        }
                    }
                }
            }
            CheckDouble();
            PlayerPrefs.SetInt("nCat", nCat);
            PlayerPrefs.SetString("cats", cats);
            print("3nCat="+nCat+" cats="+cats);
        }
    }
}
