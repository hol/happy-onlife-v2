/*
* Copyright 2022 European Commission
*
* Licensed under the EUPL, Version 1.1 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
using UnityEngine;

public class SpaceshipMaterial : MonoBehaviour
{
    [Header("in Editor")]
    public bool change;
    public bool multilplier;
    public float multiEmit=0.1f;

    [Header("in Game")]
    public Transform[] objects;
    public Color32[] cs;
    public Color32[] em;
    
    void Start()
    {
        SetColor();
    }

    public void SetColor()
    {
        for (int i = 0; i < objects.Length; i++)
        {
            objects[i].GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].SetColor("_Color", cs[i]);
            objects[i].GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].SetColor("_EmissionColor", (Color)em[i] * Mathf.Pow(2, multiEmit));
        }
    }
    
    void Update()
    {
        if(change)
        {
            for (int i = 0; i < objects.Length; i++)
            {
                objects[i].GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].SetColor("_Color", cs[i]);
                if(multilplier)
                {
                    objects[i].GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].SetVector("_EmissionColor", (Color)em[i] * Mathf.Pow(2, multiEmit));
                }
                else
                {
                    objects[i].GetChild(0).transform.GetChild(0).transform.GetComponent<Renderer>().materials[0].SetColor("_EmissionColor", em[i]);
                }
                
                
            }
        }
    }
    
}
