﻿<size=36><b>Over Happy Onlife</b></size>

Happy Onlife is een spel voor kinderen en volwassenen met als doel bij te leren over de risico's en de mogelijkheden van internet. Daarnaast wil het spel goed online gedrag bevorderen. 

Het spel helpt ouders en leerkrachten om concepten rond digitale technologie actief aan te brengen bij kinderen tussen 8 en 12 jaar.

Het is gebaseerd op het traditionele “Slangen en Ladder spel”, gecombineerd met quizvragen over het onderwerp. Quizvragen over het gebruik van internet, sociale netwerken en online spelletjes zijn ontworpen om discussies uit te lokken. Ze geven de moderator de kans om de spelers aan te leren hoe verantwoord en evenwichtig om te gaan met digitale media.

Belangrijke inzichten rond gebruik, overmatig gebruik en misbruik van digitale media, zoals bijvoorbeeld cyberpesten, werden opgenomen in Happy Onlife. Daarnaast reikt het spel ook eenvoudige en duidelijke activiteiten en strategieën van preventie en bemiddeling aan. 

De strategieën en de informatie gebruikt in het spel en de bijhorende brochure zijn geldig op het moment van publicatie, maar kunnen op termijn achterhaald zijn.

Naast deze applicatie is het spel ook beschikbaar als bordspel in het Engels en het Italiaans. Dit bordspel kan worden gedownload en afgedrukt van de JRC Science Hub webpagina (https://ec.europa.eu/jrc/en/scientific-tool/happy-onlife-game-raise-awareness-internet-risks-and-opportunities).

Andere taalversies zullen beschikbaar zijn in een later stadium. 

<size=36><b>Background</b></size>

Om het DG CNECT werkprogramma "Better Internet for Kids (BIK)" te ondersteunen, hebben JRC onderzoekers sinds 2011 onderzoek verricht naar het fenomeen cyberpesten als een vorm van pesten die snel evolueert door het wijdverbreide gebruik van informatie- en communicatietechnologie. JRC onderzoekers hebben ook materiaal ontwikkeld om het bewustzijn en het vertrouwen te verhogen en om kinderen, gezinnen en scholen aan te zetten evenwichtig en gezond online te leven om zo cyberpesten onder tieners te voorkomen. 


Happy Onlife is beschikbaar als een spel op papier, op internet ( <i>https://web.jrc.ec.europa.eu/happyonlife/</i> ) en via een gratis downloadbare app in officiële winkels. De digitale game is beschikbaar in de modus voor één of twee spelers. Spelers kunnen één persoon of teams van meerdere spelers zijn.
Voor ons onderzoek met scholen hebben we het in verschillende klassen getest met behulp van een overheadprojector (LIM) en hebben we de klas in twee teams verdeeld. De kinderen vonden het geweldig!

<size=36><b>Dankwoord</b></size>

De Portugese vertaling en aanpassing van het digitale spel (eerste versie) is van Patricia Dias en Rita Brito (Onderzoekscentrum voor Communicatie en Cultuur, Katholieke Universiteit van Portugal), september 2016. Vanaf 2017 maakt APAN (Portugese Adverteerders Vereniging) ook deel uit van het Portugese team. APAN is een vereniging die tot doel heeft de belangen van haar leden met betrekking tot commerciële communicatie te verdedigen, te beschermen en te bevorderen.

De Roemeense vertaling en bewerking van het digitale spel (eerste versie) is van Anca Velicu (Instituut voor Sociologie aan de Roemeense Academie, Bucurest) en Monica Mitarca (Faculteit voor Politieke Wetenschappen aan de 'Dimitrie Cantemir' Christian University, Boekarest), augustus 2017.

De Griekse vertaling en bewerking van het digitale spel (eerste versie) is van Anastasia Economou (Pedagogisch Instituut van Cyprus), Aphrodite Stephanou (Pedagogisch Instituut van Cyprus) en Ioannis Lefkos (5e basisschool van Kalamaria - Thessaloniki en Aristoteles Universiteit van Thessaloniki), Oktober 2017.

De vertaling en aanpassing van het digitale spel in het Georgisch (eerste versie) is door National Communications Commission (GNCC) en Bidzina Makashvili, april 2019.

De nieuwe inhoud in deze versie is het resultaat van het project "Do-It-Together with Happy Onlife" in samenwerking met de opdrachtgever:
• Savino Accetta en Andrea Donati (Banda degli Onesti, Italië);
• Patricia Dias, Rita Brito, Susana Paiva en Manuela Botelho (Mediasmart APAN, Portugal);
• Nicoleta Fotiade en Anca Velu, (Mediawise, Roemenië);
• Manuela Berlingeri en Elisa Arcangeli (Universiteit van Urbino, Italië).

We danken ook studenten, leerkrachten en ouders die vertrouwen hebben gesteld in dit project vanaf de conceptie-, ontwikkelings- en validatiefasen die zijn uitgevoerd volgens een aanpak van burgerbetrokkenheid en participatief onderzoek.

De geluidseffecten komen van de website <i>http://www.soundgator.com/</i>

<b>Version 3.0 (Build 2022)</b>