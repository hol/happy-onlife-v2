﻿<size=36><b>Acerca de Happy Onlife</b></size>

Este juego nació al comprobar que niños y adultos comparten poco sus experiencias con las nuevas tecnologías. Por un lado los adultos usan estas nuevas herramientas como apoyo esencial para su trabajo y sus comunicaciones, tanto profesionales como privadas, mientras que los niños de 8 a 12 años pasan la mayor parte de su tiempo en línea de modo recreacional, incluso si utilizan internet como fuente de información y apoyo imprescindible a su aprendizaje. La edad de la preadolescencia es aquella de la construcción de identidad en el grupo, que hoy en día está cambiando con la llegada de las redes sociales y de la mensajería instantánea. Este juego – una mezcla entre el juego de la oca y las preguntas de opción múltiple – ofrece una actividad ideal de introducción, de unos 20 minutos, para promover los intercambios y las reflexiones entre niños y adultos, sobre la experiencia del mundo digital y las estrategias de su uso equilibrado, seguro y respetuoso.

El juego tiene reglas simples y es fácil de implementar. Su dinámica se  basa en juegos conocidos por todos: el tradicional juego de la oca y preguntas de opción múltiple. La presencia de un adulto como moderador del juego, aunque no sea esencial, es necesaria para alcanzar los objetivos de concienciación y de intercambio intergeneracional. Para el resto, sólo hace falta seguir las reglas del juego y divertirse mientras se aprende.

<size=36><b>Background</b></size>

Happy Onlife se llevó a cabo en apoyo de la Agenda Europea para los Derechos de los Niños y la Estrategia para una Internet mejor para los niños, que forman parte del programa de trabajo de la DG CNET (Comisión Europea). Los investigadores del CCI han creado materiales para concienciar y promover la confianza así como para conferir conocimiento a niños, familias y escuelas con el objectivo de fomentar una vida en línea sana y equilibrada y ayudar a prevenir el ciberacoso entre los adolescentes.  

Happy Onlife está disponible como juego de papel, como versión web (<i>https://web.jrc.ec.europa.eu/happyonlife/</i>) y como aplicación, que puede descargarse de forma gratuita desde las tiendas oficiales. El juego digital está disponible en modo de uno o dos jugadores. Los jugadores pueden ser individuales o equipos de varios.
Para nuestra investigación con las escuelas, lo hemos probamos en diferentes clases utilizando el retroproyector (LIM) y subdividiendo la clase en dos equipos. ¡A los chicos les encantó!

<size=36><b>Agradecimientos</b></size> 

La traducción y adaptación al Portugués de la primera versión del juego digital son de Patricia Dias y Rita Brito (Centro de Investigación para la Comunicación y la Cultura, Universidad Católica de Portugal), septiembre de 2016. A partir de 2017, APAN (Asociación de Anunciantes de Portugal) también forma parte del equipo portugués. APAN es una asociación cuyo propósito es defender, salvaguardar y promover los intereses de sus miembros relacionados con la comunicación comercial.

La traducción y adaptación al Rumano de la primera versión del juego digital son de Anca Velicu (Instituto de Sociología de la Academia Rumana, Bucurest) y Mónica Mitarca (Facultad de Ciencias Políticas de la Universidad Cristiana 'Dimitrie Cantemir', Bucarest), agosto de 2017.

La traducción y adaptación al Griego de la primera versión del juego digital son de Anastasia Economou (Instituto Pedagógico de Chipre), Afrodita Stephanou (Instituto Pedagógico de Chipre) e Ioannis Lefkos (5a Escuela Primaria de Kalamaria - Salónica y la Universidad Aristóteles de Salónica), Octubre de 2017.

La traducción y adaptación al Georgiano de la primera versión del juego digital son de la Comisión Nacional de Comunicaciones (GNCC) y Bidzina Makashvili, abril de 2019.

Los nuevos contenidos en esta versión son el resultado del proyecto "Do-It-Together with Happy Onlife" en colaboración con:
• Savino Accetta y Andrea Donati (Banda degli Onesti, Italia);
• Patricia Dias, Rita Brito, Susana Paiva y Manuela Botelho (Mediasmart APAN, Portugal);
• Nicoleta Fotiade y Anca Velu, (Mediawise, Rumania);
• Manuela Berlingeri y Elisa Arcangeli (Universidad de Urbino, Italia).

También agradecemos a los estudiantes, maestros y padres que han confiado en este proyecto desde sus fases de concepción, desarrollo y validación llevadas a cabo siguiendo un enfoque de participación ciudadana e investigación participativa.

Los efectos de sonido provienen del sitio web <i>http://www.soundgator.com/</i>

<b>Version 3.0 (Build 2022)</b>